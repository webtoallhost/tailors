<?php include ('config/config.inc.php'); ?>
<!DOCTYPE html>
<html>
<head>
<style>
.snippet {
position: relative;
/background: #FFF;/
padding: 2rem 50%;
margin: 1.5rem 25;
/* width: 100px; */
/box-shadow: 0 .4rem .8rem -.1rem rgba(0, 32, 128, .1), 0 0 0 1px #F0F2F7;/
border-radius: .25rem;
}

.examples .snippet:before {
display: inline-block;
position: absolute;
top: 0;
left: 0;
padding: 0 5px;
content: attr(data-title);
font-size: .75rem;
font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
color: white;
background-color: rgb(255, 25, 100);
border-radius: .25rem 0 .25rem 0;
}

.stage {
display: flex;
justify-content: center;
align-items: center;
position: relative;
padding: 2rem 0;
/* margin: 222px -5%; */
overflow: hidden;
}

.filter-contrast {
filter: contrast(5);
background-color: white;
}


/**
* ==============================================
* Dot Pulse
* ==============================================
*/
.dot-pulse {
position: relative;
left: -9896px;
width: 10px;
height: 10px;
border-radius: 5px;
background-color: #9880FF;
color: #9880FF;
box-shadow: 9999px 0 0 -5px #9880FF;
-webkit-animation: dot-pulse 1.5s infinite linear;
animation: dot-pulse 1.5s infinite linear;
-webkit-animation-delay: .25s;
animation-delay: .25s;
}

.dot-pulse::before, .dot-pulse::after {
content: '';
display: inline-block;
position: absolute;
top: 0;
width: 10px;
height: 10px;
border-radius: 5px;
background-color: #9880FF;
color: #9880FF;
}

.dot-pulse::before {
box-shadow: 9984px 0 0 -5px #9880FF;
-webkit-animation: dot-pulse-before 1.5s infinite linear;
animation: dot-pulse-before 1.5s infinite linear;
-webkit-animation-delay: 0s;
animation-delay: 0s;
}

.dot-pulse::after {
box-shadow: 10014px 0 0 -5px #9880FF;
-webkit-animation: dot-pulse-after 1.5s infinite linear;
animation: dot-pulse-after 1.5s infinite linear;
-webkit-animation-delay: .5s;
animation-delay: .5s;
}

@-webkit-keyframes dot-pulse-before {
0% {
box-shadow: 9984px 0 0 -5px #9880FF;
}
30% {
box-shadow: 9984px 0 0 2px #9880FF;
}
60%,
100% {
box-shadow: 9984px 0 0 -5px #9880FF;
}
}

@keyframes dot-pulse-before {
0% {
box-shadow: 9984px 0 0 -5px #9880FF;
}
30% {
box-shadow: 9984px 0 0 2px #9880FF;
}
60%,
100% {
box-shadow: 9984px 0 0 -5px #9880FF;
}
}

@-webkit-keyframes dot-pulse {
0% {
box-shadow: 9999px 0 0 -5px #9880FF;
}
30% {
box-shadow: 9999px 0 0 2px #9880FF;
}
60%,
100% {
box-shadow: 9999px 0 0 -5px #9880FF;
}
}

@keyframes dot-pulse {
0% {
box-shadow: 9999px 0 0 -5px #9880FF;
}
30% {
box-shadow: 9999px 0 0 2px #9880FF;
}
60%,
100% {
box-shadow: 9999px 0 0 -5px #9880FF;
}
}

@-webkit-keyframes dot-pulse-after {
0% {
box-shadow: 10014px 0 0 -5px #9880FF;
}
30% {
box-shadow: 10014px 0 0 2px #9880FF;
}
60%,
100% {
box-shadow: 10014px 0 0 -5px #9880FF;
}
}

@keyframes dot-pulse-after {
0% {
box-shadow: 10014px 0 0 -5px #9880FF;
}
30% {
box-shadow: 10014px 0 0 2px #9880FF;
}
60%,
100% {
box-shadow: 10014px 0 0 -5px #9880FF;
}
}

</style>

</head>
<body>
<script>
setTimeout(function(){
window.location.href = '<?php echo $sitename; ?>';
}, 5000);
</script>

<main>
    <div class="row">
<div class="col-3">
<div class="snippet" data-title=".dot-pulse">
<div class="stage">
<div class="dot-pulse"></div>
<br><br>
<p align="center" style="padding-top:50px;">your store registered successfully....</p> 
</div>
</div>
</div>
</div>
</div>
</div>
</main>
</body>
</html>
