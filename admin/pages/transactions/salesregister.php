<?php
$menu = "12,49";
$thispageid = 49;
include ('../../config/config.inc.php');
$dynamic = '1';
$datatable = '1';
include ('../../require/header.php');
$_SESSION['salesregister_id'] = '';

if (isset($_REQUEST['delete']) || isset($_REQUEST['delete_x'])) {
    $chk = $_REQUEST['chk'];
    $chk = implode('.', $chk);
    $msg = delsalesregister($chk);
}

if ($_POST['fdate'] != '' && $_POST['tdate'] != '') {
    $params .= "AND ( (`date`>='" . date("Y-m-d", strtotime($_POST['fdate'])) . "' AND `date`<='" . date("Y-m-d", strtotime($_POST['tdate'])) . "' ) OR ( `date`='" . date("Y-m-d", strtotime($_POST['tdate'])) . "'  ))";
}
if ($_POST['fdate'] != '' && $_POST['tdate'] == '') {
    $params .= "AND ((`date`>='" . date("Y-m-d", strtotime($_POST['fdate'])) . "' OR `date`<='" . date("Y-m-d") . "')  )";
}
if ($_POST['fdate'] == '' && $_POST['tdate'] != '') {
    $params .= "AND ((`date`>='1970-01-01' OR `date`<='" . date("Y-m-d", strtotime($_POST['tdate'])) . "')   )";
}
?>
<script type="text/javascript" >
    function validcheck(name)
    {
        var chObj = document.getElementsByName(name);
        var result = false;
        for (var i = 0; i < chObj.length; i++) {
            if (chObj[i].checked) {
                result = true;
                break;
            }
        }
        if (!result) {
            return false;
        } else {
            return true;
        }
    }

    function checkdelete(name)
    {
        if (validcheck(name) == true)
        {
            if (confirm("Please confirm you want to Delete this Register"))
            {
                return true;
            } else
            {
                return false;
            }
        } else if (validcheck(name) == false)
        {
            alert("Select the check box whom you want to delete.");
            return false;
        }
    }

</script>
<script type="text/javascript">
    /* function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
     
     /*
     var DocumentContainer = document.getElementById(divName);
     var html = DocumentContainer.innerHTML;
     
     var WindowObject = window.open("", "PrintWindow",
     "width=2480,height=3000");
     WindowObject.document.writeln(html);
     WindowObject.document.close();
     WindowObject.focus();
     WindowObject.print();
     WindowObject.close();
     document.getElementById(divName).style.display='block';
     */
</script>
<script type="text/javascript">
    function checkall(objForm) {
        len = objForm.elements.length;
        var i = 0;
        for (i = 0; i < len; i++) {
            if (objForm.elements[i].type == 'checkbox') {
                objForm.elements[i].checked = objForm.check_all.checked;
            }
        }
    }
</script>
<style type="text/css">
    .row { margin:0;}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Sales Register
            <!--<small>Manage All Sales Register </small>-->
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-briefcase"></i> Sales</a></li>
            <li class="active"><a href="#"> Sales Register</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><a href="<?php echo $sitename; ?>sales/addsalesregister.htm">Add New Sales Register</a></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <?php echo $msg; ?>

                <form name=""  method="post">
                    <div class="row">
                        <div class="col-md-2">
                            <label>From Date</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker" name="fdate" id="datete" value="<?php echo $_REQUEST['fdate']; ?>" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To Date</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control datepicker" name="tdate" id="tdate" value="<?php
                                if ($_REQUEST['tdate']) {
                                    echo $_REQUEST['tdate'];
                                } else {
                                    echo date("d-M-Y");
                                }
                                ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <br />
                            <input type="submit" class="btn btn-success" name="ssearch" value="Search">
                        </div>
                    </div>
                </form>

                <form name="form1" method="post" action="">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped"  style="width: 100%">
                            <thead>
                                <tr>
                                    <th width="5%">S.id</th>
                                    <th>Date</th>
                                    <th>Invoice No</th>
                                    <th>Customer Name</th>
                                    <th>Customer Tin No</th>                                   
                                    <th>Total Amount(<i class="fa fa-inr"></i>)</th>
                                    <th data-sortable="false" align="center" width="5%" style="text-align: center; padding-right:0; padding-left: 0;">Edit</th>
                                    <th data-sortable="false" align="center" width="5%" style="text-align: center; padding-right:0; padding-left: 0;">Print DC</th>
                                    <th data-sortable="false" align="center" width="5%" style="text-align: center; padding-right:0; padding-left: 0;">Print Bill</th>
                                    <th data-sortable="false" align="center" width="5%" style="text-align: center; padding-right:0; padding-left: 0;">Printed</th>
                                    <th data-sortable="false" align="center" width="5%" style="text-align: center; padding-right:0; padding-left: 0;"><input name="check_all" id="check_all" value="1" onclick="javascript:checkall(this.form)" type="checkbox" /></th>
                                </tr>
                            </thead>
                            <?php
                            if ($_SESSION['locid'] != '') {
                                $q = " AND `sitelocid`='" . $_SESSION['locid'] . "'";
                            } else {
                                $q = '';
                            }
                            $query = "SELECT * FROM `sales_register` WHERE `status`!='2' " . $q . " $params   AND `com_id`='" . $_SESSION['UID'] . "'  ORDER BY `date` DESC  ";
                            $_SESSION['sales_register_url'] = $query;
                            $depart = DB($query);
                            $ndepart = DB_NUM($query);
                            if ($ndepart > 0) {
                                ?>
                                <tbody>
                                    <?php
                                    $i = '1';
                                    while ($fdepart = mysql_fetch_array($depart)) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo date("d-M-Y", strtotime(getsalesregister('date', $fdepart['rid']))); ?></td>
                                            <td><?php echo $fdepart['billno2']; ?></td>
                                            <td><?php
                                                if (getcustomer('firstname', getsalesregister('customername', $fdepart['rid'])) != '') {
                                                    echo getcustomer('firstname', getsalesregister('customername', $fdepart['rid']));
                                                } else {
                                                    echo getsalesregister('customer', $fdepart['rid']);
                                                }
                                                ?></td>
                                            <td><?php echo getcustomer('tinnumber', getsalesregister('customername', $fdepart['rid'])); ?></td>
                                            <td style="text-align: right;"><?php echo formatInIndianStyle(getsalesregister('ftotal', $fdepart['rid'])); ?></td>
                                            <td align="center" width="10%"><i class="fa fa-edit" onclick="javascript:editthis('<?php echo $fdepart['rid']; ?>');" style="cursor:pointer;"></i></td>
                                            <td align="center" width="10%"><i class="fa fa-print" onclick="javascript:return print_dc(<?php echo $fdepart['rid']; ?>);" style="cursor:pointer;"></i></td>
                                            <td align="center" width="10%"><i class="fa fa-print" onclick="javascript:return print_bill(<?php echo $fdepart['rid']; ?>);" style="cursor:pointer;"></i></td>
                                            <td align="center" width="10%"><span id="checked_<?php echo $fdepart['rid']; ?>" class="btn <?php
                                                if ($fdepart['sales_print_status'] == '0') {
                                                    echo 'btn-info';
                                                } else {
                                                    echo 'btn-success';
                                                }
                                                ?>" <?php if ($fdepart['sales_print_status'] == '0') { ?> onclick="check_printstatus(<?php echo $fdepart['rid']; ?>)"<?php } ?>> <?php if ($fdepart['sales_print_status'] == '1') { echo "Checked"; } else { echo "Check"; }?></span></td>
                                            <td align="center" width="10%"><input type="checkbox" name="chk[]" id="chk[]" value="<?php echo $fdepart['rid']; ?>" /></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="8">&nbsp;</th>
                                        <th style="margin:0px auto;"><button type="submit" class="btn btn-danger" name="delete" id="delete" value="Delete" onclick="return checkdelete('chk[]');"> DELETE </button></th>
                                        <th style="color:#fff;"> <a target="__blank" class="btn btn-info" href="<?php echo $sitename; ?>pages/sales/export-sales-reg.php" ><i class="fa fa-download "></i>&nbsp;&nbsp;EXPORT</a></th>
                                    </tr>
                                </tfoot>
                            <?php } ?>
                        </table>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    
    function editthis(a)
    {
        window.location.href = '<?php echo $sitename; ?>sales/' + a + '/editsalesregister.htm';
    }
    
    function print_dc(a)
    {
        $.post("<?php echo $sitename; ?>pages/sales/bill_page.php", {bill_id: a}, function (data) {
            $("#Bill_Print").html(data);
            printDiv("Bill_Print");
        });

    }
    
    function print_bill(a)
    {
        $.post("<?php echo $sitename; ?>pages/sales/bill_page.php", {dc_id: a}, function (data) {
            $("#Dc_Print").html(data);
            printDiv("Dc_Print");
        });
    }

    function printDiv(a) {
        var printContents = document.getElementById(a).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
    
    function check_printstatus(a)
    {
        $.post("<?php echo $sitename; ?>pages/sales/bill_page.php", {sales_print_id: a}, function (data) {
            $("#checked_" + a).html("Checked");
            $("#checked_" + a).removeClass("btn-info").addClass("btn-success");
            $("#checked_" + a).removeAttr("onclick");
        });
    }
    
</script>
<?php
include ('../../require/footer.php');
?>
<div id="Bill_Print" style="display: none; margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:71%;"></div>
<div id="Dc_Print" style="display: none; margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:71%;"></div>