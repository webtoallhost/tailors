<?php
if (isset($_REQUEST['uid'])) {
    $thispageeditid = 11;
} else {
    $thispageaddid = 11;
}
$menu = "11,11,11,11";
include ('../../config/config.inc.php');
$dynamic = '1';
include ('../../require/header.php');

if (isset($_REQUEST['submit'])) {
    @extract($_REQUEST);
    $getid = $_REQUEST['banid'];
    $ip = $_SERVER['REMOTE_ADDR'];
    if ($imagename != '') {
        $imagec = $imagename;
    } else {
        $imagec = time();
    }
    $imag = strtolower($_FILES["image"]["name"]);
    if ($getid != '') {
        $linkimge = $db->prepare("SELECT * FROM `banner` WHERE `bid` = ? ");
        $linkimge->execute(array($getid));
        $linkimge1 = $linkimge->fetch();
        $pimage = $linkimge1['image'];
    }
    if ($imag) {
        if ($pimage != '') {
            unlink("../../../images/banner/" . $pimage);
        }
        $main = $_FILES['image']['name'];
        $tmp = $_FILES['image']['tmp_name'];
        $size = $_FILES['image']['size'];
        $width = 1350;
        $height = 500;
        $extension = getExtension($main);
        $extension = strtolower($extension);
         if (($extension == 'jpg') || ($extension == 'png') || ($extension == 'gif') || ($extension == 'jpeg')) {
            $m = $imagec;
            $imagev = $m . "." . $extension;
            $thumppath = "../../../images/banner/";
            $aaa = Imageupload($main, $size, $width, $thumppath, $thumppath, '255', '255', '255', $height, strtolower($m), $tmp);
            $eee = compress_image($thumppath . $imagev, 80);
            move_uploaded_file($tmp, $thumppath . $imagev);
        } else {
            $ext = '1';
        }
        $image = $imagev;
    } else {
        if ($_REQUEST['banid']) {
            $image = $pimage;
        } else {
            $image = '';
        }
    }
    if ($ext == '1') {
        $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-close"></i> Invalid File Format! Try jpg/png/gif/jpeg files only </div>';
    } else {
        
        $msg =addtailor($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $getid);
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tailor
            <small><?php
                if ($_REQUEST['banid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Tailor</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i> User(s)</a></li>            
            <li><a href="<?php echo $sitename; ?>user/tailor.htm"><i class="fa fa-circle-o"></i> Tailor</a></li>
            <li class="active"><?php
                if ($_REQUEST['banid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Tailor</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <form name="department" id="department" action="#" method="post" enctype="multipart/form-data">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['banid'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Tailor</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name <span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the Name" name="name" id="name" value="<?php echo gettailor('name', $_REQUEST['banid']); ?>" required="required"/>
                        </div>
                        <div class="col-md-6">
                            <label>Emailid <span style="color:#FF0000;">*</span></label>
                            <input type="email" class="form-control" placeholder="Enter the Emailid" name="emailid" id="emailid"   value="<?php echo gettailor('emailid', $_REQUEST['banid']); ?>" required="required"/>
                        </div>
                    </div>
                    <div class="clearfix"><br /></div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Address<span style="color:#FF0000;">*</span></label>
                            <textarea id="editor" name="address" class="form-control" rows="5" cols="80" required="required"><?php echo gettailor('address', $_REQUEST['banid']); ?></textarea>
                        </div>
                    </div>
                    <div class="clearfix"><br /></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Mobile Number<span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the Mobile Number" name="mobile" id="mobile" value="<?php echo gettailor('mobile', $_REQUEST['banid']); ?>" required="required"/>
                        </div>
                        <div class="col-md-6">
                            <label>Username<span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the Username" name="username" id="username" value="<?php echo gettailor('username', $_REQUEST['banid']); ?>" required="required"/>
                        </div>
                    </div>
                    <div class="clearfix"><br /></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Password<span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the password" name="password" id="password" value="<?php echo gettailor('password', $_REQUEST['banid']); ?>" required="required"/>
                        </div>
                        <div class="col-md-6">
                            <label>Status  <span style="color:#FF0000;">*</span></label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" <?php
                                if (gettailor('status', $_REQUEST['banid']) == '1') {
                                    echo 'selected';
                                }
                                ?>>Active</option>
                                <option value="0" <?php
                                if (gettailor('status', $_REQUEST['banid']) == '0') {
                                    echo 'selected';
                                }
                                ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"><br /></div>
                    <!--<div class="row">-->
                    <!--    <div class="col-md-6">-->
                    <!--        <div class="form-group">-->
                    <!--            <label>Image Alt<span style="color:#FF0000;"> *</span></label>                                  -->
                    <!--            <input type="text" name="imagealt" class="form-control" value="<?php echo strtolower(getbanner('image_alt', $_REQUEST['banid'])); ?>" required />                     -->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="col-md-6">-->
                    <!--        <div class="form-group">-->
                    <!--            <label>Image Name<span style="color:#FF0000;"> *</span></label>                                  -->
                    <!--            <input type="text" name="imagename" pattern="[A-Za-z0-9 -_]{2,110}" class="form-control" value="<?php echo strtolower(getbanner('imagename', $_REQUEST['banid'])); ?>" required />           -->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--<div class="row">                                             -->
                    <!--    <div class="col-md-6 col-sm-6 col-xs-12">-->
                    <!--        <div class="form-group">-->
                    <!--            <label>Image <span style="color:#FF0000;"> *(Recommended Size 1390 Pixels Width * 350 Pixels Height)</span></label>-->
                    <!--            <input class="form-control spinner" <?php if (getbanner('image', $_REQUEST['banid']) == '') { ?> required="required" <?php } ?> name="image" type="file"> -->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <?php if (getbanner('image', $_REQUEST['banid']) != '') { ?>-->
                    <!--        <div class="col-md-6 col-sm-6 col-xs-12" id="delimage">-->
                    <!--            <label> </label>-->
                    <!--           <img src="<?php echo $fsitename; ?>images/banner/<?php echo getbanner('image', $_REQUEST['banid']); ?>" style="padding-bottom:10px;" height="100" />-->
                    <!--           <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deleteimage('<?php echo getbanner('image', $_REQUEST['banid']); ?>', '<?php echo $_REQUEST['banid']; ?>', 'banner', '../images/banner/', 'image', 'bid');"><i class="fa fa-close">&nbsp;Delete Image</i></button>-->
                    <!--                </div>-->
                    <!--    <?php } ?>-->
                    <!--</div>-->
                    <div class="row">
                        <!--<div class="col-md-6">-->
                        <!--    <label>Order <span style="color:#FF0000;">*</span></label>-->
                        <!--    <input type="number" name="order" id="order" min="1" max="100" required="required" class="form-control" placeholder="Order" value="<?php echo getbanner('order', $_REQUEST['banid']); ?>" />-->
                        <!--</div>-->
                        
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>user/tailor.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['banid'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SUBMIT';
                                }
                                ?></button>
                        </div>
                    </div>
                </div>
            </div><!-- /.box -->
        </form>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include ('../../require/footer.php'); ?>
