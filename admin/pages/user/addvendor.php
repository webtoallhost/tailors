<?php
if (isset($_REQUEST['uid'])) {
    $thispageeditid = 15;
} else {
    $thispageaddid = 15;
}
$menu = "11,11,15";
include ('../../config/config.inc.php');
$dynamic = '1';
include ('../../require/header.php');


if (isset($_REQUEST['submit'])) {
    @extract($_REQUEST);
    $getid = $_REQUEST['banid'];
    $usergroup = $_REQUEST['banid'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $imagec = time();
    $imag = strtolower($_FILES["image"]["name"]);
  
    if ($getid != '') {
        $linkimge = $db->prepare("SELECT * FROM `vendor` WHERE `uid` = ? ");
        $linkimge->execute(array($getid));
        $linkimge1 = $linkimge->fetch();
        $pimage = $linkimge1['image'];
    }
    if ($imag!='') {
        if ($pimage != '') {
            unlink("../../../images/vendor/" . $pimage);
        }
        $main = $_FILES['image']['name'];
        $tmp = $_FILES['image']['tmp_name'];
        $size = $_FILES['image']['size'];
        $width = 1000;
        $height = 1000;
        $extension = getExtension($main);
        $extension = strtolower($extension);
        if (($extension == 'jpg') || ($extension == 'png') || ($extension == 'gif')  || ($extension == 'jpeg')) {
            $m = $imagec;
            $imagev = $m . "." . $extension;
            $thumppath = "../../../images/vendor/";
            move_uploaded_file($tmp, $thumppath . $imagev);
        } else {
            $ext = '1';
        }
        $image = $imagev;
    } else {
        if ($_REQUEST['banid']) {
            $image = $pimage;
        } else {
            $image = '';
        }
    }

        $msg =addvendor($postcode,$sendmail,$bestseller,$image,$name, $emailid, $address, $mobile, $usergroup, $username, $password, $status, $getid);
 
 
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Showroom
            <small><?php
                if ($_REQUEST['banid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Showroom</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i> User(s)</a></li>            
            <li><a href="<?php echo $sitename; ?>user/vendor.htm"><i class="fa fa-circle-o"></i> Showroom</a></li>
            <li class="active"><?php
                if ($_REQUEST['banid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Showroom</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <form name="department" id="department" action="#" method="post" enctype="multipart/form-data">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['banid'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Showroom</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Name <span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the Name" name="name" id="name" value="<?php echo getvendor('name', $_REQUEST['banid']); ?>" required=""/>
                        </div>
                        <div class="col-md-6">
                            <label>Emailid <span style="color:#FF0000;">*</span></label>
                            <input type="email" class="form-control" placeholder="Enter the Emailid" name="emailid" id="emailid"   value="<?php echo getvendor('emailid', $_REQUEST['banid']); ?>" required=""/>
                        </div>
                    </div>
                    <div class="clearfix"><br /></div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Address<span style="color:#FF0000;">*</span></label>
                            <textarea id="editor" name="address" class="form-control" rows="5" cols="80" required=""><?php echo getvendor('address', $_REQUEST['banid']); ?></textarea>
                        </div>
                    </div>
					 <div class="clearfix"><br /></div>
					
                                            <div class="row">                                             
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Vendor Logo <span style="color:#FF0000;">*</span></label>
                                <input class="form-control spinner" name="image" type="file" <?php if ( getvendor('image', $_REQUEST['banid']) == '') { ?> required="required" <?php } ?>> 
                            </div>
                        </div>
                        <?php if ( getvendor('image', $_REQUEST['banid']) != '') { ?>
                            <div class="col-md-6 col-sm-6 col-xs-12" id="delimage">
                                <label> </label>
                                <img src="<?php echo $fsitename; ?>images/vendor/<?php echo  getvendor('image', $_REQUEST['banid']); ?>" style="padding-bottom:10px;" height="100" />
                                <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deleteimage('<?php echo  getvendor('image', $_REQUEST['banid']); ?>', '<?php echo $_REQUEST['banid']; ?>', 'vendor', '../images/vendor/', 'image', 'uid');"><i class="fa fa-close">&nbsp;Delete Image</i></button>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"><br /></div>
					
                    <div class="row">
                        <div class="col-md-6">
                            <label>Mobile Number<span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the Mobile Number" name="mobile" id="mobile" value="<?php echo getvendor('mobile', $_REQUEST['banid']); ?>" required=""/>
                        </div>
                        <div class="col-md-6">
                            <label>Postal Code<span style="color:#FF0000;">*</span></label>
                          
                       <input type="text" required="required" name="postcode" id="postcode" value="<?php echo getvendor('postcode', $_REQUEST['banid']); ?>" class="form-control">     
                        </div>
                    </div>
                    <div class="clearfix"><br /></div>
                    <div class="row">
					 <div class="col-md-6">
                            <label>Username<span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the Username" name="username" id="username"  value="<?php echo getvendor('username', $_REQUEST['banid']); ?>" required=""/>
                        </div>
                        <div class="col-md-6">
                            <label>Password<span style="color:#FF0000;">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter the password" name="password" id="password"  value="<?php echo getvendor('password', $_REQUEST['banid']); ?>" required=""/>
                        </div>
						</div>
						<div class="clearfix"><br /></div>
                    <div class="row">
                        <div class="col-md-6">
                            <label>Status  <span style="color:#FF0000;">*</span></label>
                            <select name="status" id="status" class="form-control">
                                <option value="1" <?php
                                if (getvendor('status', $_REQUEST['banid']) == '1') {
                                    echo 'selected';
                                }
                                ?>>Active</option>
                                <option value="0" <?php
                                if (getvendor('status', $_REQUEST['banid']) == '0') {
                                    echo 'selected';
                                }
                                ?>>Inactive</option>
                            </select>
                        </div>
						 <div class="col-md-6">
						<br>
						<input type="checkbox" name="sendmail" value="1">&nbsp;&nbsp;<label>Send Mail</label>
                       &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"   name="bestseller" id="bestseller" value="1" <?php if(getvendor('bestseller', $_REQUEST['banid'])=='1') { ?> checked="checked" <?php } ?>/>&nbsp;&nbsp;Best Seller
                       
                    </div>
                    </div>
                   
				   
					<div class="clearfix"><br /></div>
                    <!--<div class="row">-->
                    <!--    <div class="col-md-6">-->
                    <!--        <div class="form-group">-->
                    <!--            <label>Image Alt<span style="color:#FF0000;"> *</span></label>                                  -->
                    <!--            <input type="text" name="imagealt" class="form-control" value="<?php echo strtolower(getbanner('image_alt', $_REQUEST['banid'])); ?>" required />                     -->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <div class="col-md-6">-->
                    <!--        <div class="form-group">-->
                    <!--            <label>Image Name<span style="color:#FF0000;"> *</span></label>                                  -->
                    <!--            <input type="text" name="imagename" pattern="[A-Za-z0-9 -_]{2,110}" class="form-control" value="<?php echo strtolower(getbanner('imagename', $_REQUEST['banid'])); ?>" required />           -->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--<div class="row">                                             -->
                    <!--    <div class="col-md-6 col-sm-6 col-xs-12">-->
                    <!--        <div class="form-group">-->
                    <!--            <label>Image <span style="color:#FF0000;"> *(Recommended Size 1390 Pixels Width * 350 Pixels Height)</span></label>-->
                    <!--            <input class="form-control spinner" <?php if (getbanner('image', $_REQUEST['banid']) == '') { ?> required="required" <?php } ?> name="image" type="file"> -->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--    <?php if (getbanner('image', $_REQUEST['banid']) != '') { ?>-->
                    <!--        <div class="col-md-6 col-sm-6 col-xs-12" id="delimage">-->
                    <!--            <label> </label>-->
                    <!--           <img src="<?php echo $fsitename; ?>images/banner/<?php echo getbanner('image', $_REQUEST['banid']); ?>" style="padding-bottom:10px;" height="100" />-->
                    <!--           <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deleteimage('<?php echo getbanner('image', $_REQUEST['banid']); ?>', '<?php echo $_REQUEST['banid']; ?>', 'banner', '../images/banner/', 'image', 'bid');"><i class="fa fa-close">&nbsp;Delete Image</i></button>-->
                    <!--                </div>-->
                    <!--    <?php } ?>-->
                    <!--</div>-->
                    <div class="row">
                        <!--<div class="col-md-6">-->
                        <!--    <label>Order <span style="color:#FF0000;">*</span></label>-->
                        <!--    <input type="number" name="order" id="order" min="1" max="100" required="required" class="form-control" placeholder="Order" value="<?php echo getbanner('order', $_REQUEST['banid']); ?>" />-->
                        <!--</div>-->
                        
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>user/vendor.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['banid'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SUBMIT';
                                }
                                ?></button>
                        </div>
                    </div>
                </div>
            </div><!-- /.box -->
        </form>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include ('../../require/footer.php'); ?>
