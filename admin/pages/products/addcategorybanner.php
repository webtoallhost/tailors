<?php
$menu = "25,25,52";
if (isset($_REQUEST['id'])) {
    $thispageeditid = 52;
} else {
    $thispageaddid = 52;
}
include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');


if (isset($_REQUEST['submit'])) {
    $i = 1;
    @extract($_REQUEST);

    $_SESSION['cb_id'] = $_REQUEST['id'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $strupload = '1';
    $vaal = checkimage123('categorybanner', $imagename, 'cbid', $_REQUEST['id']);
    if ($vaal == 'true')
    {
        $pimage = getcategorybanner('image', $_REQUEST['id']);
        $imag = strtolower($_FILES["image"]["name"]);
        if ($imag)
        {
            $main = $_FILES['image']['name'];
            $tmp = $_FILES['image']['tmp_name'];
            $size = $_FILES['image']['size'];
            $extension = getExtension($main);
            $extension = strtolower($extension);
            if (($extension == 'jpg') || ($extension == 'png') || ($extension == 'gif') || ($extension == 'jpeg')) {
                if ($pimage != '') {
                    unlink("../../../images/category/banner/" . $pimage);
                }
                $width = 1280;
                $height = 400;
                $m = trim($imagename);
                $image = strtolower($m) . "." . $extension;
                $thumppath = "../../../images/category/banner/";
                $aaa = Imageuploadd($main, $size, $width, $thumppath, $thumppath, '255', '255', '255', $height, strtolower($m), $tmp);
            } else {
                $strupload = '2';
                $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-close"></i> Invalid File Format! Try jpg/png/gif/jpeg files only </div>';
            }
        } else {
            $image = '';
            if (isset($_REQUEST['id'])) {
                $image = $pimage;
            }
        }

        if ($strupload == '1')
        {
            $msg = addcategorybanner($category, $link, $imagename, $imagealt, $image, $order, $status, $ip, $_REQUEST['id'], $thispageid);
        }
    } else {
        $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-close"></i> Image name Already Exist!! try Different</div>';
    }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category Banner Mgmt
            <small><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Category Banner Mgmt </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-product-hunt"></i> Product(s)</a></li>
            <li><a href="<?php echo $sitename; ?>products/categorybanner.htm">Category Banner Mgmt </a></li>
            <li class="active"><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Category Banner Mgmt</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['id'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Category Banner Mgmt</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Category Banner Mgmt
                        </div>
                        <div class="panel-body">                        
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Category <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="category" id="category" class="form-control" required="required">
                                        <option value="">Select Category</option>
                                        <?php
                                        $cat=pFETCH("SELECT `cid`,`category` FROM `category` WHERE `status`=?",'1');
                                        while($fcat=$cat->fetch(PDO::FETCH_ASSOC))
                                        {
                                        ?>
                                        <option value="<?php echo $fcat['cid']; ?>" <?php if($fcat['cid']==getcategorybanner('cid',$_REQUEST['id'])) { echo 'selected'; } ?>><?php echo stripslashes($fcat['category']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Link</label>
                                    <textarea name="link" id="link" class="form-control"><?php echo getcategorybanner('link', $_REQUEST['id']); ?></textarea>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Image Name <span id="addstar" style="color:#FF0000;"></span></label>                                  
                                    <input type="text" name="imagename" id="imagename" pattern="[A-Z a-z 0-9 .,&_-]{1,55}" title="Special character not allowed." class="form-control" placeholder="Enter The Image Name" value="<?php echo getcategorybanner('imagename', $_REQUEST['id']); ?>"  />
                                </div> 
                                <div class="col-md-6">
                                    <label>Image Alt <span id="addstar1" style="color:#FF0000;"></span></label>                                  
                                    <input type="text" name="imagealt" pattern="[A-Z a-z 0-9 .,&_-]{1,55}" title="Special character not allowed." id="imagealtid"  class="form-control" placeholder="Enter The Image Alternate Name" value="<?php echo getcategorybanner('imagealt', $_REQUEST['id']); ?>"  /> 
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Image </label><span style="color:#FF0000;"> *(Recommended Size 1280 Pixels Width * 240 Pixels Height)</span>
                                    <input type="file" name="image" id="image" />
                                </div>
                                <?php if (getcategorybanner('image', $_REQUEST['id']) != '') { ?>
                                    <div class="col-md-6" id="delimage">
                                        <img src="<?php echo $fsitename; ?>images/category/banner/<?php echo getcategorybanner('image', $_REQUEST['id']); ?>" style="padding-bottom:10px;" height="100" />
                                        <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deleteimage('<?php echo getcategorybanner('image', $_REQUEST['id']); ?>', '<?php echo $_REQUEST['id']; ?>', 'categorybanner', '../../images/category/banner/', 'image', 'cbid');"><i class="fa fa-close">&nbsp;Delete Image</i></button>
                                    </div>
                                <?php } ?>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Order<span style="color:#FF0000;">*</span></label>                                  
                                    <input type="number" class="form-control" name="order" required="required" placeholder="Order" value="<?php echo getcategorybanner('order', $_REQUEST['id']); ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if (getcategorybanner('status', $_REQUEST['id']) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (getcategorybanner('status', $_REQUEST['id']) == '0') {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>

                                    </select>
                                </div>
                                <div id="txtHint1"><b></b></div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>products/categorybanner.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6"><!--validatePassword();-->
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['id'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>