<?php
$menu = "25,25,31";
if (isset($_REQUEST['id'])) {
    $thispageid = 31;
} else {
    $thispageid = 31;
}
include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');
if (isset($_REQUEST['submit'])) {

    @extract($_REQUEST);
    //print_r($_REQUEST);
    //exit;
    $_SESSION['company_id'] = $_REQUEST['id'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $date = date("Y-m-d h:i:sa");
    $getvalue = implode(',', $cid);
    $msg = addattributegroup($subcategory, $category, $getvalue, $status, $date, $ip, $_REQUEST['id']);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Attribute Group Mgmt
            <small><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Attribute Group Details </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-cogs"></i> Master</a></li>
            <li><a href="<?php echo $sitename; ?>products/attributegroup.htm"> Attribute Group Details </a></li>
            <li class="active"><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Attribute Group Details</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['id'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Attribute Group Details</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Attribute Group
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Attribute Name <span style="color:#FF0000;">*</span></label>                                  
                                    <input type="text" name="subcategory" id="category"  class="form-control" value="<?php echo getattributegroup('name', $_REQUEST['id']); ?>" required />
                                  
								  <br />
                                  
								  
                                    <label>Category <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="category" id="category" class="form-control" required="required">
                                        <option value="">Please Select the Category</option>
                                        <?php
                                        $s = DB("SELECT * FROM `category` WHERE `status`='1'");
                                        while ($cate = mysqli_fetch_array($s))
                                        {
                                        ?>
                                        <option value="<?php echo $cate['cid']; ?>" <?php if (getattributegroup('cid', $_REQUEST['id']) == $cate['cid']) { echo 'selected'; } ?>><?php echo stripslashes($cate['category']); ?></option>
                                        <?php } ?>
                                    </select>
                                     <br />
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if (getattributegroup('status', $_REQUEST['id']) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (getattributegroup('status', $_REQUEST['id']) == '0') {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Attribute<span style="color:#FF0000;">*</span></label>
                                    <select name="cid[]" class="form-control" required multiple style="height:320px;">
                                        <?php
                                        $valset = explode(',', getattributegroup('attribute', $_REQUEST['id']));
                                        $s = DB("SELECT * FROM `attribute`");
                                        while ($cate = mysqli_fetch_array($s)) {
                                            ?>
                                            <option value="<?php echo $cate['id']; ?>" <?php
                                            if (in_array($cate['id'], $valset)) {
                                                echo 'selected';
                                            }
                                            ?>><?php echo $cate['name']; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>products/attributegroup.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['id'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>