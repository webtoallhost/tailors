<div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Product Mgmt 
<!--                            <a target="_blank" style="float: right;margin-top: -7px;"  href="<?php echo $fsitename . 'productview' . '/' . getproduct('link', $_REQUEST['id']) ?>" class="btn  btn-primary" >Preview</a>-->
                        </div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-md-12"  id="adddetails">
                                    <?php
                                    $subcaeteded = explode(',', getproduct('sid', $_REQUEST['id']));
                                    $innercaeteded = explode(',', getproduct('innerid', $_REQUEST['id']));
                                    $caeteded = explode(',', getproduct('cid', $_REQUEST['id']));
                                    if ($_REQUEST['id'] != '') {
                                        $ss = 0;
                                        foreach ($caeteded as $catedid) {
                                            $idss = rand(3, 6) . time();
                                            ?>
                                            <div class="row" id="<?php echo $idss ?>" style="margin-bottom: 10px">
                                                <div class="col-md-4">
                                                    <label>Category Name <span style="color:#FF0000;">*</span></label>                                  
                                                    <select name="cid[]" class="form-control" required onchange="getsubcategoryp(this.value, '<?php echo $idss ?>')">
                                                        <option value="">Select Category</option>
                                                        <?php
                                                        $getmanuf = $db->prepare("SELECT * FROM `category`");
                                                        $getmanuf->execute(array());
                                                        $getmanuf1 = $getmanuf->rowCount();
                                                        while ($fdepart = $getmanuf->fetch(PDO::FETCH_ASSOC)) {
                                                            ?>
                                                            <option value="<?php echo $fdepart['cid']; ?>"
                                                            <?php
                                                            if ($fdepart['cid'] == $catedid)
                                                            {
                                                                echo 'selected="selected"';
                                                            }
                                                            ?>><?php echo $fdepart['category']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4"  id='sid<?php echo $idss ?>' >
                                                    <label>SubCategory Name <span style="color:#FF0000;"></span></label>                                  
                                                    <select name="sid[]" class="form-control" onchange="getinnercategoryp(this.value, '<?php echo $idss ?>')"  >
                                                        <option value="">Select Subcategory</option>
                                                        <?php
                                                        $s = $db->prepare("SELECT * FROM `subcategory` WHERE FIND_IN_SET(?,`cid`) AND `status`= ? ");
                                                        $s->execute(array($catedid, '1'));
                                                        while ($cate = $s->fetch()) {
                                                            ?>
                                                            <option value="<?php echo $cate['sid']; ?>" <?php
                                                            if ($cate['sid'] == $subcaeteded[$ss]) {
                                                                echo 'selected';
                                                            }
                                                            ?>><?php echo $cate['subcategory']; ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div> 
                                                <div class="col-md-4" id="nid<?php echo $idss ?>"  >
                                                    <label>Inner Category Name <span style="color:#FF0000;"></span></label>                                  
                                                    <select name="nid[]" class="form-control">
                                                        <option value="">Select Inner Category</option>
                                                        <?php
                                                        $s = $db->prepare("SELECT * FROM `innercategory` WHERE FIND_IN_SET(?,`subcategory`) AND `status`= ? ");
                                                        $s->execute(array($subcaeteded[$ss], '1'));
                                                        while ($cate = $s->fetch()) {
                                                            ?>
                                                            <option value="<?php echo $cate['innerid']; ?>" <?php
                                                            if ($cate['innerid'] == $innercaeteded[$ss]) {
                                                                echo 'selected';
                                                            }
                                                            ?>
                                                                    ><?php echo $cate['innername']; ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-1">
                                                    <?php if ($ss > 0) { ?>
                                                        <br />
                                                        <i class="fa fa-trash fa-2x" style="margin-top: 6px;color:#ff0000"  onclick="removethis(<?php echo $idss ?>, $(this))" ></i>    <?php }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            $ss++;
                                        }
                                    } else {

                                        $idss = rand(3, 6) . time();
                                        ?>
                                        <div class="row" id="<?php echo $ids ?>" style="margin-bottom: 10px">
                                            <div class="col-md-4">
                                                <label>Category Name <span style="color:#FF0000;">*</span></label>                                  
                                                <select name="cid[]" class="form-control" required onchange="getsubcategoryp(this.value,<?php echo $idss ?>)" >
                                                    <option value="">Select Category</option>
                                                    <?php
                                                    $getmanuf = $db->prepare("SELECT * FROM `category`");
                                                    $getmanuf->execute();
                                                    while ($fdepart = $getmanuf->fetch(PDO::FETCH_ASSOC)) {
                                                        ?>
                                                        <option value="<?php echo $fdepart['cid']; ?>"
                                                        <?php
                                                        if ($fdepart['cid'] == getproduct('cid', $_REQUEST['pid'])) {
                                                            echo 'selected="selected"';
                                                        }
                                                        ?>><?php echo $fdepart['category']; ?></option>
                                                            <?php } ?>
                                                </select>
                                            </div>

                                            <div class="col-md-4"   id='sid<?php echo $idss ?>' >
                                                <label>SubCategory Name <span style="color:#FF0000;"></span></label>                                  
                                                <select name="sid[]"  class="form-control" onchange="getinnercategoryp(this.value,<?php echo $idss ?>)"  >
                                                    <option value="">Select Subcategory</option>

                                                </select>
                                            </div> 

                                            <div class="col-md-4" id="nid<?php echo $idss ?>">
                                                <label>Inner Category Name  <span style="color:#FF0000;"></span> </label>                                  
                                                <select name="nid[]"  class="form-control">
                                                    <option value="">Select Inner-Category</option>
                                                    <?php
                                                    $s = $db->prepare("SELECT * FROM `innercategory` WHERE `subcategory`= ?  AND `status`= ? ");
                                                    $s->execute(array($subcaeteded[$ss], '1'));

                                                    //$s = DB("SELECT * FROM `innercategory` WHERE `subcategory`='$subcaeteded[$nn]'  AND `status`=1");
                                                    while ($cate = $s->fetch()) {
                                                        ?>
                                                        <option value="<?php echo $cate['innerid']; ?>" <?php
                                                        if ($cate['innerid'] == $innercaeteded[$ss]) {
                                                            echo 'selected';
                                                        }
                                                        ?>><?php echo $cate['innername']; ?></option>
                                                            <?php } ?>
                                                </select>
                                            </div>

                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="btn btn-success" onclick="additem()">Add</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Brand </label>
                                    <select name="brand" id="brand" class="form-control">
                                        <option value="">Select Brand</option>
                                        <?php
                                        $getmanuf = $db->prepare("SELECT * FROM `brand` WHERE `status`=?");
                                        $getmanuf->execute(array('1'));
                                        $getmanuf1 = $getmanuf->rowCount();

                                        while ($fdepart = $getmanuf->fetch(PDO::FETCH_ASSOC)) {
                                            ?>
                                            <option value="<?php echo $fdepart['brid']; ?>"
                                            <?php
                                            if ($fdepart['brid'] == getproduct('brand', $_REQUEST['id'])) {
                                                echo 'selected="selected"';
                                            }
                                            ?> > <?php echo $fdepart['bname']; ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Product Name <span style="color:#FF0000;">*</span></label>
                                    <input type="text" class="form-control" required="required" placeholder="Enter The Product Name" name="product" id="product" value='<?php
                                    if ($_REQUEST['id'] != '') {
                                        echo getproduct('productname', $_REQUEST['id']);
                                    }
                                    ?>'/>
                                </div>
                                <div class="col-md-6">
                                    <label>Link <span style="color:#FF0000;">*</span></label>
                                    <input type="text" class="form-control"  pattern="[A-Za-z0-9.,&_- ]{1,55}" title="Special character not allowed." required="required" placeholder="Enter The Link" name="link" id="link" value="<?php
                                    if ($_REQUEST['id'] != '') {
                                        echo getproduct('link', $_REQUEST['id']);
                                    }
                                    ?>"/>
                                </div>

                            </div>
                            <br/>
                            <div class="row"><br/></div>
                            <div class="panel panel-info">
                              <div class="panel-heading">
                                    Attribute
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Attribute Group <span style="color:#FF0000;"></span></label>

                                            <select id="attribute_group" onchange="setattrivalue(this.value)" class="form-control" name="attribute_group" <?php
                                            if (getproduct('attribute_group', $_REQUEST['id']) != '') {
                                                //echo 'disabled';
                                            }
                                            ?>>
                                                <option value="0">Select Attribute Group</option>
                                                <?php
                                                $s = $db->prepare("SELECT * FROM `attribute_group` WHERE `status`= ? ");
                                                $s->execute(array('1'));
                                                while ($cate = $s->fetch()) {
                                                    ?>
                                                    <option value="<?php echo $cate['id']; ?>" <?php
                                                    if ($cate['id'] == getproduct('attribute_group', $_REQUEST['id'])) {
                                                        echo 'selected';
                                                    }
                                                    ?>><?php echo $cate['name']; ?></option>
                                                        <?php } ?>
                                            </select>
                                            <input type="hidden"   name="attributegr"  value="<?php echo getproduct('attribute_group', $_REQUEST['id']) ?>" id="attribute_g_hidden" />
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <?php
                                        if ($_REQUEST['id'] != '') {
                                            ?>
                                            <div class="col-md-12 new_m"  id="addattribute">
                                                <?php
                                                $sattr = $db->prepare("SELECT * FROM `productattribute` WHERE `pid`= ? ");
                                                $sattr->execute(array($_REQUEST['id']));
                                                $sn = 0;
                                                while ($sattrf = $sattr->fetch()) {
                                                    if ($sattrf['attr_values'] != '') {
                                                        $sn++;
                                                        $labelsvalues = explode('**', $sattrf['attr_values']);
                                                        $checked = '';
                                                        if ($sattrf['default'] == '1') {
                                                            $checked = 'checked';
                                                        }
                                                        ?>
                                                        <div class="row new_m" id='remove_<?php echo $sn ?>'   >
                                                            <div class="col-md-1">
                                                                <br />
                                                                <input type="radio"  <?php echo $checked; ?>  name="default_attri" value="<?php echo $sn ?>" />
                                                            </div>
                                                            <?php
                                                            foreach ($labelsvalues as $valuesid) {
                                                                $single_att1 = $db->prepare("SELECT * FROM `attribute_value` WHERE `vid`= ? ");
                                                                $single_att1->execute(array($valuesid));
                                                                $single_att = $single_att1->fetch();
                                                                $attr_id = $single_att['valid'];

                                                                $single_att_name1 = $db->prepare("SELECT * FROM `attribute` WHERE `id`='" . $attr_id . "'");
                                                                $single_att_name1->execute(array());
                                                                $single_att_name = $single_att_name1->fetch();
                                                                ?>
                                                                <div class="col-md-3">
                                                                    <label><?php echo $single_att_name['name']; ?></label>
                                                                    <select name="attr_values_<?php echo $sn ?>[]"  class="form-control" >
                                                                        <option value="">Select</option>
                                                                        <?php
                                                                        $single_att_others = $db->prepare("SELECT * FROM `attribute_value` WHERE `valid`= ? ");
                                                                        $single_att_others->execute(array($attr_id));

                                                                        while ($single_att_othersf = $single_att_others->fetch()) {
                                                                            ?>

                                                                            <option value="<?php echo $single_att_othersf['vid'] ?>" <?php
                                                                            if ($single_att_othersf['vid'] == $valuesid) {
                                                                                echo 'selected';
                                                                            }
                                                                            ?>><?php echo $single_att_othersf['value'] ?></option>
                                                                                <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            <!--<div class="col-md-2">
                                                                <label>Price</label>
                                                                <input type="text" class="form-control"  required="required"  name="price_attr_<?php //echo $sn ?>"  value="<?php //echo $sattrf['price']; ?>"/>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>Special Price</label>
                                                                <input type="text" class="form-control"    name="sprice_attr_<?php //echo $sn ?>"  value="<?php //echo $sattrf['sprice']; ?>"/>
                                                            </div>-->
                                                            <div class="col-md-1">
                                                                <br />
                                                                <i class="fa fa-trash fa-2x" style="margin-top: 6px;color:#ff0000"  onclick="removethisattr(<?php echo $sn ?>)" ></i>
                                                            </div>
                                                            <input type="hidden" name="check_rows[]" value="<?php echo $sn ?>" />
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>		 
                                        </div>
                                       <!--<div class="row new_m">
                                            <div class="col-md-12"><a class="btn btn-success" onclick="additem_attr()">Add Attribute</a>
                                                <input type="hidden" id="check_row" value="<?php //echo $sn ?>" />
                                            </div>
                                        </div>-->
                                        <?php
                                    } else {
                                        ?>
                                      <!--<div class="row new_m" id="addattribute">

                                        </div>		 
                                        <div class="row new_m">
                                            <div class="col-md-12">  <a class="btn btn-success" onclick="additem_attr()">Add Attribute</a>
                                                <input type="hidden" id="check_row" value="0" />
                                            </div>
                                        </div>-->
                                        <?php
                                    }
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Availability</label>
                                    <select name="availability" class="form-control">
                                        <option value="1"<?php
                                        if (getproduct('availability', $_REQUEST['id']) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>In Stock</option>
                                        <option value="0" <?php
                                        if (getproduct('availability', $_REQUEST['id']) == '0') {
                                            echo 'selected';
                                        }
                                        ?>>Out of Stock</option>
                                    </select>
                                </div>
								  <div class="col-md-3">
                                    <label>
                                        <input type="checkbox" name="new"  id="new"  value="1"
                                        <?php
                                        if ($_REQUEST['id'] != '') {
                                            $ched = getproduct('new', $_REQUEST['id']);
                                            if ($ched == '1') {
                                                echo 'checked="checked"';
                                            }
                                        } 
                                        
                                        ?>> New Arrival</label>
                                </div> 

                                <div class="col-md-3">
                                    <label>
                                        <input type="checkbox" name="deal"  id="deal"  value="1"
                                        <?php
                                        if ($_REQUEST['id'] != '') {
                                            $ched = getproduct('deal', $_REQUEST['id']);
                                            if ($ched == '1') {
                                                echo 'checked="checked"';
                                            }
                                        }
                                        ?>> Best Seller</label>
                                </div>
								<div class="col-md-3">
                                    <label>
                                        <input type="checkbox" name="recommend"  id="recommend"  value="1"
                                        <?php
                                        if ($_REQUEST['id'] != '') {
                                            $ched = getproduct('recommend', $_REQUEST['id']);
                                            if ($ched == '1') {
                                                echo 'checked="checked"';
                                            }
                                        } 
                                        
                                        ?>> Recommended</label>
                                </div> 

                                <div class="col-md-3">
                                    <label>
                                        <input type="checkbox" name="offers"  id="offers"  value="1"
                                        <?php
                                        if ($_REQUEST['id'] != '') {
                                            $ched = getproduct('offers', $_REQUEST['id']);
                                            if ($ched == '1') {
                                                echo 'checked="checked"';
                                            }
                                        }
                                        ?>> Offers</label>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Price <span style="color:#FF0000;"></span></label>
                                    <input type="number" step="0.01" class="form-control" placeholder="Enter The Price" name="price" id="price" value="<?php
                                    if ($_REQUEST['id'] != '') {
                                        echo getproduct('price', $_REQUEST['id']);
                                    }
                                    ?>"/>
                                </div>
                                <div class="col-md-6">
                                    <label>Special Price</label>
                                    <input  type="number" step="0.01" name="sprice" id="sprice" class="form-control" value="<?php
                                    if ($_REQUEST['id'] != '') {
                                        echo getproduct('sprice', $_REQUEST['id']);
                                    }
                                    ?>">
                                </div>

                            </div>
                            <br />
                                                   <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Product Note <span style="color:#FF0000;"></span></label>
                                                                <textarea class="form-control"    name="note" id="note" cols="3"><?php
                            if ($_REQUEST['id'] != '') {
                                echo getproduct('note', $_REQUEST['id']);
                            }
                            ?></textarea>
                                                            </div>
                                                        </div>
                                                        <br />
                            

<div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if (isset($_REQUEST['id'])) {

                                        if (getproduct('image', $_REQUEST['id']) != '') {
                                            ?>
                                            <table>
                                                <?php
                                                $pimg = explode(",", getproduct('image', $_REQUEST['id']));
                                                $i = 0;
                                                foreach ($pimg as $img) {
                                                    if ($img == '') {
                                                        break;
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <div class="col-md-6" id="delimage<?php echo $i; ?>">
                                                                <img src="<?php echo $fsitename; ?>images/product/small/<?php echo $img; ?>" style="padding-bottom:10px;" height="100" />
                                                                <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deletemultiimage('<?php echo $img; ?>', '<?php echo $_REQUEST['id']; ?>', 'product', '../../images/product/small/', 'image', 'pid', 'delimage<?php echo $i; ?>');"><i class="fa fa-close">&nbsp;Delete Image</i></button>
                                                                <br />
                                                                <br />
                                                                <input type="file" name="image[<?php echo $i; ?>]" id="image[<?php echo $i; ?>]"/>
                                                                <br />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="com" id="com" />
                                                        <table id="myTable">
                                                            <?php
                                                            foreach ($pimg as $r) {
                                                                if ($r != '') {
                                                                    echo "<tr><td></td></tr>";
                                                                }
                                                            }
                                                            ?>
                                                            <tr id="add_rows">
                                                                <td align="left"><a href="javascript:insRow()" class="invitlink">Add another Images</a>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>

                                                </tr>
                                            </table>
                                            <?php
                                        } else {
                                            ?>
                                            <table id="myTable">
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="com" id="com" />
                                                        <input type="file" name="image[0]" id="image[0]" />
                                                    </td>
                                                </tr>
                                                <tr id="add_rows">
                                                    <td align="left">
                                                        <a href="javascript:insRow()" class="invitlink">Add another Images</a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <table id="myTable">
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="com" id="com" />
                                                    <input type="file" name="image[0]" id="image[0]" />
                                                    <br/>
                                                </td>
                                            </tr>
                                            <tr id="add_rows">
                                                <td align="left">
                                                    <a href="javascript:insRow()" style=" color:#069; font-weight:bold;">Add another Images</a>
                                                </td>
                                            </tr>
                                        </table>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div> <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Short Description</label>
                                    <textarea name="shortdescription"  id="editor2" class="form-control"><?php
                                        if ($_REQUEST['id'] != '') {
                                            echo getproduct('sortdescription', $_REQUEST['id']);
                                        }
                                        ?></textarea>
                                </div>

                            </div><br/>
                            <div class="row">
                                <?php if (getproduct('videolink', $_REQUEST['id']) != '') { ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12" id="delimage">
                                        <label> </label>
                                        <iframe width="200" height="100" src="https://www.youtube.com/embed/<?php echo stripslashes(getproduct('videolink', $_REQUEST['id'])); ?>" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                <?php } ?>

                               
                            </div>
                            <br/>
                            

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Description </label>
                                    <textarea name="description" id="editor1" class="form-control" placeholder="Enter The Description"><?php
                                        if ($_REQUEST['id'] != '') {
                                            echo getproduct('description', $_REQUEST['id']);
                                        }
                                        ?></textarea>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">

                                    <label>Payment, Shipping and Tax Info</label>
                                    <textarea name="paymentinfo" id="editor3" class="form-control" >
                                        <?php
                                        if ($_REQUEST['id'] != '') {
                                            echo getproduct('paymentinfo', $_REQUEST['id']);
                                        }
                                        ?></textarea>

                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Order<span style="color:#FF0000;">*</span></label>                                  
                                    <input type="number" class="form-control" name="order" required="required" value="<?php
                                    if ($_REQUEST['id'] != '') {
                                        echo getproduct('order', $_REQUEST['id']);
                                    }
                                    ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        //  echo  getcategory('status', $_REQUEST['id']);
                                        if (getproduct('status', $_REQUEST['id']) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (getproduct('status', $_REQUEST['id']) == '0') {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>

                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>