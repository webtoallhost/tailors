<?php
$menu = "25,25,34";
if (isset($_REQUEST['id'])) {
    $thispageid = 34;
} else {
    $thispageid = 34;
}
include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');

if (isset($_REQUEST['submit'])) {
    @extract($_REQUEST);
    $_SESSION['product_id'] = $_REQUEST['id'];
   
   $ip = $_SERVER['REMOTE_ADDR'];
            $msg = addattributevalue($attribute, $value, $status, $_REQUEST['id'], $thispageid);
   
   
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Attribute
            <small><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Attribute Value</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-cogs"></i> Master</a></li>
            <li><a href="<?php echo $sitename; ?>products/attributevalues.htm"> Attribute Value Mgmt </a></li>
            <li class="active"><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Attribute Value</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['id'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Attribute Value Mgmt</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php
                    echo $msg;
                    if (isset($_REQUEST['suc'])) {
                        echo '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
                    }
                    ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Attribute Values Mgmt
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Attribute<span style="color:#FF0000;">*</span></label>
                                    <select name="attribute" id="attribute" class="form-control" required="required">
                                        <option value="">Select Attribute</option>
                                        <?php
                                        $att = DB("SELECT * FROM `attribute` ORDER BY `name` ASC");
                                        while ($fattr = mysqli_fetch_array($att)) {
                                            ?>
                                            <option value="<?php echo $fattr['id']; ?>" <?php
                                            if ($fattr['id'] == getattributevalue('valid', $_REQUEST['id'])) {
                                                echo 'selected';
                                            }
                                            ?>><?php echo stripslashes($fattr['name']); ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Value<span style="color:#FF0000;">*</span></label>
                                    <input type="text" class="form-control" required="required" name="value" id="value" value="<?php
                                    if ($_REQUEST['id'] != '') {
                                        echo getattributevalue('value', $_REQUEST['id']);
                                    }
                                    ?>"/>
                                </div>				
                            </div>
                            <br/>
                          
						  <div class="row">
                               
							   
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if (getattributevalue('status', $_REQUEST['id']) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (getattributevalue('status', $_REQUEST['id']) == '0') {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>products/attributevalues.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['id'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>