<?php
$menu = "34,34,35";
if (isset($_REQUEST['id'])) {
    $thispageeditid = 35;
} else {
    $thispageid = 35;
}

include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');

if (isset($_REQUEST['submit'])) {
    $i = 1;
    @extract($_REQUEST);
    $getid = $_REQUEST['id'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $strupload = '1';

    $pimage = getassitors('image', $_REQUEST['id']);
    $imag = strtolower($_FILES["image"]["name"]);

    if ($imag) {
        $main = $_FILES['image']['name'];
        $tmp = $_FILES['image']['tmp_name'];
        $size = $_FILES['image']['size'];

        $extension = getExtension($main);
        $extension = strtolower($extension);

        if (($extension == 'jpg') || ($extension == 'png') || ($extension == 'gif') || ($extension == 'jpeg')) {
            if ($pimage != '') {
                unlink("../../../images/assitors/" . $pimage);
            }
            $width = 70;
            $height = 70;
            $m = trim($name) . time();
            $image = md5($m);
            $thumppath = "../../../images/assitors/";
            $aaa = Imageuploadd($main, $size, $width, $thumppath, $thumppath, '255', '255', '255', $height, strtolower($image), $tmp);
            $image = $image . "." . $extension;
        } else {
            $strupload = '2';
            $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-close"></i> Invalid File Format! Try jpg/png/gif/jpeg files only </div>';
        }
    } else {
        $image = '';
        if (isset($_REQUEST['id'])) {
            $image = $pimage;
        }
    }

    $msg = addassitors(trim($name), trim($link), trim($qualification), trim($email), trim($password), trim($comments), $image, trim($order), $status, $ip, $getid);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Assitors Mgmt
            <small><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Assitors Mgmt </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i> product(s)</a></li>
            <li><a href="<?php echo $sitename; ?>products/assitors.htm">Assitors Mgmt </a></li>
            <li class="active"><?php
                if ($_REQUEST['id'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Assitors Mgmt</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['id'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Assitors Mgmt</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Assitors Mgmt
                        </div>
                        <div class="panel-body">                        
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name <span style="color:#FF0000;">*</span></label>                                  
                                    <input type="text" name="name" id="name" placeholder="Enter The Name" class="form-control" value="<?php echo getassitors('name', $_REQUEST['id']); ?>"  pattern="[A-Z a-z 0-9 .,&_]{1,55}" title="Special character not allowed." required />
                                </div>  
                                <div class="col-md-6">
                                    <label>Link <span style="color:#FF0000;">*</span></label>                                  
                                    <input type="text" name="link" id="link" placeholder="Enter The link" class="form-control" value="<?php echo getassitors('link', $_REQUEST['id']); ?>"  pattern="[A-Z a-z 0-9.,&_]{1,55}" title="Special character not allowed." required />
                                </div>  

                            </div>
                            <div class="clearfix">
                                <br/>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>E-Mail Id (User name) <span style="color:#FF0000;">*</span></label>                                  
                                    <input type="email" name="email" id="email" placeholder="Enter The Email Id" class="form-control" value="<?php echo getassitors('email', $_REQUEST['id']); ?>" required />
                                </div>

                                <div class="col-md-6">
                                    <label>Password <span style="color:#FF0000;">*</span></label>
                                    <div class="input-group">
                                        <input type="text" name="password" readonly id="promo_code" placeholder="Password here" class="form-control" value="<?php echo getassitors('password', $_REQUEST['id']); ?>"  pattern="[A-Za-z0-9]{6}" title="Special character not allowed." required />
                                        <span class="input-group-addon" onclick="makeid();"><i class="fa fa-refresh"></i></span>
                                    </div>
                                </div> 
                            </div>


                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Image </label>
                                    <input type="file" name="image" id="image" onchange="imgchktore(this.value)" />
                                </div>
                                <?php if (getassitors('image', $_REQUEST['id']) != '') { ?>
                                    <div class="col-md-6" id="delimage">
                                        <img src="<?php echo $fsitename; ?>images/assitors/<?php echo getassitors('image', $_REQUEST['id']); ?>" style="padding-bottom:10px;" height="100" />
                                        <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deleteimage('<?php echo getassitors('image', $_REQUEST['id']); ?>', '<?php echo $_REQUEST['id']; ?>', 'assitors', '../../images/assitors/', 'image', 'asid');"><i class="fa fa-close">&nbsp;Delete Image</i></button>
                                    </div>
                                <?php } ?>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Qualification <span id="addstar" style="color:#FF0000;">*</span></label>
                                    <input type="text" name="qualification" id="qualification" placeholder="Enter The Qualification "   pattern="[A-Za-z 0-9 .,&_]{1,55}" title="Special character not allowed." class="form-control" value="<?php echo getassitors('qualification', $_REQUEST['id']); ?>" required />
                                </div> 
                                <div class="col-md-6">
                                    <label>Comments <span id="addstar1" style="color:#FF0000;"></span></label> 
                                    <textarea name="comments" id="Comments"   class="form-control" placeholder="Enter the Comments "><?php echo getassitors('comments', $_REQUEST['id']); ?></textarea>
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Order<span style="color:#FF0000;">*</span></label>                                  
                                    <input type="number" class="form-control" name="order" required="required" value="<?php echo getassitors('order', $_REQUEST['id']); ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if (getassitors('status', $_REQUEST['id']) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (getassitors('status', $_REQUEST['id']) == '0') {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>

                                    </select>
                                </div>
                                <div id="txtHint1"><b></b></div>
                            </div>



                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>products/assitors.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6"><!--validatePassword();-->
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['id'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>
<script>
<?php if ($_REQUEST['id'] == '') { ?>
        makeid();
<?php } ?>
    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 8; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        $('#promo_code').val(text);
    }
</script>
