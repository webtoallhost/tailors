<?php
$menu = "12,12,16";
if (isset($_REQUEST['fid'])) {
    $thispageeditid = 16;
} else {
    $thispageid = 16;
}

include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');


if (isset($_REQUEST['submit'])) {
    $i = 1;
    @extract($_REQUEST);
    $getid = $_REQUEST['fid'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $strupload = '1';
    if ($getid == '')
        $link22 = FETCH_all("SELECT * FROM `footermgt` WHERE `link`=?", $link);
    else
        $link22 = FETCH_all("SELECT * FROM `footermgt` WHERE `pid`!=? and `link`=?", $getid, $link);
    if ($link22['pid'] == '') {
        $msg = addfootermgt($description,$metatitle,$metakeyword,$metadescription,$payment, $link, $order, $status, $ip,  $getid);
    } else {
        $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
    }
}

if (isset($_REQUEST['fid']) && ($_REQUEST['fid'] != '')) {
    $get1 = $db->prepare("SELECT * FROM `footermgt` WHERE `pid`=?");
    $get1->execute(array($_REQUEST['fid']));
    $showrecords = $get1->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Footer Mgmt
            <small><?php
                if ($_REQUEST['fid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Footer Mgmt </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i> Master(s)</a></li>
            <li><a href="<?php echo $sitename; ?>master/footermgt.htm">Footer Mgmt </a></li>
            <li class="active"><?php
                if ($_REQUEST['fid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Footer Mgmt</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['fid'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Footer Mgmt</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>

                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Footer Mgmt
                        </div>
                        <div class="panel-body">                        
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name <span style="color:#FF0000;">*</span></label>                                  
                                    <input type="text" name="payment" id="payment" placeholder="Enter The  Name" class="form-control" value="<?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['title']) : ''; ?>"  pattern="[A-Za-z0-9., -&_]{1,55}" title="Special character not allowed." required />
                                </div>  
                                <div class="col-md-6">
                                    <label> Link<span style="color:#FF0000;"> *</span></label>                                  
                                    <input type="text" name="link" id="link" placeholder="Enter The Link" class="form-control" value="<?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['link']) : ''; ?>"  pattern="[A-Za-z0-9.,&_- ]{1,55}" required />
                                </div>  
                            </div>
                            <br/>
							
							 <div class="row">
                                <div class="col-md-12">
                                    <label>Description </label>
                                    <textarea name="description" id="editor1" class="form-control" placeholder="Enter The Description">
									<?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['description']) : ''; ?>
									</textarea>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Order<span style="color:#FF0000;">*</span></label>                                  
                                    <input type="number" class="form-control" name="order" required="required" value="<?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['order']) : ''; ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php echo(isset($_REQUEST['fid'])) ? ($showrecords['status'] == '1') ? 'selected' : '' : 'selected'; ?>>Active</option>
                                        <option value="0" <?php echo(isset($_REQUEST['fid'])) ? ($showrecords['status'] == '0') ? 'selected' : '' : ''; ?>>Inactive</option>
                                    </select>
                                </div>
                                <div id="txtHint1"><b></b></div>
                            </div> 
                        </div>
                    </div>
                
				
				<div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            SEO
                        </div>
                        <div class="panel-body">  
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Meta Title</label>
                                    <input type="text" name="metatitle" id="metatitle"   class="form-control"  placeholder="Enter The title" value="<?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['metatitle']) : ''; ?>" />
                                </div><br/>
                                <div class="col-md-12">
                                    <label>Meta Keywords</label>
                                    <textarea name="metakeyword" class="form-control" id="metakeyword"  placeholder="Enter The Meta Keyword" id="contact_number"><?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['metakeyword']) : ''; ?></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label>Meta Description</label>
                                    <textarea name="metadescription" class="form-control" id="metadescription"  placeholder="Enter The Meta Description" id="contact_number"><?php echo (isset($_REQUEST['fid'])) ? stripslashes($showrecords['metadescription']) : ''; ?></textarea>
                                </div>
                            </div>
                            <br />
                        </div> 
                    </div>
				</div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>master/footermgt.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6"><!--validatePassword();-->
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['fid'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>