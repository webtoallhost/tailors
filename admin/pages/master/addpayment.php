<?php
$menu = '12,12,15';
if (isset($_REQUEST['prid'])) {
    $thispageeditid = 15;
} else {
    $thispageid = 15;
}

include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');

if (isset($_REQUEST['submit'])) {
    $i = 1;
    @extract($_REQUEST);
    $getid = $_REQUEST['prid'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $strupload = '1';
    if ($getid == '')
        $link22 = FETCH_all("SELECT * FROM `payment` WHERE `image_name`=?", $imagename);
    else
        $link22 = FETCH_all("SELECT * FROM `payment` WHERE `pid`!=? and `image_name`=?", $getid, $imagename);

    if ($link22['pid'] == '') {
        $pimage = getpayment('image', $_REQUEST['prid']);
        $imag = strtolower($_FILES["image"]["name"]);
        if ($imag) {
            $main = $_FILES['image']['name'];
            $tmp = $_FILES['image']['tmp_name'];
            $size = $_FILES['image']['size'];
            $extension = getExtension($main);
            $extension = strtolower($extension);
            if (($extension == 'jpg') || ($extension == 'png') || ($extension == 'gif') || ($extension == 'jpeg')) {
                if ($pimage != '') {
                    unlink("../../../images/payment/" . $pimage);
                }
                $width = 53;
                $height = 45;
                $m = trim($imagename);
                $image = strtolower($m) . "." . $extension;
                $thumppath = "../../../images/payment/";
                $aaa = Imageuploadd($main, $size, $width, $thumppath, $thumppath, '255', '255', '255', $height, strtolower($m), $tmp);
            } else {
                $strupload = '2';
                $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-close"></i> Invalid File Format! Try jpg/png/gif/jpeg files only </div>';
            }
        } else {
            $image = '';
            if (isset($_REQUEST['prid'])) {
                $image = $pimage;
            }
        }
        $msg = addpayment($payment,  $imagename, $imagealt, $image, $order, $status, $ip, $thispageid, $getid);
    } else {
        $msg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
    }
}

if (isset($_REQUEST['prid']) && ($_REQUEST['prid'] != '')) {
    $get1 = $db->prepare("SELECT * FROM `payment` WHERE `pid`=?");
    $get1->execute(array($_REQUEST['prid']));
    $showrecords = $get1->fetch(PDO::FETCH_ASSOC);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Payment Mgmt
            <small><?php
                if ($_REQUEST['prid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Payment Mgmt </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i> Master(s)</a></li>
            <li><a href="<?php echo $sitename; ?>master/payment.htm">Payment Mgmt </a></li>
            <li class="active"><?php
                if ($_REQUEST['prid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> Payment Mgmt</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['prid'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> Payment Mgmt</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>

                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Payment Mgmt
                        </div>
                        <div class="panel-body">                        
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Payment Name <span style="color:#FF0000;">*</span></label>                                  
                                    <input type="text" name="payment" id="payment" placeholder="Enter The Payment Name" class="form-control" value="<?php echo (isset($_REQUEST['prid'])) ? stripslashes($showrecords['bname']) : ''; ?>"  pattern="[A-Z a-z 0-9 .,&_-]{1,55}" title="Special character not allowed." required />
                                </div>  
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Image Name <span id="addstar" style="color:#FF0000;">*</span></label>                                  
                                    <input type="text" name="imagename" id="imagenameid" placeholder="Enter The Image Name"   pattern="[A-Z a-z 0-9 .,&_-]{1,55}" title="Special character not allowed." class="form-control" value="<?php echo strtolower(getpayment('image_name', $_REQUEST['prid'])); ?>" required />
                                </div> 
                                <div class="col-md-6">
                                    <label>Image Alt <span id="addstar1" style="color:#FF0000;"></span></label>                                  
                                    <input type="text" name="imagealt" placeholder="Enter The Image Alt"    pattern="[A-Z a-z 0-9 .,&_-]{1,55}" title="Special character not allowed." id="imagealtid"  class="form-control" value="<?php echo strtolower(getpayment('imagealt', $_REQUEST['prid'])); ?>"  /> 
                                </div>
                            </div><br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Image <span id="addstar" style="color:#FF0000;">* (Recommended Size 53 Pixels Width * 43 Pixels Height)</span> </label>
                                    <input type="file" name="image" id="image" <?php
                                    if ($_REQUEST['prid'] == '') {
                                        echo 'required="required"';
                                    }
                                    ?>  onchange="imgchktore(this.value)" />
                                </div>
                                <?php if (getpayment('image', $_REQUEST['prid']) != '') { ?>
                                    <div class="col-md-6" id="delimage">
                                        <img src="<?php echo $fsitename; ?>images/payment/<?php echo getpayment('image', $_REQUEST['prid']); ?>" style="padding-bottom:10px;" height="100" />
                                        <button type="button" style="cursor:pointer;" class="btn btn-danger" name="del" id="del" onclick="javascript:deleteimage('<?php echo getpayment('image', $_REQUEST['prid']); ?>', '<?php echo $_REQUEST['prid']; ?>', 'payment', '../../images/payment/', 'image', 'pid');"><i class="fa fa-close">&nbsp;Delete Image</i></button>
                                    </div>
                                <?php } ?>
                            </div><br />
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Order<span style="color:#FF0000;">*</span></label>                                  
                                    <input type="number" class="form-control" name="order" required="required" value="<?php echo (isset($_REQUEST['prid'])) ? stripslashes($showrecords['order']) : ''; ?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php echo(isset($_REQUEST['prid'])) ? ($showrecords['status'] == '1') ? 'selected' : '' : 'selected'; ?>>Active</option>
                                        <option value="0" <?php echo(isset($_REQUEST['prid'])) ? ($showrecords['status'] == '0') ? 'selected' : '' : ''; ?>>Inactive</option>
                                    </select>
                                </div>
                                <div id="txtHint1"><b></b></div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>master/payment.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6"><!--validatePassword();-->
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['prid'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>