<?php

function getbanner($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `banner` WHERE `bid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function addbanner($title, $link, $description, $image, $imagename, $imagealt, $order, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        // $link22 = DB_QUERY("SELECT * FROM `banner` WHERE `imagename`='$imagename'");
        $link22 = FETCH_all("SELECT * FROM `banner` WHERE `imagename`=?", $imagename);
        if ($link22['imagename'] == '') {

            $resa = $db->prepare("INSERT INTO `banner` ( `title`, `link`,`content`, `image_alt`,`imagename`,`image`,`Order`, `status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($title, $link, $description, $imagename, $imagealt, $image, $order, $status, $ip, $_SESSION['UID']));
            //$insert_id = $db->lastInsertId();
            // $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            // $htry->execute(array('Ledger', 13, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i>Image Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `banner` WHERE `imagename`=? AND `bid`!=?", $imagename, $getid);
        if ($link22['imagename'] == '') {
            $resa = $db->prepare("UPDATE `banner` SET `title`=?, `link`=?,`content`=?, `image`=?,`imagename`=?,`image_alt`=?,`Order`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `bid`=?");
            $resa->execute(array(trim($title), trim($link), trim($description), trim($image), trim($imagename), trim($imagealt), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Banner Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i>Image Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delbanner($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {

        global $db;



        $get = $db->prepare("DELETE FROM `banner` WHERE `bid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function addfaq($faqq, $faqa,$image, $imagealt, $imagetitle,  $order, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        // $link22 = DB_QUERY("SELECT * FROM `banner` WHERE `imagename`='$imagename'");
        $link22 = FETCH_all("SELECT * FROM `faq` WHERE `imagetitle`=?", $imagetitle);
        if ($link22['imagetitle'] == '') {

            $resa = $db->prepare("INSERT INTO `faq` ( `faqquestion`, `faqanswer`,`image`, `imagealt`,`imagetitle`,`order`,`status`,`ip`,`updated_By`) VALUES(?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($faqq, $faqa, $image, $imagealt, $imagetitle, $order, $status, $ip, $_SESSION['UID']));
            //$insert_id = $db->lastInsertId();
            // $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            // $htry->execute(array('Ledger', 13, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `faq` WHERE `imagetitle`=? AND `faqid`!=?", $imagetitle,$getid);
        if ($link22['imagetitle'] == '') {
           //echo  trim($faqq). ' '.trim($faqa). ' '.trim($image).' '.trim($imagealt).' '. trim($imagetitle).' '.trim($order). ' '.trim($status). ' '.trim($ip).' '. $_SESSION['UID'].' '. $getid;
           //exit;
            $resa = $db->prepare("UPDATE `faq` SET `faqquestion`=?, `faqanswer`=?,`image`=?,`imagealt`=?,`imagetitle`=?, `order`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `faqid`=?");
            $resa->execute(array(trim($faqq), trim($faqa), trim($image),trim($imagealt), trim($imagetitle), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Faqs Mgmt', 12, 'Update', $_SESSION['UID'], $ip, $id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delfaq($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {

        global $db;



        $get = $db->prepare("DELETE FROM `faq` WHERE `faqid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function getfaq($a, $b) {
    global $db;

    $res = $db->prepare("SELECT * FROM `faq` WHERE `faqid`= ? ");
    $res->execute(array($b));
    $res = $res->fetch();
    return $res[$a];
}

function addstaticpagess($title, $metatitle, $metakeywords, $metadescription, $image_title, $image_alt, $image, $fullcontent, $content2, $content3, $ip, $status, $getid) {
    global $db;

    $link22 = FETCH_all("SELECT * FROM `static_pages` WHERE `image_title`=?", $imagetitle);
    if ($link22[$image_title] == '') {
        $resa = $db->prepare("UPDATE `static_pages` SET `title`=?, `metatitle`=?,`metakeywords`=?,`metadescription`=?,`image_title`=?, `image_alt`=?,`image`=?,`fullcontent`=?,`ip`=?, `Updated_by`=? WHERE `stid`=?");
        $resa->execute(array(trim($title), trim($metatitle), trim($metakeywords), trim($metadescription), trim($image_title), trim($image_alt), trim($image), trim($fullcontent), trim($ip), $_SESSION['UID'], $getid));

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('staticPages', 28, 'Update', $_SESSION['UID'], $ip, $id));
        //$ress = "UPDATE `static_pages` SET `title`=$title, `metatitle`=$metatitle,`metakeywords`=$metakeywords,`image`=$image, `image_alt`=$image_alt,`image_title`=$image_title,`fullcontent`=$fullcontent,`status`=$status,`ip`=$ip, `Updated_by`= WHERE `stid`=$getid";
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {
        $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
    }

    return $res;
}

function addvideo($title, $link, $order, $status, $ip, $date, $thispageid, $getid) {
    global $db;
    if ($getid == '') {

        $resa = $db->prepare("INSERT INTO `video` ( `title`, `link`,`order`, `status`, `ip`, `date`,`updated_by`) VALUES(?,?,?,?,?,?,?)");
        $resa->execute(array($title, $link, $order, $status, $ip, $date, $_SESSION['UID']));

        //DB("INSERT INTO `faq` SET `description`='" . stripslashes($description) . "',`seotitle`='" . stripslashes($seotitle) . "',`tags`='" . stripslashes(trim($tags)) . "',`order`='" . stripslashes(trim($order)) . "',`status`='" . stripslashes(trim($status)) . "',`date`='" . $date . "',`ip`='" . $ip . "'");
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
    } else {



        $res1 = $db->prepare("UPDATE `video` SET `title`= ? ,`link`= ? ,`order`= ? ,`status`= ? ,`date`= ? ,`ip`= ?,`updated_by`=? WHERE `vid`= ? ");
        $res1->execute(array(stripslashes($title), stripslashes($link), stripslashes($order), stripslashes($status), stripslashes($date), stripslashes($ip), stripslashes($_SESSION['UID']), stripslashes($getid)));

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('video mgmt', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
        //DB("UPDATE `faq` SET `description`='" . stripslashes($description) . "',`seotitle`='" . $seotitle . "',`tags`='" . trim($tags) . "',`order`='" . trim($order) . "',`status`='" . trim($status) . "',`date`='" . $date . "',`ip`='" . $ip . "' WHERE `faid`='$id'");


        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function getvideo($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `video` WHERE `vid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function delvideo($a) {
    global $db;

    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `video` WHERE `vid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function addbrand($brand, $link, $imagename, $imagealt, $image, $metatitle, $metakeywords, $metadescription, $order, $status, $ip, $thispageid, $id) {
    global $db;
    if ($id == '') {

        $resa = $db->prepare("INSERT INTO `brand` ( `bname`, `link`,`image_name`,`imagealt`,`image`,`metakeyword`,`metatitle`,`metadescription`,`IP`,`order`,`status`, `Updated_by` ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
        $resa->execute(array($brand, $link, $imagename, $imagealt, $image, $metakeywords, $metatitle, $metadescription, $ip, $order, $status, $_SESSION['UID']));

        //DB("INSERT INTO `faq` SET `description`='" . stripslashes($description) . "',`seotitle`='" . stripslashes($seotitle) . "',`tags`='" . stripslashes(trim($tags)) . "',`order`='" . stripslashes(trim($order)) . "',`status`='" . stripslashes(trim($status)) . "',`date`='" . $date . "',`ip`='" . $ip . "'");
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
    } else {

        $link22 = FETCH_all("SELECT * FROM `brand` WHERE `image_name`=?", $imagename);
        if ($link22[$imagename] == '') {
            $resa = $db->prepare("UPDATE `brand` SET `bname`=?, `link`=?,`image_name`=?,`imagealt`=?,`image`=?,`metakeyword`=?,`metatitle`=?,`metadescription`=?,`IP`=?,`order`=?,`status`=?, `Updated_by`=? WHERE `brid`=?");
            $resa->execute(array(trim($brand), trim($link), trim($imagename), trim($imagealt), trim($image), trim($metakeywords), trim($metatitle), trim($metadescription), trim($ip), trim($order), trim($status), $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Brand_mgmt', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    }
    return $res;
}

function getbrand($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `brand` WHERE `brid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delbrand($a) {
    global $db;

    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `brand` WHERE `brid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

/*
  function addstaticpagess($title, $metatitle, $metakeywords, $metadescription, $image_title, $image_alt, $image, $fullcontent, $content2, $content3, $ip,$status, $i) {
  global $db;
  if ($i == '') {

  } else {

  $linkk1 = $db->prepare("SELECT `stid` FROM `static_pages` WHERE `image_title`= ?  AND `stid`!= ? ");
  $linkk1->execute(array($image_title, $i));
  $linkk1 = $linkk1->fetch();


  //$linkk1 = DB_QUERY("SELECT `sid` FROM `static_pages` WHERE `image_name`='" . $image_name . "' AND `sid`!='" . $i . "'");
  if ($linkk1['stid'] == '') {

  $resa = $db->prepare("UPDATE `static_pages` SET `title`=?, `metatitle`=?,`metakeywords`=?,`image`=?, `image_alt`=?,`image_title`=?,`fullcontent`=?,`status`=?,`ip`=?, `Updated_by`=? WHERE `stid`=?");
  $resa->execute(array(trim($title), trim($metatitle), trim($metakeywords), trim($image), trim($image_alt),   trim($image_title), trim($fullcontent), trim($status),trim($ip), $_SESSION['UID'], $getid));


  $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
  $htry->execute(array('staticPages', 28, 'Update', $_SESSION['UID'], $ip, $id));

  $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
  } else
  $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
  }

  return $res;
  }

 */

function getstaticpages($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `static_pages` WHERE `stid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function delstaticpages($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('staticPages', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `static_pages` WHERE `stid` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function getcontactform($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `contact_form` WHERE `coid`=?");
    $get1->execute(array(trim($b)));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delcontactform($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Contact Forms', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `contact_form` WHERE `coid` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function delnewsletter($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Newsletter', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `newsletter` WHERE `id` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function addtest($title, $comments, $order, $status, $ip, $thispageid, $getid) {
    global $db;
    if ($getid == '') {

        $resa = $db->prepare("INSERT INTO `testimonial` ( `title`, `comments`,`order`,`status`,`ip`, `updated_by` ) VALUES(?,?,?,?,?,?)");

        //$resa = $db->prepare("INSERT INTO `testimonial` ( `title`=?, `comments`=?,`order`=?,`status`=?,`ip`=?, `Updated_by`=? );
        $resa->execute(array($title, $comments, $order, $status, $ip, $_SESSION['UID']));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
    } else {

        $resa = $db->prepare("UPDATE `testimonial` SET `title`=?, `comments`=?,`order`=?,`status`=?,`ip`=?, `updated_by`=?  WHERE `tid`=?");
        $resa->execute(array(trim($title), trim($comments), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Testimonial', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    }
    return $res;
}

function gettest($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `testimonial` WHERE `tid`=?");
    $get1->execute(array(trim($b)));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function deltest($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `testimonial` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Testimonial', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `testimonial` WHERE `tid` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function getaddress($a, $b) {
    global $db;
    $res = $db->prepare("SELECT * FROM `bill_ship_address` WHERE `CusID`= ? ");
    $res->execute(array($b));
    $res = $res->fetch();
    return $res[$a];
}

?>
