<?php
$menu = "19,19,41";
if (isset($_REQUEST['sid'])) {
    $thispageeditid = 41;
} else {
    $thispageid = 41;
}
include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');

if (isset($_REQUEST['submit'])) {
    @extract($_REQUEST);
    $getid = $_REQUEST['sid'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $km1 = implode('**', $km);
    $firstkg1 = implode('**', $firstkg);
    $secondkg1 = implode('**', $secondkg);
    $thirdkg1 = implode('**', $thirdkg);
    $msg = addshipprice($km1, $firstkg1, $secondkg1, $thirdkg1, $status, $getid, $ip);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Shipping Price Mgmt

        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-truck"></i> Shipping</a></li>
            <li><a href="<?php echo $sitename; ?>shipping/shiping_price.htm"> Shipping Price Mgmt </a></li>

        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"> Shipping Price</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Shipping Price
                        </div>
                        <div class="panel-body">
                            <?php
                            $f = '0';
                            $t = '25';
                            $firstkglist[] = '';
                            $secondkglist[] = '';
                            $thirdkglist[] = '';

                            $shipprice11 = pFETCH("SELECT * FROM `shipping_price` WHERE `sid`!=? ORDER BY `sid` ASC", '');

                            while ($shprice = $shipprice11->fetch(PDO::FETCH_ASSOC)) {
                                $firstkglist[] .= $shprice['0to1kg'];
                                $secondkglist[] .= $shprice['1to25kg'];
                                $thirdkglist[] .= $shprice['25plus'];
                            }



                            for ($i = 1; $i <= 11; $i++) {
                                if ($f == '0') {
                                    $f = '0';
                                    $t = '25';
                                } else {
                                    $f = $t;
                                    $t = $t + 25;
                                }

                                if ($f == '250') {
                                    $ft = '250+';
                                } else {
                                    $ft = $f . '-' . $t;
                                }
                                ?>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Kilo Metre(KM)</label>  

                                        <input type="text" readonly class="form-control" placeholder="Enter the Kilo Metre" name="km[]" id="km[]" required="required" pattern="[0-9 A-Z a-z .,:'()]{2,60}" title="Allowed Characters (0-9A-Za-z .,:'()]{2,60})" value='<?php echo $ft; ?>' />
                                    </div>
                                    <div class="col-md-3">
                                        <label>Weight : 0-1 Kg<span style="color:#FF0000;">*</span></label>  
                                        <input type="number" step="0.01" class="form-control" placeholder="Enter the Weight" name="firstkg[]" id="firstkg[]" required="required" pattern="[0-9 A-Z a-z .,:'()]{2,60}" title="Allowed Characters (0-9A-Za-z .,:'()]{2,60})" value='<?php echo $firstkglist[$i]; ?>' />
                                    </div>
                                    <div class="col-md-3">
                                        <label>Weight : 1-25 Kg<span style="color:#FF0000;">*</span></label>  
                                        <input type="number" step="0.01" class="form-control" placeholder="Enter the Weight" name="secondkg[]" id="secondkg[]" required="required" pattern="[0-9 A-Z a-z .,:'()]{2,60}" title="Allowed Characters (0-9A-Za-z .,:'()]{2,60})" value='<?php echo $secondkglist[$i]; ?>' />
                                    </div>
                                    <div class="col-md-3">
                                        <label>Weight : 25+ Kg<span style="color:#FF0000;">*</span></label>  
                                        <input type="number" step="0.01" class="form-control" placeholder="Enter the Weight" name="thirdkg[]" id="thirdkg[]" required="required" pattern="[0-9 A-Z a-z .,:'()]{2,60}" title="Allowed Characters (0-9A-Za-z .,:'()]{2,60})" value='<?php echo $thirdkglist[$i]; ?>' />
                                    </div>
                                </div>
                                <br />
                                <?php $f++;
                            }
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if (stripslashes(getshipprice('status', $_REQUEST['sid'])) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (stripslashes(getshipprice('status', $_REQUEST['sid']) == '0')) {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>
                </div>
                
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>shipping/shipping_price.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['sid'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>