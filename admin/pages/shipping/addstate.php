<?php
$menu = "19,19,40";
if (isset($_REQUEST['sid'])) {
    $thispageeditid = 40;
} else {
    $thispageid = 40;
}
include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');

if (isset($_REQUEST['submit'])) {
    @extract($_REQUEST);
    $getid = $_REQUEST['sid'];
    $ip = $_SERVER['REMOTE_ADDR'];
    $msg = addshipstate($statename, $country, $status, $ip, $getid);
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Shipping State Mgmt
            <small><?php
                if ($_REQUEST['sid']) {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> State Mgmt </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="#"><i class="fa fa-truck"></i> Shipping</a></li>
            <li><a href="<?php echo $sitename; ?>shipping/state.htm">Shipping States Mgmt </a></li>
            <li class="active"><?php
                if ($_REQUEST['sid'] != '') {
                    echo 'Edit';
                } else {
                    echo 'Add New';
                }
                ?> State Mgmt</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php
                        if ($_REQUEST['sid'] != '') {
                            echo 'Edit';
                        } else {
                            echo 'Add New';
                        }
                        ?> State Mgmt</h3>
                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            State Mgmt
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                  <div class="col-md-6">
                                    <label>Country Name<span style="color:#FF0000;">*</span></label> 
                                  
                                   <select name="country" class="form-control" required="required">
                                    <option value="">Select</option>
                                    <?php
                                    $getmanuf = DB("SELECT * FROM `countries` WHERE `status`='1' ");
                                    while ($fdepart = mysqli_fetch_array($getmanuf)) {
                                    ?>
                                        <option value="<?php echo $fdepart['id']; ?>" <?php if(getshipstate('country_id', $_REQUEST['sid'])==$fdepart['id']) { ?> selected="selected" <?php } ?>><?php echo $fdepart['name']; ?></option>
                                        <?php } ?>
                                   </select>
                                </div>
                                <div class="col-md-6">
                                    <label>State Name<span style="color:#FF0000;">*</span></label>  
                                    <input type="text" class="form-control" placeholder="Enter the State name" name="state" id="state" required="required" pattern="[0-9 A-Z a-z .,:'&()]{2,60}" title="Allowed Characters (0-9A-Za-z .,:'()]{2,60})" value='<?php echo stripslashes(getshipstate('name', $_REQUEST['sid'])); ?>' />
                                </div>
							
                            </div>
                            <br />
                            
                            <div class="row">
                              
                                <div class="col-md-6">
                                    <label>Status <span style="color:#FF0000;">*</span></label>                                  
                                    <select name="status" class="form-control">
                                        <option value="1" <?php
                                        if (stripslashes(getshipstate('status', $_REQUEST['sid'])) == '1') {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if (stripslashes(getshipstate('status', $_REQUEST['sid']) == '0')) {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>

                                    </select>
                                </div>

                            </div>
                        </div><br/>

                    </div>

                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="<?php echo $sitename; ?>shipping/state.htm">Back to Listings page</a>
                        </div>
                        <div class="col-md-6"><!--validatePassword();-->
                            <button type="submit" name="submit" id="submit" class="btn btn-success" style="float:right;"><?php
                                if ($_REQUEST['sid'] != '') {
                                    echo 'UPDATE';
                                } else {
                                    echo 'SAVE';
                                }
                                ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php include ('../../require/footer.php'); ?>