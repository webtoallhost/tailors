<?php

include ('../../config/config.inc.php');

//ini_set('display_errors','1');
//error_reporting(E_ALL);

function mres($value) {
    $search = array("\\", "\x00", "\n", "\r", "'", '"', "\x1a");
    $replace = array("\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z");
    return str_replace($search, $replace, $value);
}

/* Declaration table name start here */
if ($_REQUEST['types'] == 'shipcountry') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id','name','status');
    $sIndexColumn = "id";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittype" : "edittype";
    $sTable = "countries";
}

if ($_REQUEST['types'] == 'sizetable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id','size', 'order','status');
    $sIndexColumn = "id";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittype" : "edittype";
    $sTable = "size";
}
if ($_REQUEST['types'] == 'colortable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id','color', 'order', 'status');
    $sIndexColumn = "id";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittype" : "edittype";
    $sTable = "color";
}
if ($_REQUEST['types'] == 'typetable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id','type', 'order', 'status');
    $sIndexColumn = "id";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittype" : "edittype";
    $sTable = "type";
}
if ($_REQUEST['types'] == 'bannertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('bid','image', 'order', 'status');
    $sIndexColumn = "bid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editbanner" : "editbanner";
    $sTable = "banner";
}
if ($_REQUEST['types'] == 'quickbannertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('bid','title','image', 'status');
    $sIndexColumn = "bid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editbanner" : "editbanner";
    $sTable = "quickbanner";
}
if ($_REQUEST['types'] == 'offerbanner') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('bid','title','image', 'status');
    $sIndexColumn = "bid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editbanner" : "editbanner";
    $sTable = "offerbanner";
}
if ($_REQUEST['types'] == 'tailortable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('uid','name', 'emailid', 'mobile', 'status');
    $sIndexColumn = "uid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittailor" : "edittailor";
    $sTable = "tailor";
}
if ($_REQUEST['types'] == 'professionaltable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('uid','name', 'emailid', 'mobile', 'status');
    $sIndexColumn = "uid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editprofessional" : "editprofessional";
    $sTable = "professional";
}
if ($_REQUEST['types'] == 'fabrictable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('uid','name', 'emailid', 'mobile', 'status');
    $sIndexColumn = "uid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editfabric" : "editfabric";
    $sTable = "fabric";
}
if ($_REQUEST['types'] == 'logistictable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('uid','name', 'emailid', 'mobile', 'status');
    $sIndexColumn = "uid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editlogistic" : "editlogistic";
    $sTable = "logistic";
}
// if ($_REQUEST['types'] == 'vendortable') {
//     // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
//     $aColumns = array('id','name', 'emailid', 'mobile', 'status');
//     $sIndexColumn = "id";
//     $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editvendor" : "editvendor";
//     $sTable = "users";
// }
if (($_REQUEST['types'] == 'vendortable')) {
    
     $aColumns = array('uid','name', 'emailid', 'mobile', 'status');
    $sIndexColumn = "uid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editvendor" : "editvendor";
    $sTable = "vendor";
}


if ($_REQUEST['types'] == 'blogcategorytable') {
    $aColumns = array('bcid', 'category', 'order', 'status');
    $sIndexColumn = "bcid";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "editfaqcategory" : "editfaqcategory";
    $sTable = "blogcategory";
}

if ($_REQUEST['types'] == 'blogtable') {
    $aColumns = array('bid', 'category', 'title', 'order', 'status');
    $sIndexColumn = "bid";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "editfaq" : "editfaq";
    $sTable = "blog";
}

if ($_REQUEST['types'] == 'productreviewtable') 
{
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('prid', 'productname', 'email', 'status');
    $sIndexColumn = "prid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittproductreview" : "editproductreview";
    $sTable = "productreview";
}
///////ratings
///////ratings
if ($_REQUEST['types'] == 'brandtable') 
{
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('brid', 'bname', 'order', 'status');
    $sIndexColumn = "brid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editbrand" : "editbrand";
    $sTable = "brand";
}

if ($_REQUEST['types'] == 'paymenttable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('pid', 'bname', 'order', 'status');
    $sIndexColumn = "pid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editbrand" : "editbrand";
    $sTable = "payment";
}

if ($_REQUEST['types'] == 'footertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('pid', 'title', 'order', 'status');
    $sIndexColumn = "pid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editbrand" : "editbrand";
    $sTable = "footermgt";
}
if ($_REQUEST['types'] == 'categorytable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('cid', 'category', 'order', 'status',);
    $sIndexColumn = "cid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editsubcategory" : "editsubcategory";
    $sTable = "category";
}

if ($_REQUEST['types'] == 'subcategorytable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('sid', 'cid', 'subcategory', 'order', 'status',);
    $sIndexColumn = "sid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editsubcategory" : "editsubcategory";
    $sTable = "subcategory";
}
if ($_REQUEST['types'] == 'innercategorytable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('innerid', 'cid', 'subcategory', 'innername', 'order', 'status');
    $sIndexColumn = "innerid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editinnercategory" : "editinnercategory";
    $sTable = "innercategory";
}
if ($_REQUEST['types'] == 'homecategorytable') {
    // $aColumns = array('id', 'Category Name', 'status');
    $aColumns = array('innerid', 'cid', 'status');
    $sIndexColumn = "innerid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editinnercategory" : "editinnercategory";
    $sTable = "homecategory";
}


if ($_REQUEST['types'] == 'producttable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('pid', 'cid', 'productname','status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "pid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editproduct" : "editproduct";
    $sTable = "product";
}
if ($_REQUEST['types'] == 'ordertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('oid', 'datetime', 'order_id', 'cus_name', 'over_all_total', 'order_status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "oid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "vieworder" : "vieworder";
    $sTable = "norder";
}

if ($_REQUEST['types'] == 'contactustable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id', 'name', 'phone', 'email');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "id";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "editcontactus" : "editcontactus";
    $sTable = "contact";
}
if ($_REQUEST['types'] == 'newslettertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id', 'email', 'ip', 'datetime',);

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "id";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "editcontactus" : "editcontactus";
    $sTable = "newsletter";
}
if ($_REQUEST['types'] == 'faqtable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('faqid', 'faqquestion', 'order', 'status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "faqid";
    // $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editcontactus" : "editcontactus";
    $sTable = "faq";
}

if ($_REQUEST['types'] == 'statictable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('sid', 'title');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "sid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editstatic" : "editstati";
    $sTable = "static_pages";
}
if ($_REQUEST['types'] == 'customertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('CusId', 'fname', 'mobileno', 'emailid', 'Status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "CusId";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "customer";
}
////////////////
if ($_REQUEST['types'] == 'reviews') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('prid', 'name', 'email', 'rating', 'status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "prid";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "productreview";
}
////////////////
if ($_REQUEST['types'] == 'viewnotification') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id', 'date', 'from', 'message', 'read_status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "id";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "notification";
}
if ($_REQUEST['types'] == 'promo_code') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('pcid', 'promo_code', 'type', 'value', 'status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "pcid";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "promocode";
}
////////////////
//ask me
if ($_REQUEST['types'] == 'askmecustomertable') 
{
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id', 'vendorid', 'product', 'userid', 'message');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "id";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "customermsg";
}
//ask me

//
if ($_REQUEST['types'] == 'productreviewtable1') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('prid', 'name', 'email', 'product', 'rating');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "prid";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "productreview";
}
//

if ($_REQUEST['types'] == 'usertable') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('id', 'user_name', 'email_id', 'security_group', 'status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "id";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "users";
}
if ($_REQUEST['types'] == 'socialmedia') {
    // $aColumns = array('id', 'Category Name', 'Sub Category', 'Product', 'Order', 'status');
    $aColumns = array('sid', 'sname', 'order', 'status');

    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "sid";
    //$editpage = ($_REQUEST['db_table_for'] == 'live') ? "edit" : "editstati";
    $sTable = "socialmedia";
}
if ($_REQUEST['types'] == 'homebannerstable') {
    $aColumns = array('hbid', 'title', 'image', 'order', 'status');
    $sIndexColumn = "hbid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edithomebanners" : "edithomebanners";
    $sTable = "homebanners";
}

if ($_REQUEST['types'] == 'contacttable') {
    $aColumns = array('coid', 'date', 'firstname', 'emailid', 'phoneno');
    $sIndexColumn = "coid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editcontactlist" : "editcontactlist";
    $sTable = "contact_form";
}
if ($_REQUEST['types'] == 'feedbacktable') {
    $aColumns = array('fdid', 'datetime', 'name', 'email', 'contactno','replay');
    $sIndexColumn = "fdid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editcontactlist" : "editcontactlist";
    $sTable = "feedback";
}

if ($_REQUEST['types'] == 'shipstate') {
    $aColumns = array('id', 'name', 'status');
    $sIndexColumn = "id";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editstate" : "editstate";
    $sTable = "states";
}

if ($_REQUEST['types'] == 'shipprice') {
    $aColumns = array('id', 'postcode', 'status');
    $sIndexColumn = "id";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editshipping_price" : "editshipping_price";
    $sTable = "shipping_price";
}


if ($_REQUEST['types'] == 'shipcity') {
    $aColumns = array('scid', 'ship_state', 'shipping_city', 'shipping_rate', 'order', 'status');
    $sIndexColumn = "scid";
    $editpage = ($_REQUEST['db_table_for'] == 'live') ? "editcity" : "editcity";
    $sTable = "shipping_city";
}
if ($_REQUEST['types'] == 'printtrmplist') {
    $aColumns = array('id', 'name');
    $sIndexColumn = "id";
    // $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittax" : "edittax";
    $sTable = "print_template";
}

if ($_REQUEST['types'] == 'emailTemplateTable') {
    $aColumns = array('id', 'name');
    $sIndexColumn = "id";
    // $editpage = ($_REQUEST['db_table_for'] == 'live') ? "edittax" : "edittax";
    $sTable = "email_template";
}

/* Declaration table name is item and salary pending.. */

/* Declaration table name end here */
$aColumns1 = $aColumns;

function fatal_error($sErrorMessage = '') {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
    die($sErrorMessage);
}

$sLimit = "";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . intval($_GET['iDisplayStart']) . ", " . intval($_GET['iDisplayLength']);
}


$sOrder = "";



if (isset($_GET['iSortCol_0'])) {
    $sOrder = "ORDER BY  ";

    if (in_array("order", $aColumns)) {
        $sOrder .= "`order` asc, ";
    } else if (in_array("Order", $aColumns)) {
        $sOrder .= "`Order` asc, ";
    }
    for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
        if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
            $sOrder .= "`" . $aColumns[intval($_GET['iSortCol_' . $i])] . "` " . ($_GET['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
        }
        $sOrder = substr_replace($sOrder, "", -2);

        if ($sOrder == "ORDER BY") {
            $sOrder = " ";
        }
    }
}

$sWhere = "";
if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
    $sWhere .= " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        if ($aColumns[$i] == 'Status') {
            if (strtolower($_GET['sSearch']) == 'active') {
                $sWhere .= " `Status`='1' OR ";
            } elseif (strtolower($_GET['sSearch']) == 'inactive') {
                $sWhere .= " `Status`='0' OR ";
            } else {
                $sWhere .= "";
            }
        }  elseif ($_REQUEST['types'] == 'producttable') {
            $sWhere .= " `cid` IN (SELECT `cid` FROM `category` WHERE `category` LIKE '%" . mres($_GET['sSearch']) . "%' ) OR ";
        }
        elseif ($_REQUEST['types'] == 'attrivaluetable') {
          //  $sWhere .= " `valid` IN (SELECT `id` FROM `attribute` WHERE `name` LIKE '%" . mres($_GET['sSearch']) . "%' ) OR ";
        } else {
            //$sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . mres($_GET['sSearch']) . "%' OR ";
        }
$sWhere .= "`" . $aColumns[$i] . "` LIKE '%" . mres($_GET['sSearch']) . "%' OR ";

    }
    $sWhere = substr_replace($sWhere, "", -3);
    $sWhere .= ')';
}

if (($_REQUEST['types'] == 'categorytable') || ($_REQUEST['types'] == 'subcategorytable') || ($_REQUEST['types'] == 'innercategorytable') || ($_REQUEST['types'] == 'homecategorytable')) {
    $sWhere .= " AND `status`!='2'";
}
if ($_REQUEST['types'] == 'producttable') {
    $sWhere .= " AND `vendor`='".$_SESSION['UID']."'";
}
if(($_SESSION['UID'] != '1') && $_REQUEST['types'] != 'viewnotification')
{
   // $sWhere .= " AND `vendor`='".$_SESSION['UID']."'";
}
if($_REQUEST['types'] == 'askmecustomertable' && $_SESSION['UID'] != '1')
{
    $sWhere .= " AND `vendorid`='".$_SESSION['usergroup']."'";
}
if($_REQUEST['types'] == 'reviews' && $_SESSION['UID'] != '1')
{
    $sWhere .= " AND `vendor`='".$_SESSION['usergroup']."'";
}

if ($_REQUEST['types'] == 'viewnotification' && $_SESSION['type']=='logistic') {
    $sWhere .= " AND `to`='".$_SESSION['usergroup']."' AND `type`='logistics' ";
}
elseif ($_REQUEST['types'] == 'viewnotification' && $_SESSION['type']=='vendor') {
    $sWhere .= " AND `to`='".$_SESSION['usergroup']."' AND `type`='vendor'";
}
else
{
    if($_REQUEST['types'] == 'viewnotification'){
 $sWhere .= " AND `to`='admin'";    
    }
}
$sqr='';
if ($_REQUEST['ssqry']!= '') {
    $sqr= urldecode($_REQUEST['ssqry']);
}

if ($sWhere != '') {
    $sWhere = "WHERE `$sIndexColumn`!='' $sWhere";
} else {
    $sWhere = "WHERE `$sIndexColumn`!='' $sWhere";
}
/* Individual column filtering */

if ($_REQUEST['types'] == 'contacttable') {
  $sOrder = " ORDER BY coid DESC";
}
elseif ($_REQUEST['types'] == 'reviews') {
  $sOrder = " ORDER BY prid DESC";
}
elseif ($_REQUEST['types'] == 'viewnotification') {
  $sOrder = " ORDER BY read_status ASC,id DESC";
}
elseif ($_REQUEST['types'] == 'promo_code') {
  $sOrder = " ORDER BY pcid DESC";
}

elseif ($_REQUEST['types'] == 'customertable') {//productreview
  $sOrder = " ORDER BY CusID DESC";
}
elseif ($_REQUEST['types'] == 'productreview') {//productreview
  $sOrder = " ORDER BY prid DESC";
}
elseif ($_REQUEST['types'] == 'brandtable' || $_REQUEST['types'] == 'blogtable' || $_REQUEST['types'] == 'blogcategorytable' || $_REQUEST['types'] == 'bannertable'  ||  $_REQUEST['types'] == 'colortable' ||  $_REQUEST['types'] == 'sizetable' || $_REQUEST['types'] == 'typetable' || $_REQUEST['types'] == 'categorytable' || $_REQUEST['types'] == 'subcategorytable' || $_REQUEST['types'] == 'innercategorytable'  || $_REQUEST['types'] == 'attritable'  || $_REQUEST['types'] == 'attrivaluetable'  || $_REQUEST['types'] == 'attrigrptable' ) {
	$sOrder = " ORDER BY `order` ASC";	
  
}
elseif ($_REQUEST['types'] == 'quickbannertable' || $_REQUEST['types'] == 'offerbanner') {
	$sOrder = " ORDER BY `bid` DESC";	
  
}
elseif ($_REQUEST['types'] == 'vendortable' || $_REQUEST['types'] == 'tailortable' || $_REQUEST['types'] == 'fabrictable' || $_REQUEST['types'] == 'logistictable'  || $_REQUEST['types'] == 'professionaltable' ) {
	$sOrder = " ORDER BY `uid` DESC";	
  
}
elseif ($_REQUEST['types'] == 'producttable') {
  $sOrder = " ORDER BY pid DESC";
}
else{
 $sOrder = " ORDER BY id DESC";
}

$sQuery = "
        SELECT SQL_CALC_FOUND_ROWS `" . str_replace(",", "`,`", implode(",", $aColumns)) . "`
        FROM $sTable
        $sWhere
        $sqr
        $sOrder          
        $sLimit
    ";



$rResult = DB($sQuery);

$sQuery = "
        SELECT FOUND_ROWS() FROM $sTable
    ";

$rResultFilterTotal = DB($sQuery);
$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];

$sQuery = "
        SELECT COUNT(" . $sIndexColumn . ")
        FROM $sTable
    ";
$rResultTotal = DB_QUERY($sQuery);
$aResultTotal = mysqli_fetch_array($rResultTotal);
$iTotal = $rResultTotal[0];

$output = array(
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData" => array()
);

//print_r($output); die;

$ij = 1;
$k = $_GET['iDisplayStart'];

while ($aRow = mysqli_fetch_array($rResult)) {
//print_r($aRow); die;
    $k++;
    $row = array();

    for ($i = 0; $i < count($aColumns1); $i++) {
        if ($_REQUEST['types'] == 'categorytable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[cid].')">';

				$category=pFETCH("SELECT * FROM `category` WHERE `cid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }  else if ($_REQUEST['types'] == 'innercategorytable') 
        {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            }elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow['innerid'].')">';

				$category=pFETCH("SELECT * FROM `innercategory` WHERE `innerid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'cid') {
                $cate = explode(',', $aRow[$aColumns1[$i]]);
                foreach ($cate as $c) {
                    $data .= getcategory('category', $c);
                    $data .= ',';
                }
                $row[] = substr($data, 0, -1);
                $data = '';
                //$row[] =getcategory('category', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'subcategory') {
                $cate = explode(',', $aRow[$aColumns1[$i]]);
                foreach ($cate as $c) {
                    $data .= getsubcategory('subcategory', $c);
                    $data .= ',';
                }
                $row[] = substr($data, 0, -1);
                $data = '';
                //$row[] =getcategory('category', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'subcategorytable') 
        {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            }elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow['sid'].')">';

				$category=pFETCH("SELECT * FROM `subcategory` WHERE `sid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'cid') {
                $cate = explode(',', $aRow[$aColumns1[$i]]);
                foreach ($cate as $c) {
                    $data .= getcategory('category', $c);
                    $data .= ',';
                }
                $row[] = substr($data, 0, -1);
                $data = '';
                //$row[] =getcategory('category', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } 

        //askme
         else if ($_REQUEST['types'] == 'askmecustomertable') 
        {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            }
            elseif ($aColumns1[$i] == 'vendorid') {
                $row[] = getvendor('name',$aRow[$aColumns1[$i]]);
            }
            elseif ($aColumns1[$i] == 'product') {
                $row[] = getproduct('productname',$aRow[$aColumns1[$i]]);
            }
            elseif ($aColumns1[$i] == 'userid') {
                $row[] = getcustomer('fname',$aRow[$aColumns1[$i]]);
            }
            elseif ($aColumns1[$i] == 'order') {
                $orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow['sid'].')">';

                $category=pFETCH("SELECT * FROM `subcategory` WHERE `sid`!=? ORDER BY `order` ASC",0);
                while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
                {
                    if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';                    
                }       
                $orderval.='</select>';
                
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'cid') {
                $cate = explode(',', $aRow[$aColumns1[$i]]);
                foreach ($cate as $c) {
                    $data .= getcategory('category', $c);
                    $data .= ',';
                }
                $row[] = substr($data, 0, -1);
                $data = '';
                //$row[] =getcategory('category', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }
        //askme

        else if ($_REQUEST['types'] == 'askmecustomertable') 
        {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            }elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[innerid].')">';

				$category=pFETCH("SELECT * FROM `innercategory` WHERE `innerid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            }  elseif ($aColumns1[$i] == 'cid') {
                $cate = explode(',', $aRow[$aColumns1[$i]]);
                foreach ($cate as $c) {
                    $data .= getcategory('category', $c);
                    $data .= ',';
                }
                $row[] = substr($data, 0, -1);
                $data = '';
            } elseif ($aColumns1[$i] == 'subcategory') {
                $cate1 = explode(',', $aRow[$aColumns1[$i]]);
                foreach ($cate1 as $c1) {
                    $data1 .= getsubcategory('subcategory', $c1);
                    $data1 .= ',';
                }
                $row[] = substr($data1, 0, -1);
                $data1 = '';
                // $row[] = getsubcategory('subcategory', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }else if ($_REQUEST['types'] == 'blogcategorytable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[bcid].')">';

				$category=pFETCH("SELECT * FROM `blogcategory` WHERE `bcid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'blogtable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            }  elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[bid].')">';

				$category=pFETCH("SELECT * FROM `blog` WHERE `bid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'category') {
                $cat = explode(",", $aRow[$aColumns1[$i]]);
                $catss = '';
                foreach ($cat as $cats) {
                    $catss .= getblogcategory('category', $cats) . ',';
                }
                $row[] = substr($catss, 0, -1);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'producttable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'cid') {
                $row[] = getcategory('category',$aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'promo_code') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } elseif ($aColumns1[$i] == 'type') {
                $row[] = ($aRow[$aColumns1[$i]] == '1') ? "Discount" : "Amount";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'attrigrptable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'attribute') {

                $getarr = explode(',', $aRow[$aColumns1[$i]]);
                $finalstr = "";
                foreach ($getarr as $tempid) {
                    $finalstr .= "," . getattribute('name', $tempid);
                }
                $row[] = ltrim($finalstr, ',');
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'ordertable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order_status') {
                if ($aRow[$aColumns1[$i]] == '0')
                    $row[] = "Unpaid/Incomplete Order";
                if ($aRow[$aColumns1[$i]] == '1')
                    $row[] = "Processing";
                if ($aRow[$aColumns1[$i]] == '2')
                    $row[] = "Success";
                if ($aRow[$aColumns1[$i]] == '3')
                    $row[] = "Failure";
                if ($aRow[$aColumns1[$i]] == '4')
                    $row[] = "Cancelled";
            }elseif ($aColumns1[$i] == 'over_all_total') {
                $row[] = '<i class="fa fa-usd" aria-hidden="true"></i>&nbsp' . $aRow[$aColumns1[$i]];
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'datetime') {
                $row[] = date("d-m-Y h:i:s a", strtotime($aRow[$aColumns1[$i]]));
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'vendortable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } 
			elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[bid].')">';

				$category=pFETCH("SELECT * FROM `banner` WHERE `bid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            }elseif ($aColumns1[$i] == 'status') {
				if($aRow[$aColumns1[$i]]=='1') { $sele='selected="selected"'; $sele1=''; } else { $sele1='selected="selected"'; $sele=''; } 
                  $orderval='';				  
				  $orderval.='<select name="status" id="status" class="form-control" onChange="changestatus(this.value,'.$aRow[uid].')"><option value="1" '. $sele.'>Active</option>
				 <option value="0" '. $sele1.'>Inactive</option></select>';					
				   $row[] = $orderval;

            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'bannertable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'image') {
                $row[] = '<img src="' . $fsitename . 'images/banner/' . $aRow[$aColumns1[$i]] . '" style="padding-bottom:10px;" height="100" />';
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[bid].')">';

				$category=pFETCH("SELECT * FROM `banner` WHERE `bid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            }elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }//
        /////////////////
        else if ($_REQUEST['types'] == 'reviews') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'image') {
                $row[] = '<img src="' . $fsitename . 'images/banner/' . $aRow[$aColumns1[$i]] . '" style="padding-bottom:10px;" height="100" />';
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'status') {

             if($aRow[$aColumns1[$i]]==1) { $sele='selected="selected"'; $sele1=''; } 
             else { $sele1='selected="selected"'; $sele=''; }



                $orderval = '<select name="status" id="status" class="form-control" onChange="changestatus(this.value,'.$aRow['prid'].')">
                    <option value="1" '. $sele.'>Active</option>
                    <option value="0" '. $sele1.'>Deactive</option> 
                </select>';
                
                $row[] = $orderval;
            }else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }
        else if ($_REQUEST['types'] == 'offerbanner') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'image') {
                $row[] = '<img src="' . $fsitename . 'images/offer/' . $aRow[$aColumns1[$i]] . '" style="padding-bottom:10px;" height="100" />';
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }

          else if ($_REQUEST['types'] == 'quickbannertable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'image') {
                $row[] = '<img src="' . $fsitename . 'images/quickbanner/' . $aRow[$aColumns1[$i]] . '" style="padding-bottom:10px;" height="100" />';
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'typetable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[id].')">';

				$category=pFETCH("SELECT * FROM `type` WHERE `id`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }  else if ($_REQUEST['types'] == 'shipcountry') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }else if ($_REQUEST['types'] == 'sizetable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[id].')">';

				$category=pFETCH("SELECT * FROM `size` WHERE `id`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }else if ($_REQUEST['types'] == 'colortable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[id].')">';

				$category=pFETCH("SELECT * FROM `color` WHERE `id`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'partnertable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'image') {
                $row[] = '<img src="' . $fsitename . 'images/partner/' . $aRow[$aColumns1[$i]] . '" style="padding-bottom:10px;" height="100" />';
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'homebannerstable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'image') {
                $row[] = '<img src="' . $sitename . 'images/homebanner/' . $aRow[$aColumns1[$i]] . '" style="padding-bottom:10px;" height="100" />';
                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'usertable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'socialmedia') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'faqtable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;

                //$row[] = $aRow[$aColumns1[$i]] ? "Guest" : "Registered";
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'brandtable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'order') {
				$orderval = '<select name="order" id="order" class="form-control" onChange="changeorder(this.value,'.$aRow[brid].')">';

				$category=pFETCH("SELECT * FROM `brand` WHERE `brid`!=? ORDER BY `order` ASC",0);
				while ($categorylist = $category->fetch(PDO::FETCH_ASSOC))
				{
					if($aRow[$aColumns1[$i]]==$categorylist['order']) { $sele='selected="selected"'; } else { $sele=''; }
                 $orderval.='<option value="'.$categorylist['order'].'" '. $sele.'>'.$categorylist['order'].'</option>';					
				}		
                $orderval.='</select>';
				
                $row[] = $orderval;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'testimonialtable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'date') {
                $row[] = date('d-M-y', strtotime($aRow[$aColumns1[$i]]));
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }  else if ($_REQUEST['types'] == 'currencytable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'productreviewtable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'productname') {
                $row[] = getproduct('productname', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'customertable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'Status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } 
else if ($_REQUEST['types'] == 'reviews') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } 
        ///////
        else if ($_REQUEST['types'] == 'viewnotification') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'date') {
                $row[] = date('d-M-Y g:i a', strtotime($aRow[$aColumns1[$i]]));
            } elseif ($aColumns1[$i] == 'read_status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Yes" : "No";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } 
        else if ($_REQUEST['types'] == 'promo_code') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } 
        else if ($_REQUEST['types'] == 'videotable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'shipstate') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'shipprice') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'shipcity') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'ship_state') {
                $row[] = getshipstate('state_name', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'assitorsfaqtable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'assistor') {
                $row[] = getassitors('name', $aRow[$aColumns1[$i]]);
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'contacttable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'date') {
                $row[] = date('d-M-y', strtotime($aRow[$aColumns1[$i]]));
            } elseif ($aColumns1[$i] == 'status') {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else if ($_REQUEST['types'] == 'feedbacktable') {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif ($aColumns1[$i] == 'datetime') {
                $row[] = date('d-M-y', strtotime($aRow[$aColumns1[$i]]));
            } elseif ($aColumns1[$i] == 'view') {
                $row[] = $aRow[$aColumns1[$i]] ? "Yes" : "No";
            } elseif ($aColumns1[$i] == 'replay') {
                $row[] = $aRow[$aColumns1[$i]] ? "Yes" : "No";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        } else {
            if ($aColumns1[$i] == $sIndexColumn) {
                $row[] = $k;
            } elseif (($aColumns1[$i] == 'Status') || ($aColumns1[$i] == 'status')) {
                $row[] = $aRow[$aColumns1[$i]] ? "Active" : "Inactive";
            } else {
                $row[] = $aRow[$aColumns1[$i]];
            }
        }
    }
    /* Edit page  change start here */

    if ($_REQUEST['types'] == 'producttable') {
        $row[] = "<a href='" . $sitename . "products/" . $aRow[$sIndexColumn] . "/editproduct.htm' style='cursor:pointer;'><i class='fa fa-edit' ></i> View / Edit</a>";
    } elseif (($_REQUEST['types'] == 'assitorsfaqtable')) {
        $row[] = "<i class='fa fa-eye' onclick='javascript:editthis(" . $aRow[$sIndexColumn] . ");' style='cursor:pointer;'>View </i>";
    }  elseif (($_REQUEST['types'] != 'newslettertable') && ($_REQUEST['types'] != 'ordertable' && ($_REQUEST['types'] != 'customertable') && ($_REQUEST['types'] != 'contactustable'))) {
        $row[] = "<i class='fa fa-edit' onclick='javascript:editthis(" . $aRow[$sIndexColumn] . ");' style='cursor:pointer;'>View</i>";
    }
	
    if ($_REQUEST['types'] == 'ordertable') {
        $row[] = "<i class='fa fa-eye' onclick='javascript:editthis(" . $aRow[$sIndexColumn] . ");' style='cursor:pointer;'>view</i> ";
    } else if ($_REQUEST['types'] == 'customertable') {
        $row[] = "<i class='fa fa-eye' onclick='javascript:editthis(" . $aRow[$sIndexColumn] . ");' style='cursor:pointer;'>view</i>";
    } 
	else if ($_REQUEST['types'] == 'contactustable') {
        $row[] = "<i class='fa fa-eye' onclick='javascript:editthis(" . $aRow[$sIndexColumn] . ");' style='cursor:pointer;'></i> View / Edit";
    }
    $row[] = '<input type="checkbox"  name="chk[]" id="chk[]" value="' . $aRow[$sIndexColumn] . '" />';
    $output['aaData'][] = $row;
    $ij++;
}
//print_r($output);
echo json_encode($output);
?>