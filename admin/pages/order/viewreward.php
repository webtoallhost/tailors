<?php
$menu = "19,19,24";
if ($_REQUEST['oid'] != '') {
    $thispageeditid = 24;
} else {
    $thispageid =24;
}

include ('../../config/config.inc.php');
$dynamic = '1';
$datepicker = '1';
include ('../../require/header.php');


$norder = FETCH_all("SELECT * FROM `reward_history` WHERE `oid`=?", $_REQUEST['oid']);

if ($norderfetch['order_status'] == '0') {
    $odstatus = 'Unpaid/Incomplete Order';
} elseif ($norderfetch['order_status'] == '1') {
    $odstatus = 'Processing';
} elseif ($norderfetch['order_status'] == '2') {
    $odstatus = 'Success';
} elseif ($norderfetch['order_status'] == '3') {
    $odstatus = 'Failure';
} elseif ($norderfetch['order_status'] == '4') {
    $odstatus = 'Cancelled';
}

if ($norder['order_status'] == '0') {
    $ostatus = 'Awaiting for Payment';
} elseif ($norder['order_status'] == '1') {
    $ostatus = 'Awaiting Fulfillment';
} elseif ($norder['order_status'] == '2') {
    $ostatus = 'Completed';
} elseif ($norder['order_status'] == '3') {
    $ostatus = 'Cancelled';
} elseif ($norder['order_status'] == '4') {
    $ostatus = 'Declined';
} elseif ($norder['order_status'] == '5') {
    $ostatus = 'Refunded';
} elseif ($norder['order_status'] == '6') {
    $ostatus = 'Partially Refunded';
}
if ($norder['promotional_type'] == '1') {
    $ptype = $norder['promotional_code'] . ' (' . $norder['promotional_discount'] . ' %)';
} elseif ($norder['promotional_type'] == '2') {
    $ptype = $norder['promotional_code'] . ' (' . $norder['promotional_discount'] . ' ' . $norder['currency'] . ')';
} else {
    $ptype = '-';
}

    if ($norder['guest'] == '1') {
        $name = getguest1('email', $norder['CusID']) . ' ( Guest )';
        $cemail = getguest1('email', $norder['CusID']);
    } else {
        $name = getcustomer1('FirstName', $norder['CusID']);
        $cemail = getcustomer1('E-mail', $norder['CusID']);
    }
  
if (isset($_REQUEST['update'])) {
    @extract($_REQUEST);
    
    $upd=$db->prepare("UPDATE `norder` SET `shipping_address`=?,`order_status`=?,`order_comments`=?,`card_message`=?,`anonymously`=? WHERE `oid`=?");
    $upd->execute(array($shipping_address,$od_status,$order_comments,$card_message,$anonymous,$_REQUEST['oid']));
    
    if ($od_status == '0') {
        $ostatus = 'Awaiting for Payment';
    } elseif ($od_status == '1') {
        $ostatus = 'Awaiting Fulfillment';
    } elseif ($od_status == '2') {
        $ostatus = 'Completed';
    } elseif ($od_status == '3') {
        $ostatus = 'Cancelled';
    } elseif ($od_status == '4') {
        $ostatus = 'Declined';
    } elseif ($od_status == '5') {
        $ostatus = 'Refunded';
    } elseif ($od_status == '6') {
        $ostatus = 'Partially Refunded';
    }

    if ($norder['guest'] == '1') {
        $name = getguest1('email', $norder['CusID']) . ' ( Guest )';
        $cemail = getguest1('email', $norder['CusID']);
    } else {
        $name = getcustomer1('FirstName', $norder['CusID']);
        $cemail = getcustomer1('E-mail', $norder['CusID']);
    }
  
    
    $to = $cemail;
    
    $subject='Your Click2buy Order Has Been Updated '.$norder['order_id'];

    $mailcontent = '<table style="width:600px; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Order Status Changed</h2></td>
                </tr>
                <tr>
                    <td><p><b>Hi ' . $name . '</b>,</p><p>An order you recently placed on our website has had its status changed.</p><p>The status of order '.$norder['order_id'].' is now ' . $ostatus . '</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%;" align="left">
                                    <h4>Order Number : ' . $norder['order_id'] . '</h4>
                                    <p>Order Date : ' . date('d-M-Y H:i:s A', strtotime($norder['datetime'])) . '</p>
                                    <p>Payment Method : ' . $norder['payment_mode'] . '</p>
                                  
                                </td>
                                <td width="40%;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%;">
                                    <a href="http://www.click2buy.in/" target="_blank">Click2buy</a>
                                </td>
                                <td width="50%;" align="right"></td>
                        </table>
                    </td>
                </tr>
            </table>';

    if ($_REQUEST['sendmail'] == '1')
    {
         //Customer Email
		 
		 
//sendgridmail($to, $mailcontent, $subject, $cemail, '', '');

// sendoldmail($subject, $mailcontent, $cemail, $to);
    }
    $msg='<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-check"></i> Order Updated Successfully!!! </div>';
}

?>
<style>
    td {
        border: none;
    }
</style>
<?php

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           View Reward
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo $sitename; ?>order/orders.htm"><i class="fa fa-shopping-cart"></i> order</a></li>
            <li><a href="#">view Order</a></li>

        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <form method="post" autocomplete="off" enctype="multipart/form-data" action="">
            <div class="box box-info">
                <div class="box-header with-border">

                    <span style="float:right; font-size:13px; color: #333333; text-align: right;"><span style="color:#FF0000;">*</span> Marked Fields are Mandatory</span>
                </div>
                <div class="box-body">
                    <?php echo $msg; ?>
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            Customer Details
                        </div>
                        <div class="panel-body">
                           
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                       
                                         <tr>
                                                <td>
                                                    Order ID : 
                                                </td>
                                                <td>
                                                    <?php echo $norder['order_id']; ?>
                                                </td>
                                            </tr>
                                            </tr>
                                           
                                                 <tr>
                                                    <td>Username</td>
                                                    <td><?php echo $name; ?></td>
                                                    
                                                </tr>
                                                  <tr>
                                                    <td>Emailid</td>
                                                    <td><?php echo $cemail; ?></td>
                                                    
                                                </tr>
                                            
                                              <tr>
                                                <td>Pending Point Amount</td>
                                                <td>&#8377;&nbsp;<?php echo $norder['earn_reward_point']; ?>
                                                </td>
                                            </tr>
                                             
                                             
                                           
                                             
                                        </tbody>
                                    </table>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-6">
                        <a href="<?php echo $sitename; ?>order/reward-points.htm">Back to Listings page</a>
                    </div>
                    
                </div>
            </div>
            </div>
            </div>
        </form>
        <!-- /.box -->
    </section><!-- /.content 
</div><!-- /.content-wrapper -->
    <?php include ('../../require/footer.php'); ?>
    <script type="text/javascript">

        $('#cid').on('change', function () {
            var ts = $('#cid').val();
            //  alert(ts);
            $.ajax({
                url: "<?php echo $sitename; ?>pages/master/getsubcate.php",
                async: false,
                data: {pid: ts},
                success: function (data) {
                    $('#getsub').html(data);
                }
            });
        });
        function delrec(a, b) {
            if (confirm("Are you sure you want to remove this timing?")) {
                a.parent().parent().remove();
                var rtn = '';
                $.ajax({
                    url: "<?php echo $sitename; ?>pages/master/delthistime.php",
                    async: false,
                    data: {id: b},
                    success: function (data) {
                        rtn = '1';
                    }
                });
                if (rtn == '1') {

                }
            }
        }

        function imgchktore(a) {

            if (a != '')
            {
                $("#imagenameid").prop('required', true);
                $("#imagealtid").prop('required', true);
                $("#addstar").html('*');
                $("#addstar1").html('*');
            }

        }

    </script>
    <script>
        function showUser(str) {

            $("#one").hide();
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $sitename; ?>pages/master/add_branch_statebg.php?q=" + str, true);
            xmlhttp.send();
        }
        function showUser1(str) {

            // $("#two").hide();
            if (str == "") {
                document.getElementById("txtHint1").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint1").innerHTML = this.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $sitename; ?>pages/master/add_branch_citybg.php?z=" + str, true);
            xmlhttp.send();
        }
        function showUser2(str) {

            //$("#two").hide();
            if (str == "") {
                document.getElementById("city").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("city").innerHTML = this.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $sitename; ?>pages/master/add_branch_citybg.php?z=" + str, true);
            xmlhttp.send();
        }
    </script>
