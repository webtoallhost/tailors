<?php
$menu = "19,9,21";
$thispageid = 21;
include ('../../config/config.inc.php');
$dynamic = '1';
$datatable = '1';
include ('../../require/header.php');
?>
<style type="text/css">
    .row { margin:0;}
    #example1 tbody tr td:nth-child(1),tbody tr td:nth-child(2), tbody tr td:nth-child(6),tbody tr td:nth-child(7) {
        text-align:center;
    }
    #example1 tbody tr td:nth-child(5) {
        text-align:right;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Orders
            <small>List of Order(s)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i>order</a></li>
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i>Orders</a></li>            
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div id="mmssg"><?php echo $msg; ?></div>
                <form name="form1" method="post" action="">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr align="center">
                                    <th style="width:5%;">O.id</th>
                                    <th style="width:15%">Order Date</th>
                                    <th style="width:10%">Order ID</th>
                                    <th style="width:10%">Vendor</th>
                                    <th style="width:10%">Customer</th>
                                    <th style="width:10%">Amount</th>
                                    <?php if($_SESSION['type']=='admin') { ?>
                                    <th style="width:15%">Assign Logistics</th>
                                    <?php } ?>
                                    <th style="width:15%">Deliver Status</th>
                                   
                                    <th style="width:15%">Order Status</th>
                                    <th style="width:10%">View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $o = '1';
                                if($_SESSION['type']=='admin') { 
                                $ord = pFETCH("SELECT * FROM `norder` WHERE `oid`!=? ORDER BY `oid` DESC", '');
                                }
                                elseif($_SESSION['type']=='logistic') { 
                                $ord = pFETCH("SELECT * FROM `norder` WHERE `oid`!=? AND `logistic`=? ORDER BY `oid` DESC", '',$_SESSION['usergroup']);
                                }
                                else
                                {
                                $ord = pFETCH("SELECT * FROM `norder` WHERE `oid`!=? AND `vendor`=? ORDER BY `oid` DESC", '',$_SESSION['UID']);    
                                }
                                while ($ford = $ord->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $o; ?></td>
                                        <td><?php echo date('d-M-Y h:i:s A', strtotime($ford['datetime'])); ?></td>
                                        <td><?php echo $ford['order_id']; ?></td>
                                        <td><?php echo getvendor('name', getusers('usergroup',$ford['vendor']));
                                            ?></td>
                                        <td><?php echo getcustomer1('fname', $ford['CusID']);
                                            ?></td>
                                        <td><?php echo '&#8377;&nbsp;' . number_format($ford['over_all_total'], '2', '.', ''); ?></td> 
                                         <?php if($_SESSION['type']=='admin') { ?>
                                        <td>
                                               <select name="logistics" id="logistics" class="form-control"  title="<?php echo $ford['oid']; ?>">
                                                <option value="">Select Logictics</option>
                                               <?php
                                                $getmanuf = DB("SELECT * FROM `logistic` WHERE `status`='1' ORDER BY `name` ASC");
                                                while ($fdepart = mysqli_fetch_array($getmanuf)) {
                                                    ?>
                                                    <option value="<?php echo $fdepart['uid']; ?>"
                                                    <?php
                                                    if ($fdepart['uid'] == $ford['logistic']) {
                                                        echo 'selected="selected"';
                                                    }
                                                    ?>><?php echo $fdepart['name']; ?></option>
                                                        <?php } ?>
                                            </select>
                                           
                                        </td>
                                         <?php } ?>
                                         <td>
                                            <?php if($_SESSION['type']=='logistic' || $_SESSION['type']=='vendor') { ?>
                                                <select name="del_status" id="del_status" class="form-control del_statuss" title="<?php echo $ford['oid']; ?>">
                                                <option value="">Select Status</option>
                                                <option value="Picked Up" <?php if($ford['delivered_status']=='Picked Up') { ?> selected="selected" <?php } ?>>Picked Up</option>
                                                 <option value="Delivered" <?php if($ford['delivered_status']=='Delivered') { ?> selected="selected" <?php } ?>>Delivered</option>
                                                  <option value="Dispatched" <?php if($ford['delivered_status']=='Dispatched') { ?> selected="selected" <?php } ?>>Dispatched</option>
                                                </select>
                                            <?php } else { echo $ford['delivered_status']; } ?>
                                         </td>
                                        <td>
                                             <?php if($_SESSION['type']=='admin') { ?>
<input type="hidden" name="oid" id="oid" class="od_statusss" value="<?php echo $ford['oid']; ?>" />
                                            <select name="od_status" id="od_status" class="form-control od_statuss" title="<?php echo $ford['oid']; ?>">
                                                <option value="">Select Status</option>
                                                <option value="0" <?php
                                                if ($ford['order_status'] == '0') {
                                                    echo 'selected';
                                                }
                                                ?>>Order Placed</option>
                                                <option value="1" <?php
                                                if ($ford['order_status'] == '1') {
                                                    echo 'selected';
                                                }
                                                ?>>Delivered</option>
                                               
                                                <option value="2" <?php
                                                if ($ford['order_status'] == '2') {
                                                    echo 'selected';
                                                }
                                                ?>>Return Order</option>
                                                <option value="3" <?php
                                                if ($ford['order_status'] == '3') {
                                                    echo 'selected';
                                                }
                                                ?>>Cancelled</option>
                                            </select>
                                            <?php } else { 
                                             if ($ford['order_status'] == '0') { echo "Order Placed"; }
                                             else if ($ford['order_status'] == '1') { echo "Delivered";}
                                             else if ($ford['order_status'] == '2') { echo "Return Order";}
                                             else if ($ford['order_status'] == '3') { echo "Cancelled";}
                                             } ?>
                                        </td>
                                        <td><i class="fa fa-eye" style="cursor:pointer;" onclick="editthis(<?php echo $ford['oid']; ?>);"> View</i></td>
                                    </tr>
                                    <?php
                                    $o++;
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="7">&nbsp;</th>
                                   <?php /* ?><td align="center"><div class="input-group">
                                            <span class="input-group-addon">
                                                <input type="checkbox" id="sendmail" name="sendmail" value="1" style="width:20px; float: left; height: 20px;" />
                                            </span>
                                            <label for="sendmail" class="form-control bg-gray color-palette">Send Mail</label>
                                        </div></td>
                                        <td>&nbsp;</td><?php */?>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$("#logistics").change(function(){
var e = 1;
var status = $(this).val();
var oid = $(this).attr('title'); 	
if(confirm("Please confirm you want to assign this logistics")){
var params = "logistics="+status+"&orderid="+oid+"&sendmail="+e;
//alert(params);
$.ajax({
type: "POST",
url: '<?php echo $fsitename; ?>admin/config/functions_ajax.php',
data: params,
success: function(data){
//alert('success');
//window.location.assign();  
}
});// you have missed this bracket
}
});
$(".del_statuss").change(function(){
var e = 1;
var status = $(this).val();
var oid = $(this).attr('title'); 	
if(confirm("Please confirm you want to change the Deliver Status")){
var params = "deliverstatus="+status+"&orderid="+oid+"&sendmail="+e;
//alert(params);
$.ajax({
type: "POST",
url: '<?php echo $fsitename; ?>admin/config/functions_ajax.php',
data: params,
success: function(data){
//alert('success');
//window.location.assign();  
}
});// you have missed this bracket
}
});

$("#od_status").change(function(){
var e = 1;
var status = $(this).val();
var oid = $(this).attr('title'); 	
if(confirm("Please confirm you want to change the Order Status")){
var params = "orderstatus="+status+"&orderid="+oid+"&sendmail="+e;
//alert(params);
$.ajax({
type: "POST",
url: '<?php echo $fsitename; ?>admin/config/functions_ajax.php',
data: params,
success: function(data){
//alert('success');
//window.location.assign();  
}
});// you have missed this bracket
}
});

    function editthis(a)
    {
        var did = a;
        window.location.href = '<?php echo $sitename; ?>order/' + a + '/vieworder.htm';
    }

</script>
<?php
include ('../../require/footer.php');
?>  