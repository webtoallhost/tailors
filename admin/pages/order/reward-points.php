<?php
$menu = "19,19,24";
$thispageid = 24;
include ('../../config/config.inc.php');
$dynamic = '1';
$datatable = '1';
include ('../../require/header.php');
?>
<style type="text/css">
    .row { margin:0;}
    #example1 tbody tr td:nth-child(1),tbody tr td:nth-child(2), tbody tr td:nth-child(6),tbody tr td:nth-child(7) {
        text-align:center;
    }
    #example1 tbody tr td:nth-child(5) {
        text-align:right;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Reward Points
            <small>List of Reward Point(s)</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $sitename; ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="#"><i class="fa fa-asterisk"></i>Reward Points</a></li>
            <li class="active"><a href="#"><i class="fa fa-circle-o"></i>Reward Points</a></li>            
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body">
                <div id="mmssg"><?php echo $msg; ?></div>
                <form name="form1" method="post" action="">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr align="center">
                                    <th style="width:5%;">S.No</th>
                                    <th style="width:10%">Order ID</th>
                                    <th style="width:30%">Customer</th>                                   
                                    <th style="width:10%">View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $o = '1';
                                $ord = pFETCH("SELECT * FROM `reward_history` WHERE `oid`!=? ORDER BY `oid` DESC", '');
                                while ($ford = $ord->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $o; ?></td>
                                        <td style="text-align:left;"><?php echo $ford['order_id']; ?></td>
                                        <td><?php
                                            if ($ford['guest'] == '1') {
                                                echo getguest1('email', $ford['CusID']) . ' ( Guest )';
                                            } else {
                                                echo getcustomer1('E-mail', $ford['CusID']);
                                            }
                                            ?></td>
                                       
                                        <td  style="text-align:left;"><i class="fa fa-eye" style="cursor:pointer;" onclick="editthis(<?php echo $ford['oid']; ?>);"> View</i></td>
                                    </tr>
                                    <?php
                                    $o++;
                                }
                                ?>
                            </tbody>
                           
                        </table>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
    function editthis(a)
    {
        var did = a;
        window.location.href = '<?php echo $sitename; ?>order/' + a + '/viewreward.htm';
    }
    function changestatus(a, b)
    {
        var e = $('input[name="sendmail"]:checked').val();
        var status = a;
        var oid = b;
        if (confirm("Please confirm you want to change the Order Status)"))
        {
            changestatusmessage(status, oid, e);
            location.reload();
            return true;
        } else
        {
            location.reload();
            return false;
        }
    }

    function changestatusmessage(a, b, c)
    {
        var a = a;
        var b = b;
        var c = c;
        if (window.XMLHttpRequest)
        {
            oRequestsubcat = new XMLHttpRequest();
        } else if (window.ActiveXObject)
        {
            oRequestsubcat = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if ((a != '') && (b != ''))
        {
            document.getElementById("mmssg").innerHTML = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
            oRequestsubcat.open("POST", "<?php echo $fsitename; ?>get/results.htm", true);
            oRequestsubcat.onreadystatechange = getcstatus;
            oRequestsubcat.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            oRequestsubcat.send("orderstatus=" + a + "&orderid=" + b +"&sendmail="+c);
            console.log(a, b);
        }
        return false;
    }

    function getcstatus()
    {
        if (oRequestsubcat.readyState == 4)
        {
            if (oRequestsubcat.status == 200)
            {
                document.getElementById("mmssg").innerHTML = oRequestsubcat.responseText;
            } else
            {
                document.getElementById("mmssg").innerHTML = oRequestsubcat.responseText;
            }
        }
    }
</script>
<?php
include ('../../require/footer.php');
?>  