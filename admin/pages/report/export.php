<?php
//load the database configuration file
include ('../../config/config.inc.php');

if(isset($_POST['importSubmit'])){
    
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            while(($data = fgetcsv($csvFile)) !== FALSE){
                
                    //insert member data into database
                    $db->query("INSERT INTO product1 (`pid`, `cid`, `sid`, `alid`, `brand`, `innerid`, `item_code`, `productname`, `assitor`, `tamilproductname`, `attribute_group`, `link`, `videolink`, `sku`, `availability`, `total_Availability`, `price`, `sprice`, `metatitle`, `metakeyword`, `tags`, `stock`, `sortdescription`, `furtureyn`, `showinhp`, `deal`, `new`, `recommend`, `offers`, `image`, `description`, `state`, `cod_charge`, `metadescription`, `paymentinfo`, `status`, `order`, `date`, `weight`, `visit_count`, `isbn`) VALUES ('" . $data[0] . "','" . $data[1] . "','" . $data[2] . "','" . $data[3] . "','" . $data[4]. "','" . $data[5] . "','" . $data[6] . "','" . $data[7] . "','" . $data[8] . "','" . $data[9] . "','" . $data[10] . "','" . $data[11] . "','" . $data[12] . "','" . $data[13] . "','" . $data[14] . "','" . $data[15]. "','" . $data[16] . "','" . $data[17] . "','" . $data[18] . "','" . $data[19] . "','" . $data[20] . "','" . $data[21] . "','" . $data[22] . "','" . $data[23] . "','" . $data[24] . "','" . $data[25] . "','" . $data[26]. "','" . $data[27] . "','" . $data[28] . "','" . $data[29] . "','" . $data[30] . "','" . $data[31] . "','" . $data[32] . "','" . $data[33] . "','" . $data[34] . "','" . $data[35] . "','" . $data[36] . "','" . $data[37]. "','" . $data[38] . "','" . $data[39] . "','" . $data[40] . "')");
               
            }
            
            //close opened csv file
            fclose($csvFile);

            $qstring = '?status=succ';
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
}

//redirect to the listing page
header("Location: index.php".$qstring);