<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview <?php echo $tree1; ?>">
                <a href="<?php echo $sitename; ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php if($_SESSION['UID']=='1') { ?>
            <li class="treeview <?php echo $tree11; ?>" id="ul_11">
                <a href="#">
                    <i class="fa fa-asterisk"></i>
                    <span>User(s)</span>
                    <span class="label label-danger pull-right" id="span_11">6</span>
                </a>
                <ul class="treeview-menu  <?php echo $menutree11; ?>">
                    <li <?php echo $smenuitem11; ?> id="li_11"><a href="<?php echo $sitename; ?>user/tailor.htm"><i class="fa fa-circle-o"></i>Tailor Mgmt </a></li>
                    <li <?php echo $smenuitem12; ?> id="li_12"><a href="<?php echo $sitename; ?>user/professional.htm"><i class="fa fa-circle-o"></i>Professional Mgmt </a></li>
                    <li <?php echo $smenuitem13; ?> id="li_13"><a href="<?php echo $sitename; ?>user/fabric.htm"><i class="fa fa-circle-o"></i>Fabric Vendor Mgmt </a></li>
                    <li <?php echo $smenuitem14; ?> id="li_14"><a href="<?php echo $sitename; ?>user/logistic.htm"><i class="fa fa-circle-o"></i>Logistic Mgmt </a></li>
                    <li <?php echo $smenuitem15; ?> id="li_15"><a href="<?php echo $sitename; ?>user/vendor.htm"><i class="fa fa-circle-o"></i>Vendor Mgmt </a></li>
                </ul>
            </li>
            <?php } else { ?>
            <li class="treeview <?php echo $tree12; ?>" id="ul_12">
                <a href="#">
                    <i class="fa fa-asterisk"></i>
                    <span>Master(s)</span>
                    <span class="label label-danger pull-right" id="span_12">6</span>
                </a>
                <ul class="treeview-menu  <?php echo $menutree12; ?>">
                    <li <?php echo $smenuitem13; ?> id="li_13"><a href="<?php echo $sitename; ?>master/banner.htm"><i class="fa fa-circle-o"></i>Banner Mgmt </a></li>
                    
                    <!--<li <?php echo $smenuitem15; ?> id="li_15"><a href="<?php echo $sitename; ?>master/payment.htm"><i class="fa fa-circle-o"></i>Payment Mgmt </a></li>-->
                    <!--<li <?php echo $smenuitem16; ?> id="li_16"><a href="<?php echo $sitename; ?>master/footermgt.htm"><i class="fa fa-circle-o"></i>Footer Mgmt </a></li>-->
                    <!--<li <?php echo $smenuitem17; ?> id="li_17"><a href="<?php echo $sitename; ?>master/blogcategory.htm"><i class="fa fa-circle-o"></i>Blog Category Mgmt</a></li>-->
                    <!--<li <?php echo $smenuitem18; ?> id="li_18"><a href="<?php echo $sitename; ?>master/blog.htm"><i class="fa fa-circle-o"></i>Blog Mgmt</a></li>-->
                </ul>
            </li>
            <li class="treeview <?php echo $tree19; ?>" id="ul_19">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Order</span>
                    <span class="label label-warning pull-right" id="span_19">4</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree19; ?>">
                  <?php /*  ?><li <?php echo $smenuitem20; ?> id="li_20"><a href="<?php echo $sitename; ?>order/orderstatus.htm"><i class="fa fa-circle-o"></i>Order Status</a></li><?php */?>
                    <li <?php echo $smenuitem21; ?> id="li_21"><a href="<?php echo $sitename; ?>order/orders.htm"><i class="fa fa-circle-o"></i>Order(s)</a></li>
                    <li <?php echo $smenuitem22; ?> id="li_22"><a href="<?php echo $sitename; ?>order/customers.htm"><i class="fa fa-circle-o"></i>Customers</a></li>
                    <li <?php echo $smenuitem23; ?> id="li_23"><a href="<?php echo $sitename; ?>order/promo-code.htm"><i class="fa fa-circle-o"></i>Coupon Codes</a></li>
                    <li <?php echo $smenuitem24; ?> id="li_24"><a href="<?php echo $sitename; ?>order/reward-points.htm"><i class="fa fa-circle-o"></i>Reward Points</a></li>
                </ul>
            </li>
            
            <li class="treeview <?php echo $tree25; ?>" id="ul_25">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Products</span>
                    <span class="label label-info pull-right" id="span_25">9</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree25; ?>">
                    <!--<li <?php echo $smenuitem26; ?> id="li_26"><a href="<?php echo $sitename; ?>products/homecategory.htm"><i class="fa fa-circle-o"></i>Home Category Blocks</a></li>-->
                    <li <?php echo $smenuitem14; ?> id="li_14"><a href="<?php echo $sitename; ?>products/brand.htm"><i class="fa fa-circle-o"></i>Brand Mgmt</a></li>
                    <li <?php echo $smenuitem27; ?> id="li_27"><a href="<?php echo $sitename; ?>products/category.htm"><i class="fa fa-circle-o"></i>Category</a></li>
                    <li <?php echo $smenuitem52; ?> id="li_52"><a href="<?php echo $sitename; ?>products/categorybanner.htm"><i class="fa fa-circle-o"></i>Category Banners</a></li>
                    <!--<li <?php echo $smenuitem53; ?> id="li_53"><a href="<?php echo $sitename; ?>products/categoryadv.htm"><i class="fa fa-circle-o"></i>Category Advertisement</a></li>-->
                    <li <?php echo $smenuitem28; ?> id="li_28"><a href="<?php echo $sitename; ?>products/subcategory.htm"><i class="fa fa-circle-o"></i>Sub-category</a></li>
                    <li <?php echo $smenuitem29; ?> id="li_29"><a href="<?php echo $sitename; ?>products/innercategory.htm"><i class="fa fa-circle-o"></i>Inner Category</a></li>
                    <li <?php echo $smenuitem30; ?> id="li_30"><a href="<?php echo $sitename; ?>products/attribute.htm"><i class="fa fa-circle-o"></i>Attributes</a></li>
                    <li <?php echo $smenuitem31; ?> id="li_31"><a href="<?php echo $sitename; ?>products/attributegroup.htm"><i class="fa fa-circle-o"></i>Attribute Groups</a></li>
                    <li <?php echo $smenuitem34; ?> id="li_34"><a href="<?php echo $sitename; ?>products/attributevalues.htm"><i class="fa fa-circle-o"></i>Attribute Values</a></li>
                    <li <?php echo $smenuitem35; ?> id="li_35"><a href="<?php echo $sitename; ?>products/metal.htm"><i class="fa fa-circle-o"></i>Metal Mgmt</a></li>
                    <li <?php echo $smenuitem32; ?> id="li_32"><a href="<?php echo $sitename; ?>products/product.htm"><i class="fa fa-circle-o"></i>Product Mgmt</a></li>
                    <!--<li <?php echo $smenuitem33; ?> id="li_33"><a href="<?php echo $sitename; ?>products/productreview.htm"><i class="fa fa-circle-o"></i>Product Review Mgmt</a></li>-->
                </ul>
            </li>
            
            <!--<li class="treeview <?php echo $tree34; ?>" id="ul_34">-->
            <!--    <a href="#">-->
            <!--        <i class="fa fa-address-card"></i>-->
            <!--        <span>Assitors</span>-->
            <!--        <span class="label label-info pull-right" id="span_34">2</span>-->
            <!--    </a>-->
            <!--    <ul class="treeview-menu <?php echo $menutree34; ?>">-->
            <!--        <li <?php echo $smenuitem35; ?> id="li_35"><a href="<?php echo $sitename; ?>products/assitors.htm"><i class="fa fa-circle-o"></i>Assitors Mgmt</a></li>-->
            <!--      <li <?php echo $smenuitem36; ?> id="li_36"><a href="<?php echo $sitename; ?>products/assitorsfaq.htm"><i class="fa fa-circle-o"></i>Assitors FAQ Mgmt</a></li>-->
            <!--   	</ul>-->
            <!--</li>-->
            
            <li class="treeview <?php echo $tree37; ?>" id="ul_37">
                <a href="#">
                    <i class="fa fa-id-card-o"></i>
                    <span>Newsletter</span>
                    <span class="label label-primary pull-right" id="span_37">1</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree37; ?>">
                    <li <?php echo $smenuitem38; ?> id="li_38"><a href="<?php echo $sitename; ?>newsletter/newsletters.htm"><i class="fa fa-circle-o"></i>Subscribers</a></li>
                    <!--<li <?php echo $smenuitem21; ?> id="li_21"><a href="<?php echo $sitename; ?>newsletter/templates.htm"><i class="fa fa-circle-o"></i>Templates</a></li> -->
                </ul>
            </li>
            
            <li class="treeview <?php echo $tree39; ?>" id="ul_39">
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>Shipping</span>
                    <span class="label label-primary pull-right" id="span_39">2</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree39; ?>">
                    <li <?php echo $smenuitem40; ?> id="li_40"><a href="<?php echo $sitename; ?>shipping/state.htm"><i class="fa fa-circle-o"></i>Shipping States</a></li>
                    <li <?php echo $smenuitem41; ?> id="li_41"><a href="<?php echo $sitename; ?>shipping/addshipping_price.htm"><i class="fa fa-circle-o"></i>Shipping Price</a></li>
                </ul>
            </li>

            <li class="treeview <?php echo $tree42; ?>" id="ul_42">
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>Reports</span>
                    <span class="label label-primary pull-right" id="span_42">1</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree42; ?>">
                   <li <?php echo $smenuitem43; ?> id="li_43"><a href="<?php echo $sitename; ?>report/product_report.htm"><i class="fa fa-circle-o"></i>Product Report</a></li>
                </ul>
            </li>
            
            <li class="treeview <?php echo $tree44; ?>" id="ul_44">
                <a href="#">
                    <i class="fa fa-id-card-o"></i>
                    <span>CMS</span>
                    <span class="label label-primary pull-right" id="span_44">1</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree44; ?>">
                    <li <?php echo $smenuitem45; ?> id="li_45"><a href="<?php echo $sitename; ?>CMS/staticpages.htm"><i class="fa fa-circle-o"></i>Static Pages</a></li>

                </ul>
            </li>
            
            <li class="treeview <?php echo $tree46; ?>" id="ul_46">
                <a href="#">
                    <i class="fa fa-file-text-o"></i>
                    <span>Forms</span>
                    <span class="label label-warning pull-right" id="span_46">2</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree46; ?>">
                    <li <?php echo $smenuitem47; ?> id="li_47"><a href="<?php echo $sitename; ?>forms/contactlist.htm"><i class="fa fa-circle-o"></i>Contact Forms</a></li>
                    <li <?php echo $smenuitem48; ?> id="li_48"><a href="<?php echo $sitename; ?>forms/feedlist.htm"><i class="fa fa-circle-o"></i>Feedback Forms</a></li>
                </ul>
            </li>
            
            <li class="treeview <?php echo $tree49; ?>" id="ul_49">
                <a href="#">
                    <i class="fa fa-plus-square-o"></i>
                    <span>Others</span>
                    <span class="label label-success pull-right" id="span_49">1</span>
                </a>
                <ul class="treeview-menu <?php echo $menutree49; ?>">
                    <li <?php echo $smenuitem50; ?> id="li_50"><a href="<?php echo $sitename; ?>others/addimage.htm"><i class="fa fa-circle-o"></i>Image Upload</a></li>
                </ul>
            </li>

<!--<li class="treeview <?php echo $tree50; ?>" id="ul_50">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-comment-o" aria-hidden="true"></i>-->
<!--                    <span>SMS Templates</span>-->
<!--                    <span class="label label-primary pull-right" style="background-color: red !important ;">2</span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu <?php echo $menutree50; ?>">-->
<!--                    <li <?php echo $smenuitem50; ?> id="li_50"><a href="<?php echo $sitename; ?>pages/template/addtemplate.htm"><i class="fa fa-circle-o"></i>Add SMS Templates</a></li>-->
<!--                    <li <?php echo $smenuitem51; ?> id="li_51"><a href="<?php echo $sitename; ?>pages/template/managetemplates.htm"><i class="fa fa-circle-o"></i>Manage SMS Templates</a></li>-->
<!--                </ul>-->
<!--            </li>-->
            <?php } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>