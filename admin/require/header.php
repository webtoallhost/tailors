<?php

$time = $_SERVER['REQUEST_TIME'];

/**
* for a 30 minute timeout, specified in seconds
*/
$timeout_duration = 1800;

/**
* Here we look for the user's LAST_ACTIVITY timestamp. If
* it's set and indicates our $timeout_duration has passed,
* blow away any previous $_SESSION data and start a new one.
*/
if (isset($_SESSION['LAST_ACTIVITY']) && 
   ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_unset();
    session_destroy();
    session_start();
}

/**
* Finally, update LAST_ACTIVITY so that our timeout
* is based on it and not the user's login time.
*/
$_SESSION['LAST_ACTIVITY'] = $time;



if ($dynamic == '') {
    include ('config/config.inc.php');
}
if ($_SESSION['UID'] == '') {
    header("Location:" . $sitename . "pages/");
}
$actual_link = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin Control</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.4 -->

        <?php if($inline=='1'){ ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $sitename; ?>datatable/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $sitename; ?>datatable/css/buttons.dataTables.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $sitename; ?>datatable/css/select.dataTables.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $sitename; ?>datatable/css/editor.dataTables.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $sitename; ?>datatable/resources/syntax/shCore.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $sitename; ?>datatable/resources/demo.css" />
        <?php } ?>

        <link href="<?php echo $sitename; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- FontAwesome 4.3.0 -->

        <link href="<?php echo $fsitename; ?>font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons 2.0.0 -->
        <link href="<?php echo $sitename; ?>ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <!-- Theme style -->
        <link href="<?php echo $sitename; ?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->

        <link href="<?php echo $sitename; ?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo $sitename; ?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="<?php echo $sitename; ?>plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo $sitename; ?>plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo $sitename; ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="<?php echo $sitename; ?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo $sitename; ?>plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="<?php echo $sitename; ?>plugins/timepicker/bootstrap-timepicker.min.css" />
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo $sitename; ?>plugins/select2/select2.min.css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins-->
        <link rel="icon" href="<?php echo $fsitename; ?>images/favicon.png" type="image/png" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo $sitename; ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .skin-blue .main-header .navbar {
    background-color: #110c09;
}
.skin-blue .main-header li.user-header {
    background-color: #06050a;
}
.main-header .logo .logo-lg {
    display: block;
    background: #06050a;
}
.skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side {
    background-color: #06050a;
}
.skin-blue .sidebar-menu>li.header {
    color: #ffffff;
    background: #1a2226;
}
.skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a {
    color: #06050a;
    background: #ecf0f5;
    border-left-color: #27157b;
}
.main-header .sidebar-toggle:before {
    content: "\f000";
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
/*--adjust as necessary--*/
    color: #ecf0f5;
    font-size: 18px;
    padding-right: 0.5em;
    position: absolute;
    top: 10px;
    left: 0;
}
.logo-lg img{
   width: 94px;
    position: absolute;
    left: 63px;
    height: 50px;
}
.skin-blue .main-header .logo:hover {
    background-color: none!important;
    color: none!important;
}
.skin-blue .main-header .logo {
    background-color: #110c09;
    color: #fff;
    border-bottom: 0 solid transparent;
}
            </style>
        <script>
            function startTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();

                m = checkTime(m);
                s = checkTime(s);

                var ampm = h >= 12 ? 'PM' : 'AM';
                h = h % 12;
                h = h ? h : 12; // the hour '0' should be '12'
                //m = m < 10 ? '0'+m : m;
                m = m < 10 ? m : m;

                var dd = today.getDate();
                //var mm = today.getMonth()+1; //January is 0!
                var mm = today.getMonth();
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    //mm='0'+mm
                }



                var monthNames = [
                    "Jan", "Feb", "Mar",
                    "Apr", "May", "June", "Jul",
                    "Aug", "Sep", "Oct",
                    "Nov", "Dec"
                ];

                document.getElementById('txt').innerHTML =
                        h + ":" + m + ":" + s + ' ' + ampm + ' | ' + dd + ' - ' + monthNames[mm] + ' - ' + yyyy;
                var t = setTimeout(startTime, 500);
            }

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i
                }
                ;  // add zero in front of numbers < 10
                return i;
            }
        </script>
    </head>
    <?php
    if ($collapse == '1') {
        $collapse = 'sidebar-collapse';
    } else {
        $collapse = '';
    }
    ?>
    
    <body class="skin-blue sidebar-mini skin-blue sidebar-mini <?php echo $collapse; ?>" onload="startTime();">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo $sitename; ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <!-- <span class="logo-mini"><b>Lorikeet</b></span> -->
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <img src="http://13.234.94.153/images/logo/logo1.png">
                    </span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a id="txt"></a>
                            </li>
                              <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <img src="<?php echo $sitename; ?>images/notification.png" width="20"> 
                        <span class="label label-warning" id="noti">
                             <?php
                                            global $db;
                                            if($_SESSION['type']=='vendor') {
                                             $stmt = $db->prepare("SELECT * FROM notification WHERE `to`='".$_SESSION['usergroup']."' AND `type`='vendor' AND `read_status`='0' ORDER BY `id` DESC" );
                                            }
                                            elseif($_SESSION['type']=='logistic'){
                                              $stmt = $db->prepare("SELECT * FROM notification WHERE `to`='".$_SESSION['usergroup']."' AND `type`='logistics' AND `read_status`='0' ORDER BY `id` DESC" );    
                                            }
                                            else
                                            {
                                            $stmt = $db->prepare("SELECT * FROM notification WHERE `to`='admin' AND `read_status`='0' ORDER BY `id` DESC" );    
                                                                                                                                      
                                            }
                                            $stmt->execute();
                                            $senior_count = $stmt->rowCount();
                                            echo $senior_count;
                                            ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?php echo $senior_count; ?> Notification</li>
                        <li>
                           
                            <ul class="menu">
                               <?php  
                                if($_SESSION['type']=='vendor') {
                                             $stmt = $db->prepare("SELECT * FROM notification WHERE `to`='".$_SESSION['usergroup']."' AND `type`='vendor' AND `read_status`='0' ORDER BY `id` DESC" );
                                            }
                                            elseif($_SESSION['type']=='logistic'){
                                              $stmt = $db->prepare("SELECT * FROM notification WHERE `to`='".$_SESSION['usergroup']."' AND `type`='logistics' AND `read_status`='0' ORDER BY `id` DESC" );    
                                            }
                                            else
                                            {
                                            $stmt = $db->prepare("SELECT * FROM notification WHERE `to`='admin' AND `read_status`='0' ORDER BY `id` DESC" );    
                                                                                                                                      
                                            }
                                            $stmt->execute();
                                        while($listnotif = $stmt->fetch(PDO::FETCH_ASSOC))
                                        {
                                       if($listnotif['message']!='') {
                                            ?>
                                <li>
                                    <a href="<?php echo $sitename; ?>notification/<?php echo $listnotif['id']; ?>/viewnotification.htm">
                                     <?php echo $listnotif['message']; ?>
                                    </a>
                                </li>
                                <?php } } ?>
                            </ul>
                        </li>
                        <li class="footer"><a href="<?php echo $sitename; ?>notification/notification.htm">View all</a></li>
                    </ul>
                </li>
               
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php if (getprofile('image', $_SESSION['UID']) != '') { ?>
                                        <img src="<?php echo $sitename . 'pages/profile/image/' . getprofile('image', $_SESSION['UID']); ?>"  class="user-image" />
                                    <?php } else { ?>
                                        <img src="<?php echo $sitename; ?>images/noimage.jpg" class="user-image"  alt="<?php echo companylogin('fname'); ?>" />
                                    <?php } ?>
                                    <span class="hidden-xs">&nbsp;</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <?php if (getprofile('image', $_SESSION['UID']) != '') { ?>
                                            <img src="<?php echo $sitename . 'pages/profile/image/' . getprofile('image', $_SESSION['UID']); ?>" class="img-circle" />
                                        <?php } else { ?>
                                            <img src="<?php echo $fsitename; ?>admin/images/noimage.jpg" class="img-circle" alt="<?php echo companylogin('fname'); ?>" />
                                        <?php } ?>
                                        <span class="hidden-xs">&nbsp;</span>
                                        <p style="margin-top:24px;">
                                            <?php
                                            $sel1 = pFETCH("SELECT * FROM `admin_history` WHERE `admin_uid`=?", $_SESSION['UIDD']);
                                            $fsel = $sel1->fetch(PDO::FETCH_ASSOC);
                                            $_SESSION['type'] = 'admin';
                                            echo 'Welcome ' . getusers('name', $_SESSION['UID']) . '.' . getprofile('firstname', $_SESSION['UID']);
                                            ?>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-footer">
                                        <?php if ($_SESSION['type'] == 'admin') { 
										if ($_SESSION['UID'] == '1') {
										?>
                                           
										   <div class="col-xs-12 text-center">
                                                <a href="<?php echo $sitename; ?>profile/viewprofile.htm">Edit Profile</a>
                                            </div>
										<?php } ?>
                                            <div class="clearfix"><br /><br /></div>
                                           <!-- <div class="pull-left">
                                                <a href="<?php echo $sitename; ?>profile/changepassword.htm" class="btn btn-default btn-flat">Change Password</a>
                                            </div>-->
                                            <?php
                                        }
                                        if ($_SESSION['type'] == 'employee') {
                                            ?>
                                           <!-- <div class="pull-left">
                                                <a href="<?php echo $sitename; ?>pages/profile/changepassword.htm" class="btn btn-default btn-flat">Change Password</a>
                                            </div> -->
                                        <?php }
                                        ?>
                                        <div class="pull-right">
                                            <a href="<?php echo $sitename; ?>logout.php" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <?php
            include 'sidebar.php';?>
            <script type="text/javascript">
                var Settings = {
                    base_url: '<?php echo $sitename; ?>'
                }
            </script>
