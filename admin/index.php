<?php
$dynamic = '';
$menu = '1,0,0,0';
$index='1';
include ('require/header.php');
//print_r($_SESSION);
?>
<!-- Left side column. contains the logo and sidebar -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php
    if ($_SESSION['type'] == 'admin') {
        ?>
        <section class="content-header">

            <h1>Lorikeet
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Lorikeet</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                
            </div><!-- /.row -->

        </section><!-- /.content -->
    <?php } else { ?>
        <section class="content-header">
            <h1>
                Welcome to Lorikeet
            </h1>
        </section>

<?php } ?>
</div><!-- /.content-wrapper -->
<?php include 'require/footer.php'; ?>      
