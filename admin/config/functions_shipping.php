<?php
function getshipcountry($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `countries` WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addshicountry($country,$status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        // $link22 = DB_QUERY("SELECT * FROM `banner` WHERE `imagename`='$imagename'");
        $link22 = FETCH_all("SELECT * FROM `countries` WHERE `name`=?", $country);
        if ($link22['name'] == '') {

            $resa = $db->prepare("INSERT INTO `countries` (`name`,`status`) VALUES(?,?)");
            $resa->execute(array($country,$status));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping Country', '21', 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>State Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `countries` WHERE `name`=? AND `id`!=?", $country, $getid);
        if ($link22['name'] == '') {
            $resa = $db->prepare("UPDATE `countries` SET `name`=?,`status`=? WHERE `id`=?");
            $resa->execute(array(trim($country), trim($status), $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping Country', '21', 'Update', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>State Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delshipcountry($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $get = $db->prepare("DELETE FROM `countries` WHERE `id` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}
function getshipstate($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `states` WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addshipstate($statename, $country, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        // $link22 = DB_QUERY("SELECT * FROM `banner` WHERE `imagename`='$imagename'");
        $link22 = FETCH_all("SELECT * FROM `states` WHERE `name`=? AND `country_id`", $statename,$country);
        if ($link22['state_name'] == '') {

            $resa = $db->prepare("INSERT INTO `states` ( `name`,`country_id`, `status`) VALUES(?,?,?)");
            $resa->execute(array($statename, $country,$status));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping State', '21', 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>State Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `states` WHERE `name`=? AND `country_id`=? AND `id`!=?", $statename,$country, $getid);
        if ($link22['name'] == '') {
            $resa = $db->prepare("UPDATE `states` SET `name`=?,`country_id`=?,`status`=? WHERE `id`=?");
            $resa->execute(array(trim($statename),trim($country), trim($status), $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping State', '21', 'Update', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>State Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delshipstate($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $get = $db->prepare("DELETE FROM `states` WHERE `id` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function getshipprice($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `shipping_price` WHERE `sid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addshipprice($km1, $firstkg1,$secondkg1,$thirdkg1,$status,$getid,$ip) {
    global $db;
    if ($getid == '') {
		$j='0';
		     $xs = $db->prepare("DELETE FROM `shipping_price`");
                $xs->execute(array());
				
	$firstkg=explode('**',$firstkg1);		
	$secondkg=explode('**',$secondkg1);	
	$thirdkg=explode('**',$thirdkg1);	
	$km=explode('**',$km1);	
	foreach ($km as $ln => $avn) {
		
		
		
		$firstkg11=$firstkg[$j];
		$secondkg11=$secondkg[$j];
		$thirdkg11=$thirdkg[$j];
		$splitkm=explode('-',$avn);
       
		 $kmss1=$splitkm['0'];
		
		 $kmss2=$splitkm['1'];
		
		if($kmss1=='')
		{
			$kmss1='0';
		}
	//	print_r(array($avn,$kmss1,$kmss2,$firstkg11,$secondkg11,$thirdkg11,$status));
		$resa = $db->prepare("INSERT INTO `shipping_price` ( `km`,`km_from`,`km_to`,`0to1kg`,`1to25kg`, `25plus`, `status`) VALUES(?,?,?,?,?,?,?)");
		
       $resa->execute(array($avn,$kmss1,$kmss2,$firstkg11,$secondkg11,$thirdkg11,$status));
	$j++;
	}
	
	
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping Price', '21', 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
       
    } else {
      
	           $xs = $db->prepare("DELETE FROM `shipping_price`");
                $xs->execute(array());
				
          		$j='0';
	$firstkg=explode('**',$firstkg1);		
	$secondkg=explode('**',$secondkg1);	
	$thirdkg=explode('**',$thirdkg1);	
	$km=explode('**',$km1);	
	foreach ($km as $ln => $avn) {
		
		
		$firstkg11=$firstkg1['$j'];
		$secondkg11=$secondkg1['$j'];
		$thirdkg11=$thirdkg1['$j'];
		$splitkm=explode('-',$avn);
		$kmss1=$splitkm['0'];
		$kmss2=$splitkm['1'];

		 $resa = $db->prepare("INSERT INTO `shipping_price` ( `km`,`km_from`,`km_to`,`0to1kg`,`1to25kg`, `25plus`, `status`) VALUES(?,?,?,?,?,?,?)");
		
       $resa->execute(array($avn,$kmss1,$kmss2,$firstkg11,$secondkg11,$thirdkg11,$status));
	$j++;
	}
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping Price', '21', 'Update', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        
    }
    return $res;
}


function getshipcity($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `shipping_city` WHERE `scid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addshipcity($statename, $city, $rate, $order, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        // $link22 = DB_QUERY("SELECT * FROM `banner` WHERE `imagename`='$imagename'");
        $link22 = FETCH_all("SELECT * FROM `shipping_city` WHERE `shipping_city`=?", $city);
        if ($link22['shipping_city'] == '') {

            $resa = $db->prepare("INSERT INTO `shipping_city` ( `ship_state`,`shipping_city`,`shipping_rate`, `order`, `status`, `ip`, `updated_by`) VALUES(?,?,?,?,?,?,?)");
            $resa->execute(array($statename,$city,$rate, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping City', '22', 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>City Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `shipping_city` WHERE `shipping_city`=? AND `scid`!=?", $city, $getid);
        if ($link22['shipping_city'] == '') {
            $resa = $db->prepare("UPDATE `shipping_city` SET `ship_state`=?,`shipping_city`=?,`shipping_rate`=?, `order`=?, `status`=?, `ip`=?, `updated_by`=? WHERE `scid`=?");
            $resa->execute(array(trim($statename), trim($city), trim($rate), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));
            
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping City', '22', 'Update', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>City Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delshipcity($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $get = $db->prepare("DELETE FROM `shipping_city` WHERE `scid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}
?>
