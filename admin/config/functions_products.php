<?php


function getofferbanner($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `offerbanner` WHERE `bid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addofferbanner($title, $link, $description, $image,$status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `offerbanner` WHERE `title`=?", $title);
        if ($link22['title'] == '') {
            $resa = $db->prepare("INSERT INTO `offerbanner` ( `title`, `link`,`content`,`image`,`status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?)");
            $resa->execute(array($title, $link, $description, $image, $status, $ip, $_SESSION['UID']));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Title already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `offerbanner` WHERE `title`=? AND `bid`!=?", $title, $getid);
        if ($link22['title'] == '') {
            $resa = $db->prepare("UPDATE `offerbanner` SET `title`=?, `link`=?,`content`=?, `image`=?,`status`=?, `ip`=?, `Updated_By`=? WHERE `bid`=?");
            $resa->execute(array(trim($title), trim($link), trim($description), trim($image), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Offer Banner Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Title already exists!</h4></div>';
        }
    }
    return $res;
}

function delofferbanner($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $getc = FETCH_all("SELECT `image` FROM `offerbanner` WHERE `bid`=?", $c);
        unlink('../../images/offer/' . $getc['image']);
     
        $get = $db->prepare("DELETE FROM `offerbanner` WHERE `bid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function getquickbanner($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `quickbanner` WHERE `bid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addquickbanner($title, $link, $description, $image, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `quickbanner` WHERE `title`=?", $title);
        if ($link22['title'] == '') {
            $resa = $db->prepare("INSERT INTO `quickbanner` ( `title`, `link`,`content`, `image`, `status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?)");
            $resa->execute(array($title, $link, $description, $image, $status, $ip, $_SESSION['UID']));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Title already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `quickbanner` WHERE `title`=? AND `bid`!=?", $title, $getid);
        if ($link22['title'] == '') {
            $resa = $db->prepare("UPDATE `quickbanner` SET `title`=?, `link`=?,`content`=?, `image`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `bid`=?");
            $resa->execute(array(trim($title), trim($link), trim($description), trim($image),trim($status), trim($ip), $_SESSION['UID'], $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Quick Banner Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Title already exists!</h4></div>';
        }
    }
    return $res;
}

function delquickbanner($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $getc = FETCH_all("SELECT `image` FROM `quickbanner` WHERE `bid`=?", $c);
        unlink('../../images/quickbanner/' . $getc['image']);
        
     
        $get = $db->prepare("DELETE FROM `quickbanner` WHERE `bid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}
function delsize($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
       
       $getorderno = FETCH_all("SELECT * FROM `size` WHERE `id`=?", $c);
       $get1 = $db->prepare("UPDATE `size` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
        
        $get = $db->prepare("DELETE FROM `size` WHERE `id` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}
function getsize($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `size` WHERE `id`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}


function addsize($size, $status, $getid)
{
    global $db;
    if ($getid == '')
    {
        try {
            $link21 = FETCH_all("SELECT * FROM `size` WHERE `size`= ?", $size);
            if ($link21['id'] == '') {
	$getorderno = FETCH_all("SELECT * FROM `size` WHERE `id`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
                $qa = $db->prepare("INSERT INTO `size` (`size`,`order`,`status`) VALUES (?,?,?)");


                $qa->execute(array(trim($size), $order,trim($status)));

                $insert_id = $db->lastInsertId();
                $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
                $htry->execute(array('Size', $thispageid, 'Insert', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $insert_id));

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> Size already exists!</h4></div>';
            }
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
        } finally {
            
        }
    } else {
		  $link2 = FETCH_all("SELECT * FROM `size` WHERE `size`= ?  AND `id`!= ? ",$size, $getid);
        if ($link2['id'] == '') {

            $ws = $db->prepare("UPDATE `size` SET `size`= ?,`status`= ?  WHERE `id`= ? ");
            $ws->execute(array(trim($size),trim($status), $getid));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Color', $thispageid, 'Update', $_SERVER['REMOTE_ADDR'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i>x</button><h4><i class="icon fa fa-close"></i> Size already exists!</h4></div>';
        }
    }
    return $res;
}
function delcolor($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
       $getorderno = FETCH_all("SELECT * FROM `color` WHERE `id`=?", $c);
       $get1 = $db->prepare("UPDATE `color` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
        $get = $db->prepare("DELETE FROM `color` WHERE `id` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}
function getcolor($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `color` WHERE `id`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}


function addcolor($color, $colorcode, $status, $getid)
{
    global $db;
    if ($getid == '')
    {
        try {
            $link21 = FETCH_all("SELECT * FROM `color` WHERE `color`= ?", $color);
            if ($link21['id'] == '') {
	$getorderno = FETCH_all("SELECT * FROM `color` WHERE `id`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
                $qa = $db->prepare("INSERT INTO `color` (`color`,`colorcode`,`order`,`status`) VALUES (?,?,?,?)");


                $qa->execute(array(trim($color), trim($colorcode), $order,trim($status)));

                $insert_id = $db->lastInsertId();
                $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
                $htry->execute(array('Color', $thispageid, 'Insert', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $insert_id));

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> Color already exists!</h4></div>';
            }
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
        } finally {
            
        }
    } else {
		  $link2 = FETCH_all("SELECT * FROM `color` WHERE `color`= ?  AND `id`!= ? ",$color, $getid);
        if ($link2['id'] == '') {

            $ws = $db->prepare("UPDATE `color` SET `color`= ?,`colorcode`=?,`status`= ?  WHERE `id`= ? ");
            $ws->execute(array(trim($color), trim($colorcode),trim($status), $getid));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Color', $thispageid, 'Update', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i>x</button><h4><i class="icon fa fa-close"></i> Color already exists!</h4></div>';
        }
    }
    return $res;
}
function delmtype($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
         $getorderno = FETCH_all("SELECT * FROM `type` WHERE `bcid`=?", $c);
       $get1 = $db->prepare("UPDATE `type` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
        
        $get = $db->prepare("DELETE FROM `type` WHERE `id` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}
function getmtype($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `type` WHERE `id`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}


function addmtype($type, $link, $metatitle, $metakeywords, $metadescription, $status, $ip, $getid)
{
    global $db;
    if ($getid == '')
    {
        try {
            $link21 = FETCH_all("SELECT * FROM `type` WHERE `type`= ?", $type);
            if ($link21['id'] == '') {
				   $getorderno = FETCH_all("SELECT * FROM `type` WHERE `id`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
                $qa = $db->prepare("INSERT INTO `type` (`type`,`link`,`order`,`status`,`metatitle`,`metadescription`,`metakeyword`,`IP`) VALUES (?,?,?,?,?,?,?,?)");


                $qa->execute(array(trim($type), trim($link), trim($order), trim($status), trim($metatitle), trim($metakeywords), trim($metadescription), $ip));

                $insert_id = $db->lastInsertId();
                $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
                $htry->execute(array('Type', $thispageid, 'Insert', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $insert_id));

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> Type already exists!</h4></div>';
            }
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
        } finally {
            
        }
    } else {
		  $link2 = FETCH_all("SELECT * FROM `type` WHERE `type`= ?  AND `id`!= ? ",$type, $getid);
        if ($link2['id'] == '') {

            $ws = $db->prepare("UPDATE `type` SET `type`= ?,`link`=?,`status`= ?,`metatitle`= ?, `metadescription`= ?,`metakeyword`= ?  WHERE `id`= ? ");
            $ws->execute(array(trim($type), trim($link),trim($status), trim($metatitle), trim($metakeywords), trim($metadescription), $getid));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Category Adv', $thispageid, 'Update', $_SERVER['REMOTE_ADDR'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i>x</button><h4><i class="icon fa fa-close"></i> Type already exists!</h4></div>';
        }
    }
    return $res;
}

function checkimage12($dbs, $imagename, $id, $val) {
    global $db;
    $s = '';
    if ($val != '') {
        $s = " AND `$id`!='$val'";
    }
    $res1 = $db->prepare("SELECT `image_name` FROM `$dbs` WHERE `image_name`= ? $s");
    $res1->execute(array($imagename));
    $res = $res1->fetch();
    if ($res['image_name'] == '') {
        $val = 'true';
    } else {
        $val = 'false';
    }
    return $val;
}

function checkimage123($dbs, $imagename, $id, $val) {
    global $db;
    $s = '';
    if ($val != '') {
        $s = " AND `$id`!='$val'";
    }
    $res1 = $db->prepare("SELECT `imagename` FROM `$dbs` WHERE `imagename`= ? $s");
    $res1->execute(array($imagename));
    $res = $res1->fetch();
    if ($res['imagename'] == '') {
        $val = 'true';
    } else {
        $val = 'false';
    }
    return $val;
}

function checkimage124($dbs, $imagename, $id, $val) {
    global $db;
    $s = '';
    if ($val != '') {
        $s = " AND `$id`!='$val'";
    }
    $res1 = $db->prepare("SELECT `image1` FROM `$dbs` WHERE `image1`= ? $s");
    $res1->execute(array($imagename));
    $res = $res1->fetch();
    if ($res['imagename'] == '') {
        $val = 'true';
    } else {
        $val = 'false';
    }
    return $val;
}

function checkimagetitle($dbs, $imagename, $id, $val) {
    global $db;
    $s = '';
    if ($val != '') {
        $s = " AND `$id`!='$val'";
    }
    $res1 = $db->prepare("SELECT `imagetitle` FROM `$dbs` WHERE `imagetitle`= ? $s");
    $res1->execute(array($imagename));
    $res = $res1->fetch();
    if ($res['imagetitle'] == '') {
        $val = 'true';
    } else {
        $val = 'false';
    }
    return $val;
}


function addcategory($type,$category, $link,$metatitle, $metakeywords, $metadescription, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        try {
            $templink = trim($link);
            $link21 = $db->prepare("SELECT * FROM `category` WHERE `link`= ? ");
            $link21->execute(array($templink));
            $link2 = $link21->fetch();
            if ($link2['link'] == '') {
                  $getorderno = FETCH_all("SELECT * FROM `category` WHERE `cid`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
               
			   
                    $qa = $db->prepare("INSERT INTO `category` (`category` ,`link`,`metatitle`,`metakeywords`,`metadescription`, `order` ,`status` ,`ip`,`type`) values (?,?,?,?,?,?,?,?,?) ");
                    $qa->execute(array(trim($type),trim($category), trim($link),$metatitle, trim($metakeywords), trim($metadescription), trim($order), trim($status), $ip));

                    $insert_id = $db->lastInsertId();
                    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
                    $htry->execute(array('Category', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));

                    $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
               
			   
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
            }
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
        } finally {
            
        }
    } else {
        $templink = trim($link);

        $link20 = $db->prepare("SELECT * FROM `category` WHERE `link`= ?  AND `cid`!= ? ");
        $link20->execute(array($templink, $id));
        $link2 = $link20->fetch();
        if ($link2['link'] == '') {

            $ws = $db->prepare("UPDATE `category` SET `type`=?,`category`= ? ,`link`= ? ,`metatitle`= ? ,`metakeywords`= ? ,`metadescription`= ? ,`status`= ? ,`ip`= ?
			WHERE `cid`= ? ");
            $ws->execute(array(trim($type),trim($category), trim($link),$metatitle, trim($metakeywords), trim($metadescription), trim($status), $ip,$getid));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Category', $thispageid, 'Update', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i>x</button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
        }
    }

    return $res;
}

function getcategory($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `category` WHERE `cid`= '".$b."'");
    return $res[$a];
}

function getcategoryy($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `category` WHERE `link`='".$b."'");
    return $res[$a];
}

function getsubcategoryys($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `subcategory` WHERE `link`='".$b."'");
    return $res[$a];
}

function gettypee($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `type` WHERE `link`='".$b."'");
    return $res[$a];
}

function getinnercategoryy($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `innercategory` WHERE `link`='".$b."'");
    return $res[$a];
}
function delcategory($a, $thispageid, $ip)
{
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
           $getorderno = FETCH_all("SELECT * FROM `category` WHERE `cid`=?", $c);
       $get1 = $db->prepare("UPDATE `category` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
       
        $get = $db->prepare("DELETE FROM `category` WHERE `cid` = ? ");
        $get->execute(array($c));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Category', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function delcategoryadv($a, $thispageid, $ip)
{
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $getc = FETCH_all("SELECT `image1`,`image2`,`image3` FROM `categoryadv` WHERE `caid`=?", $c);
        unlink('<?php echo $fsitename; ?>images/category/advbanner/' . $getc['image1']);
        unlink('<?php echo $fsitename; ?>images/category/advbanner/' . $getc['image2']);
        unlink('<?php echo $fsitename; ?>images/category/advbanner/' . $getc['image3']);
        $get = $db->prepare("DELETE FROM `categoryadv` WHERE `caid` = ? ");
        $get->execute(array($c));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Category Banner', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function getcategoryadv($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `categoryadv` WHERE `caid`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}


function addcategoryadv($category,$title,$link,$content,$image1,$image2,$image3,$status,$id, $thispageid)
{
    global $db;
    if ($id == '')
    {
        try {
            $link21 = FETCH_all("SELECT * FROM `categoryadv` WHERE `cid`= ?", $category);
            if ($link21['cid'] == '') {
                $qa = $db->prepare("INSERT INTO `categoryadv` (`cid`,`title`,`content`,`link`,`image1`,`image2`,`image3`,`status`) VALUES (?,?,?,?,?,?,?,?)");


                $qa->execute(array(trim($category), trim($title), trim($content), trim($link), trim($image1), trim($image2), trim($image3), $status));

                $insert_id = $db->lastInsertId();
                $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
                $htry->execute(array('Category Adv', $thispageid, 'Insert', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $insert_id));

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> Category already exists!</h4></div>';
            }
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
        } finally {
            
        }
    } else {
        $link2 = FETCH_all("SELECT * FROM `categoryadv` WHERE `cid`= ?  AND `caid`!= ? ",$cid, $id);
        if ($link2['cid'] == '') {

            $ws = $db->prepare("UPDATE `categoryadv` SET `cid`= ?,`title`=?,`content`= ?,`link`= ?,`image1`= ?, `image2`= ?,`image3`= ?,`status`= ?  WHERE `caid`= ? ");
            $ws->execute(array(trim($category), trim($title), trim($content), trim($link), trim($image1), trim($image2), trim($image3), $status, $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Category Adv', $thispageid, 'Update', $_SERVER['REMOTE_ADDR'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i>x</button><h4><i class="icon fa fa-close"></i> Category already exists!</h4></div>';
        }
    }
    return $res;
}

function addcategorybanner($category, $link, $imagename, $imagealt, $image, $order, $status, $ip, $id, $thispageid)
{
    global $db;
    if ($id == '')
    {
        try {
            $link21 = FETCH_all("SELECT * FROM `categorybanner` WHERE `imagename`= ?", $imagename);
            if ($link21['imagename'] == '') {
                $qa = $db->prepare("INSERT INTO `categorybanner` (`cid`,`link`,`image`,`imagename`,`imagealt`,`order`,`status`,`ip`,`updated_by`) VALUES (?,?,?,?,?,?,?,?,?)");


                $qa->execute(array(trim($category), trim($link), trim($image), trim($imagename), trim($imagealt), trim($order), trim($status), $ip,$_SESSION['UID']));

                $insert_id = $db->lastInsertId();
                $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
                $htry->execute(array('Category Banners', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
            }
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times">x</i></button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
        } finally {
            
        }
    } else {
        $link2 = FETCH_all("SELECT * FROM `categorybanner` WHERE `imagename`= ?  AND `cbid`!= ? ",$imagename, $id);
        if ($link2['imagename'] == '') {

            $ws = $db->prepare("UPDATE `categorybanner` SET `cid`= ?,`link`=?,`image`= ?,`imagename`= ?,`imagealt`= ?, `order`= ?,`status`= ?,`ip`= ?, `updated_by`= ?  WHERE `cbid`= ? ");
            $ws->execute(array(trim($category), trim($link), trim($image), trim($imagename), trim($imagealt), trim($order), trim($status), $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Category Banners', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i>x</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    }
    return $res;
}



function getcategorybanner($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `categorybanner` WHERE `cbid`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}

function delcategorybanner($a, $thispageid, $ip)
{
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $getc = FETCH_all("SELECT `image` FROM `categorybanner` WHERE `cbid`=?", $c);
        unlink('../../images/categorybanner/' . $getc['image']);
        $get = $db->prepare("DELETE FROM `categorybanner` WHERE `cbid` = ? ");
        $get->execute(array($c));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Category Banner', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Category end here  */

/* Sub category start here  */

function addsubcategory($cid, $subcategory, $link, $metatitle, $metakeywords, $metadescription,$status, $ip, $getid, $thispageid)
{
    global $db;
    if ($getid == '') {
        $templink = trim($link);
        $link21 = $db->prepare("SELECT * FROM `subcategory` WHERE `link`= ?  ");
        $link21->execute(array($templink));
        $link2 = $link21->fetch();

        if ($link2['link'] == '') {
  $getorderno = FETCH_all("SELECT * FROM `subcategory` WHERE `sid`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
            
			$fg = $db->prepare("INSERT INTO `subcategory` (`cid`,`subcategory`,`link`,`metatitle`,`metakeywords`,`metadescription`,`order`,`status`,`ip`) values(?,?,?,?,?,?,?,?,?) ");

            $fg->execute(array(trim($cid), trim($subcategory), trim($link),$metatitle, trim($metakeywords), trim($metadescription), trim($order), trim($status), $ip));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Sub Category', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
        }
    } else {
        $templink = trim($link);


        $link21 = $db->prepare("SELECT * FROM `subcategory` WHERE `link`= ?  AND `sid`!= ? ");
        $link21->execute(array($templink, $getid));
        $link2 = $link21->fetch();
        if ($link2['link'] == '') {
            $fg = $db->prepare("UPDATE `subcategory` SET `cid`= ? ,`subcategory`= ? ,`link`= ? ,`metatitle`= ? ,`metakeywords`= ? ,`metadescription`= ? ,`order`= ? ,`status`= ? ,`ip`= ?  WHERE `sid`= ? ");
            $fg->execute(array(trim($cid), trim($subcategory), trim($link), $metatitle, trim($metakeywords), trim($metadescription), trim($order), trim($status), $ip,$getid));

            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Sub Category', $thispageid, 'Update', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
        }
    }
    return $res;
}

function getsubcategory($a, $b) {
    global $db;
    $res=DB_QUERY("SELECT `$a` FROM `subcategory` WHERE `sid`='".$b."'");
    return $res[$a];
}

function getsubcategoryy($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `subcategory` WHERE `link`='".$b."'");
    return $res[$a];
}

function delsubcategory($a, $thispageid, $ip) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {

        
           $getorderno = FETCH_all("SELECT * FROM `subcategory` WHERE `sid`=?", $c);
       $get1 = $db->prepare("UPDATE `subcategory` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
       
       
        $get = $db->prepare("DELETE FROM `subcategory` WHERE `sid` = ? ");
        $get->execute(array(trim($c)));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Sub Category', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4> </div>';
    return $res;
}

/* Sub category end here  */
/* Inner category start here  */

function addinnercategory($image,$cid, $sid, $innercat, $link, $metatitle, $metakeywords, $metadescription, $status, $ip, $thispageid, $getid) {
    global $db;
    if ($getid == '') {
        $templink = trim($link);

        $link23 = $db->prepare("SELECT * FROM `innercategory` WHERE `link`= ? ");
        $link23->execute(array($templink));
        $link2 = $link23->fetch();
        if ($link2['link'] == '') {
  $getorderno = FETCH_all("SELECT * FROM `innercategory` WHERE `innerid`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;

            $ax = $db->prepare("INSERT INTO `innercategory` SET `image`=?,`cid`= ? ,`subcategory`= ? ,`innername`= ? ,`link`= ? ,`metatitle`= ? ,`metakeywords`= ? ,`metadescription`= ? ,`order`= ? ,`status`= ? ,`ip`= ? ");
            $ax->execute(array(trim($image),trim($cid), trim($sid), trim($innercat), trim($link), $metatitle, trim($metakeywords), trim($metadescription), trim($order), trim($status), $ip));

            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Inner Category', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
        }
    } else {
        $templink = trim($link);


        $link27 = $db->prepare("SELECT * FROM `innercategory` WHERE `link`= ?  AND `innerid`!= ? ");
        $link27->execute(array($templink, $id));
        $link2 = $link27->fetch();
        if ($link2['link'] == '') {

            $li = $db->prepare("UPDATE `innercategory` SET `image`=?,`cid`= ? ,`subcategory`= ? ,`innername`= ? ,`link`= ? ,`metatitle`= ? ,`metakeywords`= ? ,`metadescription`= ? ,`status`= ? ,`ip`= ? WHERE `innerid`= ? ");
            $li->execute(array(trim($image),trim($cid), trim($sid), trim($innercat), trim($link), $metatitle, trim($metakeywords), trim($metadescription), trim($status), $ip, $getid));
            
			
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Innner Category', $thispageid, 'Update', $_SESSION['UID'], $ip, $getid));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
        }
    }
    return $res;
}

function addhomecategory($cid, $link, $image, $imagename, $imagealt, $description, $metatitle, $metakeywords, $metadescription, $order, $status, $ip, $thispageid, $id) {
    global $db;
    if ($id == '') {
        $templink = trim($link);

        $link23 = $db->prepare("SELECT * FROM `homecategory` WHERE `cid`= ? ");
        $link23->execute(array($cid));
        $link2 = $link23->fetch();
        $pcount = $link23->rowcount();
        if ($pcount == '0') {


            $ax = $db->prepare("INSERT INTO `homecategory` SET `cid`= ? ,`link`= ? ,`image`= ? ,`image_name`= ? ,`image_alt`= ? ,`description`= ? ,`metatitle`= ? ,`metakeywords`= ? ,`metadescription`= ? ,`order`= ? ,`status`= ? ,`ip`= ? ");
            $ax->execute(array(trim($cid), trim($link), trim($image), trim($imagename), trim($imagealt), $description, $metatitle, trim($metakeywords), trim($metadescription), trim($order), trim($status), $ip));

            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Home Category', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Category already exists!</h4></div>';
        }
    } else {


        $link27 = $db->prepare("SELECT * FROM `homecategory` WHERE `cid`= ?  AND `innerid`!= ? ");
        $link27->execute(array($cid, $id));
        $link2 = $link27->fetch();
        $pcount = $link27->rowcount();
        if ($pcount == '0') {


            $li = $db->prepare("UPDATE `homecategory` SET `cid`= ? ,`link`= ? ,`image`= ? ,`image_name`= ? ,`image_alt`= ? ,`description`= ? ,`metatitle`= ? ,`metakeywords`= ? ,`metadescription`= ? ,`order`= ? ,`status`= ? ,`ip`= ? WHERE `innerid`= ? ");
            $li->execute(array(trim($cid), trim($link), trim($image), trim($imagename), trim($imagealt), $description, $metatitle, trim($metakeywords), trim($metadescription), trim($order), trim($status), $ip, $id));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Home Category', $thispageid, 'Update', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Category already exists!</h4></div>';
        }
    }
    return $res;
}

function getinnetcat($a, $b) {
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `innercategory` WHERE `innerid`='".$b."'");
    return $res[$a];
}

function gethomecat($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `homecategory` WHERE `innerid`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}

function delhomecategory($a, $thispageid, $ip) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `homecategory` WHERE `innerid` = ? ");
        $get->execute(array(trim($c)));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Inner Category', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function getinnercatt($a, $b) {
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `innercategory` WHERE `link`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}

function delinnercategory($a, $thispageid, $ip) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
               $getorderno = FETCH_all("SELECT * FROM `innercategory` WHERE `innerid`=?", $c);
       $get1 = $db->prepare("UPDATE `innercategory` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
       
        $get = $db->prepare("DELETE FROM `innercategory` WHERE `innerid` = ? ");
        $get->execute(array(trim($c)));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Inner Category', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Inner category end here  */


/* Attribute start here  */

function addattribute($product, $identifer, $showlisting, $status, $id, $thispageid) {
    global $db;
    if ($id == '')
    {
		 $getorderno = FETCH_all("SELECT * FROM `attribute` WHERE `id`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
		
        $r=DB("INSERT INTO `attribute` SET `name`='".trim($product)."',`vendor`='".$_SESSION['UID']."',`identifer`='".trim($identifer)."',`showlisting`='".trim($showlisting)."',`order`='".trim($order)."',`status`='".trim($status)."'");
        $setid = mysqli_insert_id($connection_string);
        $htry = DB("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Attribute', '".$thispageid."', 'Insert', '".$_SESSION['UID']."', '".$ip."', '".$insert_id."')");
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {
        $re1 = DB("UPDATE `attribute` SET `name`='".trim($product)."' ,`identifer`='".trim($identifer)."',`showlisting`='".trim($showlisting)."',`status`='".trim($status)."' WHERE `id`='".$id."' ");
        $htry = DB("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Attribute', '".$thispageid."', 'Update', '".$_SESSION['UID']."', '".$ip."', '".$insert_id."')");
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function getstates($a, $b)
{
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `shipping_state` WHERE `ssid`='".$b."'");
    return $res[$a];
}


function getassistors_new($a, $b)
{
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `assistors` WHERE `asid`='".$b."'");
    return $res[$a];
}

function getattribute($a, $b)
{
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `attribute` WHERE `id`='".$b."'");
    return $res[$a];
}

function delattribute($a, $thispageid, $ip) {
    global $db;

    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);

    foreach ($b as $c) {
        $get = DB("DELETE FROM `attribute` WHERE `id`='".$c."'");
        $htry = DB("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Attribute', '".$thispageid."', 'Delete', '".$_SESSION['UID']."', '".$ip."', '".$c."')");
        
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function addattributevalue($attribute, $value, $status, $id, $thispageid) {
    global $db;
    if ($id == '')
    {
		 $getorderno = FETCH_all("SELECT * FROM `attribute_value` WHERE `vid`!=? ORDER BY `order` DESC", 0);
        $order=$getorderno['order']+1;
		
		
        $r=DB("INSERT INTO `attribute_value` SET `valid`='".trim($attribute)."',`order`='".trim($order)."',`value`='".trim($value)."',`status`='".trim($status)."' ");
        $setid = mysqli_insert_id($connection_string);

        $htry = DB("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Attribute Value', '".$thispageid."', 'Insert', '".$_SESSION['UID']."', '".$ip."', '".$insert_id."')");

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {

        $re1 = DB("UPDATE `attribute_value` SET `valid`='".trim($attribute)."',`value`='".trim($value)."',`status`='".trim($status)."' WHERE `vid`='".$id."' ");

        $htry = DB("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Attribute Value', '".$thispageid."', 'Update', '".$_SESSION['UID']."', '".$ip."', '".$insert_id."')");
        
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function getattributevalue($a, $b)
{
    global $db;
    $res = DB_QUERY("SELECT `$a` FROM `attribute_value` WHERE `vid`='".$b."'");
    return $res[$a];
}

function delattributevalue($a, $thispageid, $ip) {
    global $db;

    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);

    foreach ($b as $c) {
        $get = DB("DELETE FROM `attribute_value` WHERE `vid`='".$c."'");
        $htry = DB("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Attribute Value', '".$thispageid."', 'Delete', '".$_SESSION['UID']."', '".$ip."', '".$c."')");
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

/* Attribute Group  start here  */

function addattributegroup($subcategory,$category, $getvalue, $status, $date, $ip, $id) {
    global $db;
    if ($id == '')
    {
		
		
        $re4 = $db->prepare("INSERT INTO `attribute_group` SET `name`= ? ,`attribute`= ? ,`status`= ?,`cid`=?");
        $re4->execute(array(trim($subcategory), trim($getvalue), trim($status),$category));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Attribute Group', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {
        $re5 = $db->prepare("UPDATE `attribute_group` SET `name`= ? ,`attribute`= ? ,`status`= ?,`cid`=? WHERE `id`= ? ");
        $re5->execute(array(trim($subcategory), trim($getvalue), trim($status), $category, $id));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Attribute Group', $thispageid, 'Update', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function getattributegroup($a, $b) {
    global $db;

    $res1 = $db->prepare("SELECT `$a` FROM `attribute_group` WHERE `id`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();

    return $res[$a];
}

function delattributegroup($a, $thispageid) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `attribute_group` WHERE `id` = ? ");
        $get->execute(array($c));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Attribute', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function addmetal($metal, $pricepergm, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $re4 = $db->prepare("INSERT INTO `metal` SET `metal`= ?,`pergm`= ?,`order`= ?,`status`= ?,`ip`=? ");
        $re4->execute(array(trim($metal), trim($pricepergm), trim($order), trim($status), $ip));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Metal Mgmt', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {
        $re5 = $db->prepare("UPDATE `metal` SET `metal`= ?, `pergm`= ?, `order`=?, `status`= ?,`ip`=? WHERE `mid`= ? ");
        $re5->execute(array(trim($metal), trim($pricepergm), trim($order), trim($status), $ip, $id));
        
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Metal Mgmt', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function getmetal($a, $b)
{
    global $db;
    $res1 = $db->prepare("SELECT `$a` FROM `metal` WHERE `mid`= ? ");
    $res1->execute(array($b));
    $res = $res1->fetch();
    return $res[$a];
}

function delmetal($a, $thispageid)
{
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `metal` WHERE `mid` = ? ");
        $get->execute(array($c));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Metal Mgmt', $thispageid, 'Delete', $_SESSION['UID'], $ip, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

/* Products start here  */

function addproduct($weight,$stock,$imagefolder,$sizeran1,$sizeid1,$sizespecial1,$sizeprice1,$item_code,$videolink,$avlcolors,$avlsizes,$defsize,$color,$brand,$discounttype,$discount,$cid, $sid, $nid, $product, $link,$price, $sprice,$new, $deal, $recommend, $offers, $img, $shortdescription, $description,$status, $metatitle, $metakeyword, $metadescription, $id)
{

    global $db;
    global $connection_string;
    if ($id == '')
    {
        $link2 = DB_QUERY("SELECT `productname` FROM `product` WHERE `productname`='".trim($product)."'");
        if ($link2['productname'] == '')
        {
            $link22 = DB_QUERY("SELECT `link` FROM `product` WHERE `link`='".trim($link)."'");
            if ($link22['link'] == '')
            {

                $getorderno = FETCH_all("SELECT * FROM `product` WHERE `pid`!=? ORDER BY `order` DESC", 0);
                $order=$getorderno['order']+1;
                $vendor=$_SESSION['UID'];
               $er = DB("INSERT INTO `product` (`code`,`stock`,`imagefolder`,`item_code`,`videolink`,`avlcolors`,`avlsizes`,`size`,`color`,`brand`,`cid`,`sid`,`innerid`,`vendor`,`productname`,`link` ,`price` ,`sprice`,`new`,`deal`,`recommend`,`offers`,`image`,`sortdescription`,`description`,`order` ,`status`,`metatitle` ,`metakeyword` ,`metadescription`,`attribute_group`,`discount`,`discounttype`) VALUES ('".$weight."','".$stock."','".$imagefolder."','".$item_code."','".trim($videolink)."','".trim($avlcolors)."','".trim($avlsizes)."','".trim($defsize)."','".trim($color)."','".trim($brand)."','".trim($cid)."', '".trim($sid)."', '".trim($nid)."', '".$_SESSION['UID']."', '".trim($product)."','".trim($link)."','".trim($price)."', '".trim($sprice)."','".trim($new)."', '".trim($deal)."', '".trim($recommend)."', '".$offers."', '".trim($img)."', '".$shortdescription."', '".$description."', '".trim($order)."', '".trim($status)."', '".trim($metatitle)."', '".trim($metakeyword)."', '".trim($metadescription)."','".$attributegr."', '".trim($discount)."', '".trim($discounttype)."')");



                $id = mysqli_insert_id($connection_string);
                $sizeran2=explode(',',$sizeran1);
                $sizeid2=explode(',',$sizeid1);
                $sizespecial2=explode(',',$sizespecial1);
                $sizeprice2=explode(',',$sizeprice1);
                
                $f=0;
                foreach($sizeran2 as $sizeran22) {
                    if($sizeran22!='') {
                $sizespecial22=$sizespecial2[$f];
                $sizeprice22=$sizeprice2[$f];
                $er11 = DB("INSERT INTO `sizeprice` (`product_id`,`size`,`sprice`,`price`) VALUES ('".$id."','".$sizeran22."','".trim($sizespecial22)."','".trim($sizeprice22)."')");
                $f++;    
                    }
                }
                // if ($attvalue1_implode!= '')
                // {
                //     $ty = DB("INSERT INTO `productattribute` SET `attr_group`='".$attgrup_implode."',`attributes`='".$attvalue_implode."',`attr_values`='".$attvalue1_implode."', `pid`='".$id."'");
                // }

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
            }
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Product Name already exists!</h4></div>';
        }
    } else {
        $link2 = DB_QUERY("SELECT `productname` FROM `product` WHERE `productname`='".trim($product)."' AND `pid`!='".$id."'");
        if ($link2['productname'] == '')
        {
            $link22 = DB_QUERY("SELECT `link` FROM `product` WHERE `link`='".trim($link)."' AND `pid`!='".$id."'");
            if ($link22['link'] == '')
            { 
               
                $ws = DB("UPDATE `product` SET `weight`='".$weight."',`stock`='".$stock."',`item_code`='".$item_code."',`imagefolder`='".$imagefolder."', `videolink`='".$videolink."',`avlcolors`='".$avlcolors."',`avlsizes`='".$avlsizes."',`size`='".$defsize."',`color`='".$color."',`brand`='".$brand."',`cid`='".$cid."',`sid`='".$sid."',`innerid`='".$nid."',`productname`='".$product."',`link`='".$link."',`price`= '".$price."' ,`sprice`= '".$sprice."',`new`= '".$new."',`deal`= '".$deal."',`recommend`= '".$recommend."' ,`offers`= '".$offers."',`image`='".$img."',`sortdescription`= '".$shortdescription."',`description`= '".$description."',`status`='".$status."',`metatitle`='".$metatitle."',`metakeyword`='".$metakeyword."',`metadescription`='".$metadescription."',`discount`='".$discount."',`discounttype`='".$discounttype."' WHERE `pid`= '".$id."'");
               
                $sizeran2=explode(',',$sizeran1);
                $sizeid2=explode(',',$sizeid1);
                $sizespecial2=explode(',',$sizespecial1);
                $sizeprice2=explode(',',$sizeprice1);
                
                $f=0;
                foreach($sizeran2 as $sizeran22) {
                $sizespecial22=$sizespecial2[$f];
                $sizeid22=$sizeid2[$f];
                $sizeprice22=$sizeprice2[$f];
               if($sizeran22!='') {
               if($sizeid2[$f]=='') {   
               
                 $er11 = DB("INSERT INTO `sizeprice` (`product_id`,`size`,`sprice`,`price`) VALUES ('".$id."','".$sizeran22."','".trim($sizespecial22)."','".trim($sizeprice22)."')");
                }
                else
                  {
                $er11 = DB("UPDATE `sizeprice` SET `size`='".$sizeran22."',`sprice`='".$sizespecial22."',`price`='".$sizeprice22."' WHERE `id`='".$sizeid22."' ");
                  }
                    $f++; 
                   }
                }

                //  $s = '0';
                // if ($attvalue1_implode != '') {
                //     if ($s == '0') {
                //         $xs = DB("DELETE FROM `productattribute` WHERE `pid`='" . $id . "'");
                //     }
                //     $ty = DB("INSERT INTO `productattribute` SET `attr_group`='" . $attgrup_implode . "',`attributes`='" . $attvalue_implode . "',`attr_values`='" . $attvalue1_implode . "', `pid`='" . $id . "'");
                //     $s++;
                // }

                $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Link already exists!</h4></div>';
            }
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Product Name already exists!</h4></div>';
        }
    }
    
    return $res;
}

function getproduct($a, $b) {
    global $db;
    $get = DB_QUERY("SELECT `$a` FROM `product` WHERE `pid`='".$b."'");
    return $get[$a];
}

function getproductt($a, $b) {
    global $db;
    $get = DB_QUERY("SELECT `$a` FROM `product` WHERE `link`='".$b."'");
    return $get[$a];
}

function delproduct($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {


        $get = $db->prepare("DELETE FROM `product` WHERE `pid` = ?  ");
        $get->execute(array(trim($c)));

        $get = $db->prepare("DELETE FROM `productattribute` WHERE `pid` = ? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4> </div>';
    return $res;
}

function addproductreview($Name, $email, $pid, $comments, $review_title, $order, $status, $ip, $thispageid, $getid) {
    global $db;
    if ($getid == '') {
        $re4 = $db->prepare("INSERT INTO `productreview` (  `name` , `email` , `productname`,`comments` ,`review_title` ,`order` ,`status`,`ip`, `updated_by`) values(?,?,?,?,?,?,?,?)");
        $re4->execute(array(trim($Name), trim($email), trim($pid), trim($comments), trim($review_title), trim($order), trim($status), trim($ip), $_SESSION['UID']));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?,?)");
        $htry->execute(array('productreview', $thispageid, 'Insert', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {
        $re5 = $db->prepare("UPDATE `productreview` SET `name`= ? , `email`=?, `productname`=?,`review_title`=?,`comments`=? ,`order`=?,`status`=?,`ip`=?, `updated_by`=? WHERE `prid`= ? ");
        $re5->execute(array(trim($Name), trim($email), trim($pid), trim($review_title), trim($comments), trim($order), trim($status), trim($ip), trim($_SESSION['UID']), $getid));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Attribute Group', $thispageid, 'Update', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function delproductreview($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `productreview` WHERE `prid` = ?  ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4> </div>';
    return $res;
}

function getproductreview($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `productreview` WHERE `prid`=?");
    $get1->execute(array(trim($b)));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

/* Products end here  */
?>