<?php

function LoginCheck($a = '', $b = '', $c = '', $d = '', $e = '') {

    global $db;
    if (($a == '') || ($b == '')) {
        $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><i class="icon fa fa-close"></i>Email or Password was empty</div>'; 
    } else {
        if ($e == '') {
            $stmt = $db->prepare("SELECT * FROM `users` WHERE `val1`=? AND `val3`=?");
            $stmt->execute(array($a, 1));
            $ress = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($ress['id'] != '') {
                if ($ress['val2'] == md5($b)) {
                    $res = "Hurray! You will redirect into dashboard soon";
                    $_SESSION['UID'] = $ress['id'];
                    if($ress['id']=='1')
                    {
                        $_SESSION['type'] = 'admin';
                    }
                    else
                    {
                     $_SESSION['type']=  $ress['type'];  
                    }
                    $_SESSION['permissionid'] = $ress['usergroup'];
                    @extract($ress);
                    if ($id != '') {
                        $e = date('Y-m-d H:i:s');
                        $sql = 'INSERT INTO `admin_history`(admin_uid,ip,checkintime) VALUES(?,?,?)';
                        $stmt1 = $db->prepare($sql);
                        $stmt1->execute(array($id, $c, $e));
                        $_SESSION['admhistoryid'] = $db->lastInsertId();
                        if ($d == '1') {
                            //if rememberme checkbox checked
                            setcookie('lemail', $a, time() + (60 * 60 * 24 * 10)); //Means 10 days change value of 10 to how many days as you want to remember the user details on user's computer
                            setcookie('lpass', $b, time() + (60 * 60 * 24 * 10));  //Here two coockies created with username and password as cookie names, $username,$password (login crediantials) as corresponding values
                        }
                    }
                } elseif ($ress['val3'] == '2') {
                    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><i class="icon fa fa-close"></i> Your Account was deactivated by Admin</div>';
                } else {
                    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><i class="icon fa fa-close"></i> Email or Password was incorrect</div>';
                }
            } else {
                $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><i class="icon fa fa-close"></i> Invalid login details!</div>';
            }
            return $res;
        }
    }
}


function logout() {
    global $db;
    $sql = $db->prepare("UPDATE `admin_history` SET `checkouttime`='" . date('Y-m-d H:i:s') . "' WHERE `id`=?");
    $sql->execute(array($_SESSION['admhistoryid']));
    // DB("UPDATE `admin_history` SET `checkouttime`='" . date('Y-m-d H:i:s') . "' WHERE `id`='" . $_SESSION['admhistoryid'] . "'");
}


function addtemplate($a, $b, $c, $d, $e, $f) {
 global $db;
    if ($f == '') {
 $ress = $db->prepare("SELECT * FROM `templates` WHERE `title`='" . $a . "'");
    $ress ->execute(array($b));
    $get = $ress ->fetch(PDO::FETCH_ASSOC);
        if ($get['tid'] == '') {
           
   $resa = $db->prepare("INSERT INTO `templates` (`uid`, `title`, `type`, `message`, `status`, `ip`, `updated_by`) VALUES (?,?,?,?,?,?,?)");
   $resa->execute(array($_SESSION['UID'], $a, $b, $c, $d, $e, $_SESSION['UID']));        
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i>Succesfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-warning"></i> We already have this Template Title</h4></div>';
        }
    } else {
$ress = $db->prepare("SELECT * FROM `templates` WHERE `title`='" . $a . "' AND `tid`!='" . $f . "'");
    $ress ->execute(array($b));
    $get = $ress ->fetch(PDO::FETCH_ASSOC);       
        if ($get['tid'] == '') {        

$resa = $db->prepare("UPDATE `templates` SET `title`=?,`type`=?,`message`=?,`status`=?,`ip`=?,`updated_by`=? WHERE `tid`=?");
            $resa->execute(array($a, $b, $c, $d, $e, $_SESSION['UID'], $f));



            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Succesfully Updated!</h4></div>';
        }
    }
    return $res;
}

function gettemplate($a, $b) {
    //$get = mysqli_fetch_array(mysqli_query("SELECT * FROM `templates` WHERE `tid`='$b' AND `status`!='2'"));
global $db;
    $getlogo1 = $db->prepare("SELECT * FROM `templates` WHERE `tid`= ? ");
    $getlogo1->execute(array($b));
    $getlogo = $getlogo1->fetch(PDO::FETCH_ASSOC);
    $res = $getlogo[$a];
    return $res;
}

function deltemplate($a) {
global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {

$ress = $db->prepare("DELETE FROM `templates` WHERE `tid` = ?");
    $ress ->execute(array($c));

       // $htry = mysqli_query("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES ('Manage Template','1','Delete','" . $_SESSION['UID'] . "','" . $_SERVER['REMOTE_ADDR'] . "','" . $c . "')");
        $get = mysqli_query("UPDATE `templates` SET `status`='2' WHERE `tid` ='" . $c . "' ");
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}


function companylogin($a) {
    /* $getlogo = mysql_fetch_array(mysql_query("SELECT * FROM `profile_area` WHERE `pid`='" . $_SESSION['UID'] . "'"));
      $res = $getlogo[$a];
      return $res; */
    /* global $db;
      $get1 =$db->prepare("SELECT * FROM `profile_area` WHERE `pid`=?");
      $get1->execute(array($_SESSION['UID']));
      $get=$get1->fetch(PDO::FETCH_ASSOC);
      $res = $get[$a];
      return $res; */
}

function companylogos($a) {
    //$getlogo = mysql_fetch_array(mysql_query("SELECT `image` FROM `profile_area` WHERE `pid`='" . $a . "'"));
    global $db;
    $getlogo1 = $db->prepare("SELECT `image` FROM `profile_area` WHERE `pid`=?");
    $getlogo1->execute(array($a));
    $getlogo = $getlogo1->fetch(PDO::FETCH_ASSOC);
    if ($getlogo['image'] != '') {
        $res = $getlogo['image'];
    } else {
        $res = $sitename . 'data/profile/logo.png';
    }
    return $res;
}

function addprofile($freeshipping,$return_policy,$title, $firstname, $lastname, $recoveryemail, $phonenumber, $cmpnyname, $image, $ip, $postcode, $address, $fcontent, $id) {
    global $db;
    if ($id != '') {
        try {
            $resa = $db->prepare("UPDATE `manageprofile` SET `freeshipping`=?,`return_policy`=?,`title`=?,`firstname`=?,`lastname`=?,`recoveryemail`=?,`phonenumber`=?,`Company_name`=?,`image`=?,`Postcode`=?,`address`=?,`fcontent`=?, `ip`=? WHERE `pid`=?");
            $resa->execute(array($freeshipping,$return_policy,$title, $firstname, $lastname, $recoveryemail, $phonenumber, $cmpnyname, $image,  $postcode, $address, $fcontent,$ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } catch (Exception $exc) {
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> ' . $exc->getTraceAsString() . '</div>';
        }
    }
    return $res;
}

function getprofile($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `manageprofile` WHERE `pid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
//    $get = mysql_fetch_array(mysql_query("SELECT * FROM `manageprofile` WHERE `pid`='$b'"));
//    $res = $get[$a];
//    return $res;
}

function companylogo($a) {
    /* global $db;
      $getlogo1 = $db->prepare("SELECT `logo` FROM `settings` WHERE `cus_id`=?");
      $getlogo1->execute(array($a));
      $getlogo=$getlogo1->fetch(PDO::FETCH_ASSOC);
      if ($getlogo['logo'] != '') {
      $res = WEB_ROOT . 'pages/master/images/' . $getlogo['logo'];
      } else {
      $res = WEB_ROOT . 'data/profile/logo.png';
      }
      return $res;
     */
}

//
///* Purchase Section Start */
//
//function addpurchase($company, $billno, $purmode, $billdate, $supplier, $taxableamt, $otheramt, $discper, $discount, $taxamt, $roff, $subtotal, $invoiceamt, $netamt, $user, $pono, $includetax, $finid, $billdesc, $adjustme, $isigst, $order, $status, $ip, $id) {
//    global $db;
//    if ($id == '') {
//        //  $ress1 = $db->prepare("SELECT `purid` FROM `purchase` WHERE `department`=? AND `status`!=?");
//        //  $ress1->execute(array(trim($a), 2));
//        // $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        //   if ($ress['purid'] == '') {
//        $resa = $db->prepare("INSERT INTO `purchase` (`CompanyID`, `BillNo`, `PurchaseMode`, `BillDate`, `SupplierID`, `TaxableAmount`, `Others_Amount`, `DiscPer`, `Discount`, `TaxAmount`, `Roff`, `SubTotal`, `InvoiceAmount`, `NetAmount`, `UserId`, `PoNo`, `IncludeTax`, `FinID`, `BillDesc`, `Adjustment`, `ISIGST`, `Order`, `Status`, `IP`, `Updated_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
//        $resa->execute(array($company, $billno, $purmode, $billdate, $supplier, $taxableamt, $otheramt, $discper, $discount, $taxamt, $roff, $subtotal, $invoiceamt, $netamt, $user, $pono, $includetax, $finid, $billdesc, $adjustme, $isigst, $order, $status, $ip, $_SESSION['UID']));
//        $insert_id = $db->lastInsertId();
//        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
//        $htry->execute(array('Purchase', 27, 'Insert', $_SESSION['UID'], $d, $insert_id));
//        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
//        // } else {
//        //   $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Purchase</h4></div>';
//        //}
//    } else {
//        //$ress1 = $db->prepare("SELECT `purid` FROM `purchase` WHERE `department`=? AND `status`!=? AND `purid`!=?");
//        //$ress1->execute(array(trim($a), 2, $e));
//        //$ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        //if ($ress['purid'] == '') {
//        $resa = $db->prepare("UPDATE `purchase` SET `CompanyID` =?, `BillNo` =?, `PurchaseMode`=?, `BillDate` =?, `SupplierID`=?, `TaxableAmount`=?, `Others_Amount`=?, `DiscPer`=?, `Discount`=?, `TaxAmount`=?, `Roff`=?, `SubTotal`=?, `InvoiceAmount`=?, `NetAmount`=?, `UserId`=?, `PoNo`=?, `IncludeTax`=?, `FinID`=?, `BillDesc`=?, `Adjustment`=?, `ISIGST`=?, `Order`=?, `Status`=?, `IP`=?, `Updated_by`=? WHERE `purid`=?");
//        $resa->execute(array($company, $billno, $purmode, $billdate, $supplier, $taxableamt, $otheramt, $discper, $discount, $taxamt, $roff, $subtotal, $invoiceamt, $netamt, $user, $pono, $includetax, $finid, $billdesc, $adjustme, $isigst, $order, $status, $ip, $_SESSION['UID'], $id));
//        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
//        $htry->execute(array('Purchase', 27, 'Update', $_SESSION['UID'], $d, $e));
//        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
//        // } else {
//        //    $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Purchase</h4></div>';
//        //}
//    }
//    return $res;
//}
//
//function getpurchase($a, $b) {
//    global $db;
//    $get1 = $db->prepare("SELECT * FROM `purchase` WHERE `purid`=?");
//    $get1->execute(array($b));
//    $get = $get1->fetch(PDO::FETCH_ASSOC);
//    $res = $get[$a];
//    return $res;
//}
//
//function delpurchase($a) {
//    global $db;
//    $b = str_replace(".", ",", $a);
//    $b = explode(",", $b);
//    foreach ($b as $c) {
//        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
//        $htry->execute(array('Purchase', 1, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
//        $get = $db->prepare("DELETE FROM `purchase` WHERE `purid` =? ");
//        $get->execute(array($c));
//    }
//    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
//    return $res;
//}
//
///* Purchase Section End */
//

/* Department Section Start */

function adddepartment($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `did` FROM `department` WHERE `department`=? AND `status`!=?");
        $ress1->execute(array(trim($a), 2));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['did'] == '') {
            $resa = $db->prepare("INSERT INTO `department` (`department`,`order`,`status`,`ip`,`updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Department', 27, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Department</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `did` FROM `department` WHERE `department`=? AND `status`!=? AND `did`!=?");
        $ress1->execute(array(trim($a), 2, $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['did'] == '') {
            $resa = $db->prepare("UPDATE `department` SET `department`=?,`order`=?,`status`=?,`ip`=?,`updated_by`=? WHERE `did`=?");
            $resa->execute(array(trim($a), 0, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Department', 27, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Department</h4></div>';
        }
    }
    return $res;
}

function getdepartment($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `department` WHERE `did`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function deldepartment($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Department', 1, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `department` WHERE `did` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Department Section End */

/* Supplier Group Start */

function addsupplier_group($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `SgID` FROM `supplier_group` WHERE `SupplierGroup`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['SgID'] == '') {
            $resa = $db->prepare("INSERT INTO `supplier_group` (`SupplierGroup`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Supplier Group', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Supplier Group</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `SgID` FROM `supplier_group` WHERE `SupplierGroup`=? AND `SgID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['SgID'] == '') {
            $resa = $db->prepare("UPDATE `supplier_group` SET `SupplierGroup`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `SgID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Supplier Group', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Supplier Group</h4></div>';
        }
    }
    return $res;
}

function getsupplier_group($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `supplier_group` WHERE `SgID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delsuppliergroup($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Supplier Group', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `supplier_group` WHERE `SgID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Supplier Group Ends */



/* Suppliers Starts */

function addsupplier($suppliername, $supplier_company, $supplier_code, $invoice_name, $currrency, $address1, $address2, $state, $city, $postcode, $country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $suppliergroup, $img, $description, $status, $order, $ip, $id) {
    global $db;
    $credit = ($credit) ? $credit : 0;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `SupID` FROM `suppliers` WHERE `Mobile`=? AND `Phone`=? AND `E-mail`=?");
        $ress1->execute(array($mobile, $phone, $email));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['SupID'] == '') {
            $resa = $db->prepare("INSERT INTO `suppliers` (`Supplier_name`, `Company`, `Supplier_code`,`Invoice_name`,`Currency`, `Adderss_1`, `Address_2`, `State`, `City`, `Postcode`, `Country`, `E-mail`, `Website`, `Mobile`, `Phone`, `GSTNum`, `Credit`, `Creditdate`, `SupplierGroup`, `Image`, `Description`, `Status`,`Order`, `IP`, `Updated_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($suppliername, $supplier_company, $supplier_code, $invoice_name, $currrency, $address1, $address2, $state, $city, $postcode, $country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $suppliergroup, $img, $description, $status, $order, $ip, $_SESSION['UID']));
            $insert_id1 = $db->lastInsertId();

            $resa = $db->prepare("INSERT INTO `ledger` (`Name`, `printname` ,`under`,`sub_under`, `status`,`supplier_id`,`Order` ,`IP` ,`Updated_By`) VALUES (?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($suppliername, $suppliername, '16', '0', '1', $insert_id1, $order, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();

            $supupdate = $db->prepare("UPDATE `suppliers` SET `Ledger_id`=? WHERE `SupID`=? ");
            $supupdate->execute(array($insert_id, $insert_id1));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Supplier ', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id1));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Supplier or Supplier name </h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `SupID` FROM `suppliers` WHERE `Mobile`=? AND `Phone`=? AND `E-mail`=? AND `SupID` != ?");
        $ress1->execute(array($mobile, $phone, $email, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['SupID'] == '') {
            $resa = $db->prepare("UPDATE `suppliers` SET `Supplier_name`=?, `Company`=?,`Supplier_code`=?,`Invoice_name`=?,`Currency`=?, `Adderss_1`=?, `Address_2`=?, `State`=?, `City`=?, `Postcode`=?, `Country`=?, `E-mail`=?, `Website`=?, `Mobile`=?, `Phone`=?, `GSTNum`=?, `Credit`=?, `Creditdate`=?, `SupplierGroup`=?, `Image`=?, `Description`=?, `Status`=?,`Order`=?, `IP`=?, `Updated_by`=? WHERE `SupID`=? ");
            $resa->execute(array($suppliername, $supplier_company, $supplier_code, $invoice_name, $currrency, $address1, $address2, $state, $city, $postcode, $country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $suppliergroup, $img, $description, $status, $order, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("UPDATE `ledger` SET `Name`=?,`printname`=?,`Order`=?,`ip`=? WHERE `supplier_id`=?");
            $htry->execute(array($suppliername, $suppliername, $order, $ip, $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Supplier', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Supplier or Supplier Name </h4></div>';
        }
    }
    return $res;
}

function delsupplier($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Supplier ', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `suppliers` WHERE `SupID` =? ");
        $get->execute(array($c));
        $get = $db->prepare("DELETE FROM `ledger` WHERE `supplier_id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function getsupplier($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `suppliers` WHERE `SupID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

/* Suppliers End */

/* AGENT STARTS */

function getagent($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `agent` WHERE `AID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addagent($agent_name, $agent_code, $invoice_name, $currrency, $address1, $address2, $state, $city, $postcode, $country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $img, $description, $status, $order, $ip, $id) {
    global $db;
    $credit = ($credit) ? $credit : 0;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `AID` FROM `agent` WHERE `Mobile`=? AND `Phone`=? AND `E-mail`=?");
        $ress1->execute(array($mobile, $phone, $email));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['AID'] == '') {
            $resa = $db->prepare("INSERT INTO `agent` (`Agent_name`, `Agent_code`,`Invoice_name`,`Currency`, `Adderss_1`, `Address_2`, `State`, `City`, `Postcode`, `Country`, `E-mail`, `Website`, `Mobile`, `Phone`, `GSTNum`, `Credit`, `Creditdate`, `Image`, `Description`, `Status`,`Order`, `IP`, `Updated_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($agent_name, $agent_code, $invoice_name, $currrency, $address1, $address2, $state, $city, $postcode, $country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $img, $description, $status, $order, $ip, $_SESSION['UID']));
            $insert_id1 = $db->lastInsertId();

            $resa = $db->prepare("INSERT INTO `ledger` (`Name`, `printname` ,`under`,`sub_under`, `status`,`Agent_id`,`Order` ,`IP` ,`Updated_By`) VALUES (?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($agent_name, $agent_name, '16', '0', '1', $insert_id1, $order, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();

            $supupdate = $db->prepare("UPDATE `agent` SET `Ledger_id`=? WHERE `AID`=? ");
            $supupdate->execute(array($insert_id, $insert_id1));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Agent ', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id1));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Agent or Agent name </h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `AID` FROM `agent` WHERE `Mobile`=? AND `Phone`=? AND `E-mail`=? AND `AID` != ?");
        $ress1->execute(array($mobile, $phone, $email, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['AID'] == '') {
            $resa = $db->prepare("UPDATE `agent` SET `Agent_name`=?, `Agent_code`=?,`Invoice_name`=?,`Currency`=?, `Adderss_1`=?, `Address_2`=?, `State`=?, `City`=?, `Postcode`=?, `Country`=?, `E-mail`=?, `Website`=?, `Mobile`=?, `Phone`=?, `GSTNum`=?, `Credit`=?, `Creditdate`=?,  `Image`=?, `Description`=?, `Status`=?,`Order`=?, `IP`=?, `Updated_by`=? WHERE `AID`=? ");
            $resa->execute(array($agent_name, $agent_code, $invoice_name, $currrency, $address1, $address2, $state, $city, $postcode, $country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $img, $description, $status, $order, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("UPDATE `ledger` SET `Name`=?,`printname`=?,`Order`=?,`ip`=? WHERE `Agent_id`=?");
            $htry->execute(array($agent_name, $agent_name, $order, $ip, $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Agent', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Agent or Agent Name </h4></div>';
        }
    }
    return $res;
}

function delagent($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Supplier ', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `agent` WHERE `AID` =? ");
        $get->execute(array($c));
        $get = $db->prepare("DELETE FROM `ledger` WHERE `Agent_id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* AGENT ENDS */
/* Item Group Start */

function additem_group($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `ItemID` FROM `item_group` WHERE `ItemGroup`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['ItemID'] == '') {
            $resa = $db->prepare("INSERT INTO `item_group` (`ItemGroup`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Item Group', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Item Group</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `ItemID` FROM `item_group` WHERE `ItemGroup`=? AND `ItemID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['ItemID'] == '') {
            $resa = $db->prepare("UPDATE `item_group` SET `ItemGroup`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `ItemID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Item Group', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Item Group</h4></div>';
        }
    }
    return $res;
}

function getitem_group($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `item_group` WHERE `ItemID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delitemgroup($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Item Group', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `item_group` WHERE `ItemID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Item Group Ends */

/* Item Type Start */

function additem_type($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `ItemID` FROM `item_type` WHERE `ItemType`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['ItemID'] == '') {
            $resa = $db->prepare("INSERT INTO `item_type` (`ItemType`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Item Type', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Item Type</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `ItemID` FROM `item_type` WHERE `ItemType`=? AND `ItemID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['ItemID'] == '') {
            $resa = $db->prepare("UPDATE `item_type` SET `ItemType`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `ItemID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Item Type', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Item Type</h4></div>';
        }
    }
    return $res;
}

function getitem_type($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `item_type` WHERE `ItemID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delitemtype($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Item Group', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `item_type` WHERE `ItemID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Item Type Ends */

/* Manufacturer Start */

function addmanufacturer($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `MID` FROM `manufacturer` WHERE `Manufacturer`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['MID'] == '') {
            $resa = $db->prepare("INSERT INTO `manufacturer` (`Manufacturer`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Manufacurer', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Manufacturer</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `MID` FROM `manufacturer` WHERE `Manufacturer`=? AND `MID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['MID'] == '') {
            $resa = $db->prepare("UPDATE `manufacturer` SET `Manufacturer`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `MID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Manufacturer', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this manufacturer</h4></div>';
        }
    }
    return $res;
}

function getmanufacturer($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `manufacturer` WHERE `MID`=? AND `Status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delmanufacturer($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Manufacturer', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `manufacturer` WHERE `MID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Manufacturer Ends */

/* Shipping Type Start */

function addshipping_type($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `ShipID` FROM `shipping_type` WHERE `ShippingType`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['ShipID'] == '') {
            $resa = $db->prepare("INSERT INTO `shipping_type` (`ShippingType`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping Type', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Shipping Type</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `ShipID` FROM `shipping_type` WHERE `ShippingType`=? AND `ItemID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['ItemID'] == '') {
            $resa = $db->prepare("UPDATE `shipping_type` SET `ShippingType`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `ItemID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Shipping Type', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Shipping Type</h4></div>';
        }
    }
    return $res;
}

function getshipping_type($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `shipping_type` WHERE `ShipID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delshippingtype($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Shipping Group', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `shipping_type` WHERE `ShipID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Shipping Type Ends */

/* Designation Section Start */

function adddesignation($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `id` FROM `designation` WHERE `department_id`=? AND `designation`=?");
        $ress1->execute(array($a, strtolower(trim($b))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['id'] == '') {
            $resa = $db->prepare("INSERT INTO `designation` (`department_id`,`designation`,`status`,`ip`,`updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($a, strtolower(trim($b)), $c, $d, $_SESSION['UID']));

            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Designation', 28, 'Insert', $_SESSION['UID'], $d, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Designation</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `id` FROM `designation` WHERE `department_id`=? AND `designation`=? AND `id`!=?");
        $ress1->execute(array($a, strtolower(trim($b)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['id'] == '') {
            $resa = $db->prepare("UPDATE `designation` SET `department_id`=?,`designation`=?,`status`=?,`ip`=?,`updated_by`=? WHERE `id`=?");
            $resa->execute(array(trim($a), trim($b), $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Designation', '28', 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Designation</h4></div>';
        }
    }
    return $res;
}

function getdesignation($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `designation` WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function deldesignation($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Designation', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `designation` WHERE `id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    //$res=$b;
    return $res;
}

/* Designation Section End */

/* Warehouse Start */

function addwarehouse($a = '', $b = '', $c = '', $d = '', $e = '', $f = '', $g = '') {
    global $db;
    if ($g == '') {
        $ress1 = $db->prepare("SELECT `WhID` FROM `warehouse` WHERE `Warehouse_Code`=?");
        $ress1->execute(array(strtolower(trim($b))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['WhID'] == '') {
            $resa = $db->prepare("INSERT INTO `warehouse` (`Warehouse_Name`,`Warehouse_Code`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($a, $b, $c, $d, $e, $f, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Warehouse', 10, 'Insert', $_SESSION['UID'], $f, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Warehouse Code Exist</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `WhID` FROM `warehouse` WHERE `Warehouse_Code`=? AND `WhID`!=?");
        $ress1->execute(array($b, $g));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['WhID'] == '') {
            $resa = $db->prepare("UPDATE `warehouse` SET `Warehouse_Name`=?,`Warehouse_Code`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `WhID`=?");
            $resa->execute(array($a, $b, $c, $d, $e, $f, $_SESSION['UID'], $g));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Warehouse', 10, 'Update', $_SESSION['UID'], $f, $g));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Warehouse Code Exist</h4></div>';
        }
    }
    return $res;
}

function getwarehouse($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `warehouse` WHERE `WhID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delwarehouse($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Warehouse', 10, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `warehouse` WHERE `WhID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Warehouse End */

/* Partner Type Start */

function addpartnertype($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `Partner_id` FROM `partner_type` WHERE `Partner_type`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['Partner_id'] == '') {
            $resa = $db->prepare("INSERT INTO `partner_type` (`Partner_type`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($a, $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Partner Type', 13, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Partner Type already Exist</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `Partner_id` FROM `partner_type` WHERE `Partner_type`=? AND `Partner_id`!=?");
        $ress1->execute(array($a, $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['PID'] == '') {
            $resa = $db->prepare("UPDATE `partner_type` SET `Partner_type`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `Partner_id`=?");
            $resa->execute(array($a, $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Partner Type', 13, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Partner Type already Exist</h4></div>';
        }
    }
    return $res;
}

function getpartnertype($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `partner_type` WHERE `Partner_id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delpartnertype($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Partner Type', 13, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `partner_type` WHERE `Partner_id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Partner Type End */


/* Partner Type Start */

function addsubledger($name, $ledgergroup, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `slid` FROM `subledgergroup` WHERE `sname`=?");
        $ress1->execute(array(strtolower(trim($name))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['slid'] == '') {
            $resa = $db->prepare("INSERT INTO `subledgergroup` (`ledgergroup`,`sname`,`Order`,`Status`,`IP`,`Updated_by` ) VALUES (?,?,?,?)");
            $resa->execute(array($ledgergroup, $name, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Partner Type', 13, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Partner Type already Exist</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `slid` FROM `subledgergroup` WHERE `sname`=? AND `slid`!=?");
        $ress1->execute(array($name, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['slid'] == '') {
            $resa = $db->prepare("UPDATE `subledgergroup` SET `ledgergroup`=?,`sname`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `slid`=?");
            $resa->execute(array($ledgergroup, $name, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Sub Ledger Group', 13, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Partner Type already Exist</h4></div>';
        }
    }

    return $res;
}

function getsubledger($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `subledgergroup` WHERE `slid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delsubledger($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Sub Ledger Group', 13, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `subledgergroup` WHERE `slid` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Partner Type End */


/* Language Start */

function addlanguage($a = '', $b = '', $c = '', $d = '', $e = '', $f = '') {
    global $db;
    if ($f == '') {
        $ress1 = $db->prepare("SELECT `Language_id` FROM `language` WHERE `Language`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['Language_id'] == '') {
            $resa = $db->prepare("INSERT INTO `language` (`Language`,`language_code`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?)");
            $resa->execute(array($a, $b, $c, $d, $e, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('language', 13, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Language already Exist</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `Language_id` FROM `language` WHERE `Language`=? AND `Language_id`!=?");
        $ress1->execute(array($a, $f));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['Language_id'] == '') {
            $resa = $db->prepare("UPDATE `language` SET `Language`=?, `language_code`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `Language_id`=?");
            $resa->execute(array($a, $b, $c, $d, $e, $_SESSION['UID'], $f));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Language', 13, 'Update', $_SESSION['UID'], $e, $f));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Language already Exist</h4></div>';
        }
    }
    return $res;
}

function getlanguage($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `language` WHERE `Language_id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function dellanguage($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Language', 14, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `language` WHERE `Language_id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Language End */


/* Properties Start */

function addproperties($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `PID` FROM `properties` WHERE `Property`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['PID'] == '') {
            $resa = $db->prepare("INSERT INTO `properties` (`Property`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($a, $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('properties', 11, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Property already Exist</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `PID` FROM `properties` WHERE `Property`=? AND `PID`!=?");
        $ress1->execute(array($a, $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['PID'] == '') {
            $resa = $db->prepare("UPDATE `properties` SET `Property`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `PID`=?");
            $resa->execute(array($a, $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('properties', 11, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> Property already Exist</h4></div>';
        }
    }
    return $res;
}

function getproperties($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `properties` WHERE `PID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delproperties($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('properties', 11, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `properties` WHERE `PID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Properties End */

/* Industry Start */

function addindustry($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `InID` FROM `industry` WHERE `Industry`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['InID'] == '') {
            $resa = $db->prepare("INSERT INTO `industry` (`Industry`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Industry', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Industry</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `InID` FROM `industry` WHERE `Industry`=? AND `InID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['InD'] == '') {
            $resa = $db->prepare("UPDATE `industry` SET `Industry`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `InID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Industry', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Industry</h4></div>';
        }
    }
    return $res;
}

function getindustry($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `industry` WHERE `InID`=? AND `Status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delindustry($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Industry', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `industry` WHERE `InID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Industry Ends */


/* Payment Terms Start */

function addpaymentterm($a = '', $b = '', $c = '', $d = '', $e = '') {
    global $db;
    if ($e == '') {
        $ress1 = $db->prepare("SELECT `PID` FROM `payment_term` WHERE `PaymentTerm`=?");
        $ress1->execute(array(strtolower(trim($a))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['InID'] == '') {
            $resa = $db->prepare("INSERT INTO `payment_term` (`PaymentTerm`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('PaymentTerm', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Payment Term</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `PID` FROM `payment_term` WHERE `PaymentTerm`=? AND `PID`!=?");
        $ress1->execute(array(strtolower(trim($a)), $e));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['PID'] == '') {
            $resa = $db->prepare("UPDATE `payment_term` SET `PaymentTerm`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `PID`=?");
            $resa->execute(array(trim($a), $b, $c, $d, $_SESSION['UID'], $e));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('PaymentTerm', 2, 'Update', $_SESSION['UID'], $d, $e));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Payment Term</h4></div>';
        }
    }
    return $res;
}

function getpaymentterm($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `payment_term` WHERE `PID`=? AND `Status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delpaymentterm($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('PaymentTerm', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `payment_term` WHERE `PID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Payment Terms Ends */

/* Currency Start */




/* currency end */

/* Channel Start Here */

function addchannel($channel_name, $ch_code, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `ChID` FROM `channel` WHERE `Channel_code`=? ");
        $ress1->execute(array($ch_code));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['ChID'] == '') {
            $resa = $db->prepare("INSERT INTO `channel` (`Channel_code`,`Channel_Name`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($ch_code, $channel_name, $description, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Channel', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Channel Code</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `ChID` FROM `channel` WHERE `Channel_code`=? AND `ChID`!=?");
        $ress1->execute(array($ch_code, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['ChID'] == '') {
            $resa = $db->prepare("UPDATE `channel` SET `Channel_code`=?,`Channel_Name`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `ChID`=? ");
            $resa->execute(array($ch_code, $channel_name, $description, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('channel', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this channel Code</h4></div>';
        }
    }
    return $res;
}

function getchannel($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `channel` WHERE `ChID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delchannel($a) {
    $ip = $_SERVER['REMOTE_ADDR'];
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Channel', 3, 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `channel` WHERE `ChID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Channel end */

/* Delivery Term Start */

function adddeliveryterm($deliveryterm, $title, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `DTID` FROM `deliveryterm` WHERE `DeliveryTerm`=?");
        $ress1->execute(array(DeliveryTerm));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['DTID'] == '') {
            $resa = $db->prepare("INSERT INTO `deliveryterm` (`DeliveryTerm`,`Title`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($deliveryterm, $title, $description, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('DeliveryTerm', 2, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this DeliveryTerm</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `DTID` FROM `deliveryterm` WHERE `DeliveryTerm`=? AND `DTID`!=?");
        $ress1->execute(array($deliveryterm, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['DTID'] == '') {
            $resa = $db->prepare("UPDATE `deliveryterm` SET `DeliveryTerm`=?,`Title`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `DTID`=?");
            $resa->execute(array($deliveryterm, $title, $description, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Delivery Term', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Delivery Term</h4></div>';
        }
    }
    return $res;
}

function getdeliveryterm($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `deliveryterm` WHERE `DTID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function deldeliveryterm($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('DeliveryTerm', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $id));
        $get = $db->prepare("DELETE FROM `deliveryterm` WHERE `DTID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Delivery Term Ends */

/* Mode of Shipment Start */

function addshipment($shipment, $title, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `MSID` FROM `shipment` WHERE `Shipment`=?");
        $ress1->execute(array(Shipment));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['MSID'] == '') {
            $resa = $db->prepare("INSERT INTO `shipment` (`Shipment`,`Title`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($shipment, $title, $description, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('shipment', 2, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Mode of Shipment</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `MSID` FROM `shipment` WHERE `Shipment`=? AND `MSID`!=?");
        $ress1->execute(array($shipment, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['MSID'] == '') {
            $resa = $db->prepare("UPDATE `shipment` SET `Shipment`=?,`Title`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `MSID`=?");
            $resa->execute(array($shipment, $title, $description, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('shipment', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Mode of Shipment</h4></div>';
        }
    }
    return $res;
}

function getshipment($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `shipment` WHERE `MSID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delshipment($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('shipment', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $id));
        $get = $db->prepare("DELETE FROM `shipment` WHERE `MSID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Mode of Shipment Ends */


/* Payment Code start here */

function addpayment($payment, $imagename, $imagealt, $image, $order, $status, $ip, $thispageid, $id) {
    global $db;

    if ($id == '') {
        try {
            $resa = $db->prepare("INSERT INTO `payment` ( `bname`, `image_name`,`imagealt`,`image`,`IP`,`order`,`status`, `Updated_by` ) VALUES(?,?,?,?,?,?,?,?)");
            $resa->execute(array($payment, $imagename, $imagealt, $image, $ip, $order, $status, $_SESSION['UID']));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Payment', $thispageid, 'Insert', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> ' . $exc->getTraceAsString() . '</h4></div>';
        }
    } else {
        try {
            $resa = $db->prepare("UPDATE `payment` SET `bname`=?, `image_name`=?,`imagealt`=?,`image`=?,`IP`=?,`order`=?,`status`=?,`Updated_by`=? WHERE `pid`=?");
            $resa->execute(array(trim($payment), trim($imagename), trim($imagealt), trim($image), trim($ip), trim($order), trim($status), $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Payment', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> ' . $exc->getTraceAsString() . '</h4></div>';
        }
    }
    return $res;
}

function getpayment($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `payment` WHERE `pid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delpayment($a) {

    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `payment` WHERE `pid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

/* Payment Code end here */

/* Footer mgt Code start here */

function addfootermgt($description,$metatitle,$metakeyword,$metadescription,$title, $link, $order, $status, $ip, $id) {
    global $db;

    if ($id == '') {
        try {
            $resa = $db->prepare("INSERT INTO `footermgt` (`description`,`metatitle`,`metadescription`,`metakeyword`,`title`,`link`,`IP`,`order`,`status`, `Updated_by` ) VALUES(?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array(stripslashes($description),$metatitle,$metadescription,$metakeyword,$title, $link, $ip, $order, $status, $_SESSION['UID']));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Footer mgt', $thispageid, 'Insert', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> ' . $exc->getTraceAsString() . '</h4></div>';
        }
    } else {
        try {
            $resa = $db->prepare("UPDATE `footermgt` SET `description`=?,`metatitle`=?,`metadescription`=?,`metakeyword`=?,`title`=?,`link`=?,`IP`=?,`order`=?,`status`=?,`Updated_by`=? WHERE `pid`=?");
            $resa->execute(array(trim(stripslashes($description)),trim($metatitle),trim($metadescription),trim($metakeyword),trim($title), trim($link), trim($ip), trim($order), trim($status), $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Footer mgt', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } catch (Exception $exc) {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> ' . $exc->getTraceAsString() . '</h4></div>';
        }
    }
    return $res;
}

function getfootermgt($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `footermgt` WHERE `pid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delfootermgt($a) {

    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `footermgt` WHERE `pid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

/* Footer mgt Code end here */





/* Enquiry Type Start */

function addenquiry($enquiry, $title, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `EID` FROM `enquiry` WHERE `EnquiryType`=?");
        $ress1->execute(array(EnquiryType));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['EID'] == '') {
            $resa = $db->prepare("INSERT INTO `enquiry` (`EnquiryType`,`Title`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($enquiry, $title, $description, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Enquiry Type', 2, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Enquiry Type</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `EID` FROM `enquiry` WHERE `EnquiryType`=? AND `EID`!=?");
        $ress1->execute(array($enquiry, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['EID'] == '') {
            $resa = $db->prepare("UPDATE `enquiry` SET `EnquiryType`=?,`Title`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `EID`=?");
            $resa->execute(array($enquiry, $title, $description, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Enquiry Type', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Enquiry Type</h4></div>';
        }
    }
    return $res;
}

function getenquiry($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `enquiry` WHERE `EID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delenquiry($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Enquiry Type', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $id));
        $get = $db->prepare("DELETE FROM `enquiry` WHERE `EID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Enquiry Type Ends */

/* Vessel Start */

function addvessel($vessel, $title, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `VID` FROM `vessel` WHERE `Vessel`=?");
        $ress1->execute(array(Vessel));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['VID'] == '') {
            $resa = $db->prepare("INSERT INTO `vessel` (`Vessel`,`Title`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($vessel, $title, $description, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Vessel', 2, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Vessel</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `VID` FROM `vessel` WHERE `Vessel`=? AND `VID`!=?");
        $ress1->execute(array($vessel, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['VID'] == '') {
            $resa = $db->prepare("UPDATE `vessel` SET `Vessel`=?,`Title`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `VID`=?");
            $resa->execute(array($vessel, $title, $description, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Vessel', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Vessel </h4></div>';
        }
    }
    return $res;
}

function getvessel($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `vessel` WHERE `VID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delvessel($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Vessel', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $id));
        $get = $db->prepare("DELETE FROM `vessel` WHERE `VID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Vessel Ends */

/* Quotation Start */

function addquotation($quotation, $title, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `QID` FROM `quotation` WHERE `Quotation`=?");
        $ress1->execute(array(Quotation));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['QID'] == '') {
            $resa = $db->prepare("INSERT INTO `quotation` (`Quotation`,`Title`,`Description`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($quotation, $title, $description, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Quotation', 2, 'Insert', $_SESSION['UID'], $ip, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Quotation</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `QID` FROM `quotation` WHERE `Quotation`=? AND `QID`!=?");
        $ress1->execute(array($quotation, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['QID'] == '') {
            $resa = $db->prepare("UPDATE `quotation` SET `Quotation`=?,`Title`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `QID`=?");
            $resa->execute(array($quotation, $title, $description, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Quotation', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Quotation </h4></div>';
        }
    }
    return $res;
}

function getquotation($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `quotation` WHERE `QID`=? AND `status`!=?");
    $get1->execute(array($b, 2));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delquotation($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Quotation', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $id));
        $get = $db->prepare("DELETE FROM `quotation` WHERE `QID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Quotation Ends */

/* City Zone Start */

function addcity($countryname, $state_name, $city, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `CTID` FROM `city` WHERE `CityName`=?");
        $ress1->execute(array(strtolower(trim($city))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['CTID'] == '') {
            $resa = $db->prepare("INSERT INTO `city` (`CountryName`,`State_name`,`CityName`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($countryname, $state_name, $city, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('City', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this City Name</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `CTID` FROM `city` WHERE `CityName`=? AND `CTID`!=?");
        $ress1->execute(array($city, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['CTID'] == '') {
            $resa = $db->prepare("UPDATE `city` SET `CountryName`=?,`State_name`=?,`CityName`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `CTID`=?");
            $resa->execute(array($countryname, $state_name, $city, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('City', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this City Name</h4></div>';
        }
    }
    return $res;
}

function getcity($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `city` WHERE `CTID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delcity($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('City', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `city` WHERE `CTID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* City Ends */



/* Employee Group Start Here */

function addemplpyeegroup($group_name, $group_code, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `GrID` FROM `employee_group` WHERE `Group_Code`=?");
        $ress1->execute(array($group_code));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['GrID'] == '') {
            $resa = $db->prepare("INSERT INTO `employee_group` (`Group_Name`,`Group_Code`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?)");
            $resa->execute(array($group_name, $group_code, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Employee Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Employee Code</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `GrID` FROM `employee_group` WHERE `Group_Code`=? AND `GrID`!=?");
        $ress1->execute(array($group_code, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['GrID'] == '') {
            $resa = $db->prepare("UPDATE `employee_group` SET `Group_Name`=?,`Group_Code`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `GrID`=? ");
            $resa->execute(array($group_name, $group_code, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Employee Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Employee Code</h4></div>';
        }
    }
    return $res;
}

function getempgroup($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `employee_group` WHERE `GrID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delemplpyeegroup($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Employee Group', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `employee_group` WHERE `GrID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Employee Group end Here */


/* Employee Start Here */

function addemployee($empname, $empcode, $enroll, $title, $first_name, $middle_name, $last_name, $dobdate, $gender, $designation, $department, $hiredon, $terminate, $terminatereason, $image, $address, $country, $state, $city, $area, $postal, $address1, $country1, $state1, $city1, $area1, $postal1, $phone, $mobile, $emailid, $website, $description, $workshift, $salaryfre, $emptype, $isdailywages, $grossamount, $grosspercetage, $basicsalary, $mrppackprice, $maxleaveallowed, $overtimerate, $maxexpenalloed, $salaryaccthead, $casleavealloed, $ourbank, $bankacctname, $bankacctnumber, $bankaccttype, $micrcode, $ifsccode, $incenttivepln, $conemail, $conpassword, $conserver, $conport, $enableserver, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `EmpID` FROM `employee` WHERE `EmpCode`=?");
        $ress1->execute(array($empcode));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['EmpID'] == '') {

            $resa = $db->prepare("INSERT INTO `employee` (`EmpName`, `EmpCode`, `Enrollno`, `Title`, `First_Name`, `Middle_Name`, `Last_Name`, `DoB`, `Gender`, `Designation`, `Department`, `Hired_On`, `Terminate_On`, `Reson_Terminat`, `Image`, `Address_1`, `Country_1`, `State_1`, `City_1`, `Area_1`, `Postal_Code_1`, `Address_2`, `Country_2`, `State_2`, `City_2`, `Area_2`, `Postal_Code_2`, `Phone_Number`, `Mobile_Number`, `E-Mail`, `Website`, `Description`, `Work_Shift`, `Salary_Frequent`, `Employee_Type`, `IsDailyWages`, `GrossAmount`, `Gross_Percentage`, `Basic_Salary`, `Max_Over_Allowed`, `Max_Leve_Allowed`, `OverTime_Rate`, `Max_Expe_Allowed`, `Salary_Accu_Head`, `Cas_Leve_Allowed`, `Our_bank_Name`, `Bank_Account_Name`, `Bank_Account_Num`, `Bank_Account_Type`, `MICR_Code`, `IFSC_Code`, `Incentive_Plan`, `E-mail-Config`, `Password`, `Server`, `Port`, `Enable_server`,`Order`, `Status`, `IP`, `Updated_by` ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($empname, $empcode, $enroll, $title, $first_name, $middle_name, $last_name, $dobdate, $gender, $designation, $department, $hiredon, $terminate, $terminatereason, $image, $address, $country, $state, $city, $area, $postal, $address1, $country1, $state1, $city1, $area1, $postal1, $phone, $mobile, $emailid, $website, $description, $workshift, $salaryfre, $emptype, $isdailywages, $grossamount, $grosspercetage, $basicsalary, $mrppackprice, $maxleaveallowed, $overtimerate, $maxexpenalloed, $salaryaccthead, $casleavealloed, $ourbank, $bankacctname, $bankacctnumber, $bankaccttype, $micrcode, $ifsccode, $incenttivepln, $conemail, $conpassword, $conserver, $conport, $enableserver, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Employee', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Employee Code</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `EmpID` FROM `employee` WHERE `EmpCode`=? AND `EmpID`!=?");
        $ress1->execute(array($empcode, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['EmpID'] == '') {
            $resa = $db->prepare("UPDATE `employee` SET `EmpName`=?, `EmpCode`=?, `Enrollno`=?, `Title`=?, `First_Name`=?, `Middle_Name`=?, `Last_Name`=?, `DoB`=?, `Gender`=?, `Designation`=?, `Department`=?, `Hired_On`=?, `Terminate_On`=?, `Reson_Terminat`=?, `Image`=?, `Address_1`=?, `Country_1`=?, `State_1`=?, `City_1`=?, `Area_1`=?, `Postal_Code_1`=?, `Address_2`=?, `Country_2`=?, `State_2`=?, `City_2`=?, `Area_2`=?, `Postal_Code_2`=?, `Phone_Number`=?, `Mobile_Number`=?, `E-Mail`=?, `Website`=?, `Description`=?, `Work_Shift`=?, `Salary_Frequent`=?, `Employee_Type`=?, `IsDailyWages`=?, `GrossAmount`=?, `Gross_Percentage`=?, `Basic_Salary`=?, `Max_Over_Allowed`=?, `Max_Leve_Allowed`=?, `OverTime_Rate`=?, `Max_Expe_Allowed`=?, `Salary_Accu_Head`=?, `Cas_Leve_Allowed`=?, `Our_bank_Name`=?, `Bank_Account_Name`=?, `Bank_Account_Num`=?, `Bank_Account_Type`=?, `MICR_Code`=?, `IFSC_Code`=?, `Incentive_Plan`=?, `E-mail-Config`=?, `Password`=?, `Server`=?, `Port`=?, `Enable_server`=?,`Order`=?, `Status`=?, `IP`=?, `Updated_by`=? WHERE `EmpID`=? ");
            $resa->execute(array($empname, $empcode, $enroll, $title, $first_name, $middle_name, $last_name, $dobdate, $gender, $designation, $department, $hiredon, $terminate, $terminatereason, $image, $address, $country, $state, $city, $area, $postal, $address1, $country1, $state1, $city1, $area1, $postal1, $phone, $mobile, $emailid, $website, $description, $workshift, $salaryfre, $emptype, $isdailywages, $grossamount, $grosspercetage, $basicsalary, $mrppackprice, $maxleaveallowed, $overtimerate, $maxexpenalloed, $salaryaccthead, $casleavealloed, $ourbank, $bankacctname, $bankacctnumber, $bankaccttype, $micrcode, $ifsccode, $incenttivepln, $conemail, $conpassword, $conserver, $conport, $enableserver, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Employee', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Employee Code</h4></div>';
        }
    }
    return $res;
}

function getemployee($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `employee` WHERE `EmpID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delemployee($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $pimage = getbank('Image', $c);
        if ($pimage != '') {
            unlink("../../images/employee/" . $pimage);
        }
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Employee', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `employee` WHERE `EmpID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Employee end Here */


/* Brand Start Here */


/* Brand end Here */


/* Sub Group Start Here */

function addsubgroup($group_name, $subgroup, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
//        $ress1 = $db->prepare("SELECT `ItSubID` FROM `itemsubgroup` WHERE `ItemSub`=?");
//        $ress1->execute(array($brand_code));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        if ($ress['ItSubID'] == '') {
        $resa = $db->prepare("INSERT INTO `itemsubgroup` (`ItemGroup`,`ItemSub`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?)");
        $resa->execute(array($group_name, $subgroup, $order, $status, $ip, $_SESSION['UID']));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Sub Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    } else {
//        $ress1 = $db->prepare("SELECT `ItSubID` FROM `itemsubgroup` WHERE `ItemSub`=? AND `ItSubID`!=?");
//        $ress1->execute(array($brand_code, $id));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//
//        if ($ress['ItSubID'] == '') {
        $resa = $db->prepare("UPDATE `itemsubgroup` SET `ItemGroup`=?,`ItemSub`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `ItSubID`=? ");
        $resa->execute(array($group_name, $subgroup, $order, $status, $ip, $_SESSION['UID'], $id));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Sub Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    }
    return $res;
}

function getsubgroup($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `itemsubgroup` WHERE `ItSubID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delsubgroup($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Sub Group', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `itemsubgroup` WHERE `ItSubID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Sub Group end Here */

/* Inner Group Start Here */

function addinnergroup($group_name, $subgroup, $innergroup, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $resa = $db->prepare("INSERT INTO `iteminnergroup` (`Group`,`SubGroup`,`InnerGroup`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?)");
        $resa->execute(array($group_name, $subgroup, $innergroup, $order, $status, $ip, $_SESSION['UID']));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Inner Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
    } else {
        $resa = $db->prepare("UPDATE `iteminnergroup` SET `Group`=?,`SubGroup`=?,`InnerGroup`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `ItinnerID`=? ");
        $resa->execute(array($group_name, $subgroup, $innergroup, $order, $status, $ip, $_SESSION['UID'], $id));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Inner Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
    }
    return $res;
}

function getinnergroup($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `iteminnergroup` WHERE `ItinnerID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delinnergroup($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Inner Group', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `iteminnergroup` WHERE `ItinnerID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Inner Group end Here */


/* UOM Start Here */

function adduom($name, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `UomID` FROM `uom` WHERE `Name`=?");
        $ress1->execute(array($name));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['UomID'] == '') {
            $resa = $db->prepare("INSERT INTO `uom` (`Name`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($name, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('UOM', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this UOM Code</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `UomID` FROM `uom` WHERE `Name`=? AND `UomID`!=?");
        $ress1->execute(array($name, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['UomID'] == '') {
            $resa = $db->prepare("UPDATE `uom` SET `Name`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `UomID`=? ");
            $resa->execute(array($name, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('UOM', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this UOM Code</h4></div>';
        }
    }
    return $res;
}

function getuom($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `uom` WHERE `UomID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function deluom($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('UOM', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `uom` WHERE `UomID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* UOM end Here */


/* Margin Start Here */

function addmargin($name, $margincode, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `MaID` FROM `margin` WHERE `Code`=?");
        $ress1->execute(array($margincode));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['MaID'] == '') {
            $resa = $db->prepare("INSERT INTO `margin` (`Name`,`Code`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?)");
            $resa->execute(array($name, $margincode, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Margin', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Margin Code</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `MaID` FROM `margin` WHERE `Code`=? AND `MaID`!=?");
        $ress1->execute(array($margincode, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['MaID'] == '') {
            $resa = $db->prepare("UPDATE `margin` SET `Name`=?,`Code`=?, `Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `MaID`=? ");
            $resa->execute(array($name, $margincode, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Margin', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Margin Code</h4></div>';
        }
    }
    return $res;
}

function getmargin($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `margin` WHERE `MaID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delmargin($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Margin', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `margin` WHERE `MaID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Margin end Here  */

/*  Group Start Here */

function addcusgroup($name, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `CuGID` FROM `customergroup` WHERE `Group_Name`=?");
        $ress1->execute(array($name));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['CuGID'] == '') {
            $resa = $db->prepare("INSERT INTO `customergroup` (`Group_Name`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($name, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Customer Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Customer Group Name</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `CuGID` FROM `customergroup` WHERE `Group_Name`=? AND `CuGID`!=?");
        $ress1->execute(array($name, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['CusID'] == '') {
            $resa = $db->prepare("UPDATE `customergroup` SET `Group_Name`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `CuGID`=? ");
            $resa->execute(array($name, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Cutomer Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Customer Group Name</h4></div>';
        }
    }
    return $res;
}

function getcusgroup($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `customergroup` WHERE `CuGID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delcusgroup($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Customer Group', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `customergroup` WHERE `CuGID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Customer Group end Here */

function addbank($bankname, $bankcode, $branchname, $ifsccode, $address, $country, $state, $city, $area, $postal, $image, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `BankID` FROM `bank` WHERE `Bank_Code`=?");
        $ress1->execute(array($name));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['BankID'] == '') {
            //(`BankID`, `Bank_Name`, `Bank_Code`, `Branch_Name`, `IFSC_Code`, `Address`, `Country`, `State`, `City`, `Area`, `Postal_Code`, `Order`, `Status`, `IP`, `Updated_by`, `Updated_type`, `date`)
            $resa = $db->prepare("INSERT INTO `bank` (`Bank_Name`, `Bank_Code`, `Branch_Name`, `IFSC_Code`, `Address`, `Country`, `State`, `City`, `Area`, `Postal_Code`, `Image`,`Order`, `Status`, `IP`, `Updated_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($bankname, $bankcode, $branchname, $ifsccode, $address, $country, $state, $city, $area, $postal, $image, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Bank', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Bank Code</h4></div>';
        }
    } else {

        $ress1 = $db->prepare("SELECT * FROM `bank` WHERE `Bank_Code`=? AND `BankID`!=?");
        $ress1->execute(array($name, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['BankID'] == '') {

            $resa = $db->prepare("UPDATE `bank` SET `Bank_Name`=?, `Bank_Code`=?, `Branch_Name`=?, `IFSC_Code`=?, `Address`=?, `Country`=?, `State`=?, `City`=?, `Area`=?, `Postal_Code`=?,`Image`=?, `Order`=?, `Status`=?, `IP`=?, `Updated_by`=? WHERE `BankID`=? ");
            $resa->execute(array($bankname, $bankcode, $branchname, $ifsccode, $address, $country, $state, $city, $area, $postal, $image, $order, $status, $ip, $_SESSION['UID'], $id));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Bank', 3, 'Update', $_SESSION['UID'], $ip, $id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Bank Code</h4></div>';
        }
    }
    return $res;
}

function getbank($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `bank` WHERE `BankID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delbank($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $pimage = getbank('Image', $c);
        if ($pimage != '') {
            unlink("../../images/bank/" . $pimage);
        }
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Bank', '3', 'Delete', $_SESSION['UID'], $ip, $c));

        $get = $db->prepare("DELETE FROM `bank` WHERE `BankID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Customer Group end Here */

function getcustomer($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `customer` WHERE `CusID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

/* Customer Start Here */

function addcustomer($employee, $agent_name, $customer_company, $cont_person, $customer_code, $invoice_name, $currency, $address1, $address2, $state, $city, $postcode, $country, $billing_address1, $billing_address2, $billing_state, $billing_city, $billing_postcode, $billing_country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $margin, $referen, $group_name, $img, $description, $bankname, $branch, $accname, $accno, $ifsccode, $note, $status, $taxtype, $paymentterm, $paymentmode, $ip, $id) {
    global $db;
    //$credit = ($credit) ? $credit : 0;
    $invoice_name = $customer_company;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `CusID` FROM `customer` WHERE `Mobile`=? AND `Phone`=?");
        $ress1->execute(array($mobile, $phone));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        $ress2 = $db->prepare("SELECT `Name`,`supplier_id` FROM `ledger` WHERE `Name`=?");
        $ress2->execute(array($agent_name));
        $ressledger = $ress2->fetch(PDO::FETCH_ASSOC);
        if ($ress['CusID'] == '' && $ressledger['Name'] == '' && $ressledger['supplier_id'] == '') {
            //,$invoice_name,$currency,

            $resa = $db->prepare("INSERT INTO `customer` ( `vendor`, `Employee`, `Agent_name`, `Company`, `Person`,`Customer_code`, `Invoice_name`,`Currency`, `Adderss_1`, `Address_2`, `State`, `City`, `Postcode`, `Country`,`Billing_Address_1`, `Billing_Address_2`, `Billing_State`, `Billing_City`, `Billing_Postcode`, `Billing_Country`,  `E-mail`, `Website`, `Mobile`, `Phone`, `GSTNum`, `Credit`, `Creditdate`,`Margin`,`Reference`, `CustomerGroup`, `Image`, `Description`,`Bank_name`,`Branch_name`,`Account_name	`,`Account_no`,`IFSC_code`,`notes`, `Status`,`taxtype`,`paymentterm`,`paymentmode`, `IP`, `Updated_by`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($_SESSION['UID'],$employee, $agent_name, $customer_company, $cont_person, $customer_code, $invoice_name, $currency, $address1, $address2, $state, $city, $postcode, $country, $billing_address1, $billing_address2, $billing_state, $billing_city, $billing_postcode, $billing_country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $margin, $referen, $group_name, $img, $description, $bankname, $branch, $accname, $accno, $ifsccode, $note, $status, $taxtype, $paymentterm, $paymentmode, $ip, $_SESSION['UID']));
            $insert_id1 = $db->lastInsertId();

            $resa = $db->prepare("INSERT INTO `ledger` (`Name`, `printname` ,`under`,`sub_under`, `status` ,`IP` ,`Updated_By` ,`customer_id`) VALUES (?,?,?,?,?,?,?,?)");
            $resa->execute(array($agent_name, $agent_name, '19', '0', '1', $ip, $_SESSION['UID'], $insert_id1));
            $insert_id = $db->lastInsertId();

            $supupdate = $db->prepare("UPDATE `customer` SET `Ledger_id`=? WHERE `CusID`=? ");
            $supupdate->execute(array($insert_id, $insert_id1));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Customer', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id1));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Customer or Customer name </h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `CusID` FROM `customer` WHERE `Company`=? AND `CusID`!=?");
        $ress1->execute(array($customer_company, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        $ressup1 = $db->prepare("SELECT `Agent_name` FROM `customer` WHERE `CusID`=?");
        $ressup1->execute(array($id));
        $ressup = $ressup1->fetch(PDO::FETCH_ASSOC);
        if ($ressup['Agent_name'] != $agent_name) {
            $ressup2 = $db->prepare("SELECT `Agent_name` FROM `customer` WHERE `Agent_name`=?");
            $ressup2->execute(array($agent_name));
            $ressupate = $ressup2->fetch(PDO::FETCH_ASSOC);
        } else {
            $ressupate['Agent_name'] = '';
        }
        if ($ress['CusID'] == '' && $ressupate['Agent_name'] == '') {
            $resa = $db->prepare("UPDATE `customer` SET `Employee`=?, `Agent_name`=?, `Company`=?, `Person`=?,`Customer_code`=?,`Invoice_name`=?,`Currency`=?, `Adderss_1`=?, `Address_2`=?, `State`=?, `City`=?, `Postcode`=?, `Country`=?,  `Billing_Address_1`=?, `Billing_Address_2`=?, `Billing_State`=?, `Billing_City`=?, `Billing_Postcode`=?, `Billing_Country`=?,`E-mail`=?, `Website`=?, `Mobile`=?, `Phone`=?, `GSTNum`=?, `Credit`=?, `Creditdate`=?,`Margin`=?,`Reference`=?, `CustomerGroup`=?, `Image`=?, `Description`=?,`Bank_name`=?,`Branch_name`=?,`Account_name`=?,`Account_no`=?,`IFSC_code`=?,`notes`=?, `Status`=?,`taxtype`=?,`paymentterm`=?,`paymentmode`=?, `IP`=?, `Updated_by`=? WHERE `CusID`=? ");
            $resa->execute(array($employee, $agent_name, $customer_company, $cont_person, $customer_code, $invoice_name, $currency, $address1, $address2, $state, $city, $postcode, $country, $billing_address1, $billing_address2, $billing_state, $billing_city, $billing_postcode, $billing_country, $email, $website, $mobile, $phone, $gstvalue, $credit, $creditdate, $margin, $referen, $group_name, $img, $description, $bankname, $branch, $accname, $accno, $ifsccode, $note, $status, $taxtype, $paymentterm, $paymentmode, $ip, $_SESSION['UID'], $id));

            $htry = $db->prepare("UPDATE `ledger` SET `Name`=?,`printname`=?,`ip`=? WHERE `customer_id`=?");
            $htry->execute(array($cont_person, $cont_person, $ip, $id));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Cutomer', 3, 'Update', $_SESSION['UID'], $ip, $id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Customer or Customer name</h4></div>';
        }
    }
    return $res;
}

function delcustomer($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Customer', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `customer` WHERE `CusID` =? ");
        $get->execute(array($c));
        $get = $db->prepare("DELETE FROM `ledger` WHERE `customer_id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Customer  end Here */



/* Holiday GroupStart Here */

function addholidaygroup($holiday_name, $holiday_code, $description, $order, $status, $ip, $id, $yday) {
    global $db;
    if ($id == '') {
//        $ress1 = $db->prepare("SELECT `HoliID` FROM `holiday_group` WHERE `holiday_Code`=?");
//        $ress1->execute(array($holiday_code));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        if ($ress['HoliID'] == '') {
        $resa = $db->prepare("INSERT INTO `holiday_group` (`holiday_Name`,`holiday_Code`,`Description`,`Order`, `leavedays`, `Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?,?)");
        $resa->execute(array($holiday_name, $holiday_code, $description, $order, $yday, $status, $ip, $_SESSION['UID']));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    } else {
//        $ress1 = $db->prepare("SELECT `HoliID` FROM `holiday_group` WHERE `holiday_Code`=? AND `HoliID`!=?");
//        $ress1->execute(array($holiday_code, $id));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//
//        if ($ress['HoliID'] == '') {
        $resa = $db->prepare("UPDATE `holiday_group` SET `holiday_Name`=?,`holiday_Code`=?,`Description`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=?, `leavedays`=? WHERE `HoliID`=? ");
        $resa->execute(array($holiday_name, $holiday_code, $description, $order, $status, $ip, $_SESSION['UID'], $yday, $id));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    }
    return $res;
}

function getholidaygroup($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `holiday_group` WHERE `HoliID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delholidaygroup($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `holiday_group` WHERE `HoliID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Holiday Group  end Here */


/* Holiday start Here */

function addholiday($holiday_name, $holiday_date, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `holiday` FROM `holiday` WHERE `holiday`=?");
        $ress1->execute(array($holiday_name));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['hid'] == '') {
            $resa = $db->prepare("INSERT INTO `holiday` (`holiday`,`date`,`status`,`ip`,`updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($holiday_name, date('Y/m/d', strtotime($holiday_date)), $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Holiday', 19, 'Insert', $_SESSION['UID'], $ip, $insert_id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Holiday</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `hid` FROM `holiday` WHERE `holiday`=? AND `hid`!=?");
        $ress1->execute(array($holiday_name, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['hid'] == '') {
            $resa = $db->prepare("UPDATE `holiday` SET `holiday`=?,`date`=?,`status`=?,`ip`=?,`Updated_by`=? WHERE `hid`=? ");
            $resa->execute(array($holiday_name, date('Y/m/d', strtotime($holiday_date)), $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Holiday', 19, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Holiday</h4></div>';
        }
    }
    return $res;
}

function getholiday($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `holiday` WHERE `hid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delholiday($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday', '19', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `holiday` WHERE `hid` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* Holiday end Here */

/*  Salary Frequent Start Here */

function addsalaryfrequent($frequent_name, $frequent_salary, $salary_period_start, $salary_period_end, $salary_date, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
//        $ress1 = $db->prepare("SELECT `SaFID` FROM `salary_frequent` WHERE `Name`=?");
//        $ress1->execute(array($frequent_name));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        if ($ress['SaFID'] == '') {
        $resa = $db->prepare("INSERT INTO `salary_frequent`(`Name`,`Salary_Frequent`,`Salary_Period_Start`,`Salary_Period_end`,`Salary_Date`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?,?,?,?)");
        $resa->execute(array($frequent_name, $frequent_salary, $salary_period_start, $salary_period_end, $salary_date, $order, $status, $ip, $_SESSION['UID']));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    } else {
//        $ress1 = $db->prepare("SELECT `SaFID` FROM `salary_frequent` WHERE `Name`=? AND `SaFID`!=?");
//        $ress1->execute(array($frequent_name, $id));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//
//        if ($ress['SaFID'] == '') {
        $resa = $db->prepare("UPDATE `salary_frequent` SET `Name`=?,`Salary_Frequent`=?,`Salary_Period_Start`=?,`Salary_Period_end`=?,`Salary_Date`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `SaFID`=? ");
        $resa->execute(array($frequent_name, $frequent_salary, $salary_period_start, $salary_period_end, $salary_date, $order, $status, $ip, $_SESSION['UID'], $id));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    }
    return $res;
}

function getsalaryfrequent($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `salary_frequent` WHERE `SaFID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delsalaryfrequent($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Salary Frequent', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `salary_frequent` WHERE `SaFID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/*  Salary Frequent  end Here */


/*  Work Shift Start Here */

function addworkshift($holidaygroup, $shift_code, $shift_name, $sh_str_time, $sh_end_time, $min_reg_time, $max_reg_time, $break_time, $min_break_time, $weekoff, $holiday, $description, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
//        $ress1 = $db->prepare("SELECT `WoShID` FROM `work_shift` WHERE `Name`=?");
//        $ress1->execute(array($shift_name));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        if ($ress['WoShID'] == '') {
        $resa = $db->prepare("INSERT INTO `work_shift`(`Holiday_Group`, `Shift_Code`, `Shift_Name`, `Shift_Start_Time`, `Shift_End_Time`, `Minimum_Register_Time`, `Maximum_Register_Time`, `Break_Time`, `Minimum_Break_Time`, `Descriprion`, `WeekOff`, `Holidays`, `Order`, `Status`, `IP`, `Updated_by`  ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $resa->execute(array($holidaygroup, $shift_code, $shift_name, $sh_str_time, $sh_end_time, $min_reg_time, $max_reg_time, $break_time, $min_break_time, $description, $weekoff, $holiday, $order, $status, $ip, $_SESSION['UID']));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    } else {
//        $ress1 = $db->prepare("SELECT `WoShID` FROM `work_shift` WHERE `Name`=? AND `WoShID`!=?");
//        $ress1->execute(array($shift_name, $id));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//
//        if ($ress['WoShID'] == '') {
        $resa = $db->prepare("UPDATE `work_shift` SET `Holiday_Group`=?, `Shift_Code`=?, `Shift_Name`=?, `Shift_Start_Time`=?, `Shift_End_Time`=?, `Minimum_Register_Time`=?, `Maximum_Register_Time`=?, `Break_Time`=?, `Minimum_Break_Time`=?, `Descriprion`=?, `WeekOff`=?, `Holidays`=?, `Order`=?, `Status`=?, `IP`=?, `Updated_by`=? WHERE `WoShID`=? ");
        $resa->execute(array($holidaygroup, $shift_code, $shift_name, $sh_str_time, $sh_end_time, $min_reg_time, $max_reg_time, $break_time, $min_break_time, $description, $weekoff, $holiday, $order, $status, $ip, $_SESSION['UID'], $id));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Holiday Group', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    }
    return $res;
}

function getworkshift($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `work_shift` WHERE `WoShID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delworkshift($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Work Shift', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `work_shift` WHERE `WoShID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/*  Work Shift end Here */

/* Incentive Plan Start Here */

function addincentiveplan($group_name, $schedule, $incent_value, $targettype, $description, $item, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
//        $ress1 = $db->prepare("SELECT `IncentID` FROM `incentive_plan` WHERE `Group_Name`=?");
//        $ress1->execute(array($group_name));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//        if ($ress['IncentID'] == '') {
        $resa = $db->prepare("INSERT INTO `incentive_plan`(`Group_Name`,`Schedule`,`Description`,`Incentive_Value`,`Target_Type`,`Item`, `Order`, `Status`, `IP`, `Updated_by` ) VALUES (?,?,?,?,?,?,?,?,?,?)");
        $resa->execute(array($group_name, $schedule, $description, $incent_value, $targettype, $item, $order, $status, $ip, $_SESSION['UID']));
        $insert_id = $db->lastInsertId();
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Incentive Plan', 3, 'Insert', $_SESSION['UID'], $ip, $insert_id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    } else {
//        $ress1 = $db->prepare("SELECT `IncentID` FROM `incentive_plan` WHERE `Name`=? AND `IncentID`!=?");
//        $ress1->execute(array($group_name, $id));
//        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
//
//        if ($ress['IncentID'] == '') {
        $resa = $db->prepare("UPDATE `incentive_plan` SET `Group_Name`=?,`Schedule`=?,`Description`=?,`Incentive_Value`=?,`Target_Type`=?,`Item`=?, `Order`=?, `Status`=?, `IP`=?, `Updated_by`=? WHERE `IncentID`=? ");
        $resa->execute(array($group_name, $schedule, $description, $incent_value, $targettype, $item, $order, $status, $ip, $_SESSION['UID'], $id));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Incentive Plan', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
//        } else {
//            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Brand Code</h4></div>';
//        }
    }
    return $res;
}

function getincentiveplan($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `incentive_plan` WHERE `IncentID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delincentiveplan($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Incentive Plan', '3', 'Delete', $_SESSION['UID'], $ip, $c));
        $get = $db->prepare("DELETE FROM `incentive_plan` WHERE `IncentID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/*  Incentive Plan end Here */


/* State Zone Start */

function addstate($countryname, $state_name, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `SID` FROM `state` WHERE `State_name`=?");
        $ress1->execute(array(strtolower(trim($state_name))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['SID'] == '') {
            $resa = $db->prepare("INSERT INTO `state` (`CountryName`,`State_name`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?)");
            $resa->execute(array($countryname, $state_name, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('State', 2, 'Insert', $_SESSION['UID'], $d, $insert_id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this State Name</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `SID` FROM `state` WHERE `State_name`=? AND `SID`!=?");
        $ress1->execute(array($state_name, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['SID'] == '') {
            $resa = $db->prepare("UPDATE `state` SET `CountryName`=?,`State_name`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `SID`=?");
            $resa->execute(array($countryname, $state_name, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('State', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this State Name</h4></div>';
        }
    }
    return $res;
}

function getstate($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `state` WHERE `SID`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delstate($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('State', 2, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `state` WHERE `SID` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* State Ends */

/* country Zone Start */

function addcountry($countryname, $country_iso_code2, $country_iso_code3, $order, $status, $ip, $id) {
    global $db;
    if ($id == '') {
        $ress1 = $db->prepare("SELECT `CID` FROM `countries` WHERE `CountryName`=?");
        $ress1->execute(array(strtolower(trim($countryname))));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);
        if ($ress['CID'] == '') {
            $resa = $db->prepare("INSERT INTO `countries`(`CountryName`,`countries_iso_code_2`,`countries_iso_code_3`,`Order`,`Status`,`IP`, `Updated_by`) VALUES (?,?,?,?,?,?,?)");
            $resa->execute(array($countryname, $country_iso_code2, $country_iso_code3, $order, $status, $ip, $_SESSION['UID']));
            $insert_id = $db->lastInsertId();
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Country', 2, 'Insert', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Country Name</h4></div>';
        }
    } else {
        $ress1 = $db->prepare("SELECT `CID` FROM `countries` WHERE `CountryName`=? AND `CID`!=?");
        $ress1->execute(array($countryname, $id));
        $ress = $ress1->fetch(PDO::FETCH_ASSOC);

        if ($ress['CID'] == '') {
            $resa = $db->prepare("UPDATE `countries` SET `CountryName`=?,`countries_iso_code_2`=?,`countries_iso_code_3`=?, `Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `CID`=?");
            $resa->execute(array($countryname, $country_iso_code2, $country_iso_code3, $order, $status, $ip, $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Country', 2, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
        } else {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-warning"></i> We already have this Country Name</h4></div>';
        }
    }
    return $res;
}

function getcountry($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `country_new` WHERE `countries_id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delcountry($a) {
    global $db;
    $ip = $_SERVER['REMOTE_ADDR'];
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Country', '3', 'Delete', $_SESSION['UID'], $ip, $id));
        $get = $db->prepare("DELETE FROM `countries` WHERE `countries_id` =? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

/* country Ends */

function words($ammt) {
    $number = $ammt;
    $no = round($number);
    $point = round($number - $no, 2) * 100;
    $hundred = null;
    $digits_1 = strlen($no);
    $i = 0;
    $str = array();
    $words = array('0' => '', '1' => 'one', '2' => 'two',
        '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
        '7' => 'seven', '8' => 'eight', '9' => 'nine',
        '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
        '13' => 'thirteen', '14' => 'fourteen',
        '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
        '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty',
        '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
        '60' => 'sixty', '70' => 'seventy',
        '80' => 'eighty', '90' => 'ninety');
    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
    while ($i < $digits_1) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += ($divider == 10) ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? ucfirst($words[$number]) .
                    " " . $digits[$counter] . $plural . " " . $hundred :
                    ucfirst($words[floor($number / 10) * 10])
                    . " " . ucfirst($words[$number % 10]) . " "
                    . ucfirst($digits[$counter]) . $plural . " " . $hundred;
        } else
            $str[] = null;
    }
    $str = array_reverse($str);
    $result = implode('', $str);
    if ($point < 19) {
        $points = ($point) ? " " . ucfirst($words[$point]) : '';
    } else {
        $points = ($point) ? " " . ucfirst($words[floor($point / 10) * 10]) . " " . ucfirst($words[$point = $point % 10]) : '';
    }

    $res .= $result . "";
    if ($points != '') {
        $res .= " and " . $points . ' Paisa Only';
    } else {
        $res .= ' Only';
    }
    return $res;
}

function compress_image($destination_url, $quality) {

    $info = getimagesize($destination_url);

    if ($info['mime'] == 'image/jpeg')
        $image = imagecreatefromjpeg($destination_url);

    elseif ($info['mime'] == 'image/gif')
        $image = imagecreatefromgif($destination_url);

    elseif ($info['mime'] == 'image/png')
        $image = imagecreatefrompng($destination_url);

    imagejpeg($image, $destination_url, $quality);
    return $destination_url;
}

function show_toast($type, $msg) {
    return '
    <script id="thissc">
        window.onload = function(){
            toastr.' . $type . '("' . $msg . '");
                $("#thissc").remove();
        }
        
    </script>';
}

function getguest($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `guest` WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function getTable($table, $auto_id, $id) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `$table` WHERE `$auto_id`=?");
    $get1->execute(array($id));
    $get = $get1->fetch();
    return $get;
}

function get_promo_code($a = '', $b = '') {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `promocode` WHERE `pcid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function add_promo_code($cid, $sid, $nid, $brand, $attributegr, $attgrup_implode, $attvalue_implode, $attvalue1_implode, $promo_code, $type, $value, $discount_amt, $order_amt, $date_from, $date_to, $customers, $status, $ip, $getid)
{
    global $db;
    if ($getid == '')
    {
        $csd = $db->prepare("SELECT `pcid` FROM `promocode` WHERE BINARY `promo_code`=?");
        $csd->execute(array($promo_code));
        if ($csd->rowCount() > 0)
        {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-times"></i>Code already exists..</h4></div>';
        } else {
            $sd = $db->prepare("INSERT INTO `promocode` SET `cid`=?,`sid`=?,`nid`=?,`brand`=?,`attributegr`=?,`attrgroup`=?,`attrvalues`=?,`attrvalues1`=?,`promo_code`=?,`type`=?,`value`=?,`discount_amt`=?,`order_amt`=?,`fromdate`=?,`todate`=?,`specific_customers`=?,`status`=?,`ip`=?");
            $sd->execute(array(
                $cid,
                $sid,
                $nid,
                $brand,
                $attributegr,
                $attgrup_implode,
                $attvalue_implode,
                $attvalue1_implode,
                $promo_code,
                $type,
                $value,
                $discount_amt,
                $order_amt,
                date("Y-m-d H:i:s", strtotime($date_from)),
                date("Y-m-d H:i:s", strtotime($date_to)),
                implode(',', $customers),
                $status,
                $ip
            ));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Succesfully Saved</h4></div>';
        }
    } else {
        $csd = $db->prepare("SELECT `pcid` FROM `promocode` WHERE BINARY `promo_code`=? AND `pcid`!=?");
        $csd->execute(array($promo_code, $getid));
        if ($csd->rowCount() > 0) {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-times"></i>Code already exists..</h4></div>';
        } else {
            $sd = $db->prepare("UPDATE `promocode` SET `cid`=?,`sid`=?,`nid`=?,`brand`=?,`attributegr`=?,`attrgroup`=?,`attrvalues`=?,`attrvalues1`=?,`promo_code`=?,`type`=?,`value`=?,`discount_amt`=?,`order_amt`=?,`fromdate`=?,`todate`=?,`specific_customers`=?,`status`=?,`ip`=? WHERE `pcid`=?");
            $sd->execute(array(
                $cid,
                $sid,
                $nid,
                $brand,
                $attributegr,
                $attgrup_implode,
                $attvalue_implode,
                $attvalue1_implode,
                $promo_code,
                $type,
                $value,
                $discount_amt,
                $order_amt,
                date("Y-m-d H:i:s", strtotime($date_from)),
                date("Y-m-d H:i:s", strtotime($date_to)),
                implode(',', $customers),
                $status,
                $ip,
                $getid
            ));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Succesfully Saved</h4></div>';
        }
    }
    return $res;
}

function add_promo_codebk($promo_code, $type, $value, $discount_amt, $order_amt, $date_from, $date_to, $customers, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $csd = $db->prepare("SELECT `pcid` FROM `promocode` WHERE BINARY `promo_code`=?");
        $csd->execute(array($promo_code));
        if ($csd->rowCount() > 0) {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-times"></i>Code already exists..</h4></div>';
        } else {
            $sd = $db->prepare("INSERT INTO `promocode` SET `promo_code`=?,`type`=?,`value`=?,`discount_amt`=?,`order_amt`=?,`fromdate`=?,`todate`=?,`specific_customers`=?,`status`=?,`ip`=?");
            $sd->execute(array(
                $promo_code,
                $type,
                $value,
                $discount_amt,
                $order_amt,
                date("Y-m-d H:i:s", strtotime($date_from)),
                date("Y-m-d H:i:s", strtotime($date_to)),
                implode(',', $customers),
                $status,
                $ip
            ));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Succesfully Saved</h4></div>';
        }
    } else {
        $csd = $db->prepare("SELECT `pcid` FROM `promocode` WHERE BINARY `promo_code`=? AND `pcid`!=?");
        $csd->execute(array($promo_code, $getid));
        if ($csd->rowCount() > 0) {
            $res = '<div class="alert alert-warning alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-times"></i>Code already exists..</h4></div>';
        } else {
            $sd = $db->prepare("UPDATE `promocode` SET `promo_code`=?,`type`=?,`value`=?,`discount_amt`=?,`order_amt`=?,`fromdate`=?,`todate`=?,`specific_customers`=?,`status`=?,`ip`=? WHERE `pcid`=?");
            $sd->execute(array(
                $promo_code,
                $type,
                $value,
                $discount_amt,
                $order_amt,
                date("Y-m-d H:i:s", strtotime($date_from)),
                date("Y-m-d H:i:s", strtotime($date_to)),
                implode(',', $customers),
                $status,
                $ip,
                $getid
            ));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Succesfully Saved</h4></div>';
        }
    }
    return $res;
}

function del_promo_code($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `promocode` WHERE `pcid` ='" . $c . "' ");
        $get->execute();
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function currency_product($a, $b = '0')
{
    global $db;
    // $code_sym = getcurrency('code', $db_class->getstore('currency', $_SESSION['FRONT_STORE_ID']));
    $code_sym = $_SESSION['FRONT_WEB_CURRENCY'];
    $code = $code_sym;
    $symbol = Currency($code_sym);
    if ($_SESSION['FRONT_WEB_CURRENCY_VALUE'] == '') {
        $cur_rate = $db->prepare("SELECT * FROM `currency_rates` WHERE `name` != ? ");
        $usd = "AUD";
        $cur_rate->execute(array($usd));
        while ($frate = $cur_rate->fetch()) {
            if ($code == $frate['name']) {
                $newvalue = $a * $frate['value'];
                //$_SESSION['FRONT_WEB_CURRENCY_VALUE'] = $frate['value'];
            }
        }
    }

    if ($code == "AUD") {
        $newvalue = $a;
    } else {
        $newvalue =''; //$newvalue = $a * $_SESSION['FRONT_WEB_CURRENCY_VALUE'];
    }
    if ($b == '1') {
        $form_sym = '';
    } elseif ($code == 'AUD') {
        $form_sym = 'AU$ ';
    } elseif ($code == 'USD') {
        $form_sym = 'US$ ';
    } else {
        $form_sym = $symbol . ' ';
    }
    return $form_sym . number_format($newvalue, 2, '.', '');
}

function getcurrency_reverse($a) {
    global $db_class;
    if ($_SESSION['revfvalue'] == '') {
        $code_sym = $db_class->getcurrency('code', $db_class->getstore('currency', $_SESSION['FRONT_STORE_ID']));
        $code = $code_sym;
        $symbol = $db_class->Currency($code_sym);
        $cur_rate = $db_class->db->prepare("SELECT * FROM `currency_rates` WHERE `name` != ? ");
        $cur_rate->bindParam(1, $usd);
        $usd = "USD";
        $cur_rate->execute();
        while ($frate = $cur_rate->fetch()) {
            if ($code == $frate['name']) {
                $newvalue = $a / $frate['value'];
                $_SESSION['revfvalue'] = $frate['value'];
            }
        }
    }
    return round($a / ($_SESSION['revfvalue']));
}

function currency_product_rev($a, $b, $c = '') {
    global $db_class;
    $code_sym = $db_class->getcurrency('code', $db_class->getstore('currency', $_SESSION['FRONT_STORE_ID']));
    $code = $code_sym;
    $symbol = $db_class->Currency($code_sym);
    $cur_rate = $db_class->db->prepare("SELECT * FROM `currency_rates` WHERE `name` != ? ");
    $cur_rate->bindParam(1, $usd);
    $usd = "USD";
    $cur_rate->execute();
    while ($frate = $cur_rate->fetch()) {
        if ($code == $frate['name']) {
            $newvalue = $a / $frate['value'];
            $_SESSION['revfvalue'] = $frate['value'];
        }
    }
    if ($b == 1) {
        $form_sym = $symbol . " ";
    }
    if ($b == 2) {
        $form_sym = $code . ' ' . $symbol;
    }
    if ($b == 3) {
        $form_sym = $symbol . " ";
    }
    if ($b == 4) {
        $form_sym = '';
    }
    if ($c == 'db') {
        return round($newvalue);
    } else {
        return $form_sym . $db_class->cur_format(round($newvalue));
    }
}
?>