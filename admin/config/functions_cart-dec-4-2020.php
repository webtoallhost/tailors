<?php
Class Cart {

    public $cart_count = '0';
    public $cart = array();
    public $total = '0';
    public $discount = '0';
    public $discount_type = '0';
    public $discount_value = '0';
    public $ftotal = '0';
    public $ship_amount = '0';

    public function __construct() {
        $this->Update_Cart_Class();
    }

    public function Update_Cart_Class() {
        $this->cart = $_SESSION['CART_PRODUCTS'];
        $this->total = $_SESSION['CART_TOTAL'];
        $this->discount = $_SESSION['CART_DISCOUNT'];
        $this->discount_type = $_SESSION['CART_DISCOUNT_TYPE'];
        $this->discount_value = $_SESSION['CART_DISCOUNT_VALUE'];
        $this->ftotal = $_SESSION['CART_TOTAL_FINAL'];
        $this->cart_count = $_SESSION['CART_COUNT'];
        $this->ship_amount = $_SESSION['SHIP_AMOUNT'];
    }

    public function Update_Cart_Session() {
        
        $_SESSION['CART_PRODUCTS'] = $this->cart;
        $_SESSION['CART_TOTAL'] = number_format($this->total, '2', '.', '');
        $_SESSION['CART_DISCOUNT'] = number_format($this->discount, '2', '.', '');
        $_SESSION['CART_DISCOUNT_TYPE'] = $this->discount_type;
        $_SESSION['CART_DISCOUNT_VALUE'] = $this->discount_value;
        $_SESSION['CART_TOTAL_FINAL'] = number_format($this->ftotal, '2', '.', '');
        $_SESSION['CART_COUNT'] = $this->cart_count;
        $_SESSION['SHIP_AMOUNT'] = number_format($this->ship_amount, '2', '.', '');
    }

    public function Calculate() {
        global $db;
      //  $this->cart_count = count(array_unique($this->cart));
        $qtys = array_count_values($this->cart);
        $this->total = '0';
   $user_id = $_SESSION['FUID']; 
 
   $as = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=? ");
         $as->execute(array($user_id));
         $this->cart_count=$as->rowCount();
        while($asd = $as->fetch())
        {
         $this->total += $asd['totprice'];   
        }
         
 
           if ($this->discount_type != '0') {
						
            $this->ftotal = $this->total - $this->discount;
        } else {
            $this->ftotal = $this->total;
        }
        
       
       
       if($this->ship_amount!='')
{
        $this->ftotal += $this->ship_amount;
}




// 		echo $this->total;
// 		echo "<br>";
// 		echo $this->discount;
// 		 echo "<br>";
//         echo $this->ship_amount;
//          echo "<br>";
//         echo $this->ftotal;
//         exit;
		$this->Update_Cart_Session();
    }

    public function Calculate1() {

        $this->cart_count = count(array_unique($this->cart));
        $qtys = array_count_values($this->cart);
        $this->total = '0';

        foreach (array_unique($this->cart) as $prod) {
            $price = (getproduct('sprice', $prod) > 0) ? getproduct('sprice', $prod) : getproduct('price', $prod);
            $this->total += $price * $qtys[$prod];
            $this->weight += getproduct('weight', $prod);
        }
        if ($this->discount_type != '0') {
            $this->ftotal = $this->total - $this->discount;
        } else {
            $this->ftotal = $this->total;
        }

        $this->weight = '0';

        $weightres = $this->weight;

        $shippost = $this->ship_postcode;

        $stateval = FETCH_all("SELECT * FROM `shipping_price` WHERE `km_from`<? or `km_to`=? ORDER BY `km_to` DESC LIMIT 1", $shippost, $shippost);

        $km_weight_price = '';
        $shipamt = '';
        if ($weightres > 25) {
            $km_weight_price = $stateval['25plus'];
        } elseif ($weightres < 25 && $weight != 1) {
            $km_weight_price = $stateval['1to25kg'];
        } elseif ($weightres <= 1) {
            $km_weight_price = $stateval['0to1kg'];
        } else {
            $km_weight_price = '0';
        }

        $shipamt = $this->ship_amount;
        $this->ship_amount;

        $this->ship_amount = number_format(($shipamt + $km_weight_price), '2', '.', '');

        $this->ftotal += number_format(($shipamt + $km_weight_price), '2', '.', '');

        $this->Update_Cart_Session();
    }

    public function DeleteAllCart() {
        unset($_SESSION['CART_PRODUCTS']);
        unset($_SESSION['CART_TOTAL']);
        unset($_SESSION['CART_COUNT']);
        unset($_SESSION['CART_DISCOUNT']);
        unset($_SESSION['CART_DISCOUNT_TYPE']);
        unset($_SESSION['CART_DISCOUNT_VALUE']);
        unset($_SESSION['CART_TOTAL_FINAL']);
        unset($_SESSION['SHIP_AMOUNT']);
    }



    public function Quick_Cart() {
        global $fsitename;
        global $db;
      
        $user_id = $_SESSION['FUID']; 
         $tempcart = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=? ORDER BY `id` DESC LIMIT 3");
         $tempcart->execute(array($user_id));
		 if ($tempcart->rowCount() > 0) {
	 $data='  <div class="tt-dropdown-menu">
                              <div class="tt-mobile-add">
                                 <h6 class="tt-title">SHOPPING CART</h6>
                                 <button class="tt-close">Close</button>                                        
                              </div>
                              <div class="tt-dropdown-inner">
                                 <div class="tt-cart-layout">
                                    <!-- layout emty cart --><!-- <a href="empty-cart.html" class="tt-cart-empty">                                                                                        <i class="icon-f-39"></i>                                                                                        <p>No Products in the Cart</p>                                                                                </a> -->                                                
                                    <div class="tt-cart-content">
                                       <div class="tt-cart-list">';
                                       $totprice=0;
                                        while($tempcartfetch = $tempcart->fetch()) {
		       $im = explode(",", getproduct('image', $tempcartfetch['productid']));
            $data.='<div class="tt-item">
                                             <a href="'.$fsitename . 'view-' . getproduct('link', $tempcartfetch['productid']) . '.htm'.'">
                                                <div class="tt-item-img"><img src="'.$fsitename . 'images/product/' . getproduct('imagefolder', $tempcartfetch['productid']) . '/' . $im[0].'" alt="'.getproduct('productname', $tempcartfetch['productid']).'" /></div>
                                                <div class="tt-item-descriptions">
                                                   <h2 class="tt-title">'.getproduct('productname', $tempcartfetch['productid']).'</h2>
                                                   <ul class="tt-add-info">
                                                      <li>'.getcolor('color',getproduct('color', $tempcartfetch['productid'])).', Size : '.getsize('size',getproduct('size', $tempcartfetch['productid'])).'</li>
                                                 </ul>
                                                   <div class="tt-quantity">'.$tempcartfetch['qty'].' X</div>
                                                   <div class="tt-price">$'.$tempcartfetch['price'].'</div>
                                                </div>
                                             </a>
                                             <div class="tt-item-close"><a href="#" class="tt-btn-close delete_cart_this_product" data-pdid="'.$tempcartfetch['productid'].'-'.$tempcartfetch['size'].'" ></a></div>
                                          </div>';
                                          $totprice+=$tempcartfetch['qty']*$tempcartfetch['price'];
                                        }
                                          
 $data.='</div>
                                       <div class="tt-cart-total-row">
                                          <div class="tt-cart-total-title">SUBTOTAL:</div>
                                          <div class="tt-cart-total-price">$'.$totprice.'</div>
                                       </div>
                                       <div class="tt-cart-btn">
                                          <div class="tt-item"><a href="http://13.234.94.153/pages/orderconfirmation.htm" class="btn">PROCEED TO CHECKOUT</a></div>
                                          <div class="tt-item">
                                          <a href="http://13.234.94.153/pages/cart.htm" class="btn-link-02 tt-hidden-mobile">View Cart</a> <a href="shopping_cart_02.html" class="btn btn-border tt-hidden-desctope">VIEW CART</a>                                                        </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                       ';
		
		 }
		 else {
            $data = '<p align="center">Your Cart is Empty</p>';
        }
          


        return $data;
    }

    public function AddCart($product_id, $qty,$sizeid) {
        global $db;
        for ($i = 1; $i <= $qty; $i++) {
            if ($this->cart == '') {
                $this->cart = array();
            }
            $this->cart[] = $this->$product_id;
        }
        $this->sizeid=$sizeid;
        $exp=explode('-',$product_id);
         $as = $db->prepare("SELECT * FROM `sizeprice` WHERE `size`=? AND `product_id`=?");
         $as->execute(array($exp['1'],$exp['0']));
		 if ($as->rowCount() > 0) {
		      $asd = $as->fetch();
		     $price = ($asd['sprice'] > 0) ? $asd['sprice'] : $asd['price'];
		
		
		  $current_price = $price * $qty;
		 }
		 else
		 {
        $price = (getproduct('sprice', $exp['0']) > 0) ? getproduct('sprice', $exp['0']) : getproduct('price', $exp['0']);
        $current_price = $price * $qty;
		
		}
   
        $user_id = $_SESSION['FUID']; 
       // $user_id = '';  
          $prid=$exp['0'];
           $szid=$exp['1'];
         $uas = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=? AND `productid`=? AND `size`=?");
        $uas->execute(array($user_id,$prid,$szid));
        if ($uas->rowCount() > 0) {
            $uas11 = $uas->fetch();
            if($qty=='1') {
                $qtyss=$uas11['qty']+1;
            }
            else
            {
              $qtyss=$qty;
            }
          $resa = $db->prepare("UPDATE `tempcart` SET `size` = ?,`price` = ?,`qty` = ?, `totprice` = ? WHERE `userid`=? AND `productid`=? AND `size`=?");
            $resa->execute(array($szid,$price,$qtyss,$current_price,$user_id,$prid,$szid));   
            
        }
        else
        {
       
          $resa = $db->prepare("INSERT INTO `tempcart` (`userid`,`productid`,`size`,`price`,`qty`,`totprice`) VALUES(?,?,?,?,?,?)");
            $resa->execute(array($user_id,$prid,$szid,$price,$qty,$current_price)); 
              
        }
        
        
        $this->Calculate();
        return $this->cart_count;
    }

    public function DeleteCart($product_id) {
        global $db;
       
       
        
        $expproid=explode('-',$product_id);
        $prid=$expproid['0'];
        $siid=$expproid['1'];
      $user_id = $_SESSION['FUID']; 
       $as = $db->prepare("SELECT * FROM `tempcart` WHERE `size`=? AND `productid`=? AND `userid`=? ");
         $as->execute(array($siid,$prid,$user_id));
		 if ($as->rowCount() > 0) {
		      $asd = $as->fetch();
		      $get = $db->prepare("DELETE FROM `tempcart` WHERE `id`= ? ");
            $get->execute(array($asd['id']));       
		 }
		 
		 
        $this->cart = array_diff($this->cart, array($this->$product_id));
         
         
        $this->Calculate();
        return $this->cart_count;
    }

    public function UpdateCart() {
        $this->cart = array();
        foreach ($_POST['product_id'] as $k => $prods) {
            for ($i = 1; $i <= $_POST['qty'][$k]; $i++) {
                $this->cart[] = $this->MD5_id($prods);
            }
        }

        $this->Calculate();
    }

    public function UpdateCart_Ajax($product_id, $qty, $sizeid) {
         global $db;
        $exp=explode('-',$product_id);
        $current_prod = $this->MD5_id($exp['0']);
        $newcart = array();
        $once = 1;
        foreach ($this->cart as $prod) {
            if ($prod == $current_prod) {
                if ($once == 1) {
                    for ($s = 1; $s <= $qty; $s++) {
                        $newcart[] = $current_prod;
                    }
                    $once = 2;
                }
            } else {
                $newcart[] = $prod;
            }
        }
        

        $this->cart = $newcart;
         $this->sizeid = $sizeid;
       $qtys = array_count_values($this->cart);

         $as = $db->prepare("SELECT * FROM `sizeprice` WHERE `size`=? AND `product_id`=?");
         $as->execute(array($exp['1'],$exp['0']));
		 if ($as->rowCount() > 0) {
		 $asd = $as->fetch();
		 $price = ($asd['sprice'] > 0) ? $asd['sprice'] : $asd['price'];
		 $current_price = $price * $qty;
		 }
		 else
		 {
        $price = (getproduct('sprice', $exp['0']) > 0) ? getproduct('sprice', $exp['0']) : getproduct('price', $exp['0']);
        $current_price = $price * $qty;
	 	}
   
        //$user_id = $_SESSION['FUID']; 
         $user_id = $_SESSION['FUID']; 
          $prid=$exp['0'];
           $szid=$exp['1'];
         $uas = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=? AND `productid`=? AND `size`=?");
        $uas->execute(array($user_id,$prid,$szid));
        if ($uas->rowCount() > 0) {
          $resa = $db->prepare("UPDATE `tempcart` SET `size` = ?,`price` = ?,`qty` = ?, `totprice` = ? WHERE `userid`=? AND `productid`=? AND `size`=?");
            $resa->execute(array($szid,$price,$qty,$current_price,$user_id,$prid,$szid));   
            
        }
        else
        {
       
          $resa = $db->prepare("INSERT INTO `tempcart` (`userid`,`productid`,`size`,`price`,`qty`,`totprice`) VALUES(?,?,?,?,?,?)");
            $resa->execute(array($user_id,$prid,$szid,$price,$qty,$current_price)); 
              
        }
        $this->Calculate();
        return array($this->cart_count, $current_price);
    }
 
/* public function Reward_point($reward_point) {
		  global $db;
		  $tot_cart=$_SESSION['CART_TOTAL_FINAL'];		
                  $user_id = $_SESSION['FUID'];   
	          $as = $db->prepare("SELECT * FROM `reward_point` WHERE `id`=?");
                  $as->execute(array('1'));
		 if ($as->rowCount() > 0) {
                 $asd = $as->fetch();
	         if (trim($asd['min_point']) <= $tot_cart) {
		 $as11 = $db->prepare("SELECT * FROM `reward_history` WHERE `CusID`=?");
                $as11->execute(array($user_id));
		if ($as11->rowCount() > 0) {
                $as11dd = $as11->fetch();
		if($as11dd['earn_reward_point_value']>=$tot_cart)
				{
					
				$earn_point=($tot_cart/$asd['min_point'])+$as11dd['earn_reward_point'];
				$earn_point_value=($earn_point*$asd['purchase_point'])+$as11dd['earn_reward_point_value'];
				
				$used_point=$as11dd['earn_reward_point'];
				$used_point_value=$as11dd['earn_reward_point_value'];
				
				$pending_point=$earn_point-$used_point;
				$pending_point_value=$earn_point_value-$used_point_value;
				
				}
				
			
			
               $user_id = $_SESSION['FUID'];
               
								
				$asss = $db->prepare("SELECT * FROM `reward_history` WHERE `CusID`=?");
                $asss->execute(array($user_id));
				if ($asss->rowCount() > 0) {
				$asss11 = $asss->fetch();
				
				$resa = $db->prepare("UPDATE `reward_history` SET `earn_reward_point`=?, `earn_reward_point_value`=?,`used_reward_point`=?,`used_reward_point_value`=?,`pending_point`=?,`pending_point_value`=?,`over_all_total`=?,`subtotal`=? WHERE `CusID`=?");
                $resa->execute(array($earn_point, $earn_point_value, $used_point, $used_point_value, $pending_point,$pending_point_value,$_SESSION['CART_TOTAL'],$_SESSION['CART_TOTAL_FINAL'],$user_id));
              
                }
				
                return number_format($used_point_value, '2', '.', '');			
				}
			
				
				
			 }
		}

		if($pending_point!='0')
		{
		 
		  $_SESSION['CART_TOTAL_FINAL']=$_SESSION['CART_TOTAL_FINAL']-number_format($used_point_value, '2', '.', '');
		  $_SESSION['Reward_point']=$used_point_value;
		  $this->used_point_value=$used_point_value;
		  return $used_point_value;
		}
		else
		{
			$err_msg="Nopoint";
		}
		 
	 }
	 
		
	
 public function Remove_Reward_point($reward_point) {
		  global $db;
		  $tot_cart=$_SESSION['CART_TOTAL_FINAL'];
		 if ($_SESSION['GUEST'] == '1') {
        $user_id = $_SESSION['GUEST_ID'];
    } else {
        $user_id = $_SESSION['FUID'];
    }
	    $as = $db->prepare("SELECT * FROM `reward_point` WHERE `id`=?");
        $as->execute(array('1'));
		if ($as->rowCount() > 0) {
            $asd = $as->fetch();
			 if (trim($asd['min_point']) <= $tot_cart) {
				
				 
			   $as11 = $db->prepare("SELECT * FROM `reward_history` WHERE `CusID`=?");
                $as11->execute(array($user_id));
				
				if ($as11->rowCount() > 0) {
                $as11dd = $as11->fetch();
				
				
				if($as11dd['earn_reward_point_value']>=$tot_cart)
				{
					
				$earn_point=($tot_cart/$asd['purchase_point'])-$as11dd['earn_reward_point'];
				$earn_point_value=($earn_point*$asd['redeem_point'])-$as11dd['earn_reward_point_value'];
				
				$used_point=$as11dd['earn_reward_point'];
				$used_point_value=$as11dd['earn_reward_point_value'];
				
				$pending_point=$earn_point-$used_point;
				$pending_point_value=$earn_point_value-$used_point_value;
				
				}
				
			   if ($_SESSION['GUEST'] == '1') {
               $user_id = $_SESSION['GUEST_ID'];
               } else {
               $user_id = $_SESSION['FUID'];
               }
								
				$asss = $db->prepare("SELECT * FROM `reward_history` WHERE `CusID`=?");
                $asss->execute(array($user_id));
				if ($asss->rowCount() > 0) {
				$asss11 = $asss->fetch();
				
				$resa = $db->prepare("UPDATE `reward_history` SET `earn_reward_point`=?, `earn_reward_point_value`=?,`used_reward_point`=?,`used_reward_point_value`=?,`pending_point`=?,`pending_point_value`=?,`over_all_total`=?,`subtotal`=? WHERE `CusID`=?");
                $resa->execute(array($earn_point, $earn_point_value, $used_point, $used_point_value, $pending_point,$pending_point_value,$_SESSION['CART_TOTAL'],$_SESSION['CART_TOTAL_FINAL'],$user_id));
              
                }
				
                return number_format($used_point_value, '2', '.', '');			
				}
				
			 }
		}

		if($pending_point!='0')
		{
		 
		  $_SESSION['CART_TOTAL_FINAL']=$_SESSION['CART_TOTAL_FINAL']-number_format($used_point_value, '2', '.', '');
		  $_SESSION['Reward_point']=$used_point_value;
		  $this->used_point_value=$used_point_value;
		  return $used_point_value;
		}
		else
		{
			$err_msg="Nopoint";
		}
		 
	 }
	 
		*/
	

	
     public function Coupon_Code($code,$shipprice) {
        global $db;
        $err_msg = 'Code Invalid';
		$this->ship_amount=$shipprice;
		$as = $db->prepare("SELECT * FROM `promocode` WHERE `promo_code`=? AND `status`=?");
        $as->execute(array($code, '1'));
        
		if ($as->rowCount() > 0) {
            $asd = $as->fetch();            
				
                if ($_SESSION['FUID'] != '') {
					
                } else {
					
                    $this->Coupon_Code_Remove();
                    return $err_msg;
                }
			$usarray[]=$asd['specific_customers'];
					
        if (in_array($_SESSION['FUID'], $usarray)) {                     
	$fdate = date("Y-m-d", strtotime($asd['fromdate']));					 
        $tdate = date("Y-m-d", strtotime($asd['todate'])); 		   
        $cdate = date("Y-m-d");		  
        if (($cdate >= $fdate) && ($cdate <= $tdate)) {			
                if ($asd['type'] == '1') {				
                    $this->discount_type = '1';
                    $this->discount_value = $asd['value'];
                    $this->discount = $this->total * ($asd['value'] / 100);
                    $_SESSION['PROMO_CODE'] = $code;
                    $this->Calculate();
                } elseif ($asd['type'] == '2') {				
                    $this->discount_type = '2';
                    $this->discount_value = $asd['value'];
                    $this->discount = $asd['value'];
                    $_SESSION['PROMO_CODE'] = $code;
                   $this->Calculate();
                } else {
                    $this->Coupon_Code_Remove();
                    return $err_msg;
                } }else {						 
                        $this->Coupon_Code_Remove();
                        return $err_msg;
                    } 	
            } else {
                $fdate = date("Y-m-d", strtotime($asd['fromdate']));					 
        $tdate = date("Y-m-d", strtotime($asd['todate'])); 		   
        $cdate = date("Y-m-d");		  
        if (($cdate >= $fdate) && ($cdate <= $tdate)) {			
                if ($asd['type'] == '1') {				
                    $this->discount_type = '1';
                    $this->discount_value = $asd['value'];
                    $this->discount = $this->total * ($asd['value'] / 100);
                    $_SESSION['PROMO_CODE'] = $code;
                    $this->Calculate();
                } elseif ($asd['type'] == '2') {				
                    $this->discount_type = '2';
                    $this->discount_value = $asd['value'];
                    $this->discount = $asd['value'];
                    $_SESSION['PROMO_CODE'] = $code;
                   $this->Calculate();
                } else {
                    $this->Coupon_Code_Remove();
                    return $err_msg;
                } }else {						 
                        $this->Coupon_Code_Remove();
                        return $err_msg;
                    } 	
                
                
            }
        } else {
            $this->Coupon_Code_Remove();
            return $err_msg;
        }
    }


 public function detaddshipamt($postcode , $pid) {
        global $db;
        $err_msg = '';
       
	  

        $manageprofile = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
        $org = $manageprofile['Postcode'];

        $dis = $postcode;

        $newval = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" . $org . "&destinations=" . $dis . "&key=AIzaSyAJDyvS5KNpnvnegYaDI63SpMlSezrM9iE"), true);

        $est_m = $newval['rows'][0]['elements'][0]['distance']['value'];

        $ress = number_format(($est_m / 1000), 0, ".", "");

		
		$newval11 = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".$dis."&sensor=true"), true);

        $statevcal=$newval11['results'][0]['address_components'][2]['long_name'];

        $as = $db->prepare("SELECT * FROM `shipping_state` WHERE `state_name`=? AND `status`=?");
        $as->execute(array($statevcal, '1'));

        if ($as->rowCount() > 0) {
            $asd = $as->fetch();
            if ($asd['shipcost'] != '') {
              $ship_amount = $asd['shipcost'];
         }
		 
		 $prolist = FETCH_all("SELECT * FROM `product` WHERE `pid`=?", $pid);

        $weight = $prolist['weight'];

        $ship_postcode=$ress;

        $stateval = FETCH_all("SELECT * FROM `shipping_price` WHERE `km_from`<? or `km_to`=? ORDER BY `km_to` DESC LIMIT 1", $ship_postcode, $ship_postcode);

        $km_weight_price = '';
        $shipamt = '';
        if ($weight > 25) {
            $km_weight_price = $stateval['25plus'];
        } elseif ($weight < 25 && $weight != 1) {
            $km_weight_price = $stateval['1to25kg'];
        } elseif ($weight <= 1) {
            $km_weight_price = $stateval['0to1kg'];
        } else {
            $km_weight_price = '0';
        }
	
        $resshikp= number_format(($ship_amount + $km_weight_price), '2', '.', '');
       
        } else {
             return $err_msg;
        }
		
		
		
      return $resshikp;
		
    }

	
	
    public function addshipamt($statecode, $postcode) {
        global $db;
        $err_msg = 'shipempty';
        $as = $db->prepare("SELECT * FROM `shipping_state` WHERE `ssid`=? AND `status`=?");
        $as->execute(array($statecode, '1'));

        $manageprofile = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
        $org = $manageprofile['Postcode'];

        $dis = $postcode;

        $newval = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" . $org . "&destinations=" . $dis . "&key=AIzaSyAJDyvS5KNpnvnegYaDI63SpMlSezrM9iE"), true);

        $est_m = $newval['rows'][0]['elements'][0]['distance']['value'];

        $ress = number_format(($est_m / 1000), 0, ".", "");

        if ($as->rowCount() > 0) {
            $asd = $as->fetch();
            if ($asd['shipcost'] != '') {
                $this->ship_postcode = $ress;
                $this->ship_amount = $asd['shipcost'];
                //$this->Calculate1();
            }
            return $this->Calculate1();
        } else {
            // $this->Coupon_Code_Remove();
            return $err_msg;
        }
    }

    public function Coupon_Code_Remove() {
        $this->discount_type = '0';
        $this->discount_value = '0';
        $this->discount = '0';
        unset($_SESSION['PROMO_CODE']);
        $this->Calculate();
    }

    public function MD5_id($value) {
        global $db;
        $as = $db->prepare("SELECT `pid` FROM `product` WHERE MD5(`pid`)=?");
        $as->execute(array($value));
        $fs = $as->fetch();
        return $fs['pid'];
    }

}

?>