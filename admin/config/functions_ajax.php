<?php
$report = 1;
include 'config.inc.php';
global $db;
if ($_POST['auser'] != '') {
    $chk = FETCH_all("SELECT `id` FROM `users` WHERE `val1`=?", $_POST['auser']);
    if ($chk['id'] != '') {
        echo 'Username Already Exists';
    } else {
        echo 'Username Available';
    }
}

if ($_POST['popupadv'] == '1') {
    $_SESSION['adpopclose'] = '1';
}

if (($_REQUEST['image'] != '') && ($_REQUEST['id'] != '') && ($_REQUEST['table'] != '') && ($_REQUEST['path'] != '') && ($_REQUEST['images'] != '') && ($_REQUEST['pid'] != '')) {

    if ($_REQUEST['table'] == 'product') {
        $pimg = explode(",", getproduct('image', $_REQUEST['id']));

        if (($key = array_search($_REQUEST['image'], $pimg))) {
            unset($pimg[$key]);
        }
        unlink("../../images/product/thump/" . $_REQUEST['image']);
        unlink("../../images/product/image/" . $_REQUEST['image']);
        unlink("../../images/product/small/" . $_REQUEST['image']);
        unlink("../../images/product/big/" . $_REQUEST['image']);
        unlink($_REQUEST['path'] . $_REQUEST['image']);

        $uimg = '';
        foreach ($pimg as $gimage) {
            if ($gimage != $_REQUEST['image']) {
                $uimg .= $gimage . ',';
            } else {
                $uimg .= '';
            }
        }
        $uimg = substr($uimg, 0, -1);

        $updateimg = $db->prepare("UPDATE `" . $_REQUEST['table'] . "` SET `" . $_REQUEST['images'] . "`=? WHERE `" . $_REQUEST['pid'] . "`=?");
        $updateimg->execute(array($uimg, $_REQUEST['id']));
    } else {
        if (is_dir($_REQUEST['path'] . 'thump/'))
            unlink($_REQUEST['path'] . 'thump/' . $_REQUEST['image']);
        if (is_dir($_REQUEST['path'] . 'big/'))
            unlink($_REQUEST['path'] . 'big/' . $_REQUEST['image']);
        if (is_dir($_REQUEST['path'] . 'small/'))
            unlink($_REQUEST['path'] . 'small/' . $_REQUEST['image']);
        unlink($_REQUEST['path'] . $_REQUEST['image']);

        $updateimg = $db->prepare("UPDATE `" . $_REQUEST['table'] . "` SET `" . $_REQUEST['images'] . "`=? WHERE `" . $_REQUEST['pid'] . "`=?");
        $updateimg->execute(array('', $_REQUEST['id']));
    }

    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`,`info`) VALUES (?,?,?,?,?,?,?)");
    $htry->execute(array($_REQUEST['table'], 9, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $_REQUEST['id'], 'Image Deletion'));

    echo '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i>Succesfully Deleted</h4></div>';
}

if ($_REQUEST['delimag'] != '') {
    $sel1 = $db->prepare("SELECT * FROM `imageup` WHERE `aiid`=?");
    $sel1->execute(array($_REQUEST['delimag']));
    $sel = $sel1->fetch();
    unlink("../../images/imageup/" . $sel['image']);

    $get = $db->prepare("DELETE FROM  `imageup` WHERE `aiid` =? ");
    $get->execute(array(trim($_REQUEST['delimag'])));

    //DB("DELETE FROM `addimages` WHERE `aid`='".$sel['aid']."'");

    echo '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i>Succesfully Deleted, Image will be deleted in few seconds</h4></div>';

    echo '<meta http-equiv="refresh" content="3;url=' . $sitename . 'others/viewimage.htm/' . '">';
    exit;
}


if ($_REQUEST['newssubmit'] != '') {
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
    $msgg = addsubscribe($_REQUEST['newssubmit'], $ip);
    echo $msgg;
}

if ($_REQUEST['changestate'] != '') {
    @extract($_REQUEST);
    ?>
    <select name="shipcity" id="shipcity" required="required" class="form-control" >
        <option value="">Select City</option>
        <?php
        $cst = pFETCH("SELECT * FROM `shipping_city` WHERE `status`=? AND `ship_state`=?", '1', $changestate);
        while ($fcst = $cst->fetch(PDO::FETCH_ASSOC)) {
            ?>
            <option value="<?php echo $fcst['scid']; ?>"><?php echo $fcst['shipping_city']; ?></option>
            <?php
        }
        ?>
    </select>
    <?php
}

if ($_POST['attribute']) {
    $nbays_attribute = $db->prepare("SELECT * FROM `subcategory` WHERE `scid`=?");
    $nbays_attribute->execute(array($_POST['attribute']));
    $checkcat = $nbays_attribute->fetch(PDO::FETCH_ASSOC);
    // $checkcat = DB_QUERY("SELECT * FROM `subcategory` WHERE `scid`='" . $_POST['attribute'] . "'");
    if ($checkcat['attributeset'] != '') {
        ?>
        <div class="panel" style="width: 100%; margin-bottom: 10px;  alignment-baseline: central">
            <div class="panel-heading" style="background-color:#f95483">
                <div class="panel-title" style="font-size:25px;color:white;font-weight:bold;">
                    Specification(s)
                </div></div>
        </div>
        <div class="panel panel-info" style="width: 100%;  alignment-baseline: central">
            <div class="panel-body color">
                <div class="row">
                    <?php
                    $serve = explode(',', $checkcat['attributeset']);
                    if (count($serve) > 0) {
                        $y = 0;
                        foreach ($serve as $i => $servicesname) {
                            $nbays_attribute_type = $db->prepare("SELECT * FROM `attributetype` where `attribute`=?");
                            $nbays_attribute_type->execute(array($servicesname));
                            //  $attrtype = DB("SELECT * FROM `attributetype` where `attribute`='" . $servicesname . "'");
                            while ($rescheck = $nbays_attribute_type->fetch(PDO::FETCH_ASSOC)) {
                                $attvalue = explode(',', $rescheck['value']);
                                $y++;
                                ?>
                                <div class="col-md-6">
                                    <select  id="attvalue" name="attvalue[]">
                                        <option value="">Select <?php echo $rescheck['attributtitle']; ?></option>
                                        <?php
                                        $attrval = explode(',', $rescheck['value']);
                                        foreach ($attrval as $i => $j) {
                                            ?>
                                            <option value="<?php echo $rescheck['attributtitle'] . '###' . $j; ?>"><?php echo $j; ?></option>  
                                        <?php } ?>     
                                    </select>
                                </div>
                                <?php
                                if ($y == '2') {
                                    echo '</div><br /><div class="row">';
                                    $y = '0';
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
}

if ($_POST['attribute1']) {
    $nbays_attribute1 = $db->prepare("SELECT * FROM `subcategory` WHERE `scid`=?");
    $nbays_attribute1->execute(array($_POST['attribute1']));
    $checkcat = $nbays_attribute1->fetch(PDO::FETCH_ASSOC);
    // $checkcat = DB_QUERY("SELECT * FROM `subcategory` WHERE `scid`='" . $_POST['attribute1'] . "'");
    if ($checkcat['attributeset'] != '') {
        ?> <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Specification(s)</div>
            </div>
            <div class="panel-body">

                <div class="row">
                    <?php
                    $serve = explode(',', $checkcat['attributeset']);
                    if (count($serve) > 0) {
                        $y = 0;
                        foreach ($serve as $i => $servicesname) {
                            $nbays_attribute1_type = $db->prepare("SELECT * FROM `attributetype` where `attribute`=?");
                            $nbays_attribute1_type->execute(array($servicesname));
                            //  $attrtype = DB("SELECT * FROM `attributetype` where `attribute`='" . $servicesname . "'");
                            while ($rescheck = $nbays_attribute1_type->fetch(PDO::FETCH_ASSOC)) {
                                $attvalue = explode(',', $rescheck['value']);
                                $y++;
                                ?>
                                <div class="col-md-6">
                                    <select class="form-control" id="attvalue" name="attvalue[]">
                                        <option value="">Select <?php echo $rescheck['attributtitle']; ?></option>
                                        <?php
                                        $attrval = explode(',', $rescheck['value']);
                                        foreach ($attrval as $i => $j) {
                                            ?>
                                            <option value="<?php echo $rescheck['attributtitle'] . '###' . $j; ?>"><?php echo $j; ?></option>  
                                        <?php } ?>     
                                    </select>
                                </div>
                                <?php
                                if ($y == '2') {
                                    echo '</div><br /><div class="row">';
                                    $y = '0';
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
}

if ($_POST['cur'] != '') {
    $_SESSION['FRONT_WEB_CURRENCY'] = $_POST['cur'];
    echo json_encode(array('suc'));
    exit;
}

if ($_REQUEST['logistics'] != '') {
global $db;
      pFETCH("UPDATE `norder` SET `logistic`=? WHERE `oid`=?", $_REQUEST['logistics'], $_REQUEST['orderid']);
      $message = "Admin Assigned one order to Delivery";
      
      $notif1 = $db->prepare("INSERT INTO `notification` SET `orderid`=?,`from`=?,`to`=?,`type`=?,`message`=?");
$notif1->execute(array($_REQUEST['orderid'],'admin',$_REQUEST['logistics'],'logistics',$message));


       echo '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-check"></i> Logistic Updated Successfully!!! </div>';

}
if ($_REQUEST['deliverstatus'] != '') {

    $norder = FETCH_all("SELECT * FROM `norder` WHERE `oid`=?", $_REQUEST['orderid']);

    $general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');

    $from = $general['recoveryemail'];

    pFETCH("UPDATE `norder` SET `delivered_status`=? WHERE `oid`=?", $_REQUEST['deliverstatus'], $_REQUEST['orderid']);


$msgs="Deliver Status Changed for ".$norder['cus_name'].' Order';
$notif = $db->prepare("INSERT INTO `notification` SET `userid`=?,`orderid`=?,`from`=?,`to`=?,`type`=?,`message`=?");
$notif->execute(array($_SESSION['UID'],$_REQUEST['orderid'],'logistic','admin','order',$msgs));

$notif1 = $db->prepare("INSERT INTO `notification` SET `userid`=?,`orderid`=?,`from`=?,`to`=?,`type`=?,`message`=?");
$notif1->execute(array($_SESSION['UID'],$_REQUEST['orderid'],'logistic',$norder['vendor'],'vendor',$msgs));

    
    
 $ostatus = $_REQUEST['deliverstatus'];
    
        $name = $norder['cus_name'];
        $cemail =$norder['email'];
    


    $type = $type ;
    $ip = $_SERVER['REMOTE_ADDR'];
    $message = 'Hi';
     $noumbers = $norder['mobile'];

    //sendsms($message, $noumbers, $ip, $type); 

    $to = $cemail;

    $subject = 'Your Tailor Has Been Updated ' . $norder['order_id'];

    $mailcontent = '<table style="width:600px; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Deliver Status Changed</h2></td>
                </tr>
                <tr>
                    <td><p><b>Hi ' . $name . '</b>,</p><p>An order you recently placed on our website has had its status changed.</p><p>The status of order ' . $norder['order_id'] . ' is now ' . $ostatus . '</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%;" align="left">
                                    <h4>Order Number : ' . $norder['order_id'] . '</h4>
                                    <p>Order Date : ' . date('d-M-Y H:i:s A', strtotime($norder['datetime'])) . '</p>
                                    <p>Payment Method : ' . $norder['payment_mode'] . '</p>                                    
                                </td>
                                <td width="40%;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%;">
                                    <a href="http://www.tailor.in/" target="_blank">Tailor</a>
                                </td>
                                <td width="50%;" align="right"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';

    sendgridApiMail($to, $mailcontent, $subject, $from, '', '');

    if ($_REQUEST['sendmail'] == '1') {
        sendoldmail($subject, $mailcontent, $from, $to);
    }
    echo '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-check"></i> Order Updated Successfully!!! </div>';
}

if ($_REQUEST['orderstatus'] != '') {

    $norder = FETCH_all("SELECT * FROM `norder` WHERE `oid`=?", $_REQUEST['orderid']);

    $general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');

    $from = $general['recoveryemail'];

 $cudate=date('Y-m-d');   
if($orderstatus=='1') {
    pFETCH("UPDATE `norder` SET `order_status`=? ,`deliver_date`=? WHERE `oid`=?", $_REQUEST['orderstatus'],$cudate, $_REQUEST['orderid']);
   
}
elseif($orderstatus=='2') {
    pFETCH("UPDATE `norder` SET `order_status`=? ,`return_date`=? WHERE `oid`=?", $_REQUEST['orderstatus'],$cudate, $_REQUEST['orderid']);
   
}
elseif($orderstatus=='3') {
    pFETCH("UPDATE `norder` SET `order_status`=? ,`cancel_date`=? WHERE `oid`=?", $_REQUEST['orderstatus'],$cudate, $_REQUEST['orderid']);
   
}
else {
    pFETCH("UPDATE `norder` SET `order_status`=? WHERE `oid`=?", $_REQUEST['orderstatus'],$_REQUEST['orderid']);
   
}
    

$msgs="Order Status Changed for ".$norder['cus_name'].' Order';
$notif = $db->prepare("INSERT INTO `notification` SET `userid`=?,`orderid`=?,`from`=?,`to`=?,`type`=?,`message`=?");
$notif->execute(array($_SESSION['UID'],$_REQUEST['orderid'],'vendor',$norder['vendor'],'order',$msgs));

    
    if ($_REQUEST['orderstatus'] == '0') {
        $ostatus = 'Awaiting for Payment';
        $type = 1;
    } elseif ($_REQUEST['orderstatus'] == '1') {
        $ostatus = 'Awaiting Fulfillment';
        $type = 3;
    } elseif ($_REQUEST['orderstatus'] == '2') {
        $ostatus = 'Completed';
         $type = 4;
    } elseif ($_REQUEST['orderstatus'] == '3') {
        $ostatus = 'Cancelled';
 $type = 5;
    } elseif ($_REQUEST['orderstatus'] == '4') {
        $ostatus = 'Declined';
 $type = 6;
    } elseif ($_REQUEST['orderstatus'] == '5') {
        $ostatus = 'Refunded';
 $type = 7;
    } elseif ($_REQUEST['orderstatus'] == '6') {
        $ostatus = 'Partially Refunded';
 $type = 8;
    }

    
        $name = $norder['cus_name'];
        $cemail =$norder['email'];
    


    $type = $type ;
    $ip = $_SERVER['REMOTE_ADDR'];
    $message = 'Hi';
     $noumbers = $norder['mobile'];

   // sendsms($message, $noumbers, $ip, $type); 

    $to = $cemail;

    $subject = 'Your Tailor Has Been Updated ' . $norder['order_id'];

    $mailcontent = '<table style="width:600px; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Order Status Changed</h2></td>
                </tr>
                <tr>
                    <td><p><b>Hi ' . $name . '</b>,</p><p>An order you recently placed on our website has had its status changed.</p><p>The status of order ' . $norder['order_id'] . ' is now ' . $ostatus . '</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%;" align="left">
                                    <h4>Order Number : ' . $norder['order_id'] . '</h4>
                                    <p>Order Date : ' . date('d-M-Y H:i:s A', strtotime($norder['datetime'])) . '</p>
                                    <p>Payment Method : ' . $norder['payment_mode'] . '</p>                                    
                                </td>
                                <td width="40%;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%;">
                                    <a href="http://www.tailor.in/" target="_blank">Tailor</a>
                                </td>
                                <td width="50%;" align="right"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';

    sendgridApiMail($to, $mailcontent, $subject, $from, '', '');

    if ($_REQUEST['sendmail'] == '1') {
        sendoldmail($subject, $mailcontent, $from, $to);
    }
    echo '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="icon fa fa-check"></i> Order Updated Successfully!!! </div>';
}

if (($_REQUEST['qucickview'] == 'yes') && ($_REQUEST['getproductid'] != '')) {
    $sentdata = array();
    global $fsitename;

    $showproduct = DB_QUERY("SELECT * FROM `product` WHERE `pid`='" . $_REQUEST['getproductid'] . "'");
    $im = explode(",", $showproduct['image']);
    $path = $_SERVER['DOCUMENT_ROOT'] . "/front_store/images/products/productImages/";
    if ($im[0] != '') {

        $sentdata['imageurl'] = $fsitename . '/images/product/' . $showproduct['productname'] . '/thump/' . $im[0];
    } else {
        $sentdata['imageurl'] = $fsitename . 'images/noimage1.png';
    }
    $sentdata['englishname'] = $showproduct['productname'];
    $sentdata['tamilname'] = $showproduct['tamilproductname'];
    $sentdata['shortdes'] = $showproduct['sortdescription'];

    if ($showproduct['attribute_group'] != '') {
        $stringattribute = '';

        $getattribute = FETCH_all("SELECT * FROM `productattribute` WHERE `pid` = ?", $_REQUEST['getproductid']);
        $atribuarray = explode(',', $getattribute['attributes']);

        foreach ($atribuarray as $atribuarray1) {

            $getattributevalue = FETCH_all("SELECT * FROM `attribute_value` WHERE `vid` = ?", $atribuarray1);
            $getattribute = FETCH_all("SELECT * FROM `attribute` WHERE `id` = ?", $getattributevalue['valid']);
            if (($getattribute['name'] != '') && ($getattributevalue['value'] != '')) {

                $stringattribute .= '<div class="product-overview"><div class="overview-content"><span style="color:#0091fc;">' . $getattribute['name'] . ': </span>' . $getattributevalue['value'] . '</div></div>';
            }
        }
    }
    $sentdata['categoryname'] = getcategory('category', $showproduct['cid']);
    $sentdata['attributelist'] = $stringattribute;
    if ($showproduct['assitor'] != '') {
        $sentdata['assitor'] = getassitors('name', $showproduct['assitor']);
    }
    $sentdata['brandname'] = getbrand('bname', $showproduct['brand']);
    if ($sentdata['sprice'] != '') {
        $sentdata['sprice'] = "₹&nbsp;" . $showproduct['sprice'];
    } else {
        $sentdata['price'] = "₹&nbsp;" . $showproduct['price'];
    }
    if ($showproduct['total_Availability'] != '0') {
        $tavail = "InStock";
    } else {
        $tavail = "OutOfStock";
    }
    $sentdata['availability'] = $tavail;
    echo json_encode($sentdata);
}


if (($_REQUEST['facebooklogin'] == 'yes') && ($_REQUEST['facebookid'] != '')) {
    $sentdata = array();

    global $fsitename;
    $getfaname = explode(' ', $_REQUEST['name']);

    $fname = $getfaname[0];
    $mname = $getfaname[1];
    $lname = $getfaname[2];
    $facebookid = $_REQUEST['facebookid'];
    $signupemail = $_REQUEST['email'];
    $address1 = '';
    $address2 = '';
    $city = '';
    $state = '';
    $zipcode = '';
    $country = '';
    $mobileno = '';
    $phoneno = '';
    $ip = $_SERVER['REMOTE_ADDR'];
    $signuppassword = '';
    global $db;
    global $fsitename;
    $chkacct = FETCH_all("SELECT * FROM `customer` WHERE `E-mail`=? ", $signupemail);

    $chk = FETCH_all("SELECT * FROM `customer` WHERE `E-mail`=? ", $signupemail);

    if (($chkacct['CusID'] != '') && ($chkacct['E-mail'] != '') && ($chkacct['Mobile'] == '')) {


        if ($chkacct['facebookid'] == '') {
            FETCH_all("UPDATE `customer` SET `facebookid`=? WHERE `E-mail`=? AND `CusID`=?  ", $_REQUEST['facebookid'], $signupemail, $chkacct['CusID']);
        }
        $_SESSION['FUID'] = $chkacct['CusID'];
    } else if (($chk['CusID'] == '') && ($chk['E-mail'] == '') && ($chk['Mobile'] == '')) {

        $chk = $db->prepare("INSERT INTO `customer` (`vendor`,`FirstName`,`LastName`,`MiddleName`,`facebookid`,`Adderss_1`,`Address_2`,`City`,`State`,`Postcode`,`Country`,`Mobile`,`Phone`,`E-mail`,`Password`,`IP`,`Status`,`signupvia`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        $chk->execute(array($_SESSION['UID'],$fname, $lname, $mname, $facebookid, $address1, $address2, $city, $state, $zipcode, $country, $mobileno, $phoneno, $signupemail, '', $ip, '1', $_SERVER['HTTP_USER_AGENT']));
        $_SESSION['FUID'] = $db->lastInsertId();
        if ($newssubscribe == '1') {
            $chk3 = FETCH_all("SELECT `id` FROM `newsletter` WHERE `email`=?", $signupemail);
            if ($chk3['id'] == '') {
                $chk = $db->prepare("INSERT INTO `newsletter` (`name`,`cusid`,`email`,`ip`) VALUES (?,?,?,?)");
                $chk->execute(array($fname, $_SESSION['FUID'], $signupemail, $ip));
            }
        }

        $general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
        $to = $general['recoveryemail'];

        $subject = "New User Registered - Click2buy.in";

        $message = '<table width="600" border="0" cellspacing="0" cellpadding="0" style="vertical-align: top;border:5px solid #000000; background:#fff">
                    <thead>
                    <th style="width:30%;">
                    <img src="' . $fsitename . 'images/' . $general['image'] . '" height="50px" border="0" />
                    </th>
                    <th  style="width:70%;font-family:Arial, Helvetica, sans-serif; color:#000; vertical-align: bottom; font-weight:bold; font-size:15px;">
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#000; font-weight:bold;">New User Registered - Click2buy.in</span>
                    </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding:5px;" colspan="3">
                            Hello Admin,
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;" colspan="3">&nbsp;</td>                        
                    </tr> 
                    <tr>
                        <td style="padding:5px;vertical-align: top;" >Name :</td>
                        <td style="padding:5px;" colspan="2">' . $fname . ' ' . $mname . '</td>
                    </tr>
                    <tr>
                        <td style="padding:5px;vertical-align: top;" >Email :</td>
                        <td style="padding:5px;" colspan="2">' . $signupemail . '</td>
                    </tr>
                    <tr>
                        <td style="padding:5px;vertical-align: top;" >Contact Number :</td>
                        <td style="padding:5px;" colspan="2">' . $mobileno . '</td>
                    </tr>
                    <tr>
                        <td style="padding:5px;" colspan="3">New user has been registered to our site today .Please provide proper services to our most valueable customer  .</td>
                    </tr>
                    <tr>
                        <td style="height:20px;" colspan="3">&nbsp;</td>                        
                    </tr>
                    <tr>
                        <td height="26" colspan="3" align="center" valign="middle" bgcolor="#333333" style="font-family:Arial, Gadget, sans-serif; font-size:10px;    vertical-align: middle; font-weight:normal; color:#fff;">&copy; ' . date('Y') . ' Click2buy All Rights Reserved.&nbsp;</td>
                    </tr>
                    </tbody>
                </table>';

        sendgridmail($to, $message, $subject, $signupemail, '', '');

        $to1 = $signupemail;
        $subject1 = "Thanks for Register with Click2buy";

        $message1 = '<table width="600" border="0" cellspacing="0" cellpadding="0" style="vertical-align: top;border:5px solid #000000; background:#fff">
                    <thead>
                    <th style="width:30%;">
                    <img src="' . $fsitename . 'images/' . $general['image'] . '" height="50px" border="0" />
                    </th>
                    <th  style="width:70%;font-family:Arial, Helvetica, sans-serif; color:#000; vertical-align: bottom; font-weight:bold; font-size:15px;">
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#000; font-weight:bold;">New User Registered - Click2buy.in</span>
                    </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td align="left" valign="top" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="padding:5px;" colspan="3">
                            Hello ' . $fname . ' ' . $mname . ' ' . $lname . ',
                        </td>
                    </tr>
                    <tr>
                        <td style="height:20px;" colspan="3">&nbsp;</td>                        
                    </tr> 
                    <tr>
                        <td style="padding:5px;" colspan="3">Thank you for Register with us</td>
                    </tr>
                    <tr>
                        <td style="height:20px;" colspan="3">&nbsp;</td>                        
                    </tr>
                    <tr>
                        <td height="26" colspan="3" align="center" valign="middle" bgcolor="#333333" style="font-family:Arial, Gadget, sans-serif; font-size:10px;    vertical-align: middle; font-weight:normal; color:#fff;">&copy; ' . date('Y') . ' Click2buy All Rights Reserved.&nbsp;</td>
                    </tr>
                    </tbody>
                </table>';

        sendgridmail($to1, $message1, $subject1, $general['recoveryemail'], '', '');
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Registered Successfully </h4></div>';
    }

    $sentdata['categoryname'] = 'hai';
    echo json_encode($sentdata);
}
?>