<?php

session_start();
ob_start();
error_reporting(0);
//ini_set('display_errors', '1');
//error_reporting(E_ALL);
if ($_SERVER['HTTP_HOST'] == 'localhost') {
    $db = new PDO('mysql:host=localhost;dbname=tailor;charset=utf8mb4', 'root', '');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $connection_string = mysqli_connect("localhost", "root", "", "tailor");
    $sitename = 'http://localhost/tailor/admin/';
    $fsitename = 'http://localhost/tailor/';
    $actual_link = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    define('WEB_ROOT', $fsitename);
    $docroot = $_SERVER['DOCUMENT_ROOT'] . "/tailor/";
    //define('ADMIN_ROOT', WEB_ROOT."admin/");
} else {
    $db = new PDO('mysql:host = localhost;dbname=demowebt_tailors;charset=utf8', 'demowebt_tailors', 'mR~K8vE}Ba]T');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $connection_string = mysqli_connect("localhost", "demowebt_tailors", "mR~K8vE}Ba]T", "demowebt_tailors");
    $sitename = "https://demo.webtoall.in/tailors/admin/";
    $site = 'https://demo.webtoall.in/tailors/';
    $fsitename = 'https://demo.webtoall.in/tailors/';
    define('WEB_ROOT', $fsitename);
    define('WEB_ROOTS', $fsitename);
    $docroot = $_SERVER['DOCUMENT_ROOT'] . '/';
    $actual_link = "http://" . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI];
    $secret = "6LfRFRgTAAAAAEIX-CbWDAY5e1Bq2juraDX2MB4";
    $sitekey = '<div class="g-recaptcha" data-sitekey="6LfRFRgTAAAAAJ26xzimG1TK0ApiWbcz2DkD-oks"></div>';
}

ini_set('date.timezone', 'Asia/Calcutta');
date_default_timezone_set('Asia/Calcutta');
ini_set('session.gc-maxlifetime', 36000);
ini_set('upload_max_filesize', '100M');
ini_set('post_max_size', '500M');
ini_set('memory_limit', '250M');
ini_set('max_input_time', '4500');
ini_set('max_execution_time', '3000');

function DB($sql) {
    global $connection_string;
    $result = mysqli_query($connection_string, $sql);
    return $result;
}

function DB_QUERY($sql) {
    global $connection_string;
    $result = mysqli_fetch_array(mysqli_query($connection_string, $sql));
    return $result;
}

function DB_NUM($sql) {
    global $connection_string;
    $result = mysqli_num_rows(mysqli_query($connection_string, $sql));
    return $result;
}

if (!function_exists('mysql_query')) {

    function mysql_query($sql) {
        global $connection_string;
        $result = mysqli_query($connection_string, $sql);
        return $result;
    }

}

if (!function_exists('mysql_fetch_array')) {

    function mysql_fetch_array($sql) {
        global $connection_string;
        $result = mysqli_fetch_array($sql);
        return $result;
    }

}

if (!function_exists('mysql_fetch_row')) {

    function mysql_fetch_row($sql) {
        global $connection_string;
        $result = mysqli_fetch_row($sql);
        return $result;
    }

}

if (!function_exists('mysql_num_rows')) {

    function mysql_num_rows($sql) {
        global $connection_string;
        $result = mysqli_num_rows($sql);
        return $result;
    }

}
if (!function_exists('mysql_insert_id')) {

    function mysql_insert_id() {
        global $connection_string;
        $result = mysqli_insert_id($connection_string);
        return $result;
    }

}
if (!function_exists('mysql_real_escape_string')) {

    function mysql_real_escape_string($val) {
        global $connection_string;
        $result = mysqli_real_escape_string($connection_string, $val);
        return $result;
    }

}
if (!function_exists('mysql_escape_string')) {

    function mysql_escape_string($val) {
        global $connection_string;
        $result = mysqli_real_escape_string($connection_string, $val);
        return $result;
    }

}
if (!function_exists('mysql_error')) {

    function mysql_error() {
        global $connection_string;
        $result = mysqli_error($connection_string);
        return $result;
    }

}
if (!function_exists('mysql_num_fields')) {

    function mysql_num_fields($val) {
        global $connection_string;
        $result = mysqli_num_fields($val);
        return $result;
    }

}
if (!function_exists('mysql_field_name')) {

    function mysql_field_name($val, $i) {
        global $connection_string;
        $result = mysqli_fetch_field_direct($val, $i);
        return $result->name;
    }

}

if (!function_exists('mysql_close')) {

    function mysql_close($val) {
        global $connection_string;
        $result = mysqli_close($connection_string);
        return $result;
    }

}

if (isset($_REQUEST['show'])) {
    $ns = DB("SHOW TABLES");
    while ($fs = mysql_fetch_array($ns)) {
        
    }
}

function pFETCH($sql, $a = '', $b = '', $c = '', $d = '', $e = '', $f = '', $g = '', $h = '', $i = '', $j = '') {
    global $db;
    $stmt3 = $db->prepare($sql);
    if (($j == '') && ($i == '') && ($h == '') && ($g == '') && ($f == '') && ($e == '') && ($d == '') && ($c == '') && ($b == '')) {
        $a = str_replace('null', '', $a);
        $stmt3->execute(array($a));
    } elseif (($j == '') && ($i == '') && ($h == '') && ($g == '') && ($f == '') && ($e == '') && ($d == '') && ($c == '')) {
        $a = str_replace('null', '', $a);
        $b = str_replace('null', '', $b);
        $stmt3->execute(array($a, $b));
    } elseif (($j == '') && ($i == '') && ($h == '') && ($g == '') && ($f == '') && ($e == '') && ($d == '')) {
        $a = str_replace('null', '', $a);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $stmt3->execute(array($a, $b, $c));
    } elseif (($j == '') && ($i == '') && ($h == '') && ($g == '') && ($f == '') && ($e == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $stmt3->execute(array($a, $b, $c, $d));
    } elseif (($j == '') && ($i == '') && ($h == '') && ($g == '') && ($f == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $stmt3->execute(array($a, $b, $c, $d, $e));
    } elseif (($j == '') && ($i == '') && ($h == '') && ($g == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f));
    } elseif (($j == '') && ($i == '') && ($h == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $g = str_replace('null', '', $g);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f, $g));
    } elseif (($j == '') && ($i == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $g = str_replace('null', '', $g);
        $h = str_replace('null', '', $h);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f, $g, $h));
    } elseif (($j == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $g = str_replace('null', '', $g);
        $h = str_replace('null', '', $h);
        $i = str_replace('null', '', $i);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f, $g, $h, $i));
    } else {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $g = str_replace('null', '', $g);
        $h = str_replace('null', '', $h);
        $i = str_replace('null', '', $i);
        $j = str_replace('null', '', $j);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f, $g, $h, $i, $j));
    }
    return $stmt3;
}

function FETCH_all($sql, $a = '', $b = '', $c = '', $d = '', $e = '', $f = '', $g = '') {
    global $db;
    $stmt3 = $db->prepare($sql);
    if (($g == '') && ($f == '') && ($e == '') && ($d == '') && ($c == '') && ($b == '')) {
        $a = str_replace('null', '', $a);
        $stmt3->execute(array($a));
    } elseif (($g == '') && ($f == '') && ($e == '') && ($d == '') && ($c == '')) {
        $a = str_replace('null', '', $a);
        $b = str_replace('null', '', $b);
        $stmt3->execute(array($a, $b));
    } elseif (($g == '') && ($f == '') && ($e == '') && ($d == '')) {
        $a = str_replace('null', '', $a);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $stmt3->execute(array($a, $b, $c));
    } elseif (($g == '') && ($f == '') && ($e == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $stmt3->execute(array($a, $b, $c, $d));
    } elseif (($g == '') && ($f == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $stmt3->execute(array($a, $b, $c, $d, $e));
    } elseif (($g == '')) {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f));
    } else {
        $a = str_replace('null', '', $a);
        $d = str_replace('null', '', $d);
        $b = str_replace('null', '', $b);
        $c = str_replace('null', '', $c);
        $e = str_replace('null', '', $e);
        $f = str_replace('null', '', $f);
        $g = str_replace('null', '', $g);
        $stmt3->execute(array($a, $b, $c, $d, $e, $f, $g));
    }
    $per = $stmt3->fetch(PDO::FETCH_ASSOC);
    return $per;
}

function strip_tags_attributes($string, $allowtags = NULL, $allowattributes = NULL) {
    $string = strip_tags($string, $allowtags);
    if (!is_null($allowattributes)) {
        if (!is_array($allowattributes))
            $allowattributes = explode(",", $allowattributes);
        if (is_array($allowattributes))
            $allowattributes = implode(")(?<!", $allowattributes);
        if (strlen($allowattributes) > 0)
            $allowattributes = "(?<!" . $allowattributes . ")";
        $string = preg_replace_callback("/<[^>]*>/i", create_function(
                        '$matches', 'return preg_replace("/ [^ =]*' . $allowattributes . '=(\"[^\"]*\"|\'[^\']*\')/i", "", $matches[0]);'
                ), $string);
    }
    return $string;
}

function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return $ext;
}

function getClientIP() {

    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        return trim(end(explode(",", $_SERVER["HTTP_X_FORWARDED_FOR"])));
    } else if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
        return $_SERVER["REMOTE_ADDR"];
    } else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } else {
        return '';
    }
}

include_once ('menuconfig.php');
include_once ('mail/sendgrid-php-example.php');
include_once ('mail2/sendgrid-php.php');
include_once ('functions_master.php');
include_once ('functions_com.php');
include_once ('functions_frontend.php');
include_once ('functions_settings.php');
include_once ('functions_shipping.php');
include_once ('functions_products.php');
include_once ('functions_cart.php');
include_once ('functions_blog.php');
include_once ('uploadimage.php');

function formatInIndianStyle($num) {

    return number_format((float) $num, 3, '.', '');
}

function postedago($time) {

    $time = time() - $time; // to get the time since that moment
    //echo $time;
    $time = ($time < 1) ? 1 : $time;
    $tokens = array(
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit)
            continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
    }
}

// if (!isset($_SESSION['FRONT_WEB_CURRENCY'])) {
//     $_SESSION['FRONT_WEB_CURRENCY'] = (getprofile('Currency1', '1') != '' && getprofile('Currency1', '1') != '0') ? getcurrency('code', getprofile('Currency1', '1')) : 'AUD';
// }
?>