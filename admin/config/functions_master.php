<?php

function getbanner($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `banner` WHERE `bid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addbanner($title, $link, $description, $image, $imagename, $imagealt, $order, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `banner` WHERE `imagename`=?", $imagename);
        if ($link22['imagename'] == '') {
            $resa = $db->prepare("INSERT INTO `banner` ( `title`, `link`,`content`, `image_alt`,`imagename`,`image`,`Order`, `status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($title, $link, $description, $imagename, $imagealt, $image, $order, $status, $ip, $_SESSION['UID']));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Image Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `banner` WHERE `imagename`=? AND `bid`!=?", $imagename, $getid);
        if ($link22['imagename'] == '') {
            $resa = $db->prepare("UPDATE `banner` SET `title`=?, `link`=?,`content`=?, `image`=?,`imagename`=?,`image_alt`=?,`Order`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `bid`=?");
            $resa->execute(array(trim($title), trim($link), trim($description), trim($image), trim($imagename), trim($imagealt), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Banner Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Image Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delbanner($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $getc = FETCH_all("SELECT `image` FROM `banner` WHERE `bid`=?", $c);
        unlink('../../images/banner/' . $getc['image']);
        
       $getorderno = FETCH_all("SELECT * FROM `banner` WHERE `id`=?", $c);
       $get1 = $db->prepare("UPDATE `banner` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
        
        
        $get = $db->prepare("DELETE FROM `banner` WHERE `bid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function gettailor($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `tailor` WHERE `uid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addtailor($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `tailor` WHERE `emailid`=?", $emailid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("INSERT INTO `tailor` ( `name`, `emailid`,`address`, `mobile`,`username`,`password`,`status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $_SESSION['UID']));
             $insertid = $db->lastInsertId();
			 $resa1 = $db->prepare("INSERT INTO `users` (`type`,`orgpassword`,`name`, `emailid`,`address`, `mobile`,`usergroup`,`val1`,`val2`,`status`,`val3`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
             $resa1->execute(array('tailor',$password,$name, $emailid, $address, $mobile,$insertid, $username, md5($password), $status,$status));
 
			$res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `tailor` WHERE `emailid`=? AND `uid`!=?", $emailid, $getid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("UPDATE `tailor` SET `name`=?, `emailid`=?,`address`=?, `mobile`=?,`username`=?,`password`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `uid`=?");
            $resa->execute(array(trim($name), trim($emailid), trim($address), trim($mobile), trim($username), trim($password), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $resa1 = $db->prepare("UPDATE `users` SET `orgpassword`=?, `val1`=?,`val2`=?, `status`=?,`val3`=? WHERE `type`=? and `usergroup`=? ");
            $resa1->execute(array(trim($password), $username, md5($password), $status,$status,'tailor', $getid));
 
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('tailor Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    }
    return $res;
}

function deltailor($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        // $getc = FETCH_all("SELECT `image` FROM `tailor` WHERE `uid`=?", $c);
        // unlink('../../images/banner/' . $getc['image']);
        $get = $db->prepare("DELETE FROM `tailor` WHERE `uid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function getprofessional($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `professional` WHERE `uid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addprofessional($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `professional` WHERE `emailid`=?", $emailid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("INSERT INTO `professional` ( `name`, `emailid`,`address`, `mobile`,`username`,`password`,`status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $_SESSION['UID']));
			
			$insertid = $db->lastInsertId();
			 $resa1 = $db->prepare("INSERT INTO `users` (`type`,`orgpassword`,`name`, `emailid`,`address`, `mobile`,`usergroup`,`val1`,`val2`,`status`,`val3`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
             $resa1->execute(array('professional',$password,$name, $emailid, $address, $mobile,$insertid, $username, md5($password), $status,$status));
 
 
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `professional` WHERE `emailid`=? AND `uid`!=?", $emailid, $getid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("UPDATE `professional` SET `name`=?, `emailid`=?,`address`=?, `mobile`=?,`username`=?,`password`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `uid`=?");
            $resa->execute(array(trim($name), trim($emailid), trim($address), trim($mobile), trim($username), trim($password), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $resa1 = $db->prepare("UPDATE `users` SET `orgpassword`=?, `val1`=?,`val2`=?, `status`=?,`val3`=? WHERE `type`=? and `usergroup`=? ");
            $resa1->execute(array(trim($password), $username, md5($password), $status,$status,'professional', $getid));
			
			
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('professional Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    }
    return $res;
}

function delprofessional($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        // $getc = FETCH_all("SELECT `image` FROM `tailor` WHERE `uid`=?", $c);
        // unlink('../../images/banner/' . $getc['image']);
        $get = $db->prepare("DELETE FROM `professional` WHERE `uid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function getfabric($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `fabric` WHERE `uid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addfabric($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `fabric` WHERE `emailid`=?", $emailid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("INSERT INTO `fabric` ( `name`, `emailid`,`address`, `mobile`,`username`,`password`,`status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $_SESSION['UID']));
            
			 $insertid = $db->lastInsertId();
			 $resa1 = $db->prepare("INSERT INTO `users` (`type`,`orgpassword`,`name`, `emailid`,`address`, `mobile`,`usergroup`,`val1`,`val2`,`status`,`val3`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
             $resa1->execute(array('fabric',$password,$name, $emailid, $address, $mobile,$insertid, $username, md5($password), $status,$status));
 
 
			$res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `fabric` WHERE `emailid`=? AND `uid`!=?", $emailid, $getid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("UPDATE `fabric` SET `name`=?, `emailid`=?,`address`=?, `mobile`=?,`username`=?,`password`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `uid`=?");
            $resa->execute(array(trim($name), trim($emailid), trim($address), trim($mobile), trim($username), trim($password), trim($status), trim($ip), $_SESSION['UID'], $getid));
            
			$resa1 = $db->prepare("UPDATE `users` SET `orgpassword`=?, `val1`=?,`val2`=?, `status`=?,`val3`=? WHERE `type`=? and `usergroup`=? ");
            $resa1->execute(array(trim($password), $username, md5($password), $status,$status,'fabric', $getid));
			
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('professional Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    }
    return $res;
}

function delfabric($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        // $getc = FETCH_all("SELECT `image` FROM `tailor` WHERE `uid`=?", $c);
        // unlink('../../images/banner/' . $getc['image']);
        $get = $db->prepare("DELETE FROM `fabric` WHERE `uid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function getlogistic($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `logistic` WHERE `uid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addlogistic($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `logistic` WHERE `emailid`=?", $emailid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("INSERT INTO `logistic` ( `name`, `emailid`,`address`, `mobile`,`username`,`password`,`status`, `ip`, `Updated_By`) VALUES(?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($name, $emailid, $address, $mobile, $username, $password, $status, $ip, $_SESSION['UID']));
            
			$insertid = $db->lastInsertId();
			 $resa1 = $db->prepare("INSERT INTO `users` (`type`,`orgpassword`,`name`, `emailid`,`address`, `mobile`,`usergroup`,`val1`,`val2`,`status`,`val3`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
             $resa1->execute(array('logistic',$password,$name, $emailid, $address, $mobile,$insertid, $username, md5($password), $status,$status));
 
 
			$res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `logistic` WHERE `emailid`=? AND `uid`!=?", $emailid, $getid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("UPDATE `logistic` SET `name`=?, `emailid`=?,`address`=?, `mobile`=?,`username`=?,`password`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `uid`=?");
            $resa->execute(array(trim($name), trim($emailid), trim($address), trim($mobile), trim($username), trim($password), trim($status), trim($ip), $_SESSION['UID'], $getid));

            $resa1 = $db->prepare("UPDATE `users` SET `orgpassword`=?, `val1`=?,`val2`=?, `status`=?,`val3`=? WHERE `type`=? and `usergroup`=? ");
            $resa1->execute(array(trim($password), $username, md5($password), $status,$status,'logistic', $getid));
			
			
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('professional Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    }
    return $res;
}

function dellogistic($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        // $getc = FETCH_all("SELECT `image` FROM `tailor` WHERE `uid`=?", $c);
        // unlink('../../images/banner/' . $getc['image']);
        $get = $db->prepare("DELETE FROM `logistic` WHERE `uid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function getvendor($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `vendor` WHERE `uid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addvendor($postcode,$sendmail,$bestseller,$image,$name, $emailid, $address, $mobile, $usergroup, $username, $password, $status, $getid) {
    global $db;
    global $sitename;
    global $fsitename;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT * FROM `vendor` WHERE `emailid`=?", $emailid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("INSERT INTO `vendor` (`postcode`,`bestseller`,`image`,`name`, `emailid`,`address`, `mobile`,`username`,`password`,`status`, `ip`, `Updated_By`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($postcode,$bestseller,$image,$name, $emailid, $address, $mobile, $username, $password, $status, $ip, $_SESSION['UID']));
            if($sendmail=='1')
			{
			if($status=='1')
			{
 $general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
       $from = $general['recoveryemail'];
  $to = $emailid;
    
    $subject='Your Lorikeet Status Has Been Updated ';

    $mailcontent = '<table style="width:600px; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Access Status Changed</h2></td>
                </tr>
                <tr>
                    <td><p><b>Hi ' . $name . '</b>,</p><p>An registration you recently placed on our website has had its status changed.</p><p>Now you add your products with below details</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%;" align="left">
                                    <h4>Login URL  : ' . $sitename . '</h4>
                                    <p>Username : ' . $username . '</p>
                                    <p>Password : ' . $password . '</p>
                                  
                                </td>
                                <td width="40%;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%;">
                                    <a href="'.$fsitename.'" target="_blank">Tailor</a>
                                </td>
                                <td width="50%;" align="right"></td>
                        </table>
                    </td>
                </tr>
            </table>';
 sendgridApiMail($to, $mailcontent, $subject, $from, '');	

			}				
			}
			
			 $insertid = $db->lastInsertId();
			 $resa1 = $db->prepare("INSERT INTO `users` (`type`,`orgpassword`,`name`, `emailid`,`address`, `mobile`,`usergroup`,`val1`,`val2`,`status`,`val3`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
             $resa1->execute(array('vendor',$password,$name, $emailid, $address, $mobile,$insertid, $username, md5($password), $status,$status));
 
 
			$res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `vendor` WHERE `emailid`=? AND `uid`!=?", $emailid, $getid);
        if ($link22['emailid'] == '') {
            $resa = $db->prepare("UPDATE `vendor` SET `postcode`=?,`bestseller`=?,`image`=?,`name`=?, `emailid`=?,`address`=?, `mobile`=?,`username`=?,`password`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `uid`=?");
            $resa->execute(array($postcode,$bestseller,$image,trim($name), trim($emailid), trim($address), trim($mobile), trim($username), trim($password), trim($status), trim($ip), $_SESSION['UID'], $getid));


$link221 = FETCH_all("SELECT * FROM `users` WHERE `type`=? AND `usergroup`=?", 'vendor', $getid);

if($link221['id']!='')
{
            $resa1 = $db->prepare("UPDATE `users` SET `orgpassword`=?, `val1`=?,`val2`=?, `status`=?,`val3`=? WHERE `type`=? and `usergroup`=? ");
            $resa1->execute(array(trim($password), $username, md5($password), $status,$status,'vendor', $getid));
}
else
{
	$resa1 = $db->prepare("INSERT INTO `users` (`type`,`orgpassword`,`name`, `emailid`,`address`, `mobile`,`usergroup`,`val1`,`val2`,`status`,`val3`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
             $resa1->execute(array('vendor',$password,$name, $emailid, $address, $mobile,$getid, $username, md5($password), $status,$status));
 
}
			 if($sendmail=='1')
			{
			if($status=='1')
			{
 $general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
       $from = $general['recoveryemail'];
  $to = $emailid;
    
    $subject='Your Tailor Status Has Been Updated ';

    $mailcontent = '<table style="width:600px; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Access Status Changed</h2></td>
                </tr>
                <tr>
                    <td><p><b>Hi ' . $name . '</b>,</p><p>An registration you recently placed on our website has had its status changed.</p><p>Now you add your products with below details</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%;" align="left">
                                    <h4>Login URL  : ' . $sitename . '</h4>
                                    <p>Username : ' . $username . '</p>
                                    <p>Password : ' . $password . '</p>
                                  
                                </td>
                                <td width="40%;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%;">
                                    <a href="http://www.tailor.in/" target="_blank">Tailor</a>
                                </td>
                                <td width="50%;" align="right"></td>
                        </table>
                    </td>
                </tr>
            </table>';
 sendgridApiMail($to, $mailcontent, $subject, $from, '');	

			}				
	
	else
			{
 $general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
       $from = $general['recoveryemail'];
  $to = $emailid;
    
    $subject='Your Tailor Status Has Been Suspended by Admin';

    $mailcontent = '<table style="width:600px; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Access Status Changed</h2></td>
                </tr>
                <tr>
                    <td><p><b>Hi ' . $name . '</b>,</p><p>Your vendor access status changed.</p><p>Contact admin to recover your details</p></td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="60%;" align="left">
                                    <h4>Admin Mailid  : ' . $manageprofile['recoveryemail'] . '</h4>
                                    <p>Contact Number : ' . $manageprofile['phonenumber'] . '</p>
                                    
									
                                </td>
                                <td width="40%;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="50%;">
                                    <a href="http://www.tailor.in/" target="_blank">Tailor</a>
                                </td>
                                <td width="50%;" align="right"></td>
                        </table>
                    </td>
                </tr>
            </table>';
 sendgridApiMail($to, $mailcontent, $subject, $from, '');	
			
			}
		}
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('professional Mgmt', 11, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Emailid already exists!</h4></div>';
        }
    }
    return $res;
}
function delvendor($a)
{
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
       
        $get = $db->prepare("DELETE FROM `vendor` WHERE `uid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}


function addfaq($faqq, $faqa, $image, $imagealt, $imagetitle, $order, $status, $ip, $getid) {
    global $db;
    if ($getid == '') {
        // $link22 = DB_QUERY("SELECT * FROM `banner` WHERE `imagename`='$imagename'");
        $link22 = FETCH_all("SELECT * FROM `faq` WHERE `imagetitle`=?", $imagetitle);
        if ($link22['imagetitle'] == '') {

            $resa = $db->prepare("INSERT INTO `faq` ( `faqquestion`, `faqanswer`,`image`, `imagealt`,`imagetitle`,`order`,`status`,`ip`,`updated_By`) VALUES(?,?,?,?,?,?,?,?,?)");
            $resa->execute(array($faqq, $faqa, $image, $imagealt, $imagetitle, $order, $status, $ip, $_SESSION['UID']));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT * FROM `faq` WHERE `imagetitle`=? AND `faqid`!=?", $imagetitle, $getid);
        if ($link22['imagetitle'] == '') {
            $resa = $db->prepare("UPDATE `faq` SET `faqquestion`=?, `faqanswer`=?,`image`=?,`imagealt`=?,`imagetitle`=?, `order`=?, `status`=?, `ip`=?, `Updated_By`=? WHERE `faqid`=?");
            $resa->execute(array(trim($faqq), trim($faqa), trim($image), trim($imagealt), trim($imagetitle), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Faqs Mgmt', 12, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    }
    return $res;
}

function delfaq($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $get = $db->prepare("DELETE FROM `faq` WHERE `faqid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function getfaq($a, $b) {
    global $db;

    $res = $db->prepare("SELECT `$a` FROM `faq` WHERE `faqid`= ? ");
    $res->execute(array($b));
    $res = $res->fetch();
    return $res[$a];
}

function addstaticpagess($title, $metatitle, $metakeywords, $metadescription, $image_title, $image_alt, $image, $fullcontent, $content2, $content3, $ip, $status, $getid)
{
    global $db;
    $link22 = FETCH_all("SELECT * FROM `static_pages` WHERE `image_title`=?", $imagetitle);
    if ($link22[$image_title] == '') {
        $resa = $db->prepare("UPDATE `static_pages` SET `title`=?, `metatitle`=?,`metakeywords`=?,`metadescription`=?,`image_title`=?, `image_alt`=?,`image`=?,`fullcontent`=?,`ip`=?, `Updated_by`=? WHERE `stid`=?");
        $resa->execute(array(trim($title), trim($metatitle), trim($metakeywords), trim($metadescription), trim($image_title), trim($image_alt), trim($image), trim($fullcontent), trim($ip), $_SESSION['UID'], $getid));

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('staticPages', 28, 'Update', $_SESSION['UID'], $ip, $id));
        //$ress = "UPDATE `static_pages` SET `title`=$title, `metatitle`=$metatitle,`metakeywords`=$metakeywords,`image`=$image, `image_alt`=$image_alt,`image_title`=$image_title,`fullcontent`=$fullcontent,`status`=$status,`ip`=$ip, `Updated_by`= WHERE `stid`=$getid";
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    } else {
        $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
    }

    return $res;
}

function addvideo($title, $link, $order, $status, $ip, $date, $thispageid, $getid)
{
    global $db;
    if ($getid == '') {

        $resa = $db->prepare("INSERT INTO `video` ( `title`, `link`,`order`, `status`, `ip`, `date`,`updated_by`) VALUES(?,?,?,?,?,?,?)");
        $resa->execute(array($title, $link, $order, $status, $ip, $date, $_SESSION['UID']));
        //DB("INSERT INTO `faq` SET `description`='" . stripslashes($description) . "',`seotitle`='" . stripslashes($seotitle) . "',`tags`='" . stripslashes(trim($tags)) . "',`order`='" . stripslashes(trim($order)) . "',`status`='" . stripslashes(trim($status)) . "',`date`='" . $date . "',`ip`='" . $ip . "'");
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
    } else {

        $res1 = $db->prepare("UPDATE `video` SET `title`= ? ,`link`= ? ,`order`= ? ,`status`= ? ,`date`= ? ,`ip`= ?,`updated_by`=? WHERE `vid`= ? ");
        $res1->execute(array(stripslashes($title), stripslashes($link), stripslashes($order), stripslashes($status), stripslashes($date), stripslashes($ip), stripslashes($_SESSION['UID']), stripslashes($getid)));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('video mgmt', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
        
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    }
    return $res;
}

function getvideo($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT `$a` FROM `video` WHERE `vid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delvideo($a) {
    global $db;

    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $get = $db->prepare("DELETE FROM `video` WHERE `vid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function addbrand($brand, $link, $imagename, $imagealt, $image, $metatitle, $metakeywords, $metadescription, $order, $status, $ip, $thispageid, $id) {
    global $db;
    if ($id == '') {

        $resa = $db->prepare("INSERT INTO `brand` ( `vendor`, `bname`, `link`,`image_name`,`imagealt`,`image`,`metakeyword`,`metatitle`,`metadescription`,`IP`,`order`,`status`, `Updated_by` ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $resa->execute(array($_SESSION['UID'], $brand, $link, $imagename, $imagealt, $image, $metakeywords, $metatitle, $metadescription, $ip, $order, $status, $_SESSION['UID']));

        //DB("INSERT INTO `faq` SET `description`='" . stripslashes($description) . "',`seotitle`='" . stripslashes($seotitle) . "',`tags`='" . stripslashes(trim($tags)) . "',`order`='" . stripslashes(trim($order)) . "',`status`='" . stripslashes(trim($status)) . "',`date`='" . $date . "',`ip`='" . $ip . "'");
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
    } else {

        $link22 = FETCH_all("SELECT * FROM `brand` WHERE `image_name`=?", $imagename);
        if ($link22[$imagename] == '') {
            $resa = $db->prepare("UPDATE `brand` SET `bname`=?, `link`=?,`image_name`=?,`imagealt`=?,`image`=?,`metakeyword`=?,`metatitle`=?,`metadescription`=?,`IP`=?,`order`=?,`status`=?, `Updated_by`=? WHERE `brid`=?");
            $resa->execute(array(trim($brand), trim($link), trim($imagename), trim($imagealt), trim($image), trim($metakeywords), trim($metatitle), trim($metadescription), trim($ip), trim($order), trim($status), $_SESSION['UID'], $id));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Brand_mgmt', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
    }
    return $res;
}

function getbrand($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `brand` WHERE `brid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delbrand($a) {
    global $db;

    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
          $getorderno = FETCH_all("SELECT * FROM `brand` WHERE `brid`=?", $c);
       $get1 = $db->prepare("UPDATE `brand` SET `order`=`order`-1 WHERE `order`>'".$getorderno['order']."' ");
       $get1->execute(array());
        
        $get = $db->prepare("DELETE FROM `brand` WHERE `brid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function getstaticpages($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `static_pages` WHERE `stid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function delstaticpages($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('staticPages', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `static_pages` WHERE `stid` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

//function getcontactform($a, $b) {
//    global $db;
//    $get1 = $db->prepare("SELECT * FROM `contact_form` WHERE `coid`=?");
//    $get1->execute(array(trim($b)));
//    $get = $get1->fetch(PDO::FETCH_ASSOC);
//    $res = $get[$a];
//    return $res;
//}
function getfeedbackform($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `feedback` WHERE `fdid`=?");
    $get1->execute(array(trim($b)));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delfeedbackform($a, $thispageid) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);

    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Feedbacck Forms', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `feedback` WHERE `fdid` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}
//
//function delcontactform($a) {
//    global $db;
//    $b = str_replace(".", ",", $a);
//    $b = explode(",", $b);
//    foreach ($b as $c) {
//        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
//        $htry->execute(array('Contact Forms', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
//        $get = $db->prepare("DELETE FROM `contact_form` WHERE `coid` =? ");
//        $get->execute(array(trim($c)));
//    }
//    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
//    return $res;
//}

function delnewsletter($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Newsletter', $thispageid, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($c)));
        $get = $db->prepare("DELETE FROM `newsletter` WHERE `id` =? ");
        $get->execute(array(trim($c)));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}

function addtest($title, $comments, $order, $status, $ip, $thispageid, $getid) {
    global $db;
    if ($getid == '') {

        $resa = $db->prepare("INSERT INTO `testimonial` (`title`, `comments`,`order`,`status`,`ip`, `updated_by`) VALUES(?,?,?,?,?,?)");
        $resa->execute(array($title, $comments, $order, $status, $ip, $_SESSION['UID']));

        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully inserted</h4></div>';
    } else {

        $resa = $db->prepare("UPDATE `testimonial` SET `title`=?, `comments`=?,`order`=?,`status`=?,`ip`=?, `updated_by`=?  WHERE `tid`=?");
        $resa->execute(array(trim($title), trim($comments), trim($order), trim($status), trim($ip), $_SESSION['UID'], $getid));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Testimonial', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
    }
    return $res;
}

function gettest($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `testimonial` WHERE `tid`=?");
    $get1->execute(array(trim($b)));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function getaddress($a, $b) {
    global $db;
    $res = $db->prepare("SELECT * FROM `bill_ship_address` WHERE `CusID`= ? ");
    $res->execute(array($b));
    $res = $res->fetch();
    return $res[$a];
}

function addimage($image, $imagename, $imagealt, $status, $ip, $thispageid, $getid) {
    global $db;
    if ($getid == '') {

        $link23 = $db->prepare("SELECT * FROM `imageup` WHERE `image_name`= ? ");
        $link23->execute(array($imagename));
        $link22 = $link23->fetch();
        if ($link22['image_name'] == '') {
            $qa = $db->prepare("INSERT INTO `imageup` (`image`,`image_name`,`image_alt` ,`status` ,`ip`,`updated_by`) values (?,?,?,?,?,?) ");
            $qa->execute(array($image, $imagename, $imagealt, $status, $ip, $_SESSION['UID']));
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('imageup', $thispageid, 'Insert', $_SESSION['UID'], $ip, $getid));
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><h4><i class="icon fa fa-check"></i> Successfully Saved</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button><h4><i class="icon fa fa-close"></i> Image Name already exists!</h4></div>';
        }
        return $res;
    }
}

function getimage($a, $b) {
    global $db;
    $res = $db->prepare("SELECT * FROM `imageup` WHERE `aiid`= ? ");
    $res->execute(array($b));
    $res = $res->fetch();
    return $res[$a];
}
?>