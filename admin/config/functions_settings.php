<?php

function addusers($name, $username, $password, $usergroup, $status, $getid) {
    global $db;
    if ($getid == '') {
        $link22 = FETCH_all("SELECT `id` FROM `users` WHERE `val1`=?", $username);
        if ($link22['id'] == '') {
            $ress="INSERT INTO `users` (`name`, `val1`,`val2`,`val3`,`usergroup`) VALUES('".$name."','".$username."','".$password."','".$status."','".$usergroup."')";
            $resa = $db->prepare("INSERT INTO `users` (`name`, `val1`,`val2`,`val3`,`usergroup`) VALUES(?,?,?,?,?)");
            $resa->execute(array($name, $username, md5($password), $status, $usergroup));
            
            $id=$db->lastInsertId();
            
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Users Mgmt', 40, 'INSERT', $_SESSION['UID'], $ip, $id));
            
            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Username already exists!</h4></div>';
        }
    } else {
        $link22 = FETCH_all("SELECT `id` FROM `users` WHERE `val1`=? AND `id`!=?", $username, $getid);
        if ($link22['id'] == '') {
            $resa = $db->prepare("UPDATE `users` SET `name`=?,`val1`=?,`val2`=?,`val3`=?,`usergroup`=? WHERE `id`=?");
            $resa->execute(array(trim($name), trim($username), md5(trim($password)), trim($status), trim($usergroup), $getid));
			
            $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
            $htry->execute(array('Users Mgmt', 40, 'UPDATE', $_SESSION['UID'], $ip, $id));

            $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button><h4><i class="icon fa fa-check"></i>Successfully Updated</h4></div>';
        } else {
            $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i>Username already exists!</h4></div>';
        }
    }
    return $res;
}

function delusers($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Users Mgmt', 40, 'DELETE', $_SESSION['UID'], $ip, $c));
        
        $get = $db->prepare("DELETE FROM `users` WHERE `id` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

function getusers($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `users` WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function getassistors($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `assistors` WHERE `asid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}
function getpermission($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `permission` WHERE `perid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function delpermission($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Permission group', 26, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("UPDATE `permission` SET `status`=? WHERE `perid` =? ");
        $get->execute(array(2, $c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted</h4></div>';
    return $res;
}


function getreward($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `reward_point` WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function getsendgrid($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `sendgrid` WHERE `sgid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function addreward($purchase_point, $redeem_point,$min_point) {
    global $db;


    $resa = $db->prepare("UPDATE `reward_point` SET `purchase_point`=?,`redeem_point`=?,`min_point`=? WHERE `id`=?");
    $resa->execute(array(trim($purchase_point), trim($redeem_point),trim($min_point), '1'));
    // $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
    // $htry->execute(array('RewardPoint', 41, 'Update', $_SESSION['UID'], $d, $e));
    $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';

    return $res;
}

function addsendgrid($a, $b, $c,$f,$g, $d, $e) {
    global $db;
    if ($e == '') {

        $resa = $db->prepare("INSERT INTO `sendgrid` (`api_key`,`username`,`password`,`semail`,`saddress`,`ip`) VALUES (?,?,?,?,?,?)");
        $resa->execute(array(trim($a), trim($b),trim($f), trim($g), trim($c), trim($d)));
        $insert_id = $db->lastInsertId();

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('sendgrid', 41, 'Insert', $_SESSION['UID'], $d, $insert_id));


        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i>Send Grid Detail Successfully Inserted!</h4></div>';
    } else {

        $resa = $db->prepare("UPDATE `sendgrid` SET `api_key`=?,`username`=?,`password`=?,`semail`=?,`saddress`=?,`ip`=? WHERE `sgid`=?");
        $resa->execute(array(trim($a), trim($b), trim($c),trim($f), trim($g), trim($d), $e));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('sendgrid', 41, 'Update', $_SESSION['UID'], $d, $e));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
    }
    return $res;
}

function delsendgrid($a) {
    global $db;
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('sendgrid', 41, 'Delete', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], $c));
        $get = $db->prepare("DELETE FROM `sendgrid`  WHERE `sgid` =?");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    //echo "UPDATE `faq` SET `status`='2' WHERE `fid` ='" . $c . "'";
    return $res;
}

function getTableValue($table, $a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM " . $table . " WHERE `id`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function getgeneral($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `generalsettings` WHERE `generalid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function addgeneral($accbank,$homecontent1, $homecontent2, $fcontent, $about, $facebook, $beforehead, $afterbody, $address, $copyrights, $og_tag, $ip, $id) {
    global $db;



    $resa = $db->prepare("UPDATE `generalsettings` SET `accbank`=?,`og_tags`=?,`facebook`=?,`about`=?,`copyrights`=?,`beforehead`=?,`afterbody`=?,`addressinfo`=?,`homecontent1`=?,`homecontent2`=?,`ip`=?, `updated_id`=?,`updated_type`=? WHERE `generalid`=?");
    $resa->execute(array(trim($accbank),trim($og_tag), trim($facebook), trim($about), trim($copyrights), trim($beforehead), trim($afterbody), trim($address), trim($homecontent1), trim($homecontent2), trim($ip), $_SESSION['UID'], $_SESSION['type'], $id));

    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
    $htry->execute(array('General Settings', 2, 'Update', $_SESSION['UID'], $ip, $id));
    $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
    return $res;
}

/* home block function start here */

function addhomeblocks($icon1, $title1, $content1, $status1, $icon2, $title2, $content2, $status2, $icon3, $title3, $content3, $status3, $ip, $id) {
    global $db;
    try {
        $resa = $db->prepare("UPDATE `homeblocks` SET `icon1`=?,`title1`=?,`content1`=?,`status1`=?,`icon2`=?,`title2`=?,`content2`=?,`status2`=?,`icon3`=?,`title3`=?,`content3`=?,`status3`=?,`ip`=?,`updated_id`=?,`updated_type`=? WHERE `hid`=?");
        $resa->execute(array(trim($icon1), trim($title1), trim($content1), trim($status1), trim($icon2), trim($title2), trim($content2), trim($status2), trim($icon3), trim($title3), trim($content3), trim($status3), trim($ip), $_SESSION['UID'], $_SESSION['type'], $id));

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Home Blocks', 3, 'Update', $_SESSION['UID'], $ip, $id));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
    } catch (Exception $exc) {
        $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> ' . $exc . '</h4></div>';
    }


    return $res;
}

function gethomeblocks($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `homeblocks` WHERE `hid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

/* home block function start here */

function getsocialmedia($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `socialmedia` WHERE `sid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function addsocialmedia($social_media, $link, $order, $status, $ip, $id) {
    global $db;

    $resa = $db->prepare("UPDATE `socialmedia` SET `sname`=?,`link`=?,`order`=?,`status`=?,`ip`=?, `updated_id`=? WHERE `sid`=?");
    $resa->execute(array(trim($social_media), trim($link), trim($order), trim($status), trim($ip), $_SESSION['UID'], $id));

    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
    $htry->execute(array('Social Media', 7, 'Update', $_SESSION['UID'], $ip, $id));
    $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
    return $res;
}

function gethomebanner($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `homebanners` WHERE `hbid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
//$res = "SELECT * FROM `sendgrid` WHERE `sgid`='$b'";
    return $res;
}

function addhomebanner($title1, $link1, $content1, $imagealt1, $imagename1, $image1, $title2, $link2, $content2, $imagealt2, $imagename2, $image2, $title3, $link3, $content3, $imagealt3, $imagename3, $image3, $title4, $link4, $content4, $imagealt4, $imagename4, $image4, $ip, $thispageid, $getid, $status1, $status2, $status3, $status4) {
    global $db;

    // $ress="UPDATE `homebanners` SET `title1`='".$title1."', `link1`='".$link1."', `content1`='".$content1."', `image_alt1`='".$imagealt1."', `image_name1`='".$imagename1."',`image1`='".$image1."',`title2`='".$title2."',`link2`='".$link2."',`content2`='".$content2."',`image_alt2`='".$imagealt2."', `image_name2`='".$imagename2."',`image2`='".$image2."',`title3`='".$title3."',`link3`='".$link3."',`content3`='".$content3."', `image_alt3`='".$imagealt3."', `image_name3`='".$imagename3."',`image3`='".$image3."',`title4`='".$title4."',`link4`='".$link4."',`content4`='".$content4."', `image_alt4`='".$imagealt4."', `image_name4`='".$imagename4."',`image4`='".$image4."', `ip`='".$ip."', `updated_by`='".$_SESSION['UID']."' WHERE `hbid`='".$getid."'";

    $resa = $db->prepare("UPDATE `homebanners` SET `title1`=?, `link1`=?, `content1`=?, `image_alt1`=?, `image_name1`=?,`image1`=?,`title2`=?,`link2`=?,`content2`=?,`image_alt2`=?, `image_name2`=?,`image2`=?,`title3`=?,`link3`=?,`content3`=?, `image_alt3`=?, `image_name3`=?,`image3`=?,`title4`=?,`link4`=?,`content4`=?, `image_alt4`=?, `image_name4`=?,`image4`=?,`ip`=?, `updated_by`=?,`status1`=?,`status2`=?,`status3`=?,`status4`=? WHERE `hbid`=?");
    $resa->execute(array(trim($title1), trim($link1), trim($content1), trim($imagealt1), trim($imagename1), trim($image1), trim($title2), trim($link2), trim($content2), trim($imagealt2), trim($imagename2), trim($image2), trim($title3), trim($link3), trim($content3), trim($imagealt3), trim($imagename3), trim($image3), trim($title4), trim($link4), trim($content4), trim($imagealt4), trim($imagename4), trim($image4), trim($ip), $_SESSION['UID'], $status1, $status2, $status3, $status4, $getid));
    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
    $htry->execute(array('Home Banners', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
    $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i>Successfully Saved</h4></div>';
    return $res;
}

function delhomebanners($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $get = $db->prepare("DELETE FROM `homebanners` WHERE `hbid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
    $htry->execute(array('Home Banners', $thispageid, 'Update', $_SESSION['UID'], $ip, $id));
    return $res;
}

function getcaptcha($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `captcha` WHERE `cid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function addcaptcha($sitekey, $secret, $status, $ip, $cid) {
    global $db;
    if ($cid == '') {

        $resa = $db->prepare("INSERT INTO `captcha`(`sitekey`,`secret`,`ip`,`status`,`updated_by`,`updated_type`)VALUES(?,?,?,?,?,?)");
        $resa->execute(array($sitekey, $secret, $ip, $status, $_SESSION['UID'], $_SESSION['UIDD']));
        $insert_id = $db->lastInsertId();

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Captcha Mgmt', '35', 'Insert', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($insert_id)));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
    } else {
        $resa = $db->prepare("UPDATE `captcha` SET `sitekey`=?,`secret`=?,`ip`=?,`status`=?,`updated_by`=?,`updated_type`=? WHERE `cid`=?");
        $resa->execute(array($sitekey, $secret, $ip, $status, $_SESSION['UID'], $_SESSION['UIDD'], $cid));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('Captcha Mgmt', '35', 'Update', $_SESSION['UID'], $_SERVER['REMOTE_ADDR'], trim($cid)));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i>Successfully Updated</h4></div>';
    }
    return $res;
}


function updateEmailTemplate($name, $subject, $message, $id) {
    global $db;
    $res = $db->prepare("UPDATE `email_template` SET `name`=?,`subject`=?,`message`=?  WHERE `id`=?");
    $res->execute(array($name, $subject, $message, $id));
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Updated!</h4></div>';
    return $res;
}

function updatePrintTemplate($name, $subject, $message, $id) {
    global $db;
    $res = $db->prepare("UPDATE `print_template` SET `name`=?,`subject`=?,`message`=?  WHERE `id`=?");
    $res->execute(array($name, $subject, $message, $id));
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Updated!</h4></div>';
    return $res;
}

function update_currency_rates() {
    global $db;
    $cus = $db->prepare("SELECT * FROM `currency` WHERE `cuid` ");
    $cus->execute();
    $trunc = $db->prepare("TRUNCATE TABLE `currency_rates`");
    $trunc->execute();
    $currat = $db->prepare("INSERT INTO `currency_rates` SET `name`= ? , `value` = ? ");
    while ($fcu = $cus->fetch()) {
        $currsy = currencycv("AUD", $fcu['code'], 1);
        $currat->bindParam(1, $fcu['code']);
        $currat->bindParam(2, $currsy);
        $currat->execute();
    }
}

function currencycv($from_Currency, $to_Currency, $amount) {
    global $db;
    $from = $from_Currency;
    $to = $to_Currency;
    $url = 'http://finance.yahoo.com/d/quotes.csv?f=l1d1t1&s=' . $from . $to . '=X';
    $handle = fopen($url, 'r');

    if ($handle) {
        $result = fgetcsv($handle);
        fclose($handle);
    }

    return $result[0];
}

function Currency($var) {
    global $db;
    $var = str_replace("ALL", "Lek", $var);
    $var = str_replace("AFN", "؋", $var);
    $var = str_replace("ARS", "$", $var);
    $var = str_replace("AWG", "ƒ", $var);
    $var = str_replace("AUD", "$", $var);
    $var = str_replace("AZN", "ман", $var);
    $var = str_replace("BSD", "$", $var);
    $var = str_replace("BBD", "$", $var);
    $var = str_replace("BYR", "p.", $var);
    $var = str_replace("BZD", "BZ$", $var);
    $var = str_replace("BMD", "$", $var);
    $var = str_replace("BOB", '$b', $var);
    $var = str_replace("BAM", "KM", $var);
    $var = str_replace("BWP", "P", $var);
    $var = str_replace("BGN", "лв", $var);
    $var = str_replace("BRL", "R$", $var);
    $var = str_replace("BND", "$", $var);
    $var = str_replace("KHR", "៛", $var);
    $var = str_replace("CAD", "$", $var);
    $var = str_replace("KYD", "$", $var);
    $var = str_replace("CLP", "$", $var);
    $var = str_replace("CNY", "¥", $var);
    $var = str_replace("COP", "$", $var);
    $var = str_replace("CRC", "₡", $var);
    $var = str_replace("HRK", "kn", $var);
    $var = str_replace("CUP", "₱", $var);
    $var = str_replace("CZK", "KĿ", $var);
    $var = str_replace("DKK", "kr", $var);
    $var = str_replace("DOP", "RD$", $var);
    $var = str_replace("XCD", "$", $var);
    $var = str_replace("EGP", "£", $var);
    $var = str_replace("SVC", "$", $var);
    $var = str_replace("EUR", "€", $var);
    $var = str_replace("FKP", "£", $var);
    $var = str_replace("FJD", "$", $var);
    $var = str_replace("GHS", "¢", $var);
    $var = str_replace("GIP", "£", $var);
    $var = str_replace("GTQ", "Q", $var);
    $var = str_replace("GGP", "£", $var);
    $var = str_replace("GYD", "$", $var);
    $var = str_replace("HNL", "L", $var);
    $var = str_replace("HKD", "$", $var);
    $var = str_replace("HUF", "Ft", $var);
    $var = str_replace("ISK", "kr", $var);
    $var = str_replace("INR", "₹", $var);
    $var = str_replace("IDR", "Rp", $var);
    $var = str_replace("IRR", "﷼", $var);
    $var = str_replace("IMP", "£", $var);
    $var = str_replace("ILS", "₪", $var);
    $var = str_replace("JMD", "J$", $var);
    $var = str_replace("JPY", "¥", $var);
    $var = str_replace("JEP", "£", $var);
    $var = str_replace("KZT", "лв", $var);
    $var = str_replace("KPW", "₩", $var);
    $var = str_replace("KRW", "₩", $var);
    $var = str_replace("KGS", "лв", $var);
    $var = str_replace("LAK", "₭", $var);
    $var = str_replace("LBP", "£", $var);
    $var = str_replace("LRD", "$", $var);
    $var = str_replace("MKD", "ден", $var);
    $var = str_replace("MYR", "RM", $var);
    $var = str_replace("MUR", "₨", $var);
    $var = str_replace("MXN", "$", $var);
    $var = str_replace("MNT", "₮", $var);
    $var = str_replace("MZN", "MT", $var);
    $var = str_replace("NAD", "$", $var);
    $var = str_replace("NPR", "₨", $var);
    $var = str_replace("ANG", "ƒ", $var);
    $var = str_replace("NZD", "$", $var);
    $var = str_replace("NIO", "C$", $var);
    $var = str_replace("NGN", "₦", $var);
    $var = str_replace("KPW", "₩", $var);
    $var = str_replace("NOK", "kr", $var);
    $var = str_replace("OMR", "﷼", $var);
    $var = str_replace("PKR", "₨", $var);
    $var = str_replace("PAB", "B/.", $var);
    $var = str_replace("PYG", "Gs", $var);
    $var = str_replace("PEN", "S/.", $var);
    $var = str_replace("PHP", "₱", $var);
    $var = str_replace("PLN", "zł", $var);
    $var = str_replace("QAR", "﷼", $var);
    $var = str_replace("RON", "lei", $var);
    $var = str_replace("RUB", "руб", $var);
    $var = str_replace("SHP", "£", $var);
    $var = str_replace("SAR", "﷼", $var);
    $var = str_replace("RSD", "Дин.", $var);
    $var = str_replace("SCR", "₨", $var);
    $var = str_replace("SGD", "$", $var);
    $var = str_replace("SBD", "$", $var);
    $var = str_replace("SOS", "S", $var);
    $var = str_replace("ZAR", "R", $var);
    $var = str_replace("KRW", "₩", $var);
    $var = str_replace("LKR", "₨", $var);
    $var = str_replace("CHF", "CHF", $var);
    $var = str_replace("SEK", "kr", $var);
    $var = str_replace("SRD", "$", $var);
    $var = str_replace("SYP", "£", $var);
    $var = str_replace("TWD", "NT$", $var);
    $var = str_replace("THB", "฿", $var);
    $var = str_replace("TTD", "TT$", $var);
    $var = str_replace("TRY", "₺", $var);
    $var = str_replace("TVD", "$", $var);
    $var = str_replace("UAH", "₴", $var);
    $var = str_replace("GBP", "£", $var);
    $var = str_replace("USD", "$", $var);
    $var = str_replace("UYU", '$U', $var);
    $var = str_replace("UZS", "лв", $var);
    $var = str_replace("VEF", "Bs", $var);
    $var = str_replace("VND", "₫", $var);
    $var = str_replace("YER", "﷼", $var);
    $var = str_replace("ZWD", "Z$", $var);
    return $var;
}

function addtax($tax_name, $tax_per, $order, $status, $ip, $thispageid, $getid) {
    global $db;
    if ($getid == '') {

        $resa = $db->prepare("INSERT INTO `tax` (`taxname`,`taxpercentage`,`Order`,`Status`,`IP`,`Updated_by`) VALUES (?,?,?,?,?,?)");
        $resa->execute(array($tax_name, $tax_per, $order, $status, $ip, $_SESSION['UID']));

        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('tax', $thispageid, 'Insert', $_SESSION['UID'], $ip, $getid));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i>Successfully Inserted</h4></div>';
    } else {



        $resa = $db->prepare("UPDATE `tax` SET `taxname`=?,`taxpercentage`=?,`Order`=?,`Status`=?,`IP`=?,`Updated_by`=? WHERE `tid`=?");
        $resa->execute(array($tax_name, $tax_per, $order, $status, $ip, $_SESSION['UID'], $getid));
        $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
        $htry->execute(array('tax', $thispageid, 'Insert', $_SESSION['UID'], $ip, $getid));
        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Successfully Updated!</h4></div>';
    }
    return $res;
}

function gettax($a, $b) {
    global $db;
    $get1 = $db->prepare("SELECT * FROM `tax` WHERE `tid`=?");
    $get1->execute(array($b));
    $get = $get1->fetch(PDO::FETCH_ASSOC);
    $res = $get[$a];
    return $res;
}

function deltax($a) {
    $b = str_replace(".", ",", $a);
    $b = explode(",", $b);
    foreach ($b as $c) {
        global $db;
        $get = $db->prepare("DELETE FROM `tax` WHERE `tid` = ? ");
        $get->execute(array($c));
    }
    $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    $htry = $db->prepare("INSERT INTO `history` (`page`,`pageid`,`action`,`userid`,`ip`,`actionid`) VALUES (?,?,?,?,?,?)");
    $htry->execute(array('tax', $thispageid, 'delete', $_SESSION['UID'], $ip, $getid));
    return $res;
}

function changerate($cid, $sid, $nid, $brand, $changetype, $valuetype, $value, $oldback, $fileName) {
    global $db;
    try {    
        $resa = $db->prepare("INSERT INTO `dailyratechanges` (`cid`, `sid`,`innerid`,`brand`,`change_type`,`value_type`,`value`,`old_backup`,`csv_file`) VALUES (?,?,?,?,?,?,?,?,?)");
        $resa->execute(array($cid, $sid, $nid, $brand, $changetype, $valuetype, $value, $oldback, $fileName));



       if ($valuetype == '1') {
           $valueterm = $value / 100;
      } else {
           $valueterm = $value / 100;
       }


        if ($changetype == '1') {
            $pricetrm = "`price` +" . $valueterm;
        } else {
            $pricetrm = "`price` -" . $valueterm;
       }
           
			$resa = $db->prepare("UPDATE `product` SET `price`=? WHERE `cid`=? AND `sid`=? AND `innerid`=? AND `brand`=? ");
$resa->execute(array($pricetrm, trim($cid), trim($sid), trim($nid), trim($brand)));           	


        $res = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button><h4><i class="icon fa fa-check"></i> Successfully Updated</h4></div>';
    } catch (Exception $exc) {

        $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i>' . $exc . '</h4></div>';
    }



    // $res = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Successfully Deleted!</h4></div>';
    return $res;
}

?>
