<!DOCTYPE html>
<html>
   <head>
      <!-- Google Tag Manager -->
      <!-- End Facebook Pixel Code -->
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
      <title> Polo Shirt</title>
      <link rel="stylesheet" type="text/css" href="https://www.itailor.com/designpolo/assets/css/fonts.css"/>
      <link rel="stylesheet" type="text/css" href="css/dependency.css"/>
      <link rel="stylesheet" type="text/css" href="css/speziicoz.userinterface.css"/>
      <link rel="stylesheet" type="text/css" href="css/preloader.css"/>
      <link rel="stylesheet" type="text/css" href="css/template.css"/>
      <link rel="stylesheet" type="text/css" href="css/template-responsive.css"/>
      <link rel="stylesheet" type="text/css" href="css/template-responsive-mobile.css"/>

     <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
      <script src="js/dependency.js" type="text/javascript"></script>
      <script src="js/speziicoz.controller.js" type="text/javascript"></script>
      <script src="js/speziicoz.helper.js" type="text/javascript"></script>
      <script src="js/iTailor.design.polo.js" type="text/javascript"></script>
      <script src="Js/snowfall.min.js"></script>
    <script src="Js/bat.js"></script>
 </head>
   <body>
        <script src="js/SaveDesign.js"></script>
      <!-- Google Tag Manager (noscript) -->
      <!-- <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WCB237C"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> -->
      <!-- End Google Tag Manager (noscript) -->
      <!-- page loading -->
      <section id="mastwrapper">
         <section id="mastcontainer">
            <div class="container-inner">
               <section id="mastdesigner">
                  <section id="mastdetail">
                     <span class="design-price">
                     &#36; 44.95                            </span>
                     <p>
                        <span>WHITE POLO</span>
                     </p>
                     <div class="design-viewer">
                        <ul>
                           <li class="design-group-view frontView">
                              <div class="design-front-view switchRight">
                                 <img src="images/blog/front-view.png" width="80" height="100">
                              </div>
                              <div class="design-back-view switchLeft">
                                 <img src="images/blog/back-view.png" width="80" height="100">
                              </div>
                           </li>
                           <li class="design-current-view">
                              <span class="frontView active">Front View</span>
                              <span class="backView">Back View</span>
                           </li>
                           <li class="design-premium">
                              <img src="images/blog/Guarantee.png">
                              <span class="premium-fabric-text">Premium Fabric</span>
                           </li>
                           <li>
                              <img id="zoom-fabric" style="cursor: pointer;" src="images/blog/info.png" width="30" height="38">
                           </li>
                          <!--  <li>
                              <figure class="view-option-detail view-button-detail">
                                 <div class="view-button-current">
                                    <img src="images/blog/A8.png">
                                 </div>
                                 <div class="view-button-crossfade">
                                    <img src="images/blog/empty.png">
                                 </div>
                              </figure>
                              <figure class="view-option-detail view-thread-detail">
                                 <div class="view-thread-current">
                                    <img src="images/blog/A82.png">
                                 </div>
                                 <div class="view-thread-crossfade">
                                    <img src="images/blog/empty.png">
                                 </div>
                              </figure>
                           </li> -->
                        </ul>
                     </div>
                  </section>
                  <div class="design-model">
                     <div class="design-options">
                        <div class="design-options-wrapper">
                           <div class="design-options-inner">
                              <p>CONTRAST POSITION</p>
                              <div class="contrast-position-wrapper">
                                 <div class="contrast-position-scroll">
                                    <ul>
                                       <li class="option-heading contrast-position-collar">
                                          <span>1.</span>
                                          <span>Collar Contrast</span>
                                       </li>
                                       <li class="contrast-position-collar">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastCollarOutside" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastCollarOutside">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">1.1</span>
                                             <span>Outside</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                       <li class="contrast-position-collar">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastCollarInside" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastCollarInside">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">1.2</span>
                                             <span>Inside</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                    </ul>
                                    <ul>
                                       <li class="option-heading contrast-position-front">
                                          <span>2.</span>
                                          <span>Front Contrast</span>
                                       </li>
                                       <li class="contrast-position-front">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastFrontOutside" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastFrontOutside">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">2.1</span>
                                             <span>Outside</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                       <li class="contrast-position-front">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastFrontBox" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastFrontBox">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">2.3</span>
                                             <span>Box</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                    </ul>
                                    <ul>
                                       <li class="option-heading contrast-position-sleeve">
                                          <span>3.</span>
                                          <span>Sleeve Contrast</span>
                                       </li>
                                       <li class="contrast-position-sleeve">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastSleeve" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastSleeve">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">3.1</span>
                                             <span>Sleeve</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                       <li class="contrast-position-sleeve contrast-position-sleeve-cuff">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastSleeveCuff" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow"  for="contrastSleeveCuff">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">3.2</span>
                                             <span>Cuff</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                       <li class="contrast-position-sleeve contrast-position-sleeve-elbow">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastSleeveElbow" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow"  for="contrastSleeveElbow">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">3.3</span>
                                             <span>Elbow</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                    </ul>
                                    <ul>
                                       <li class="option-heading contrast-position-pocket">
                                          <span>4.</span>
                                          <span>Pocket Contrast</span>
                                       </li>
                                       <li class="contrast-position-pocket">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastPocketMain" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastPocketMain">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">4.1</span>
                                             <span>Main Pocket</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                       <li class="contrast-position-pocket">
                                          <div class="checklist-container">
                                             <input type="checkbox" id="contrastPocketTrimming" class="checklist">
                                             <span class="checklist-content">
                                             <span class="checklist-text">
                                             <label class="checklist-text-overflow" for="contrastPocketTrimming">
                                             <span class="checklist-form"></span>
                                             <span class="checklist-index">4.2</span>
                                             <span>Trimming</span>
                                             </label>
                                             <span class="option-color"></span>
                                             </span>
                                             </span>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="design-model-inner">
                        <div id="design-model-monogram">
                           <span></span>
                        </div>
                        <div id="model-designer" class="design-model-block frontView">
                           <div class="design-model-wrapper">
                              <div class="design-model-front">
                                 <div class="design-model-front-current">
                                    <img src="images/blog/empty.png">
                                 </div>
                                 <div class="design-model-front-crossfade">
                                    <img src="images/blog/empty.png">
                                 </div>
                              </div>
                              <div class="design-model-back">
                                 <div class="design-model-back-current">
                                    <img src="images/blog/empty.png">
                                 </div>
                                 <div class="design-model-back-crossfade">
                                    <img src="images/blog/empty.png">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="design-model-double-pocket design-model-block">
                           <div class="checklist-container">
                              <input type="checkbox" id="doublePocket" class="checklist">
                              <span class="checklist-content">
                              <span class="checklist-text">
                              <label class="checklist-text-overflow" for="doublePocket">
                              <span class="checklist-form"></span>
                              <span class="checklist-index"></span>
                              <span>2 Pockets</span>
                              </label>
                              </span>
                              </span>
                           </div>
                        </div>
                        <div class="design-model-collar-slim design-model-block">
                           <div class="checklist-container">
                              <input type="checkbox" id="collarSlim" class="checklist">
                              <span class="checklist-content">
                              <span class="checklist-text">
                              <label class="checklist-text-overflow" for="collarSlim">
                              <span class="checklist-form"></span>
                              <span class="checklist-index"></span>
                              <span>Slim</span>
                              </label>
                              </span>
                              </span>
                           </div>
                        </div>
                        <div id="design-model-premium">
                           <div class="premium-soft-touch">
                              <img src="images/blog/Soft.png">
                           </div>
                           <div class="premium-fabric">
                              <span class="premium-fabric-text">Premium Fabric</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="design-measurement">
                     <div class="design-measurement-inner">
                        <div class="design-measurement-choice design-measurement-option measurement-active">
                           <em class="measurement-heading" data-lang="great-choice-select-you-measurement-option">Great Choice!  Please Select Your Measurement Option</em>
                           <ul>
                              <li id="measurement-body">
                                 <h4 data-lang="option-body-size">Body Size</h4>
                                 <p data-lang="option-body-size-detail">Part of the tailor-made experience is getting yourself measured up. With the assistance of our easy-to-follow video measuring guide, get yourself measured up in no time!</p>
                                 <img class="measurement-arrow" src="images/blog/Arrow-down.png">
                                 <img src="images/blog/Measurement.png">
                              </li>
                              <li id="measurement-standard">
                                 <h4 data-lang="standard-size">Standard Sizes</h4>
                                 <p data-lang="standard-size-detail">Standard sizes provide an equally amazing fit. Select from an array of sizes from our standard size chart. Enjoy your Tailor-made product with the perfect combination of the right size and your creative style choices!</p>
                                 <img class="measurement-arrow" src="images/blog/Arrow-down.png">
                                 <img src="images/blog/SML.png">
                              </li>
                           </ul>
                        </div>
                        <div class="design-measurement-choice design-measurement-body">
                           <p class="measurement-heading" data-lang="your-body-size">YOUR BODY SIZES</p>
                           <div class="measurement-cotent">
                              <div class="design-measurement-guide">
                                 <div class="design-measurement-guide-image">
                                    <img class="lazyLoad" data-original="https://www.itailor.com/iTailor-data/webroot/video/Polo/images/English/neck.jpg">
                                 </div>
                                 <div class="design-measurement-guide-media">
                                    <video id="media" loop muted controls preload="none" poster="images/blog/empty.png">
                                       <source src="https://www.itailor.com/iTailor-data/webroot/video/Polo/video/Neck.mp4" type="video/mp4">
                                       <source src="https://www.itailor.com/iTailor-data/webroot/video/Polo/video/Neck.ogv" type="video/ogg">
                                    </video>
                                 </div>
                              </div>
                              <div class="design-measurement-form">
                                 <p>
                                    <span class="design-script-overflow">
                                    <span data-lang="enter-your-measurements-boxes">Enter your measurements in the corresponding boxes</span>
                                    <span>:</span>
                                    <span class="measurement-position" data-lang="neck">Neck</span>
                                    <span>generally range from</span>
                                    <span class="measurement-min">
                                    9                                                    </span>
                                    <span>To</span>
                                    <span class="measurement-max">
                                    23                                                    </span>
                                    <span class="measurement-unit">
                                    inch                                                    </span>
                                    </span>
                                 </p>
                                 <ul class="design-measurement-form-input">
                                    <li>
                                       <span class="design-measurement-name" data-lang="neck">Neck</span>
                                       <input type="text" class="measurement-input measurement-cm neckSizeCm" data-pos="Neck">
                                       <input type="text" class="measurement-input measurement-inch neckSizeInch" data-pos="Neck">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="chest">Chest</span>
                                       <input type="text" class="measurement-input measurement-cm chestSizeCm" data-pos="Chest">
                                       <input type="text" class="measurement-input measurement-inch chestSizeInch" data-pos="Chest">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="waist">Waist</span>
                                       <input type="text" class="measurement-input measurement-cm waistSizeCm" data-pos="Waist">
                                       <input type="text" class="measurement-input measurement-inch waistSizeInch" data-pos="Waist">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="hip">Hip</span>
                                       <input type="text" class="measurement-input measurement-cm hipSizeCm" data-pos="Hip">
                                       <input type="text" class="measurement-input measurement-inch hipSizeInch" data-pos="Hip">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="length">Length</span>
                                       <input type="text" class="measurement-input measurement-cm lengthSizeCm" data-pos="Length">
                                       <input type="text" class="measurement-input measurement-inch lengthSizeInch" data-pos="Length">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="shoulder">Shoulder</span>
                                       <input type="text" class="measurement-input measurement-cm shoulderSizeCm" data-pos="Shoulder">
                                       <input type="text" class="measurement-input measurement-inch shoulderSizeInch" data-pos="Shoulder">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="sleeve">Sleeve</span>
                                       <input type="text" class="measurement-input measurement-cm sleeveSizeCm" data-pos="Sleeve">
                                       <input type="text" class="measurement-input measurement-inch sleeveSizeInch" data-pos="Sleeve">
                                    </li>
                                    <li>
                                       <span class="design-measurement-name" data-lang="sleeve">Sleeve</span>
                                       <input type="text" class="measurement-input measurement-cm shortSleeveSizeCm" data-pos="ShortSleeve">
                                       <input type="text" class="measurement-input measurement-inch shortSleeveSizeInch" data-pos="ShortSleeve">
                                    </li>
                                    <li>
                                       <div class="radiolist-container">
                                          <input type="radio" id="measurement-cm" class="radiolist measurement-option" name="measurement-option" value="cm">
                                          <span class="radiolist-content">
                                          <span class="radiolist-text">
                                          <label class="radiolist-text-overflow" for="measurement-cm">
                                          <span class="radiolist-form"></span>
                                          <span class="radiolist-index"></span>
                                          <span>Cm</span>
                                          </label>
                                          </span>
                                          </span>
                                       </div>
                                       <div class="radiolist-container">
                                          <input type="radio" id="measurement-inch" class="radiolist measurement-option" name="measurement-option" value="inch">
                                          <span class="radiolist-content">
                                          <span class="radiolist-text">
                                          <label class="radiolist-text-overflow" for="measurement-inch">
                                          <span class="radiolist-form"></span>
                                          <span class="radiolist-index"></span>
                                          <span>Inch</span>
                                          </label>
                                          </span>
                                          </span>
                                       </div>
                                    </li>
                                 </ul>
                                 <p data-lang="your-fit">Select Your Size</p>
                                 <ul class="measurement-select-fit">
                                    <li>
                                       <div class="radiolist-container">
                                          <input type="radio" id="measurement-fit-standard" class="radiolist measurement-fit" name="measurement-fit" value="Comfortable">
                                          <span class="radiolist-content">
                                          <span class="radiolist-text">
                                          <label class="radiolist-text-overflow" for="measurement-fit-standard">
                                          <span class="radiolist-form"></span>
                                          <span class="radiolist-index"></span>
                                          <span data-lang="signature-fit">Signature Standard Fit</span>
                                          </label>
                                          </span>
                                          </span>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="radiolist-container">
                                          <input type="radio" id="measurement-fit-slim" class="radiolist measurement-fit" name="measurement-fit" value="Slim">
                                          <span class="radiolist-content">
                                          <span class="radiolist-text">
                                          <label class="radiolist-text-overflow" for="measurement-fit-slim">
                                          <span class="radiolist-form"></span>
                                          <span class="radiolist-index"></span>
                                          <span data-lang="euro-fit">Euro Slim Fit</span>
                                          </label>
                                          </span>
                                          </span>
                                       </div>
                                    </li>
                                    <li>
                                       <span data-lang="quantity">Quantity</span>
                                       <div id="design-quantity" class="design-select-list sz-dropdown">
                                          <div class="sz-dropdown-text">1</div>
                                          <div class="sz-dropdown-inner">
                                             <div class="sz-dropdown-overflow">
                                                <ul class="sz-dropdown-list">
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>1</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>2</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>3</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>4</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>5</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>6</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>7</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>8</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>9</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>10</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>11</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>12</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>13</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>14</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>15</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>16</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>17</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>18</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>19</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>20</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>21</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>22</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>23</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>24</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>25</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>26</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>27</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>28</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>29</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>30</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>31</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>32</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>33</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>34</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>35</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>36</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>37</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>38</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>39</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>40</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>41</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>42</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>43</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>44</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>45</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>46</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>47</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>48</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>49</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>50</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>51</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>52</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>53</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>54</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>55</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>56</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>57</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>58</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>59</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>60</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>61</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>62</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>63</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>64</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>65</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>66</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>67</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>68</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>69</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>70</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>71</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>72</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>73</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>74</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>75</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>76</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>77</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>78</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>79</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>80</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>81</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>82</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>83</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>84</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>85</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>86</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>87</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>88</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>89</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>90</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>91</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>92</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>93</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>94</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>95</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>96</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>97</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>98</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>99</span></a>
                                                   </li>
                                                   <li class="sz-dropdown-item">
                                                      <a href="Javascript:void(0);"><span>100</span></a>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                              <p data-lang="save-design-option" style="margin-top: 10px; font-size: 12px; text-align: center;">The 'Save Design' option is available on the Checkout page. Please add your order to the shopping cart to Save a Design.</p>
                           </div>
                        </div>
                        <div class="design-measurement-choice design-measurement-standard">
                           <p class="measurement-heading" data-lang="sizes-chart">Size Chart</p>
                           <div class="measurement-cotent">
                              <ul class="measurement-standard-list">
                                 <li>
                                    <div class="radiolist-container">
                                       <input type="radio" id="measurement-standard-cm" class="radiolist measurement-standard-size" name="measurement-standard-size" value="cm">
                                       <span class="radiolist-content">
                                       <span class="radiolist-text">
                                       <label class="radiolist-text-overflow" for="measurement-standard-cm">
                                       <span class="radiolist-form"></span>
                                       <span class="radiolist-index"></span>
                                       <span>Cm</span>
                                       </label>
                                       </span>
                                       </span>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="radiolist-container">
                                       <input type="radio" id="measurement-standard-inch" class="radiolist measurement-standard-size" name="measurement-standard-size" value="inch">
                                       <span class="radiolist-content">
                                       <span class="radiolist-text">
                                       <label class="radiolist-text-overflow" for="measurement-standard-inch">
                                       <span class="radiolist-form"></span>
                                       <span class="radiolist-index"></span>
                                       <span>Inch</span>
                                       </label>
                                       </span>
                                       </span>
                                    </div>
                                 </li>
                              </ul>
                              <div class="measurement-table-standard">
                                 <div class="measurement-table-standard-heading">
                                    <ul>
                                       <li><span data-lang="size">Size</span></li>
                                       <li><span>S</span></li>
                                       <li><span>M</span></li>
                                       <li><span>L</span></li>
                                       <li><span>XL</span></li>
                                       <li><span>XXL</span></li>
                                       <li><span>3XL</span></li>
                                       <li><span>4XL</span></li>
                                    </ul>
                                 </div>
                                 <div class="measurement-table-standard-row measurement-table-cm">
                                    <ul>
                                       <li><span data-lang="neck">Neck</span></li>
                                       <li><span>38.1</span></li>
                                       <li><span>40.64</span></li>
                                       <li><span>41.91</span></li>
                                       <li><span>43.18</span></li>
                                       <li><span>45.72</span></li>
                                       <li><span>48.26</span></li>
                                       <li><span>50.8</span></li>
                                    </ul>
                                 </div>
                                 <div class="measurement-table-standard-row measurement-table-cm">
                                    <ul>
                                       <li><span data-lang="chest">Chest</span></li>
                                       <li><span>89-91.5</span></li>
                                       <li><span>91.5-99</span></li>
                                       <li><span>99-106.5</span></li>
                                       <li><span>106.5-112</span></li>
                                       <li><span>112-119.5</span></li>
                                       <li><span>119.5-127</span></li>
                                       <li><span>127-134.5</span></li>
                                    </ul>
                                 </div>
                                 <div class="measurement-table-standard-row measurement-table-inch">
                                    <ul>
                                       <li><span>Neck</span></li>
                                       <li><span>15</span></li>
                                       <li><span>16</span></li>
                                       <li><span>16.5</span></li>
                                       <li><span>17</span></li>
                                       <li><span>18</span></li>
                                       <li><span>19</span></li>
                                       <li><span>20</span></li>
                                    </ul>
                                 </div>
                                 <div class="measurement-table-standard-row measurement-table-inch">
                                    <ul>
                                       <li><span>Chest</span></li>
                                       <li><span>33-36</span></li>
                                       <li><span>36-39</span></li>
                                       <li><span>39-42</span></li>
                                       <li><span>42-44</span></li>
                                       <li><span>44-47</span></li>
                                       <li><span>47-50</span></li>
                                       <li><span>50-53</span></li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="measurement-standard-fit">
                                 <div id="measurement-standard-add" class="measurement-standard-add-size" data-lang="add-more-size">ADD OTHER SIZES</div>
                                 <div class="measurement-standard-select">
                                    <ul>
                                       <li>
                                          <span class="measurement-standard-select-heading" data-lang="your-fit">Select Your Size</span>
                                          <div class="measurement-standard-fit-size design-select-list sz-dropdown">
                                             <div class="sz-dropdown-text">--</div>
                                             <div class="sz-dropdown-inner">
                                                <div class="sz-dropdown-overflow">
                                                   <ul class="sz-dropdown-list">
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>S</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>M</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>L</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>XL</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>XXL</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>3XL</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>4XL</span></a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <span class="measurement-standard-select-heading" data-lang="quantity">Quantity</span>
                                          <div class="measurement-standard-quantity design-select-list sz-dropdown">
                                             <div class="sz-dropdown-text">1</div>
                                             <div class="sz-dropdown-inner">
                                                <div class="sz-dropdown-overflow">
                                                   <ul class="sz-dropdown-list">
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>1</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>2</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>3</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>4</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>5</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>6</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>7</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>8</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>9</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>10</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>11</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>12</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>13</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>14</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>15</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>16</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>17</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>18</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>19</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>20</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>21</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>22</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>23</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>24</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>25</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>26</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>27</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>28</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>29</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>30</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>31</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>32</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>33</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>34</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>35</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>36</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>37</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>38</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>39</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>40</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>41</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>42</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>43</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>44</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>45</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>46</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>47</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>48</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>49</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>50</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>51</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>52</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>53</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>54</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>55</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>56</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>57</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>58</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>59</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>60</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>61</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>62</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>63</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>64</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>65</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>66</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>67</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>68</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>69</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>70</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>71</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>72</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>73</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>74</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>75</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>76</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>77</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>78</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>79</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>80</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>81</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>82</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>83</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>84</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>85</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>86</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>87</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>88</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>89</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>90</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>91</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>92</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>93</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>94</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>95</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>96</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>97</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>98</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>99</span></a>
                                                      </li>
                                                      <li class="sz-dropdown-item">
                                                         <a href="Javascript:void(0);"><span>100</span></a>
                                                      </li>
                                                   </ul>
                                                </div>
                                             </div>
                                          </div>
                                       </li>
                                       <li>
                                          <div id="measurement-standard-remove" class="measurement-standard-remove-size"></div>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <p data-lang="save-design-option" style="margin-top: 20px; font-size: 12px; text-align: center;">The 'Save Design' option is available on the Checkout page. Please add your order to the shopping cart to Save a Design.</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="design-controller">
                     <ul>
                        <li class="design-controller-back"><span data-lang="back">Back</span></li>
                        <li class="design-controller-next"><span data-lang="next">Next</span></li>
                        <li id="designer-checkout" class="design-controller-checkout designer-checkout" data-lang="checkout"><span>CHECK OUT</span></li>
                     </ul>
                     <div class="clearfix"></div>
                  </div>
                  <div class="design-style">
                     <div class="design-style-inner">
                        <div class="design-style-menu">
                           <div class="design-style-list">
                              <p>
                                 <span>1.1</span>
                                 <span data-lang="choose-your-fabric">Choose your Fabric</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="FabricScroll1" data-style="fabric">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BKD350.png" title="Deep Black" alt="Deep Black">
                                                      <figcaption>
                                                         <span class="product-price">
                                                         &#36; 39.95                                                                                </span>
                                                      </figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD360.png" title="Deep Navy" alt="Deep Navy">
                                                      <figcaption>
                                                         <span class="product-price">
                                                         &#36; 39.95                                                                                </span>
                                                      </figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/RDD335.png" title="Cherry" alt="Cherry">
                                                      <figcaption>
                                                         <span class="product-price">
                                                         &#36; 39.95                                                                                </span>
                                                      </figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/WHD100.png" title="White" alt="White">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD225.png" title="PVC Blue" alt="PVC Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD230.png" title="Carolina Blue" alt="Carolina Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD240.png" title="French Blue" alt="French Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD310.png" title="Pacific Blue" alt="Pacific Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD220.png" title="Scuba Blue" alt="Scuba Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GYD220.png" title="Cement" alt="Cement">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD335.png" title="Shocking Blue" alt="Shocking Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD240.png" title="Green Apple" alt="Green Apple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD260.png" title="Dark Green" alt="Dark Green">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD110.png" title="Corn" alt="Corn">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD215.png" title="Yellow" alt="Yellow">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/ORD315.png" title="Orange Peach" alt="Orange Peach">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD110.png" title="Baby Pink" alt="Baby Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD220.png" title="Pink Candy" alt="Pink Candy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD320.png" title="Shocking Pink" alt="Shocking Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD350.png" title="Burgundy" alt="Burgundy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD220.png" title="Lilac" alt="Lilac">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD323.png" title="Violet Blooming Rose" alt="Violet Blooming Rose">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD325.png" title="Dark Purple" alt="Dark Purple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD110.png" title="Oatmeal" alt="Oatmeal">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD210.png" title="Khaki" alt="Khaki">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD320.png" title="Chocolate" alt="Chocolate">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GYD120.png" title="Smoke" alt="Smoke">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>1.2</span>
                                 <span data-lang="choose-your-fabric">Choose your Fabric</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="FabricScroll2" data-style="fabric">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/WHD100.png" title="White" alt="White">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD225.png" title="PVC Blue" alt="PVC Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD230.png" title="Carolina Blue" alt="Carolina Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD240.png" title="French Blue" alt="French Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD310.png" title="Pacific Blue" alt="Pacific Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD220.png" title="Scuba Blue" alt="Scuba Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GYD220.png" title="Cement" alt="Cement">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD335.png" title="Shocking Blue" alt="Shocking Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD240.png" title="Green Apple" alt="Green Apple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD260.png" title="Dark Green" alt="Dark Green">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD110.png" title="Corn" alt="Corn">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD215.png" title="Yellow" alt="Yellow">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/ORD315.png" title="Orange Peach" alt="Orange Peach">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD110.png" title="Baby Pink" alt="Baby Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD220.png" title="Pink Candy" alt="Pink Candy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD320.png" title="Shocking Pink" alt="Shocking Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD350.png" title="Burgundy" alt="Burgundy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD220.png" title="Lilac" alt="Lilac">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD323.png" title="Violet Blooming Rose" alt="Violet Blooming Rose">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD325.png" title="Dark Purple" alt="Dark Purple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD110.png" title="Oatmeal" alt="Oatmeal">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD210.png" title="Khaki" alt="Khaki">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD320.png" title="Chocolate" alt="Chocolate">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GYD120.png" title="Smoke" alt="Smoke">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>1.3</span>
                                 <span data-lang="choose-your-fabric">Choose your Fabric</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="FabricScroll3" data-style="fabric">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT006.png" title="PoloPT006" alt="PoloPT006">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT014.png" title="PoloPT014" alt="PoloPT014">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT015.png" title="PoloPT015" alt="PoloPT015">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT017.png" title="PoloPT017" alt="PoloPT017">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT018.png" title="PoloPT018" alt="PoloPT018">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT019.png" title="PoloPT019" alt="PoloPT019">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT021.png" title="PoloPT021" alt="PoloPT021">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT023.png" title="PoloPT023" alt="PoloPT023">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT024.png" title="PoloPT024" alt="PoloPT024">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT025.png" title="PoloPT025" alt="PoloPT025">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list design-fabric-combination">
                              <p>
                                 <span>1.4</span>
                                 <span data-lang="choose-your-fabric">Choose your Fabric</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper design-combination">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="CombinationScroll" data-style="combination">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/No.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-1.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-2.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-3.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-4.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-5.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-6.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-7.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-8.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CM-9.png">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="design-combination-options">
                                       <div class="design-combination-block">
                                          <div class="design-combination-script">
                                             <p>
                                                <span class="design-script-overflow">
                                                <span></span>
                                                <span>CHOOSE MAIN FABRIC</span>
                                                </span>
                                             </p>
                                          </div>
                                          <div class="design-style-wrapper">
                                             <div class="design-style-scroll-content">
                                                <div class="design-style-scroll" data-scroll="FabricCombinationScroll1" data-style="allFabric">
                                                   <ul>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BKD350.png" title="Deep Black" alt="Deep Black">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD360.png" title="Deep Navy" alt="Deep Navy">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/RDD335.png" title="Cherry" alt="Cherry">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/WHD100.png" title="White" alt="White">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD225.png" title="PVC Blue" alt="PVC Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD230.png" title="Carolina Blue" alt="Carolina Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD240.png" title="French Blue" alt="French Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD310.png" title="Pacific Blue" alt="Pacific Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD220.png" title="Scuba Blue" alt="Scuba Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GYD220.png" title="Cement" alt="Cement">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD335.png" title="Shocking Blue" alt="Shocking Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD240.png" title="Green Apple" alt="Green Apple">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD260.png" title="Dark Green" alt="Dark Green">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD110.png" title="Corn" alt="Corn">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD215.png" title="Yellow" alt="Yellow">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/ORD315.png" title="Orange Peach" alt="Orange Peach">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD110.png" title="Baby Pink" alt="Baby Pink">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD220.png" title="Pink Candy" alt="Pink Candy">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD320.png" title="Shocking Pink" alt="Shocking Pink">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD350.png" title="Burgundy" alt="Burgundy">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD220.png" title="Lilac" alt="Lilac">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD323.png" title="Violet Blooming Rose" alt="Violet Blooming Rose">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD325.png" title="Dark Purple" alt="Dark Purple">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD110.png" title="Oatmeal" alt="Oatmeal">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD210.png" title="Khaki" alt="Khaki">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD320.png" title="Chocolate" alt="Chocolate">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GYD120.png" title="Smoke" alt="Smoke">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT006.png" title="PoloPT006" alt="PoloPT006">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT014.png" title="PoloPT014" alt="PoloPT014">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT015.png" title="PoloPT015" alt="PoloPT015">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT017.png" title="PoloPT017" alt="PoloPT017">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT018.png" title="PoloPT018" alt="PoloPT018">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT019.png" title="PoloPT019" alt="PoloPT019">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT021.png" title="PoloPT021" alt="PoloPT021">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT023.png" title="PoloPT023" alt="PoloPT023">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT024.png" title="PoloPT024" alt="PoloPT024">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT025.png" title="PoloPT025" alt="PoloPT025">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                   </ul>
                                                   <div class="clearfix"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="design-combination-block">
                                          <div class="design-combination-script">
                                             <p>
                                                <span class="design-script-overflow">
                                                <span></span>
                                                <span>CHOOSE COMBINATION FABRIC</span>
                                                </span>
                                             </p>
                                          </div>
                                          <div class="design-style-wrapper">
                                             <div class="design-style-scroll-content">
                                                <div class="design-style-scroll" data-scroll="FabricCombinationScroll2" data-style="allFabricCombination">
                                                   <ul>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BKD350.png" title="Deep Black" alt="Deep Black">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD360.png" title="Deep Navy" alt="Deep Navy">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/RDD335.png" title="Cherry" alt="Cherry">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/WHD100.png" title="White" alt="White">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD225.png" title="PVC Blue" alt="PVC Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD230.png" title="Carolina Blue" alt="Carolina Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD240.png" title="French Blue" alt="French Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD310.png" title="Pacific Blue" alt="Pacific Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD220.png" title="Scuba Blue" alt="Scuba Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GYD220.png" title="Cement" alt="Cement">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/BLD335.png" title="Shocking Blue" alt="Shocking Blue">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD240.png" title="Green Apple" alt="Green Apple">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/GRD260.png" title="Dark Green" alt="Dark Green">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD110.png" title="Corn" alt="Corn">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/YLD215.png" title="Yellow" alt="Yellow">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/ORD315.png" title="Orange Peach" alt="Orange Peach">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD110.png" title="Baby Pink" alt="Baby Pink">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD220.png" title="Pink Candy" alt="Pink Candy">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD320.png" title="Shocking Pink" alt="Shocking Pink">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD350.png" title="Burgundy" alt="Burgundy">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD220.png" title="Lilac" alt="Lilac">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD323.png" title="Violet Blooming Rose" alt="Violet Blooming Rose">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PLD325.png" title="Dark Purple" alt="Dark Purple">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD110.png" title="Oatmeal" alt="Oatmeal">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD210.png" title="Khaki" alt="Khaki">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BRD320.png" title="Chocolate" alt="Chocolate">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GYD120.png" title="Smoke" alt="Smoke">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT006.png" title="PoloPT006" alt="PoloPT006">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT014.png" title="PoloPT014" alt="PoloPT014">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT015.png" title="PoloPT015" alt="PoloPT015">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT017.png" title="PoloPT017" alt="PoloPT017">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT018.png" title="PoloPT018" alt="PoloPT018">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT019.png" title="PoloPT019" alt="PoloPT019">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT021.png" title="PoloPT021" alt="PoloPT021">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT023.png" title="PoloPT023" alt="PoloPT023">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT024.png" title="PoloPT024" alt="PoloPT024">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                      <li>
                                                         <figure>
                                                            <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/PoloPT025.png" title="PoloPT025" alt="PoloPT025">
                                                            <figcaption></figcaption>
                                                         </figure>
                                                      </li>
                                                   </ul>
                                                   <div class="clearfix"></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="design-style-menu">
                           <div class="design-style-list">
                              <p>
                                 <span>2.1</span>
                                 <span data-lang="choose-your-collar">Choose your Collar Style</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="StyleCollarScroll" data-style="collar">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CL-1.png">
                                                      <figcaption><span>CL-1</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CL-2.png">
                                                      <figcaption><span>CL-2</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CL-3.png">
                                                      <figcaption><span>CL-3</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="images/blog/CL-4.png">
                                                      <figcaption><span>CL-4</span></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>2.2</span>
                                 <span data-lang="choose-your-sleeve">Please Choose Sleeve Style</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="StyleSleeveScroll" data-style="sleeve">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Sleeve/SL-1.png">
                                                      <figcaption><span>Short Sleeve</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Sleeve/SL-2.png">
                                                      <figcaption><span>Short Sleeve + Cuff</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Sleeve/SL-3.png">
                                                      <figcaption><span>Long Sleeve</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Sleeve/SL-4.png">
                                                      <figcaption><span>Long Sleeve + Elbow</span></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>2.3</span>
                                 <span data-lang="choose-your-front">Choose your Front Style</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="StyleFrontScroll" data-style="front">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Front/FS-1.png">
                                                      <figcaption><span>FS-1</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Front/FS-2.png">
                                                      <figcaption><span>FS-2</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Front/FS-3.png">
                                                      <figcaption><span>FS-3</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Front/FS-4.png">
                                                      <figcaption><span>FS-4</span></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>2.4</span>
                                 <span data-lang="choose-your-pocket">Select Pocket Style</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="StylePocketScroll" data-style="pocket">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/No-pocket.png" title="No Pocket" alt="No Pocket">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-1.png" title="Classic Round" alt="Classic Round">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-2.png" title="Classic Angle" alt="Classic Angle">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-3.png" title="Diamond Straight" alt="Diamond Straight">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-4.png" title="Classic Square" alt="Classic Square">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-5.png" title="Round Flap" alt="Round Flap">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-6.png" title="Angle Flap" alt="Angle Flap">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Pocket/PK-7.png" title="Diamond Flap" alt="Diamond Flap">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>2.5</span>
                                 <span data-lang="choose-your-bottom">Choose your Bottom Style</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="StyleBottomScroll" data-style="bottom">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Bottom/BT-1.png">
                                                      <figcaption><span>BT-1</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Bottom/BT-2.png">
                                                      <figcaption><span>BT-2</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Bottom/BT-3.png">
                                                      <figcaption><span>BT-3</span></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="design-style-menu">
                           <div class="design-style-list">
                              <p>
                                 <span>3.1</span>
                                 <span data-lang="choose-you-contrast-fabric">Choose your Contrast Fabric</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="ContrastFabricScroll" data-style="contrast">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/WHD100.png" title="White" alt="White">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD225.png" title="PVC Blue" alt="PVC Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD230.png" title="Carolina Blue" alt="Carolina Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD240.png" title="French Blue" alt="French Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD310.png" title="Pacific Blue" alt="Pacific Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BKD350.png" title="Deep Black" alt="Deep Black">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD220.png" title="Scuba Blue" alt="Scuba Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GYD220.png" title="Cement" alt="Cement">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD335.png" title="Shocking Blue" alt="Shocking Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/BLD360.png" title="Deep Navy" alt="Deep Navy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GRD240.png" title="Green Apple" alt="Green Apple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/GRD260.png" title="Dark Green" alt="Dark Green">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/YLD110.png" title="Corn" alt="Corn">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/YLD215.png" title="Yellow" alt="Yellow">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/ORD315.png" title="Orange Peach" alt="Orange Peach">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/PKD110.png" title="Baby Pink" alt="Baby Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/RDD220.png" title="Pink Candy" alt="Pink Candy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/PKD320.png" title="Shocking Pink" alt="Shocking Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/RDD335.png" title="Cherry" alt="Cherry">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/RDD350.png" title="Burgundy" alt="Burgundy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/PLD220.png" title="Lilac" alt="Lilac">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/PLD323.png" title="Violet Blooming Rose" alt="Violet Blooming Rose">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/PLD325.png" title="Dark Purple" alt="Dark Purple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/BRD110.png" title="Oatmeal" alt="Oatmeal">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/BRD210.png" title="Khaki" alt="Khaki">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/BRD320.png" title="Chocolate" alt="Chocolate">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/S/GYD120.png" title="Smoke" alt="Smoke">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/TDA14925-1.png" title="Shirt Pattern Fabric" alt="Shirt Pattern Fabric">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/TDA14925-2.png" title="Black Shirt Fabric" alt="Black Shirt Fabric">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/TDA14925-3.png" title="Shirt Fabric" alt="Shirt Fabric">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/TDA15295-1.png" title="Print Shirt Fabric" alt="Print Shirt Fabric">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="images/blog/S/TDA15295-3.png" title="Print Shirt Fabric" alt="Print Shirt Fabric">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list">
                              <p>
                                 <span>3.2</span>
                                 <span>CHOOSE YOUR BUTTONS / THREAD</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="ContrastButtonScroll" data-style="button">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A8.png" title="White" alt="White">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A1.png" title="Cream" alt="Cream">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A16.png" title="Yellow" alt="Yellow">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A92.png" title="Light Green" alt="Light Green">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A37.png" title="Light Pink" alt="Light Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A25.png" title="Pink" alt="Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A11.png" title="Powder Blue" alt="Powder Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A13.png" title="Aqua" alt="Aqua">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A66.png" title="Navy" alt="Navy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A34.png" title="Orange" alt="Orange">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A3.png" title="Red" alt="Red">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A47.png" title="Purple" alt="Purple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A91.png" title="Charcoal" alt="Charcoal">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/A6.png" title="Black" alt="Black">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX1.png" title="EX1" alt="EX1">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX2.png" title="EX2" alt="EX2">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX3.png" title="EX3" alt="EX3">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX5.png" title="EX5" alt="EX5">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX6.png" title="EX6" alt="EX6">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX7.png" title="EX7" alt="EX7">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX8.png" title="EX8" alt="EX8">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX9.png" title="EX9" alt="EX9">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Button/EX10.png" title="EX10" alt="EX10">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="design-style-wrapper">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="ContrastThreadScroll" data-style="buttonThread">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A8.png" title="White" alt="White">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A1.png" title="Cream" alt="Cream">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A16.png" title="Yellow" alt="Yellow">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A92.png" title="Light Green" alt="Light Green">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A11.png" title="Light Blue" alt="Light Blue">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A66.png" title="Navy" alt="Navy">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A47.png" title="Purple" alt="Purple">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A25.png" title="Pink" alt="Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A37.png" title="Light Pink" alt="Light Pink">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A3.png" title="Red" alt="Red">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A34.png" title="Orange" alt="Orange">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A52.png" title="Bronze" alt="Bronze">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A91.png" title="Chacoal" alt="Chacoal">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A6.png" title="Black" alt="Black">
                                                      <figcaption></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list design-monogram-contrast">
                              <p>
                                 <span>3.3</span>
                                 <span data-lang="choose-your-monogram-prosition">Choose your Monogram Position</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper design-monogram">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="ContrastMonogramScroll" data-style="monogram">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Monogram/No-Mono.png">
                                                      <figcaption><span>No Monogram</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Monogram/On-Sleeve.png">
                                                      <figcaption><span>On Sleeve</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Monogram/On-Chest.png">
                                                      <figcaption><span>On Chest</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Monogram/On-Bottom.png">
                                                      <figcaption><span>On Bottom</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Monogram/On-Back.png">
                                                      <figcaption><span>On Back</span></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="design-monogram-options">
                                       <div class="design-monogram-script">
                                          <p>
                                             <span class="design-script-overflow">
                                             <span></span>
                                             <span data-lang="enter-desired-monogram">Enter Desired Monogram/Initials { English Script Only}</span>
                                             </span>
                                          </p>
                                          <div class="design-monogram-input-text">
                                             <input tyle="text" class="design-monogram-text" maxlength="10" placeholder="Enter Desired Monogram/Initials">
                                             <span>{English Script Only}</span>
                                             <div class="clearfix"></div>
                                          </div>
                                          <p>
                                             <span class="design-script-overflow">
                                             <span></span>
                                             <span data-lang="monogram-color">Monogram Color</span>
                                             </span>
                                          </p>
                                       </div>
                                       <div class="design-style-wrapper">
                                          <div class="design-style-scroll-content">
                                             <div class="design-style-scroll" data-scroll="ContrastMonogramColorScroll" data-style="buttonThread">
                                                <ul>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A8.png" title="White" alt="White">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A1.png" title="Cream" alt="Cream">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A16.png" title="Yellow" alt="Yellow">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A92.png" title="Light Green" alt="Light Green">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A11.png" title="Light Blue" alt="Light Blue">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A66.png" title="Navy" alt="Navy">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A47.png" title="Purple" alt="Purple">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A25.png" title="Pink" alt="Pink">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A37.png" title="Light Pink" alt="Light Pink">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A3.png" title="Red" alt="Red">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A34.png" title="Orange" alt="Orange">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A52.png" title="Bronze" alt="Bronze">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A91.png" title="Chacoal" alt="Chacoal">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                   <li>
                                                      <figure>
                                                         <img data-toggle="tooltip" class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Button/Thread/A6.png" title="Black" alt="Black">
                                                         <figcaption></figcaption>
                                                      </figure>
                                                   </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                             </div>
                                          </div>
                                          <em data-lang="monogram-color">Monogram Color</em>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="design-style-list design-embroidered-contrast">
                              <p>
                                 <span>3.4</span>
                                 <span>SELECT YOUR EMBROIDERY STYLE</span>
                              </p>
                              <div class="design-style-block">
                                 <div class="design-style-block-inner">
                                    <div class="design-style-wrapper design-embroidered">
                                       <div class="design-style-scroll-content">
                                          <div class="design-style-scroll" data-scroll="ContrastEmbroideredScroll" data-style="extra">
                                             <ul>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Extra/No-Extra.png">
                                                      <figcaption><span>No Extra</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Extra/number.png">
                                                      <figcaption><span>Extra Number</span></figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure>
                                                      <img class="lazyLoad" data-original="https://www.itailor.com/images/models/polo/Menu/Extra/text.png">
                                                      <figcaption><span>Extra Text</span></figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="design-embroidered-options">
                                       <div class="design-embroidered-script">
                                          <div class="design-embroidered-block design-embroidered-condition" data-cond="number">
                                             <p>
                                                <span class="design-script-overflow">
                                                <span></span>
                                                <span>Select Number</span>
                                                </span>
                                             </p>
                                             <div id="embroidered-number" class="design-embroidered-list sz-dropdown">
                                                <div class="sz-dropdown-text">0</div>
                                                <div class="sz-dropdown-inner">
                                                   <div class="sz-dropdown-overflow">
                                                      <ul class="sz-dropdown-list">
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>0</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>1</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>2</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>3</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>4</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>5</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>6</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>7</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>8</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>9</span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="design-embroidered-block design-embroidered-condition" data-cond="text">
                                             <p>
                                                <span class="design-script-overflow">
                                                <span></span>
                                                <span>Select Text</span>
                                                </span>
                                             </p>
                                             <div id="embroidered-text" class="design-embroidered-list sz-dropdown">
                                                <div class="sz-dropdown-text">A</div>
                                                <div class="sz-dropdown-inner">
                                                   <div class="sz-dropdown-overflow">
                                                      <ul class="sz-dropdown-list">
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>A</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>B</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>C</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>D</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>E</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>F</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>G</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>H</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>I</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>J</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>K</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>L</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>M</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>N</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>O</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>P</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>Q</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>R</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>S</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>T</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>U</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>V</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>W</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>X</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>Y</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>Z</span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="design-embroidered-block">
                                             <p>
                                                <span class="design-script-overflow">
                                                <span></span>
                                                <span>Select Color</span>
                                                </span>
                                             </p>
                                             <div id="embroidered-color" class="design-embroidered-list sz-dropdown">
                                                <div class="sz-dropdown-text">Black</div>
                                                <div class="sz-dropdown-inner">
                                                   <div class="sz-dropdown-overflow">
                                                      <ul class="sz-dropdown-list">
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>Black</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>Navy</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>Red</span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="design-embroidered-block">
                                             <p>
                                                <span class="design-script-overflow">
                                                <span></span>
                                                <span>Select Position</span>
                                                </span>
                                             </p>
                                             <div id="embroidered-position" class="design-embroidered-list sz-dropdown">
                                                <div class="sz-dropdown-text">On Waist</div>
                                                <div class="sz-dropdown-inner">
                                                   <div class="sz-dropdown-overflow">
                                                      <ul class="sz-dropdown-list">
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>On Waist</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>On Back</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>On Chest</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>On Left Sleeve</span></a>
                                                         </li>
                                                         <li class="sz-dropdown-item">
                                                            <a href="Javascript:void(0);"><span>On Right Sleeve</span></a>
                                                         </li>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <section id="mastmenu">
                     <div class="menu-content">
                        <div class="menu-inner">
                           <aside class="logo-3d">
                              <img src="https://www.itailor.com/designpolo/assets/images/iTaiolor-logo-3d.png" width="152" height="71">
                           </aside>
                           <div class="menu-wrapper">
                              <ul>
                                 <li class="menu-list">
                                    <h4>
                                       <span>1.</span>
                                       <b data-lang="fabric">Fabric</b>
                                    </h4>
                                    <div class="menu-sub-list">
                                       <div class="menu-sub-list-wrapper">
                                          <div class="menu-sub-list-content">
                                             <div class="menu-sub-list-inner">
                                                <ul id="fabric-selection" class="text-uppercase">
                                                   <li class="">
                                                      <div class="background-effect"></div>
                                                      <span data-lang="promotion">Promotion</span>
                                                      <!-- <span class="category-price" style="positon: relative; color: #46D5FE;"><div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>&#36; 44.95</span> -->
                                                      <span class="category-price">&#36; 39.95</span>
                                                   </li>
                                                   <li class="">
                                                      <div class="background-effect"></div>
                                                      <span data-lang="dri-fit">Dri Fit</span>
                                                      <!-- <span class="category-price" style="positon: relative; color: #46D5FE;"><div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>&#36; 49.95</span> -->
                                                      <span class="category-price">&#36; 44.95</span>
                                                   </li>
                                                   <li class="">
                                                      <div class="background-effect"></div>
                                                      <span data-lang="prema">Premium Polo</span>
                                                      <!-- <span class="category-price" style="positon: relative; color: #46D5FE;"><div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>&#36; 54.95</span> -->
                                                      <span class="category-price">&#36; 49.95</span>
                                                   </li>
                                                   <li class="combination">
                                                      <div class="background-effect"></div>
                                                      <span data-lang="combination">Combination</span>
                                                      <!-- <span class="category-price" style="positon: relative; color: #46D5FE;"><div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>&#36; 59.95</span> -->
                                                      <span class="category-price">&#36; 54.95</span>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="menu-list">
                                    <h4>
                                       <span>2.</span>
                                       <b data-lang="style">Style</b>
                                    </h4>
                                    <div class="menu-sub-list">
                                       <div class="menu-sub-list-wrapper">
                                          <div class="menu-sub-list-content">
                                             <div class="menu-sub-list-inner">
                                                <ul>
                                                   <li class="menu-style-collar">
                                                      <div class="background-effect"></div>
                                                      <span class="index">2.1</span>
                                                      <span data-lang="collar">Collar</span>
                                                   </li>
                                                   <li class="menu-style-sleeve">
                                                      <div class="background-effect"></div>
                                                      <span class="index">2.2</span>
                                                      <span data-lang="sleeve">Sleeve</span>
                                                   </li>
                                                   <li class="menu-style-front">
                                                      <div class="background-effect"></div>
                                                      <span class="index">2.3</span>
                                                      <span data-lang="front">Front</span>
                                                   </li>
                                                   <li class="menu-style-pocket">
                                                      <div class="background-effect"></div>
                                                      <span class="index">2.4</span>
                                                      <span data-lang="pockets">Pockets</span>
                                                   </li>
                                                   <li class="menu-style-bottom">
                                                      <div class="background-effect"></div>
                                                      <span class="index">2.5</span>
                                                      <span data-lang="bottom">Bottom</span>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="menu-list">
                                    <h4>
                                       <span>3.</span>
                                       <b data-lang="color-contrast">Color Contrast</b>
                                    </h4>
                                    <div class="menu-sub-list">
                                       <div class="menu-sub-list-wrapper">
                                          <div class="menu-sub-list-content">
                                             <div class="menu-sub-list-inner">
                                                <ul>
                                                   <li class="menu-contrast-contrast">
                                                      <div class="background-effect"></div>
                                                      <span class="index">3.1</span>
                                                      <span data-lang="contrast">Contrast</span>
                                                   </li>
                                                   <li class="menu-contrast-button">
                                                      <div class="background-effect"></div>
                                                      <span class="index">3.2</span>
                                                      <span data-lang="buttons">Buttons</span>
                                                   </li>
                                                   <li class="menu-contrast-monogram">
                                                      <div class="background-effect"></div>
                                                      <span class="index">3.3</span>
                                                      <span data-lang="monogram">Monogram</span>
                                                   </li>
                                                   <li class="menu-contrast-embroidery">
                                                      <div class="background-effect"></div>
                                                      <span class="index">3.4</span>
                                                      <span>Add Embroidery</span>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="menu-list menu-list-measurement">
                                    <h4>
                                       <span>4.</span>
                                       <b data-lang="measurements">Measurements</b>
                                    </h4>
                                    <div class="menu-sub-list">
                                       <div class="menu-sub-list-wrapper">
                                          <div class="menu-sub-list-content">
                                             <div class="menu-sub-list-inner">
                                                <ul>
                                                   <li>
                                                      <div class="background-effect"></div>
                                                      <span class="index">4.1</span>
                                                      <span>Measurement</span>
                                                   </li>
                                                   <li>
                                                      <div class="background-effect"></div>
                                                      <span class="index">4.2</span>
                                                      <span>Measurement Body Size</span>
                                                   </li>
                                                   <li>
                                                      <div class="background-effect"></div>
                                                      <span class="index">4.3</span>
                                                      <span>Measurement Standard Size</span>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="divider"></li>
                                 <li class="shopping-bag">
                                    <p data-lang="shopping-bag">Shopping Bag</p>
                                    <p>
                                       <span class="shopping-item">0</span>
                                       <span data-lang="Items">Items</span>
                                    </p>
                                    <ul class="shopping-bag-detail hide-block">
                                       <li>
                                          <span class="shopping-detail" data-lang="total">Total</span>
                                          <span>:</span>
                                          <span class="sum-price shopping-total">0</span>
                                       </li>
                                       <li>
                                          <span class="shopping-detail" data-lang="shipping">Shipping</span>
                                          <span>:</span>
                                          <span class="sum-price shopping-shipping">0</span>
                                       </li>
                                       <li>
                                          <span class="shopping-detail" data-lang="subtotal">Subtotal</span>
                                          <span>:</span>
                                          <span class="sum-price shopping-subtotal">0</span>
                                       </li>
                                    </ul>
                                 </li>
                                 <li class="checkout">
                                    <a href="Javascript:void(0);" class="checkout-button designer-checkout"><span data-lang="checkout">CHECK OUT</span></a>
                                 </li>
                              </ul>
                           </div>
                           <div class="menu-wrapper-mobile">
                              <div class="menu-wrapper-mobile-list">
                                 <div class="menu-wrapper-mobile-scroll">
                                    <ul class="text-uppercase">
                                       <li class="">
                                          <div class="background-effect"></div>
                                          <span data-lang="promotion">Promotion</span>
                                          <span class="category-price" style="positon: relative; color: #46D5FE;">
                                             <div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>
                                             &#36; 44.95
                                          </span>
                                          <span class="category-price">&#36; 39.95</span>
                                       </li>
                                       <li class="">
                                          <div class="background-effect"></div>
                                          <span data-lang="dri-fit">Dri Fit</span>
                                          <span class="category-price" style="positon: relative; color: #46D5FE;">
                                             <div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>
                                             &#36; 49.95
                                          </span>
                                          <span class="category-price">&#36; 44.95</span>
                                       </li>
                                       <li class="">
                                          <div class="background-effect"></div>
                                          <span data-lang="prema">Premium Polo</span>
                                          <span class="category-price" style="positon: relative; color: #46D5FE;">
                                             <div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>
                                             &#36; 54.95
                                          </span>
                                          <span class="category-price">&#36#36; 49.95</span>
                                       </li>
                                       <li class="combination">
                                          <div class="background-effect"></div>
                                          <span data-lang="combination">Combination</span>
                                          <span class="category-price" style="positon: relative; color: #46D5FE;">
                                             <div style="position: absolute; top: 45%; left: -5%; width: 110%; border-top: solid 1px #cccccc;"></div>
                                             &#36; 59.95
                                          </span>
                                          <span class="category-price">&#36; 54.95</span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="menu-wrapper-mobile-list">
                                 <div class="menu-wrapper-mobile-scroll">
                                    <ul>
                                       <li class="menu-style-collar">
                                          <div class="background-effect"></div>
                                          <span class="index">2.1</span>
                                          <span data-lang="collar">Collar</span>
                                       </li>
                                       <li class="menu-style-sleeve">
                                          <div class="background-effect"></div>
                                          <span class="index">2.2</span>
                                          <span data-lang="sleeve">Sleeve</span>
                                       </li>
                                       <li class="menu-style-front">
                                          <div class="background-effect"></div>
                                          <span class="index">2.3</span>
                                          <span data-lang="front">Front</span>
                                       </li>
                                       <li class="menu-style-pocket">
                                          <div class="background-effect"></div>
                                          <span class="index">2.4</span>
                                          <span data-lang="pockets">Pockets</span>
                                       </li>
                                       <li class="menu-style-bottom">
                                          <div class="background-effect"></div>
                                          <span class="index">2.5</span>
                                          <span data-lang="bottom">Bottom</span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="menu-wrapper-mobile-list">
                                 <div class="menu-wrapper-mobile-scroll">
                                    <ul>
                                       <li class="menu-contrast-contrast">
                                          <div class="background-effect"></div>
                                          <span class="index">3.1</span>
                                          <span data-lang="contrast">Contrast</span>
                                       </li>
                                       <li class="menu-contrast-button">
                                          <div class="background-effect"></div>
                                          <span class="index">3.2</span>
                                          <span data-lang="buttons">Buttons</span>
                                       </li>
                                       <li class="menu-contrast-monogram">
                                          <div class="background-effect"></div>
                                          <span class="index">3.3</span>
                                          <span data-lang="monogram">Monogram</span>
                                       </li>
                                       <li class="menu-contrast-embroidery">
                                          <div class="background-effect"></div>
                                          <span class="index">3.4</span>
                                          <span>Add Embroidery</span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               </section>
            </div>
            <h1 style="padding: 30px 0 50px; text-align: center;color: #696d79;text-transform: uppercase;font-size: 16px;">Custom Made Polo</h1>
         </section>
      </section>
      <div id="popup" class="popupMaya">
         <div class="popupMaya-history"></div>
         <div class="popupMaya-data"></div>
         <div class="popupMaya-alert"></div>
      </div>
      <div id="zoom-fabric-content">
         <div class="zoom-fabric-inner">
            <div class="close-zoom close-zoom-button">
               <img src="https://www.itailor.com/designpolo/assets/images/CloseButton.png">
            </div>
            <div class="zoom-image"></div>
         </div>
         <div class="close-zoom-area close-zoom-button"></div>
      </div>
      <div id="confirm-leave-design">
         <div class="leave-design-inner">
            <p>Are you sure you want to leave The POLO Designer?</p>
            <p>Your selection will not be saved</p>
            <ul>
               <li class="ok-confirm-button"><span data-lang="ok">OK</span></li>
               <li class="close-confirm-button"><span data-lang="no">NO</span></li>
            </ul>
         </div>
         <div class="close-confirm-design close-confirm-button"></div>
      </div>
      <div id="corona">
         <style>
            #mastcontainer {
            top: 50px !important;
            }
            #corona {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: rgba(0, 0, 0, 0.6);
            z-index: 0;
            opacity: 0;
            visibility: hidden;
            -webkit-transition: opacity 300ms, z-index 0ms 300ms, visibility 0ms 300ms;
            -moz-transition: opacity 300ms, z-index 0ms 300ms, visibility 0ms 300ms;
            transition: opacity 300ms, z-index 0ms 300ms, visibility 0ms 300ms;
            }
            #corona.active {
            -webkit-transition: opacity 300ms, z-index 0ms 0ms, visibility 0ms 0ms;
            -moz-transition: opacity 300ms, z-index 0ms 0ms, visibility 0ms 0ms;
            transition: opacity 300ms, z-index 0ms 0ms, visibility 0ms 0ms;
            z-index: 9999;
            opacity: 1;
            visibility: visible;
            }
            #corona .close {
            position: absolute;
            right: 0;
            top: 0;
            width: 52px;
            height: 48px;
            cursor: pointer;
            z-index: 2;
            }
            #corona article {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            max-width: 640px;
            height: 0;
            margin: auto;
            }
            #corona p {
            position: relative;
            padding: 3% 5%;
            box-shadow: 0 0 15px #444;
            border: 1px solid #444;
            background: #000;
            color: #fff;
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            transform: translateY(-50%);
            }
            #corona p span {
            margin: 0.75em 0;
            line-height: 1.25;
            display: block;
            }
            #corona-img,
            #corona-img img {
            width: 320px;
            }
            #corona-img {
            position: absolute;
            left: 0;
            right: 0;
            margin: auto;
            overflow: hidden;
            cursor: pointer;
            -webkit-transform: translateX(83px);
            -moz-transform: translateX(83px);
            transform: translateX(83px);
            }
            #corona-img img {
            margin-left: 19px;
            max-width: none;
            }
            @media screen and (max-width: 768px) {
            #mastcontainer {
            top: 0px !important;
            }
            #corona-img {
            top: 0;
            -webkit-transform: translateX(0);
            -moz-transform: translateX(0);
            transform: translateX(0);
            }
            }
            @media screen and (max-width: 640px) {
            #corona p {
            font-size: 0.875em;
            }
            }
            @media screen and (max-width: 480px) {
            #mastcontainer {
            top: 50px !important;
            }
            #corona p {
            font-size: 0.8125em;
            }
            #corona-img,
            #corona-img img {
            width: 260px;
            }
            }
            @media screen and (max-width: 375px) {
            #corona p {
            font-size: 0.75em;
            }
            }
         </style>
         <article>
            <p>
               <span>Corona Virus Updates</span>
               <span>We are open!</span>
               <span>Please enjoy designing and order your custom made clothes at the best price in the world anywhere! The delivery is 8-10 weeks, for more information our team can assist you at info@itailor.com or support@itailor.com</span>
               <i class="close"><img src="https://www.itailor.com/designpolo/assets/images/CloseButton.png"></i>
            </p>
         </article>
      </div>
      <!-- Google Code for Main List -->
      <!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
      <script type="text/javascript">
         /* <![CDATA[ */
         var google_conversion_id = 1020984207;
         var google_conversion_label = "ITo0CMmmsgMQj_fr5gM";
         var google_custom_params = window.google_tag_params;
         var google_remarketing_only = true;
         /* ]]> */
      </script>
      <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
      <noscript>
         <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1020984207/?value=0&amp;label=ITo0CMmmsgMQj_fr5gM&amp;guid=ON&amp;script=0"/>
         </div>
      </noscript>
      <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-20112889-1']);
         _gaq.push(['_trackPageview']);
         
         (function() {
             var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
             ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
             var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
      </script>
      <!-- Web Analytics -->
      
      <script src="//static.getclicky.com/js" type="text/javascript"></script>
      <script type="text/javascript">try {clicky.init(100978324);}catch(e){}</script>
      <!-- Facebook Pixel Code by Design Providers -->
      <script>
         !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
         n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
         n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
         t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
         document,'script','https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '1045757025555236'); // Insert your pixel ID here.
         fbq('track', 'PageView');
         fbq('track', 'ViewContent');
         fbq('track', 'Search');
      </script>
      <!-- DO NOT MODIFY -->
      <!-- End Facebook Pixel Code -->
      <!-- Bing Code by Design Providers -->
      <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5715244"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
      <noscript><img src="//bat.bing.com/action/0?ti=5715244&Ver=2" height="0" width="0" /></noscript>
      <!-- Clicky -->
      <script>var clicky_site_ids = clicky_site_ids || []; clicky_site_ids.push(101238997);</script>
      <script async src="js/staticcom.js"></script>        <script>
         window.publicObject = {customer: {PERSONAL: null}};
         window.PROJECT_DB = {baseUrl: "https:\/\/www.itailor.com\/designpolo\/", designer: {"gobal":{"product":"polo","curr":"USD","language":"English","sign":"&#36;","country":"India","detect":"computer","ip":"157.49.218.226"},"size":{"body":{"inch":{"neck":{"min":9,"max":23},"chest":{"min":28,"max":75},"waist":{"min":24,"max":73},"hip":{"min":24,"max":73},"length":{"min":19,"max":42},"shoulder":{"min":14,"max":27},"sleeve":{"min":18,"max":30},"shortsleeve":{"min":8.5,"max":20}},"cm":{"neck":{"min":23,"max":58},"chest":{"min":71,"max":190},"waist":{"min":61,"max":185.25},"hip":{"min":71,"max":190},"length":{"min":48,"max":108},"shoulder":{"min":35,"max":69},"sleeve":{"min":46,"max":77},"shortsleeve":{"min":22,"max":51}}},"standard":{"inch":{"s":{"size":"s","neck":15,"chest":39.75,"waist":35.01,"hip":38.94,"length":29,"shoulder":17,"sleeve":24.5,"armhole":19.5,"bicep":0,"cuff":9,"sleeveshort":9,"cuffshort":12.6,"text":"33-36"},"m":{"size":"m","neck":16,"chest":42.5,"waist":38.16,"hip":41.11,"length":29.5,"shoulder":18,"sleeve":25.25,"armhole":20.5,"bicep":0,"cuff":9.5,"sleeveshort":9.75,"cuffshort":13.3,"text":"36-39"},"l":{"size":"l","neck":16.5,"chest":45.5,"waist":42.09,"hip":43.08,"length":30,"shoulder":19,"sleeve":25.75,"armhole":21.5,"bicep":0,"cuff":10,"sleeveshort":10.75,"cuffshort":14,"text":"39-42"},"xl":{"size":"xl","neck":17,"chest":47.5,"waist":44.46,"hip":45.64,"length":31,"shoulder":20,"sleeve":26,"armhole":21.5,"bicep":0,"cuff":10.5,"sleeveshort":11.25,"cuffshort":15,"text":"42-44"},"xxl":{"size":"xxl","neck":18,"chest":50,"waist":47.02,"hip":48,"length":32,"shoulder":20.5,"sleeve":27,"armhole":22.5,"bicep":0,"cuff":10.12,"sleeveshort":11.75,"cuffshort":15.5,"text":"44-47"},"3xl":{"size":"3xl","neck":19,"chest":53,"waist":49.97,"hip":51.94,"length":33,"shoulder":22,"sleeve":27.25,"armhole":23.5,"bicep":0,"cuff":10.56,"sleeveshort":12,"cuffshort":16,"text":"47-50"},"4xl":{"size":"4xl","neck":20,"chest":56,"waist":53.12,"hip":54.5,"length":34,"shoulder":23,"sleeve":27.5,"armhole":25,"bicep":0,"cuff":11,"sleeveshort":12.5,"cuffshort":16.5,"text":"50-53"}},"cm":{"s":{"size":"s","neck":38.1,"chest":100.97,"waist":88.92,"hip":98.92,"length":73.66,"shoulder":43.18,"sleeve":62.23,"armhole":49.53,"bicep":0,"cuff":22.86,"sleeveshort":22.86,"cuffshort":32,"text":"89-91.5"},"m":{"size":"m","neck":40.64,"chest":107.95,"waist":96.92,"hip":104.42,"length":74.93,"shoulder":45.72,"sleeve":64.14,"armhole":52.07,"bicep":0,"cuff":24.13,"sleeveshort":24.77,"cuffshort":33.78,"text":"91.5-99"},"l":{"size":"l","neck":41.91,"chest":115.57,"waist":106.92,"hip":109.42,"length":76.2,"shoulder":48.26,"sleeve":65.41,"armhole":54.61,"bicep":0,"cuff":25.4,"sleeveshort":27.31,"cuffshort":35.56,"text":"99-106.5"},"xl":{"size":"xl","neck":43.18,"chest":120.65,"waist":112.92,"hip":115.92,"length":78.74,"shoulder":50.8,"sleeve":66.04,"armhole":54.61,"bicep":0,"cuff":26.67,"sleeveshort":28.58,"cuffshort":38.1,"text":"106.5-112"},"xxl":{"size":"xxl","neck":45.72,"chest":127,"waist":119.42,"hip":121.92,"length":81.28,"shoulder":52.07,"sleeve":68.58,"armhole":57.15,"bicep":0,"cuff":25.7,"sleeveshort":29.85,"cuffshort":39.37,"text":"112-119.5"},"3xl":{"size":"3xl","neck":48.26,"chest":134.62,"waist":126.92,"hip":131.92,"length":83.82,"shoulder":55.88,"sleeve":69.22,"armhole":59.69,"bicep":0,"cuff":26.82,"sleeveshort":30.48,"cuffshort":40.64,"text":"119.5-127"},"4xl":{"size":"4xl","neck":50.8,"chest":142.24,"waist":134.92,"hip":138.42,"length":86.36,"shoulder":58.42,"sleeve":69.85,"armhole":63.5,"bicep":0,"cuff":27.94,"sleeveshort":31.75,"cuffshort":41.91,"text":"127-134.5"}}}},"category":[{"id":"117","name":"Promotion","nameStr":"Promotion","price":"39.95","regular":"44.95"},{"id":"118","name":"Dri Fit","nameStr":"Dri Fit","price":"44.95","regular":"49.95"},{"id":"119","name":"Premium Polo","nameStr":"Premium Polo","price":"49.95","regular":"54.95"},{"id":"120","name":"Combination","nameStr":"Combination","price":"54.95","regular":"59.95"}],"fabric":{"117":[{"id":"BKD350","name":"Deep Black","category":"117"},{"id":"BLD360","name":"Deep Navy","category":"117"},{"id":"RDD335","name":"Cherry","category":"117"}],"118":[{"id":"WHD100","name":"White","category":"118"},{"id":"BLD225","name":"PVC Blue","category":"118"},{"id":"BLD230","name":"Carolina Blue","category":"118"},{"id":"BLD240","name":"French Blue","category":"118"},{"id":"BLD310","name":"Pacific Blue","category":"118"},{"id":"BLD220","name":"Scuba Blue","category":"118"},{"id":"GYD220","name":"Cement","category":"118"},{"id":"BLD335","name":"Shocking Blue","category":"118"},{"id":"GRD240","name":"Green Apple","category":"118"},{"id":"GRD260","name":"Dark Green","category":"118"},{"id":"YLD110","name":"Corn","category":"118"},{"id":"YLD215","name":"Yellow","category":"118"},{"id":"ORD315","name":"Orange Peach","category":"118"},{"id":"PKD110","name":"Baby Pink","category":"118"},{"id":"S/RDD220","name":"Pink Candy","category":"118"},{"id":"S/PKD320","name":"Shocking Pink","category":"118"},{"id":"S/RDD350","name":"Burgundy","category":"118"},{"id":"S/PLD220","name":"Lilac","category":"118"},{"id":"S/PLD323","name":"Violet Blooming Rose","category":"118"},{"id":"S/PLD325","name":"Dark Purple","category":"118"},{"id":"S/BRD110","name":"Oatmeal","category":"118"},{"id":"S/BRD210","name":"Khaki","category":"118"},{"id":"S/BRD320","name":"Chocolate","category":"118"},{"id":"S/GYD120","name":"Smoke","category":"118"}],"119":[{"id":"PoloPT006","name":"PoloPT006","category":"119"},{"id":"PoloPT014","name":"PoloPT014","category":"119"},{"id":"PoloPT015","name":"PoloPT015","category":"119"},{"id":"PoloPT017","name":"PoloPT017","category":"119"},{"id":"PoloPT018","name":"PoloPT018","category":"119"},{"id":"PoloPT019","name":"PoloPT019","category":"119"},{"id":"PoloPT021","name":"PoloPT021","category":"119"},{"id":"PoloPT023","name":"PoloPT023","category":"119"},{"id":"PoloPT024","name":"PoloPT024","category":"119"},{"id":"PoloPT025","name":"PoloPT025","category":"119"}]},"combination":[{"id":"No","name":"No","path":"No"},{"id":"CM-1","name":"CM 1","path":"CM-1"},{"id":"CM-2","name":"CM 2","path":"CM-2"},{"id":"CM-3","name":"CM 3","path":"CM-3"},{"id":"CM-4","name":"CM 4","path":"CM-4"},{"id":"CM-5","name":"CM 5","path":"CM-5"},{"id":"CM-6","name":"CM 6","path":"CM-6"},{"id":"CM-7","name":"CM 7","path":"CM-7"},{"id":"CM-8","name":"CM 8","path":"CM-8"},{"id":"CM-9","name":"CM 9","path":"CM-9"}],"style":{"collar":[{"id":"CL-1","name":"CL-1","path":"CL-1"},{"id":"CL-2","name":"CL-2","path":"CL-2"},{"id":"CL-3","name":"CL-3","path":"CL-3"},{"id":"CL-4","name":"CL-4","path":"CL-4"}],"sleeve":[{"id":"SL-1","name":"Short Sleeve","type":"Short Sleeve","path":"ShortSleeve"},{"id":"SL-2","name":"Short Sleeve + Cuff","type":"Short Sleeve Cuff","path":"ShortSleeve"},{"id":"SL-3","name":"Long Sleeve","type":"Long Sleeve","path":"LongSleeve"},{"id":"SL-4","name":"Long Sleeve + Elbow","type":"Long Sleeve Elbow","path":"LongSleeve"}],"front":[{"id":"FS-1","name":"FS-1","path":"2Button"},{"id":"FS-2","name":"FS-2","path":"3Button"},{"id":"FS-3","name":"FS-3","path":"3ButtonSlim"},{"id":"FS-4","name":"FS-4","path":"4Button"}],"pocket":[{"id":"No-pocket","path":"none","status":"Y","fk":"false","tp":"false","button":"false","option":"false","name":"No Pocket"},{"id":"PK-1","path":"PK-1","status":"Y","fk":"mix-p","tp":"false","button":"false","option":"false","name":"Classic Round"},{"id":"PK-2","path":"PK-2","status":"Y","fk":"mix-p","tp":"false","button":"false","option":"false","name":"Classic Angle"},{"id":"PK-3","path":"PK-3","status":"Y","fk":"mix-p","tp":"false","button":"false","option":"false","name":"Diamond Straight"},{"id":"PK-4","path":"PK-4","status":"Y","fk":"mix-p","tp":"false","button":"false","option":"false","name":"Classic Square"},{"id":"PK-5","path":"PK-5","status":"Y","fk":"fk-2","tp":"true","button":"true","option":"false","name":"Round Flap"},{"id":"PK-6","path":"PK-6","status":"Y","fk":"fk-1","tp":"false","button":"true","option":"false","name":"Angle Flap"},{"id":"PK-7","path":"PK-7","status":"Y","fk":"fk-1","tp":"false","button":"true","option":"false","name":"Diamond Flap"}],"bottom":[{"id":"BT-1","name":"BT-1","path":"Straight"},{"id":"BT-2","name":"BT-2","path":"Straight-Vent"},{"id":"BT-3","name":"BT-3","path":"Vent-Uneven"}]},"contrast":{"contrast":[{"id":"WHD100","name":"White"},{"id":"BLD225","name":"PVC Blue"},{"id":"BLD230","name":"Carolina Blue"},{"id":"BLD240","name":"French Blue"},{"id":"BLD310","name":"Pacific Blue"},{"id":"BKD350","name":"Deep Black"},{"id":"BLD220","name":"Scuba Blue"},{"id":"GYD220","name":"Cement"},{"id":"BLD335","name":"Shocking Blue"},{"id":"BLD360","name":"Deep Navy"},{"id":"GRD240","name":"Green Apple"},{"id":"GRD260","name":"Dark Green"},{"id":"YLD110","name":"Corn"},{"id":"YLD215","name":"Yellow"},{"id":"ORD315","name":"Orange Peach"},{"id":"PKD110","name":"Baby Pink"},{"id":"S/RDD220","name":"Pink Candy"},{"id":"S/PKD320","name":"Shocking Pink"},{"id":"RDD335","name":"Cherry"},{"id":"S/RDD350","name":"Burgundy"},{"id":"S/PLD220","name":"Lilac"},{"id":"S/PLD323","name":"Violet Blooming Rose"},{"id":"S/PLD325","name":"Dark Purple"},{"id":"S/BRD110","name":"Oatmeal"},{"id":"S/BRD210","name":"Khaki"},{"id":"S/BRD320","name":"Chocolate"},{"id":"S/GYD120","name":"Smoke"}],"contrastShirt":[{"id":"TDA14925-1","name":"Shirt Pattern Fabric"},{"id":"TDA14925-2","name":"Black Shirt Fabric"},{"id":"TDA14925-3","name":"Shirt Fabric"},{"id":"TDA15295-1","name":"Print Shirt Fabric"},{"id":"TDA15295-3","name":"Print Shirt Fabric"}],"button":[{"id":"A8","name":"White","path":"A8"},{"id":"A1","name":"Cream","path":"A1"},{"id":"A16","name":"Yellow","path":"A16"},{"id":"A92","name":"Light Green","path":"A92"},{"id":"A37","name":"Light Pink","path":"A37"},{"id":"A25","name":"Pink","path":"A25"},{"id":"A11","name":"Powder Blue","path":"A11"},{"id":"A13","name":"Aqua","path":"A13"},{"id":"A66","name":"Navy","path":"A66"},{"id":"A34","name":"Orange","path":"A34"},{"id":"A3","name":"Red","path":"A3"},{"id":"A47","name":"Purple","path":"A47"},{"id":"A91","name":"Charcoal","path":"A91"},{"id":"A6","name":"Black","path":"A6"},{"id":"EX1","name":"EX1","path":"EX1"},{"id":"EX2","name":"EX2","path":"EX2"},{"id":"EX3","name":"EX3","path":"EX3"},{"id":"EX5","name":"EX5","path":"EX5"},{"id":"EX6","name":"EX6","path":"EX6"},{"id":"EX7","name":"EX7","path":"EX7"},{"id":"EX8","name":"EX8","path":"EX8"},{"id":"EX9","name":"EX9","path":"EX9"},{"id":"EX10","name":"EX10","path":"EX10"}],"buttonThread":[{"id":"A8","path":"A8","code":"#FFFFFF","name":"White"},{"id":"A1","path":"A1","code":"#e2ddb5","name":"Cream"},{"id":"A16","path":"A16","code":"#fcff06","name":"Yellow"},{"id":"A92","path":"A92","code":"#95ce88","name":"Light Green"},{"id":"A11","path":"A11","code":"#8b9bef","name":"Light Blue"},{"id":"A66","path":"A66","code":"#0c1f61","name":"Navy"},{"id":"A47","path":"A47","code":"#6d45b8","name":"Purple"},{"id":"A25","path":"A25","code":"#ea85c7","name":"Pink"},{"id":"A37","path":"A37","code":"#f9cfde","name":"Light Pink"},{"id":"A3","path":"A3","code":"#7c0c11","name":"Red"},{"id":"A34","path":"A34","code":"#fb6e07","name":"Orange"},{"id":"A52","path":"A52","code":"#bcaa87","name":"Bronze"},{"id":"A91","path":"A91","code":"#676b6a","name":"Chacoal"},{"id":"A6","path":"A6","code":"#000000","name":"Black"}],"monogram":[{"id":"No-Mono","name":"No Monogram"},{"id":"On-Sleeve","name":"On Sleeve"},{"id":"On-Chest","name":"On Chest"},{"id":"On-Bottom","name":"On Bottom"},{"id":"On-Back","name":"On Back"}],"extra":{"style":[{"id":"No-Extra","name":"No Extra","path":"No-Extra"},{"id":"number","name":"Extra Number","path":"number"},{"id":"text","name":"Extra Text","path":"text"}],"color":[{"id":"Black","path":"Black","code":"#000000","name":"Black"},{"id":"Navy","path":"Navy","code":"#000080","name":"Navy"},{"id":"Red","path":"Red","code":"#ff0000","name":"Red"}],"position":[{"id":"Waist","name":"On Waist","path":"Waist"},{"id":"Back","name":"On Back","path":"Back"},{"id":"Chest","name":"On Chest","path":"Chest"},{"id":"SleeveLeft","name":"On Left Sleeve","path":"SleeveLeft"},{"id":"SleeveRight","name":"On Right Sleeve","path":"SleeveRight"}],"number":[{"id":0,"name":0,"path":null},{"id":1,"name":1,"path":null},{"id":2,"name":2,"path":null},{"id":3,"name":3,"path":null},{"id":4,"name":4,"path":null},{"id":5,"name":5,"path":null},{"id":6,"name":6,"path":null},{"id":7,"name":7,"path":null},{"id":8,"name":8,"path":null},{"id":9,"name":9,"path":null}],"text":[{"id":"A","name":"A","path":null},{"id":"B","name":"B","path":null},{"id":"C","name":"C","path":null},{"id":"D","name":"D","path":null},{"id":"E","name":"E","path":null},{"id":"F","name":"F","path":null},{"id":"G","name":"G","path":null},{"id":"H","name":"H","path":null},{"id":"I","name":"I","path":null},{"id":"J","name":"J","path":null},{"id":"K","name":"K","path":null},{"id":"L","name":"L","path":null},{"id":"M","name":"M","path":null},{"id":"N","name":"N","path":null},{"id":"O","name":"O","path":null},{"id":"P","name":"P","path":null},{"id":"Q","name":"Q","path":null},{"id":"R","name":"R","path":null},{"id":"S","name":"S","path":null},{"id":"T","name":"T","path":null},{"id":"U","name":"U","path":null},{"id":"V","name":"V","path":null},{"id":"W","name":"W","path":null},{"id":"X","name":"X","path":null},{"id":"Y","name":"Y","path":null},{"id":"Z","name":"Z","path":null}]}},"color":[{"id":"A8","path":"A8","code":"#FFFFFF","name":"White"},{"id":"A1","path":"A1","code":"#e2ddb5","name":"Cream"},{"id":"A16","path":"A16","code":"#fcff06","name":"Yellow"},{"id":"A92","path":"A92","code":"#95ce88","name":"Light Green"},{"id":"A11","path":"A11","code":"#8b9bef","name":"Light Blue"},{"id":"A66","path":"A66","code":"#0c1f61","name":"Navy"},{"id":"A47","path":"A47","code":"#6d45b8","name":"Purple"},{"id":"A25","path":"A25","code":"#ea85c7","name":"Pink"},{"id":"A37","path":"A37","code":"#f9cfde","name":"Light Pink"},{"id":"A3","path":"A3","code":"#7c0c11","name":"Red"},{"id":"A34","path":"A34","code":"#fb6e07","name":"Orange"},{"id":"A52","path":"A52","code":"#bcaa87","name":"Bronze"},{"id":"A91","path":"A91","code":"#676b6a","name":"Chacoal"},{"id":"A6","path":"A6","code":"#000000","name":"Black"}],"defaults":{"SDID":false,"fabric":"BLD230","fabricStr":"Carolina Blue","fabricCategory":"118","category":"118","categoryStr":"Dri Fit","categoryType":"Dri Fit","categoryOption":"","combination":"No","combinationStr":"No","combinationPath":"No","combinationFabric":"BLD230","combinationFabricStr":"Carolina Blue","combinationFabricCategory":"118","price":"","regular":"","collar":"CL-1","collarStr":"CL-1","collarPath":"CL-1","collarStyle":"Normal","sleeve":"SL-1","sleeveStr":"Short Sleeve","sleevePath":"ShortSleeve","sleeveType":"Short Sleeve","front":"FS-1","frontStr":"FS-1","frontPath":"2Button","pocket":"No-pocket","pocketStr":"No Pocket","pocketPath":"none","pocketStyle":"2Pocket","pocketFk":"false","pocketTp":"false","pocketButton":"false","pocketOption":"false","bottom":"BT-1","bottomStr":"BT-1","bottomPath":"Straight","button":"A8","buttonStr":"White","buttonThread":"A8","buttonThreadStr":"White","monogram":"No-Mono","monogramStr":"No Monogram","monogramTxt":"","monogramColor":"A8","monogramColorStr":"White","monogramCode":"#FFFFFF","extra":"No-Extra","extraStr":"No Extra","extraText":"","extraColor":"","extraColorCode":"","extraPosition":"","contrastCollarOutside":"BLD230","contrastCollarOutsideStr":"","contrastCollarOutsideStatus":"N","contrastCollarInside":"BLD230","contrastCollarInsideStr":"","contrastCollarInsideStatus":"N","contrastFrontOutside":"BLD230","contrastFrontOutsideStr":"","contrastFrontOutsideStatus":"N","contrastFrontInside":"BLD230","contrastFrontInsideStr":"","contrastFrontInsideStatus":"N","contrastFrontBox":"BLD230","contrastFrontBoxStr":"","contrastFrontBoxStatus":"N","contrastSleeve":"BLD230","contrastSleeveStr":"","contrastSleeveStatus":"N","contrastSleeveCuff":"BLD230","contrastSleeveCuffStr":"","contrastSleeveCuffStatus":"N","contrastSleeveElbow":"BLD230","contrastSleeveElbowStr":"","contrastSleeveElbowStatus":"N","contrastPocketMain":"BLD230","contrastPocketMainStr":"","contrastPocketMainStatus":"N","contrastPocketTop":"BLD230","contrastPocketTopStr":"","contrastPocketTopStatus":"N","contrastPocketTrimming":"BLD230","contrastPocketTrimmingStr":"","contrastPocketTrimmingStatus":"N","measurement":{"option":"body","standard":{"unit":"inch","size":{"s":0,"m":0,"l":0,"xl":0,"xxl":0,"3xl":0,"4xl":0}},"body":{"fit":"Comfortable","unit":"inch","quantity":1,"size":{"chest":0,"hip":0,"length":0,"neck":0,"shortsleeve":0,"shoulder":0,"sleeve":0,"waist":0}}}},"quantity":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":12},{"id":13},{"id":14},{"id":15},{"id":16},{"id":17},{"id":18},{"id":19},{"id":20},{"id":21},{"id":22},{"id":23},{"id":24},{"id":25},{"id":26},{"id":27},{"id":28},{"id":29},{"id":30},{"id":31},{"id":32},{"id":33},{"id":34},{"id":35},{"id":36},{"id":37},{"id":38},{"id":39},{"id":40},{"id":41},{"id":42},{"id":43},{"id":44},{"id":45},{"id":46},{"id":47},{"id":48},{"id":49},{"id":50},{"id":51},{"id":52},{"id":53},{"id":54},{"id":55},{"id":56},{"id":57},{"id":58},{"id":59},{"id":60},{"id":61},{"id":62},{"id":63},{"id":64},{"id":65},{"id":66},{"id":67},{"id":68},{"id":69},{"id":70},{"id":71},{"id":72},{"id":73},{"id":74},{"id":75},{"id":76},{"id":77},{"id":78},{"id":79},{"id":80},{"id":81},{"id":82},{"id":83},{"id":84},{"id":85},{"id":86},{"id":87},{"id":88},{"id":89},{"id":90},{"id":91},{"id":92},{"id":93},{"id":94},{"id":95},{"id":96},{"id":97},{"id":98},{"id":99},{"id":100}],"fitSize":[{"id":"s","name":"S","path":null},{"id":"m","name":"M","path":null},{"id":"l","name":"L","path":null},{"id":"xl","name":"XL","path":null},{"id":"xxl","name":"XXL","path":null},{"id":"3xl","name":"3XL","path":null},{"id":"4xl","name":"4XL","path":null}],"combinationCategory":{"117":{"id":"117","name":"Promotion","nameStr":"Promotion","price":"39.95","regular":"44.95"},"118":{"id":"118","name":"Dri Fit","nameStr":"Dri Fit","price":"44.95","regular":"49.95"},"119":{"id":"119","name":"Premium Polo","nameStr":"Premium Polo","price":"49.95","regular":"54.95"}},"combinationFabric":{"main":{"id":"BLD230","name":"Carolina Blue","category":"118"},"combination":{"id":"BLD230","name":"Carolina Blue","category":"118"}}}, routes: {"Fabric":{"M":"https:\/\/www.itailor.com\/images\/models\/polo\/Fabric\/M\/","LL":"https:\/\/www.itailor.com\/images\/models\/polo\/Fabric\/LL\/","S":"https:\/\/www.itailor.com\/images\/models\/polo\/Fabric\/S\/"},"Menu":{"Collar":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Collar\/","Sleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Sleeve\/","Front":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Front\/","Pocket":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Pocket\/","Bottom":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Bottom\/","Button":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Button\/Button\/","Thread":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Button\/Thread\/","Monogram":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Monogram\/","Extra":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Extra\/","Combination":"https:\/\/www.itailor.com\/images\/models\/polo\/Menu\/Combination\/"},"MainBody":{"Front":{"Straight":{"LongSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Straight\/LongSleeve\/","ShortSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Straight\/ShortSleeve\/","Combination":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Straight\/Combination\/"},"Straight-Vent":{"LongSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Straight-Vent\/LongSleeve\/","ShortSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Straight-Vent\/ShortSleeve\/","Combination":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Straight-Vent\/Combination\/"},"Vent-Uneven":{"LongSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Vent-Uneven\/LongSleeve\/","ShortSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Vent-Uneven\/ShortSleeve\/","Combination":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Front\/Vent-Uneven\/Combination\/"}},"FrontMix":{"CollarInside":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/CollarInside\/","CollarOutside":{"Normal":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/CollarOutside\/Normal\/","Slim":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/CollarOutside\/Slim\/"},"FrontBox":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/FrontBox\/","FrontIn":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/FrontIn\/","FrontOut":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/FrontOut\/","Pocket":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/Pocket\/","Trimming":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/Trimming\/","Sleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/Sleeve\/","SleeveCuff":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/SleeveCuff\/","SleeveElbow":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/SleeveElbow\/","Extra":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/FrontMix\/Extra\/"},"Back":{"LongSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Back\/LongSleeve\/","ShortSleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/Back\/ShortSleeve\/"},"BackMix":{"CollarOutside":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/BackMix\/CollarOutside\/","Sleeve":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/BackMix\/Sleeve\/","SleeveCuff":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/BackMix\/SleeveCuff\/","SleeveElbow":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/BackMix\/SleeveElbow\/","Extra":"https:\/\/www.itailor.com\/images\/models\/polo\/MainBody\/BackMix\/Extra\/"}},"HttpHost":"https:\/\/www.itailor.com\/","Measurement":{"video":"https:\/\/www.itailor.com\/iTailor-data\/webroot\/video\/Polo\/video\/","image":"https:\/\/www.itailor.com\/iTailor-data\/webroot\/video\/Polo\/images\/"}}};
         window.ITAILOR_PROJECT = "polo";
      </script>

   </body>
</html>