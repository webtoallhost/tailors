<?php include "require/header.php"; ?>
        <div id="tt-pageContent">
             <?php
            $banner = DB("SELECT * FROM `banner` WHERE `status`='1' AND `image`!='' ORDER BY `order` ASC ");
            $bcount = mysqli_num_rows($banner);
            if ($bcount != '0') {
             ?>
            <div class="container-indent nomargin">
                <div class="container-fluid">
                    <div class="row">
                        <div class="slider-revolution revolution-default">
                            <div class="tp-banner-container">
                                <div class="tp-banner revolution">
                                    <ul>
                                        <?php while ($bannerlist = mysqli_fetch_array($banner)) { ?>
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                                            <img
                                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAMgAQMAAAD4P+14AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAPdJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABg9uBAAAAAAADI/7URVFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVWFPTgQAAAAAADyf20EVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVpDw4JAAAAAAT9f+0JIwAAAAAAAAAAALAJ8T4AAZAZiOkAAAAASUVORK5CYII="
                                                data-lazyload="<?php echo $fsitename.'images/banner/'.$bannerlist['image']; ?>"
                                                alt="slide1"
                                                data-bgposition="center center"
                                                data-bgfit="cover"
                                                data-bgrepeat="no-repeat"
                                            />
                                          <div
                                                class="tp-caption tp-caption1 lft stb"
                                                data-x="center"
                                                data-y="center"
                                                data-hoffset="0"
                                                data-voffset="0"
                                                data-speed="600"
                                                data-start="900"
                                                data-easing="Power4.easeOut"
                                                data-endeasing="Power4.easeIn"
                                            >
                                                <div class="tp-caption1-wd-1 tt-base-color"><?php echo $bannerlist['title']; ?>
                                                </div>
                                               
                                              
                                              <div class="tp-caption1-wd-3" style="font-size:20px;"><?php echo $bannerlist['content']; ?></div>
                                              <?php if($bannerlist['link']!='') { ?>
                                                <div class="tp-caption1-wd-4"><a href="<?php echo $bannerlist['link']; ?>" target="_blank" class="btn btn-xl" data-text="SHOP NOW!">SHOP NOW!</a></div>
                                              <?php }  ?>
                                          </div>
                                            
                                        </li>
                                        <?php } ?>
                                       </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="container-indent0">
                <div class="container-fluid">
                    <div class="row tt-layout-promo-box">
                        <div class="col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',1); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',1); ?>" alt="<?php echo getquickbanner('title',1); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',1); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?php echo getquickbanner('link',2); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',2); ?>" alt="<?php echo getquickbanner('title',2); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',2); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',3); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',3); ?>" alt="<?php echo getquickbanner('title',3); ?>"/>
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',3); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',4); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',4); ?>" alt="<?php echo getquickbanner('title',4); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',4); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',5); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',5); ?>" alt="<?php echo getquickbanner('title',5); ?>"/>
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',5); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-12">
                                    <a href="<?php echo getquickbanner('link',6); ?>" class="tt-promo-box tt-one-child">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',6); ?>" alt="<?php echo getquickbanner('title',6); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',6); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
                    <style>
                    
   @media (max-width: 480px){
    .via{
                     position: unset !important;
    left: 0 !important;
    margin-top: 0 !important;
                }
                
                .mp{
           width: 140px;
        margin-left: 7px;
                }
                .hert{
     position: absolute;
    margin-top: 39px !important;
                }
                #w3review{
                    width:262px;
                }
}
     .via{
                 position: absolute;
                 left: 75%;
                 margin-top: -57px;
                 text-transform: uppercase;

                }
                @media (max-width: 1024px){
.tt-product-design02:not(.tt-view) .tt-description, .tt-product:not(.tt-view) .tt-description {
    margin-top: 18px;
    position: absolute;
}
      }  
      
     /* @media (max-width: 1024px){
.tt-product-design02:not(.tt-view) .tt-image-box .tt-img img, .tt-product:not(.tt-view) .tt-image-box .tt-img img {
   
    height: 250px !important;
}
}*/
      </style>

<!-- ==================new section -strar-->


<div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-block-title">
                        <h1 class="tt-title">SHOWROOMS</h1>
                       
                         <a href="<?php echo $fsitename; ?>pages/listing1.htm"><input class="btn btn-xl via"  type="button" value="View All"></a>
                    </div>
                    
                    <div class="row tt-layout-product-item">
                         <?php
                            $trending = DB("SELECT * FROM `vendor` WHERE `status`='1' ORDER BY `uid` DESC LIMIT 4");
                            $tcount = mysqli_num_rows($trending);
                            if ($tcount != '0') {
                            while ($trendinglist = mysqli_fetch_array($trending)) {
                                $link22 = FETCH_all("SELECT * FROM `users` WHERE `usergroup`=? AND `type`=? ", $trendinglist['uid'],'vendor');
                              if($trendinglist['image']!='')  {
                              $imgres = $fsitename.'images/vendor/' . $trendinglist['image'];   
                           
                              }
                             else {
                                $imgres=$fsitename.'images/noimage.png'; 
                             }
                            ?>
                          <div class="col-6 col-md-4 col-lg-3">
                            <div class="tt-product thumbprod-center">
                                <div class="tt-image-box">
                                  
                                    <!--<a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>-->
                                    <a href="<?php echo $fsitename; ?><?php echo $link22['id']; ?>.htm">
                                        <span class="tt-img"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['name']; ?>" /></span>
                                        <span class="tt-img-roll-over"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['name']; ?>" /></span>
                                        
                                    </a>

                                </div>
                                <div class="tt-description">
                                  
                                    <h2 class="tt-title"><a href="<?php echo $fsitename; ?><?php echo $link22['id']; ?>.htm" class="fontbolder"><?php echo $trendinglist['name']; ?></a></h2>
                                 
                                   
                                </div>
                            </div>
                        </div>
                    <?php } } ?>
                        
                    </div>
                </div>
            </div>



<!-- new section===end========= -->




            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-block-title">
                        <h1 class="tt-title">RECENT ARRIVALS</h1>
                        <div class="tt-description">TOP VIEW IN THIS WEEK</div>
                         <a href="<?php echo $fsitename; ?>listing.htm?offer=new"><input class="btn btn-xl via"  type="button" value="View All"></a>
                    </div>
                    
                    <div class="row tt-layout-product-item">
                         <?php
                            $trending = DB("SELECT * FROM `product` WHERE `status`='1' AND `new`='1' ORDER BY `pid` DESC LIMIT 4");
                            $tcount = mysqli_num_rows($trending);
                            if ($tcount != '0') {
                            while ($trendinglist = mysqli_fetch_array($trending)) {
                              if($trendinglist['image']!='')  {
                              $imgpath = $fsitename.'images/product/' . $trendinglist['imagefolder'] . '/';   
                              $imgs= explode(',',$trendinglist['image']);
                              $imgres=$imgpath.$imgs['0'];
                              }
                             else {
                                $imgres=$fsitename.'images/noimage.png'; 
                             }
                            ?>
                          <div class="col-6 col-md-4 col-lg-3">
                            <div class="tt-product thumbprod-center">
                                <div class="tt-image-box">
                                    <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $trendinglist['pid']; ?>" data-tooltip="Quick View" data-tposition="left"></a>
                                   <?php if(isset($_SESSION['FUID'])) { 
                                    $_SESSION['RED_URL']='';
                                     $getwish = DB_QUERY("SELECT * FROM `wishlist` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `wishlistid`='".$trendinglist['pid']."' ");
                                     if($getwish['id']!='') { $wcls="tt-btn-wishlist add_wishlist_product active";  } else { $wcls='tt-btn-wishlist add_wishlist_product'; }    
                                    ?>
                                    <a href="#" class="<?php echo $wcls; ?>" data-pdid="<?php echo $trendinglist['pid']; ?>" data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } else {
                                     $_SESSION['RED_URL']=$fsitename;
                                    ?>
                                    <!--onclick="window.location.href = '<?php echo $fsitename; ?>pages/login.htm';"-->
                                     <a  class="tt-btn-wishlist haiclick"  data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } ?>
                                    <!--<a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>-->
                                    <a href="<?php echo $fsitename; ?>view/<?php echo $trendinglist['link']; ?>.htm">
                                        <span class="tt-img"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['productname']; ?>" /></span>
                                        <span class="tt-img-roll-over"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['productname']; ?>" /></span>
                                         <?php if($trendinglist['discounttype']=='percentage') { ?>
                                         <span class="tt-label-location"><span class="tt-label-sale">Discount <?php echo $trendinglist['discount']; ?>%</span></span>
                                         <?php } ?>
                                    </a>

                                </div>
                                <div class="tt-description">
                                    <div class="tt-row">
                                        <ul class="tt-add-info">
                                            <li><a href="<?php echo $fsitename . getmtype('link',getcategory('type',$trendinglist['cid'])) .'/'. getcategory('link',$trendinglist['cid']).'.htm'; ?>"><?php echo getcategory('category',$trendinglist['cid']); ?></a></li>
                                        </ul>
                                       <div class="tt-rating">
                                                        <?php   $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $trendinglist['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
  
                  $emptrat=5-$avgrat;
                  ?>
                                                <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>   
                                <?php if($reviewdetails['avgg']==0) { ?>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                <?php } ?>
                                </div>
                                    </div>
                                    <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $trendinglist['link']; ?>.htm"><?php echo $trendinglist['productname']; ?></a></h2>
                                    <?php if($trendinglist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$ <?php echo $trendinglist['sprice']; ?></span> <span class="old-price">$ <?php echo $trendinglist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$ <?php echo $trendinglist['price']; ?></span></div>
                                 
                                    <?php } ?>
                                    <div class="tt-product-inside-hover">
                                        
                                        <?php
                                            $getqtyc = DB_QUERY("SELECT * FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$trendinglist['pid']."' AND `size`='".getsize('size',$trendinglist['size'])."' ");
                                            if($getqtyc['id']!='') { 
                                                $tempqty=$getqtyc['qty'];
                                               
                                            }
                                            else
                                            {
                                               $tempqty  =1;  
                                            }
                                            ?>
                                             
                                            
                                        <div class="tt-row-btn">
                                            <?php if($trendinglist['stock']=='1') { ?>
                                             <input size="100"  title="Numbers Only" name="qty[]" id="qty1"  value="<?php echo $tempqty; ?>" type="hidden"/>
                                        <?php if(isset($_SESSION['FUID'])) { 
                                        $_SESSION['RED_URL']='';
                                        ?> 
                                             <button type="button" data-pdid="<?php echo $trendinglist['pid'].'-'.getsize('size',$trendinglist['size']); ?>"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg add_cart_this_product btnclass" data-toggle="modal" data-target="#modalAddToCartProduct<?php echo $trendinglist['pid']; ?>" data-go-cart='1'>
                                       ADD TO CART
                                        </button>
                                        <?php } else {
                                        $_SESSION['RED_URL']=$fsitename;
                                        ?>
                                         <button type="button"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg haiclick mp btnclass" data-toggle="modal">
                                       ADD TO CART
                                        </button>
                                        <?php } ?>
                                          <!-- modal (AddToCartProduct) -->
                                          <?php } ?>
                                        <div class="tt-row-btn hert">
                                            
                                            <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $trendinglist['pid']; ?>"></a> <a href="#" class="tt-btn-wishlist"></a> 
                                            <!--<a href="#" class="tt-btn-compare"></a>-->
                                        </div>
                                              
                                              </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalAddToCartProduct<?php echo $trendinglist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                         <?php 
                                    $useid=$_SESSION['FUID'];
                                   $asd = DB_QUERY("SELECT SUM(`totprice`) AS `fftotprice`,`qty`,`totprice`,COUNT(*) AS totrecd FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$trendinglist['pid']."' AND `size`='".getsize('size',$trendinglist['size'])."' ");
                                 $asd1 = DB_QUERY("SELECT COUNT(*) AS totrecd,  SUM(`totprice`) AS `fftotprice` FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' ");
                                
                                 ?>
                        <div class="tt-modal-addtocart mobile">
                            <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                           <a href="<?php echo $fsitename; ?>listings.htm" class="btn-link btn-close-popup">CONTINUE SHOPPING</a> 
                           <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn-link">VIEW CART</a>
                        </div>
                        <div class="tt-modal-addtocart desctope">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                                    <div class="tt-modal-product">
                                        <div class="tt-img"><img src="<?php echo $fsitename; ?>images/loader.svg" data-src="<?php echo $imgres; ?>" alt="" /></div>
                                        <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $trendinglist['link']; ?>.htm"><?php echo $trendinglist['productname']; ?></a></h2>
                                        <?php if($asd['qty']!='') { ?>
                                        <div class="tt-qty">QTY: <span><?php echo $asd['qty']; ?></span></div>
                                        <?php } ?>
                                    </div>
                                    <?php if($asd['fftotprice']!='') { ?>
                                    <div class="tt-product-total">
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd['fftotprice']; ?></span></div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($asd1['fftotprice']!='') { ?>
                                <div class="col-12 col-lg-6">
                                    <a href="#" class="tt-cart-total">
                                        There are <?php echo $asd1['totrecd']; ?> items in your cart
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd1['fftotprice']; ?></span></div>
                                    </a>
                                    <a href="<?php echo $fsitename; ?>listings.htm" class="btn btn-border btn-close-popup">CONTINUE SHOPPING</a> 
                                    <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn">VIEW CART</a> 
                                    
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        <div class="modal fade" id="ModalquickView<?php echo $trendinglist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-6">
                                    <div class="tt-mobile-product-slider arrow-location-center">
                                    <?php foreach($imgs as $imgs11) { 
                                         $imgres=$imgpath.$imgs11;
                                        ?>    
                                    <div><img src="#" data-lazy="<?php echo $imgres; ?>" alt="" /></div>
                                    <?php } ?>
                                        <!--
								//video insertion template
								<div>
									<div class="tt-video-block">
										<a href="#" class="link-video"></a>
										<video class="movie" src="video/video.mp4" poster="video/video_img.jpg"></video>
									</div>
								</div> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-6">
                                    <div class="tt-product-single-info">
                                        <div class="tt-add-info">
                                            <ul>
                                                <li><span>SKU:</span><?php echo $trendinglist['item_code']; ?></li>
                                                <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                            </ul>
                                        </div>
                                        <h2 class="tt-title"><?php echo $trendinglist['productname']; ?></h2>
                                     <?php if($trendinglist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $trendinglist['sprice']; ?></span> <span class="old-price">$ <?php echo $trendinglist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $trendinglist['price']; ?></span></div>
                                    <?php } ?>
                                       <div class="tt-rating">
                <?php   $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $trendinglist['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                  $emptrat=5-$avgrat;
                  ?>
                                                <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>     </div>
                                          <div class="tt-add-info">
                                        <ul>
                                            <li><span>Size:</span> <?php echo getsize('size',$trendinglist['size']); ?></li>
                                            <li><span>Color:</span> <?php echo getcolor('color',$trendinglist['color']); ?></li>
                                        </ul>
                                    </div>
                                        <div class="tt-wrapper"><?php echo $trendinglist['sortdescription']; ?></div>
                                     
                                      <?php if($trendinglist['stock']=='1') { ?>
                                        <div class="tt-wrapper">
                                            <div class="tt-row-custom-01">
                                                <div class="col-item">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn"></span> <input type="text" value="<?php echo $tempqty; ?>" size="5"  id="qty1" /> <span class="plus-btn"></span></div>
                                                </div>
                                                <div class="col-item">
                                            <?php if(isset($_SESSION['FUID'])) {
                                            $_SESSION['RED_URL']='';
                                            ?> 
                                            <a href="#" class="btn btn-lg add_cart_this_product" data-pdid="<?php echo $trendinglist['pid'].'-'.getsize('size',$trendinglist['size']); ?>"><i class="icon-f-39"></i>ADD TO CART</a>
                                               <?php } else { 
                                                $_SESSION['RED_URL']=$fsitename;
                                               ?>
                                               <a href="#" class="btn btn-lg haiclick"><i class="icon-f-39"></i>ADDs TO CART</a>
                                               <?php } ?>
                                               
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                            <?php } } ?>
                        
                    </div>
                </div>
            </div>
            <div class="container-indent">
                <div class="container-fluid-custom">
                    <div class="row tt-layout-promo-box">
                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo getofferbanner('link',1); ?>" class="tt-promo-box">
                                <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',1); ?>" alt="<?php echo getofferbanner('title',1); ?>" />
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small clchang"><?php echo getofferbanner('title',1); ?></div>
                                        <div class="tt-title-large"><?php echo getofferbanner('content',1); ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo getofferbanner('link',2); ?>" class="tt-promo-box">
                                <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',2); ?>" alt="<?php echo getofferbanner('title',2); ?>" />
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small  clchang"><?php echo getofferbanner('title',2); ?></div>
                                        <div class="tt-title-large"><?php echo getofferbanner('content',2); ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo getofferbanner('link',3); ?>" class="tt-promo-box">
                                <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',3); ?>" alt="<?php echo getofferbanner('title',3); ?>" />
                                <div class="tt-description">
                                    <div class="tt-background"></div>
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background "></div>
                                        <div class="tt-title-small clchang"><?php echo getofferbanner('title',3); ?></div>
                                        <div class="tt-title-large"><span class="tt-base-color clchang"><?php echo getofferbanner('content',3); ?></span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-block-title">
                        <h2 class="tt-title">POPULAR SEARCH</h2>
                        <div class="tt-description">TOP SALE IN THIS WEEK</div>
                        <a href="<?php echo $fsitename; ?>listing.htm?offer=deal"> <input class="btn btn-xl via"  type="button" value="View All"></a>
                    </div>
                    <div class="row tt-layout-product-item">
                         <?php
                            $bestseller = DB("SELECT * FROM `product` WHERE `status`='1' AND `deal`='1' ORDER BY `pid` DESC LIMIT 4");
                            $bcount = mysqli_num_rows($bestseller);
                            if ($bcount != '0') {
                            while ($bestsellerlist = mysqli_fetch_array($bestseller)) {
                              if($bestsellerlist['image']!='')  {
                              $imgpath = $fsitename.'images/product/' . $bestsellerlist['imagefolder'] . '/';   
                              $imgs= explode(',',$bestsellerlist['image']);
                              $imgres=$imgpath.$imgs['0'];
                              }
                             else {
                                $imgres=$fsitename.'images/noimage.png'; 
                             }
                            ?>
                            <?php
                                            $getqtyc = DB_QUERY("SELECT * FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$bestsellerlist['pid']."' AND `size`='".getsize('size',$bestsellerlist['size'])."' ");
                                            if($getqtyc['id']!='') { 
                                                $tempqty=$getqtyc['qty'];
                                               
                                            }
                                            else
                                            {
                                               $tempqty  =1;  
                                            }
                                            ?>
                                           
                        <div class="col-6 col-md-4 col-lg-3">
                            <div class="tt-product thumbprod-center">
                                <div class="tt-image-box">
                                      <input size="100"  title="Numbers Only" name="qty[]" id="qty1"  value="<?php echo $tempqty; ?>" type="hidden"/>
                                    <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $bestsellerlist['pid']; ?>" data-tooltip="Quick View" data-tposition="left"></a>
                                     <?php if(isset($_SESSION['FUID'])) { 
                                    $_SESSION['RED_URL']='';
                                     $getwish = DB_QUERY("SELECT * FROM `wishlist` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `wishlistid`='".$bestsellerlist['pid']."' ");
                                     if($getwish['id']!='') { $wcls="tt-btn-wishlist add_wishlist_product active";  } else { $wcls='tt-btn-wishlist add_wishlist_product'; }    
                                    ?>
                                    <a href="#" class="<?php echo $wcls; ?>" data-pdid="<?php echo $bestsellerlist['pid']; ?>" data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } else {
                                     $_SESSION['RED_URL']=$fsitename;
                                    ?>
                                    <!--onclick="window.location.href = '<?php echo $fsitename; ?>pages/login.htm';"-->
                                     <a  class="tt-btn-wishlist haiclick"  data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } ?>
                                    <!--<a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>-->
                                    <a href="<?php echo $fsitename; ?>view/<?php echo $bestsellerlist['link']; ?>.htm">
                                        <span class="tt-img"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $bestsellerlist['productname']; ?>" /></span>
                                        <span class="tt-img-roll-over"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $bestsellerlist['productname']; ?>" /></span>
                                        <?php if($bestsellerlist['discounttype']=='percentage') { ?>
                                         <span class="tt-label-location"><span class="tt-label-sale">Discount <?php echo $bestsellerlist['discount']; ?>%</span></span>
                                         <?php } ?>
                                    </a>
                                    
                                </div>
                                <div class="tt-description">
                                    <div class="tt-row">
                                        <ul class="tt-add-info">
                                            <li><a href="<?php echo $fsitename . getmtype('link',getcategory('type',$bestsellerlist['cid'])) .'/'. getcategory('link',$bestsellerlist['cid']).'.htm'; ?>"><?php echo getcategory('category',$bestsellerlist['cid']); ?></a></li>
                                        </ul>
                                        <div class="tt-rating">
                                           
                                            <?php   $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $bestsellerlist['pid'],'1');
                                                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                                                  $emptrat=5-$avgrat;
                                                  ?>
                                                <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>    
                                    <?php if($reviewdetails['avgg']==0) { ?>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                <?php } ?>
                                </div>
                                    </div>
                                    <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $bestsellerlist['link']; ?>.htm"><?php echo $bestsellerlist['productname']; ?></a></h2>
                                   <?php if($bestsellerlist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['sprice']; ?></span> <span class="old-price">$ <?php echo $bestsellerlist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['price']; ?></span></div>
                                 
                                    <?php } ?>
                                    <div class="tt-product-inside-hover">
                                         <?php if($bestsellerlist['stock']=='1') { ?>
                                        <div class="tt-row-btn">
                                          <?php if(isset($_SESSION['FUID'])) {
                                          $_SESSION['RED_URL']='';
                                          ?> 
                                        <button type="button" data-pdid="<?php echo $bestsellerlist['pid'].'-'.getsize('size',$bestsellerlist['size']); ?>"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg add_cart_this_product" data-toggle="modal" data-target="#modalAddToCartProduct<?php echo $bestsellerlist['pid']; ?>" data-go-cart='1'>
                                       ADD TO CART
                                        </button>
                                        <?php } else { 
                                         $_SESSION['RED_URL']=$fsitename;
                                        ?>
                                         <button type="button"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg haiclick"  data-toggle="modal">
                                       ADD TO CART
                                        </button>
                                        <?php } ?>
                                        </div>
                                        <?php } ?>
                                        <div class="tt-row-btn hert">
                                            <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $bestsellerlist['pid']; ?>"></a> <a href="#" class="tt-btn-wishlist"></a> 
                                            <!--<a href="#" class="tt-btn-compare"></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 <div class="modal fade" id="modalAddToCartProduct<?php echo $trendinglist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                         <?php 
                                    $useid=$_SESSION['FUID'];
                                   $asd = DB_QUERY("SELECT SUM(`totprice`) AS `fftotprice`,`qty`,`totprice`,COUNT(*) AS totrecd FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$bestsellerlist['pid']."' AND `size`='".getsize('size',$bestsellerlist['size'])."' ");
                                 $asd1 = DB_QUERY("SELECT COUNT(*) AS totrecd,  SUM(`totprice`) AS `fftotprice` FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' ");
                                
                                 ?>
                        <div class="tt-modal-addtocart mobile">
                            <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                           <a href="<?php echo $fsitename; ?>listings.htm" class="btn-link btn-close-popup">CONTINUE SHOPPING</a> 
                           <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn-link">VIEW CART</a>
                        </div>
                        <div class="tt-modal-addtocart desctope">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                                    <div class="tt-modal-product">
                                        <div class="tt-img"><img src="<?php echo $fsitename; ?>images/loader.svg" data-src="<?php echo $imgres; ?>" alt="" /></div>
                                        <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $bestsellerlist['link']; ?>.htm"><?php echo $bestsellerlist['productname']; ?></a></h2>
                                        <?php if($asd['qty']!='') { ?>
                                        <div class="tt-qty">QTY: <span><?php echo $asd['qty']; ?></span></div>
                                        <?php } ?>
                                    </div>
                                    <?php if($asd['fftotprice']!='') { ?>
                                    <div class="tt-product-total">
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd['fftotprice']; ?></span></div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-lg-6">
                                <?php if($asd1['fftotprice']!='') { ?>
                                    <a href="#" class="tt-cart-total">
                                        There are <?php echo $asd1['totrecd']; ?> items in your cart
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd1['fftotprice']; ?></span></div>
                                    </a>
                                <?php } ?>
                                    <a href="<?php echo $fsitename; ?>listings.htm" class="btn btn-border btn-close-popup">CONTINUE SHOPPING</a> 
                                    <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn">VIEW CART</a> 
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                      
                         <!-- modal (quickViewModal) -->
        <div class="modal fade" id="ModalquickView<?php echo $bestsellerlist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-6">
                                    <div class="tt-mobile-product-slider arrow-location-center">
                                    <?php foreach($imgs as $imgs11) { 
                                         $imgres=$imgpath.$imgs11;
                                        ?>    
                                    <div><img src="#" data-lazy="<?php echo $imgres; ?>" alt="" /></div>
                                    <?php } ?>
                                        <!--
								//video insertion template
								<div>
									<div class="tt-video-block">
										<a href="#" class="link-video"></a>
										<video class="movie" src="video/video.mp4" poster="video/video_img.jpg"></video>
									</div>
								</div> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-6">
                                    <div class="tt-product-single-info">
                                        <div class="tt-add-info">
                                            <ul>
                                                <li><span>SKU:</span><?php echo $bestsellerlist['item_code']; ?></li>
                                                <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                            </ul>
                                        </div>
                                        <h2 class="tt-title"><?php echo $bestsellerlist['productname']; ?></h2>
                                     <?php if($bestsellerlist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['sprice']; ?></span> <span class="old-price">$ <?php echo $bestsellerlist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['price']; ?></span></div>
                                    <?php } 
                                          $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $bestsellerlist['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                  $emptrat=5-$avgrat;
                                    ?>
                                    <div class="tt-review">
                                    <div class="tt-rating">
                                    <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>
                                    </div>
                                    <a class="product-page-gotocomments-js">(<?php echo $reviewdetails['totcmts']; ?> Customer Review)</a>
                                </div>
                                        <div class="tt-add-info">
                                        <ul>
                                            <li><span>Size:</span> <?php echo getsize('size',$bestsellerlist['size']); ?></li>
                                            <li><span>Color:</span> <?php echo getcolor('color',$bestsellerlist['color']); ?></li>
                                        </ul>
                                    </div>
                                        <div class="tt-wrapper"><?php echo $bestsellerlist['sortdescription']; ?></div>
                                     <?php if($bestsellerlist['stock']=='1') { ?>
                                        <div class="tt-wrapper">
                                            <div class="tt-row-custom-01">
                                                <div class="col-item">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn"></span> <input type="text" value="<?php echo $tempqty; ?>" id="qty1" size="5" /> <span class="plus-btn"></span></div>
                                                </div>
                                                <div class="col-item">
                                              
                                               <?php if(isset($_SESSION['FUID'])) {
                                               $_SESSION['RED_URL']='';
                                               ?> 
                                                    <a href="#" class="btn btn-lg add_cart_this_product" data-pdid="<?php echo $bestsellerlist['pid'].'-'.getsize('size',$bestsellerlist['size']); ?>"><i class="icon-f-39"></i>ADD TO CART</a>
                                                    <?php } else {
                                                     $_SESSION['RED_URL']=$fsitename;
                                                    ?>
                                                    <a href="#" class="btn btn-lg haiclick"><i class="icon-f-39"></i>ADDs TO CART</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                            <?php } } ?>
                    </div>
                </div>
            </div>
            <?php
            $blog = DB("SELECT * FROM `blog` WHERE `status`='1' AND `image`!='' ORDER BY `order` ASC LIMIT 3 ");
            $bcount = mysqli_num_rows($blog);
            if ($bcount != '0') { ?>
            <div class="container-indent">
                <div class="container">
                    <div class="tt-block-title">
                        <h2 class="tt-title">LATEST FROM BLOG</h2>
                        <div class="tt-description">THE FRESHEST AND MOST EXCITING NEWS</div>
                    </div>
                    <div class="tt-blog-thumb-list">
                        <div class="row">
                            <?php  while ($bloglist = mysqli_fetch_array($blog)) { 
                            $bimage=$fsitename.'images/blog/'.$bloglist['image'];
                            ?>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="tt-blog-thumb bgback">
                                    <div class="tt-img">
                                        <a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><img src="images/loader.svg" data-src="<?php echo $bimage; ?>" alt="<?php echo stripslashes($fblogs['title']); ?>" /></a>
                                    </div>
                                    <div class="tt-title-description">
                                        <div class="tt-background"></div>
                                        <div class="tt-tag"><a href="<?php echo $fsitename . getblogcategory('link',$bloglist['category']) . '/blogs'; ?>.htm"><?php echo getblogcategory('category',$bloglist['category']); ?></a></div>
                                        <div class="tt-title"><a href="<?php echo $fsitename . 'view/' . $bloglist['link']; ?>/blog.htm"><?php echo stripslashes($bloglist['title']); ?></a></div>
                                        <p><?php echo stripslashes(substr($bloglist['description'], 0, 120)); ?></p>
                                       
                                        
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                          </div>
                    </div>
                </div>
            </div>
            <?php } ?>
<div class="container-indent">
                <div class="container">
                    <div class="row tt-services-listing">
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <a href="#" class="tt-services-block">
                                <div class="tt-col-icon"><i class="icon-f-48"></i></div>
                                <div class="tt-col-description">
                                    <h4 class="tt-title">FREE SHIPPING</h4>
                                    <p>Free shipping on all US order or order above $99</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <a href="#" class="tt-services-block">
                                <div class="tt-col-icon"><i class="icon-f-35"></i></div>
                                <div class="tt-col-description">
                                    <h4 class="tt-title">SUPPORT 24/7</h4>
                                    <p>Contact us 24 hours a day, 7 days a week</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <a href="#" class="tt-services-block">
                                <div class="tt-col-icon"><i class="icon-e-09"></i></div>
                                <div class="tt-col-description">
                                    <h4 class="tt-title">30 DAYS RETURN</h4>
                                    <p>Simply return it within 24 days for an exchange.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include "require/footer.php"; ?>       