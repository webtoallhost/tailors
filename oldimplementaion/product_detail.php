<?php
$pdetail = '1';
$selid = '3';
$getstatic = '1';
$dynamic = '1';
$product = '3';
include 'admin/config/config.inc.php';

if(isset($_REQUEST['rewsubmit']))
{
global $db;
extract($_REQUEST); 
if($_SESSION['FUID']!='') {
    $userid=$_SESSION['FUID'];
}
else
{
   $userid=''; 
}
$ip = $_SERVER['REMOTE_ADDR'];
$vendorid=getusers('usergroup',getproduct('vendor',$rproduct));

$resa = $db->prepare("INSERT INTO `productreview` (`vendor`,`product`,`rating`,`name`,`email`,`comments`,`ip`,`userid`,`status`) VALUES (?,?,?,?,?,?,?,?,?) ");
$resa->execute(array($vendorid,$rproduct,$ratingval,$rname,$remail,$rcomment,$ip,$userid,1));

$msg='success';

}

if (($_REQUEST['pid'] != '')) {
 
    $proid = str_replace('_', ' ', $_REQUEST['pid']);
    $pageu = DB_QUERY("SELECT * FROM `product` WHERE `link`='" . $proid . "'");

    $pcategory = DB_QUERY("SELECT * FROM `category` WHERE `cid`='" . $pageu['cid'] . "'");
    if ($pageu['sid'] != '') {
        $psubcat = DB_QUERY("SELECT * FROM `subcategory` WHERE `sid`='" . $pageu['sid'] . "' AND `cid`='" . $pageu['cid'] . "'");
    }
    if ($pageu['innerid'] != '') {
        $pincat = DB_QUERY("SELECT * FROM `innercategory` WHERE `subcategory`='" . $pageu['sid'] . "' and `innerid`='" . $pageu['innerid'] . "'");
    }
    if ($pageu['sprice'] > 0) {
        $sp = 100 - (number_format(($pageu['sprice'] / $pageu['price']), '2', '.', '') * 100);
    } else {
        $sp = '';
    }
    $im = explode(",", $pageu['image']);
    $pid = $pageu['pid'];
} else {
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    exit;
}

$img = explode(',', $pageu['image']);
if (file_exists('<?php echo $fsitename; ?>images/product/' . stripslashes($pageu['productname']) . '/' . $img['0'])) {
    if ($img[0] != '') {
        $fullimage = $fsitename . '<?php echo $fsitename; ?>images/product/' . stripslashes($pageu['productname']) . '/' . $img['0'];
    }
}
foreach ($img as $timg) {

    $ogimage .= '<meta property="og:image" content="' . $fsitename . '<?php echo $fsitename; ?>images/product/' . $timg . '" />
	<link rel="image_src" type="image/jpeg" href="' . $fsitename . '<?php echo $fsitename; ?>images/product/' . $timg . '"/>';
}
if ($pageu['addesc'] != '') {
    $addesc = $pageu['addesc'];
} else {
    $addesc = $pageu['adtitle'];
}
include 'require/header.php';

?>
<link href="<?php echo $fsitename; ?>css/rating.css" rel="stylesheet">
<style>
    /*21.01.2021*/
.video-link-product [class^="icon-"] {
   color: #ffffff;
    font-size: 66px;
    text-shadow: 0px 0px 25px #0c0a08;
    /*background: #2879fe;*/
    /* background-size: 453px 300px; */
        background-size: cover;
    border-bottom: 2px solid;
    line-height: 68px;
    margin-top: 86px; 

}

#screeblur{
    position: absolute;
    width: 100%;
    height: 100%;
    
    background-color: rgba(0,0,0,0.5);
    z-index: 2;
    cursor: pointer;
}
#js-include-desktop-menu nav>ul{

    margin-left: -16px;
    /*21.01.2021*/
}
.posi img{
    
}




/*=========04.01.2021*/
.btn{
    background: #3260aa;
    color: #fff;
    outline: 0;
    box-shadow: 0px 0px 4px #fff;
}

.tt-collapse-block .tt-item .tt-collapse-title:hover {
    color: #ffffff;
}
.tt-review-block .tt-review-form .tt-message-info {
    color: #ffffff;
}
.tt-collapse-block .tt-item.active .tt-collapse-title {
    color: #ffffff;
}
/*=========04.01.2021*/


            #custom-product-item .slick-arrow {
                position: absolute;
                top: 50%;
                z-index: 2;
                cursor: pointer;
                font-size: 0;
                line-height: 0;
                background: none;
                border: none;
                width: 38px;
                height: 38px;
                background: #f7f8fa;
                color: #191919;
                font-weight: 500;
                border-radius: 50%;
                transition: all 0.2s linear;
                transform: translate(0%, -50%);
            }
            #custom-product-item {
                opacity: 0;
                transition: opacity 0.2s linear;
            }
            #custom-product-item.tt-show {
                opacity: 1;
            }

            #custom-product-item .slick-arrow:hover {
                background: #2879fe;
                color: #ffffff;
            }

            #custom-product-item .slick-arrow:before {
                font-family: "wokiee";
                font-size: 20px;
                line-height: 1;
            }
            #custom-product-item .slick-prev {
                left: 10px;
            }
            #custom-product-item .slick-prev:before {
                content: "\e90d";
            }
            #custom-product-item .slick-next {
                right: 10px;
            }
            #custom-product-item .slick-next:before {
                content: "\e90e";
            }
            #smallGallery .slick-arrow.slick-disabled,
            #custom-product-item .slick-arrow.slick-disabled {
                opacity: 0;
                pointer-events: none;
            }
        </style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li><a href="listing-left-column.html">Shop</a></li>
                    <li>T-Shirt</li>
                </ul>
            </div>
        </div>
        <?php
            $imgpath = $fsitename.'images/product/' . $pageu['imagefolder'] . '/';   
            $imgall=explode(',',$pageu['image']);
            
            ?>
        <div id="tt-pageContent">
            <div class="container-indent">
                <!-- mobile product slider  -->
                
                <div class="tt-mobile-product-layout visible-xs">
                    <div class="tt-mobile-product-slider arrow-location-center" id="zoom-mobile__slider">
                    <?php foreach($imgall as $imgall11) { ?>   
                    <div><img data-lazy="<?php echo $imgpath.$imgall11; ?>"  /></div>
                    <?php } ?>   
                  
                        <!-- <div>
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="" data-src="http://www.youtube.com/embed/GhyKqj_P2E4" allowfullscreen></iframe>
					</div>
				</div> -->
                    </div>
                    <div id="zoom-mobile__layout">
                        <a class="zoom-mobile__close btn" href="#">Back</a>
                        <div id="tt-fotorama" data-nav="thumbs" data-auto="false" data-allowfullscreen="false" dataa-fit="cover"></div>
                    </div>
                </div>
                <!-- /mobile product slider  -->
                <div class="container container-fluid-mobile">
                    <div class="row">
                        <div class="col-6 hidden-xs">
                            <div class="tt-product-vertical-layout">
                                <div class="tt-product-single-img">
                                    <div>
                                    
                                        <button class="tt-btn-zomm tt-top-right"><i class="icon-f-86"></i></button> <img class="zoom-product" src="<?php echo $imgpath.$imgall['0']; ?>" data-zoom-image="<?php echo $imgpath.$imgall['0']; ?>" alt="" />
                                        <div id="custom-product-item"><button type="button" class="slick-arrow slick-prev">Previous</button> <button type="button" class="slick-arrow slick-next">Next</button></div>
                                    </div>
                                </div>
                                <div class="tt-product-single-carousel-vertical">
                                    <ul id="smallGallery" class="tt-slick-button-vertical slick-animated-show-js">
                                  
                                        <?php 
                                        $f=0;
                                        foreach($imgall as $imgall1){  ?>
                                        <li>
                                        <a <?php if($f==0) { ?> class="zoomGalleryActive" <?php } ?> href="#" data-image="<?php echo $imgpath.$imgall1; ?>" data-zoom-image="<?php echo $imgpath.$imgall1; ?>"><img src="<?php echo $imgpath.$imgall1; ?>" alt="" /></a>
                                        </li>
                                       <style>
                                           .video-link-product [class^="icon-"]{
                                            background: url('<?php echo $imgpath.$imgall1; ?>');
                                           }
                                       </style>
                                        <?php $f++; } ?>
                                        <?php
                                          if($pageu['videolink']!='') { 
                                        $vurl=explode('=',$pageu['videolink']);

                                        ?>
                                       <li class="slick-slide slick-active" data-slick-index="8" aria-hidden="false" tabindex="0" style="width: 81px;">
                                    <div class="video-link-product" data-toggle="modal" data-type="youtube" data-target="#modalVideoProduct" data-value="https://www.youtube.com/embed/<?php echo $vurl['1']; ?>">
                                        <img src="images/product/product-small-empty.png" alt="" class="loading" data-was-processed="true">
                                        <div id="screeblur">
                                            <!-- <i class="icon-f-26"></i> -->
                                            <img src="<?php echo $fsitename?>images/logo/videoicon.png" width="50">
                                        </div>
                                    </div>
                                </li>
                                <?php
                                          }
                                        ?>
                                      
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-6">
                            <div class="tt-product-single-info">
                                <div class="tt-add-info">
                                    
                                    <ul>
                                        <li><span>SKU:</span> <?php echo $pageu['item_code']; ?></li>
                                        <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                    </ul>
                                  
                                </div>
                                <h1 class="tt-title"><?php echo $pageu['productname']; ?></h1>
                                <ul class="tt-list-btn">
                                        <li>
                                 <?php if(isset($_SESSION['FUID'])) { 
                                    $_SESSION['RED_URL']='';
                                     $getwish = DB_QUERY("SELECT * FROM `wishlist` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `wishlistid`='".$pageu['pid']."' ");
                                     if($getwish['id']!='') { $wcls="btn-link add_wishlist_product";  } else { $wcls='btn-link add_wishlist_product'; }    
                                    ?>
                                    <a href="#" class="<?php echo $wcls; ?>" data-pdid="<?php echo $pageu['pid']; ?>"><i class="icon-n-072"></i>ADD TO WISH LIST</a></a> 
                                    <?php } else {
                                     $_SESSION['RED_URL']=$fsitename;
                                    ?>
                                    <nav class="main-nav">
                                     <a  class="btn-link cd-signin"><i class="icon-n-072"></i>ADD TO WISH LIST</a></a> 
                                    </nav>
                                    <?php } ?>
                                     </li>
                                        
                                    </ul>
                                    <br>
                                <div id="pprice"></div>
                                <?php if($pageu['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price" id="sprice1">$ <?php echo $pageu['sprice']; ?></span>&nbsp;&nbsp;<span id="price1" class="old-price">$  <?php echo $pageu['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price" id="price12">$ <?php echo $pageu['price']; ?></span></div>
                                   <?php }
                                    
                                     $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $pageu['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                  $emptrat=5-$avgrat;
                                    ?>
                                    <div class="tt-review">
                                    <div class="tt-rating">
                                    <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>
                                    </div>
                                    <a class="product-page-gotocomments-js">(<?php echo $reviewdetails['totcmts']; ?> Customer Review)</a>
                                </div>
                                    <div class="tt-wrapper">
                                    <div class="tt-add-info">
                                        <ul>
                                            <!-- <li><span style="color:#2879fe;">Seller:</span>&nbsp;&nbsp;<?php echo getvendor('name',getusers('usergroup',$pageu['vendor'])); ?></li>
                                            <li><span style="color:#2879fe;">Contact Number:</span>&nbsp;&nbsp;<?php echo getvendor('mobile',getusers('usergroup',$pageu['vendor'])); ?></li>
                                           -->
                                            <li><span style="color:#2879fe;">Type:</span>&nbsp;&nbsp;<?php echo getcategory('category',$pageu['cid']); ?></li>
                                           <li><span style="color:#2879fe;">Size:</span>&nbsp;&nbsp;<?php echo getsize('size',$pageu['size']); ?></li>
                                           <li><span style="color:#2879fe;">Color:</span>&nbsp;&nbsp;<?php echo getcolor('color',$pageu['color']); ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tt-wrapper"><?php echo $pageu['sortdescription']; ?>
                            <br>
                          <!--   <?php if($pageu['videolink']!='') { ?>
                               <a style="color: #ff2c2c;font-size: 29px;text-shadow: 0px 0px 25px #2879fe;" data-toggle="modal" data-type="youtube" data-target="#modalVideoProduct" data-value="<?php echo $pageu['videolink']; ?>"
                                <i class="icon-g-76"></i>
                            </a>
                            <?php } ?> -->
                            </div>
                               

                               <div class="tt-swatches-container">
<?php
$size = DB("SELECT * FROM `sizeprice` WHERE `product_id`='".$pageu['pid']."' ");
$scount = mysqli_num_rows($size); 
if ($scount != '0') {
?>
<div class="tt-wrapper">
<div class="tt-title-options">AVAILABLE SIZE:</div>
<ul class="tt-options-swatch options-large">
<li class="active"><a href="#" onclick="getprice('<?php echo getsize('size',$pageu['size']); ?>',<?php echo $pageu['sprice']; ?>,<?php echo $pageu['price']; ?>);"><?php echo getsize('size',$pageu['size']); ?></a></li>

<?php while ($sizelist = mysqli_fetch_array($size)) { ?>
                                        
<li><a href="#" onclick="getprice('<?php echo $sizelist['size']; ?>',<?php echo $sizelist['sprice']; ?>,<?php echo $sizelist['price']; ?>);"><?php echo $sizelist['size']; ?></a></li>
<?php } ?>

<!-- <li class="active"><a href="#">6</a></li> -->
</ul>
</div>
<?php } ?>
<div class="tt-wrapper product-information-buttons">

    <?php if($pageu['innerid']!='') { ?>
<a data-toggle="modal" data-target="#modalProductInfo" href="#">Size Guide</a> 
    <?php } ?>
<a data-toggle="modal" data-target="#modalProductInfo-02" href="#">Return Policies</a>
</div><div class="tt-wrapper"><div class="tt-title-options">AVAILABLE COLOR:</div>
<ul class="tt-options-swatch options-large">
<li  class="active">
    <a class="options-color tt-color-bg-09" style="background-color:<?php echo getcolor('colorcode',$pageu['color']); ?>"><?php echo getcolor('color',$pageu['color']); ?></a>
    </li>   
<?php $avlcolor=explode(',',$pageu['avlcolors']); 
$exprodname=explode('-',$pageu['productname']);
foreach($avlcolor as $avlcolor1) { 
 
$getcolorlink = DB_QUERY("SELECT * FROM `product` WHERE `productname` LIKE '%" .trim($exprodname['0']). "%' AND `color`='".$avlcolor1."' AND `pid`!='".$pageu['pid']."' ");
if($getcolorlink['pid']!='') {
 $plink=$fsitename . 'view/' . getproduct('link', $getcolorlink['pid']) . '.htm';   
}
else
{
  $plink='';   
}
?> 
<li>
    <?php if($plink!='') {  ?>
    <a onclick="window.location.href = '<?php echo $plink; ?>'; " class="options-color tt-color-bg-09" style="background-color:<?php echo getcolor('colorcode',$avlcolor1); ?>"><?php echo getcolor('color',$avlcolor1); ?></a>
    <?php } else { ?>
     <a class="options-color tt-color-bg-09" style="background-color:<?php echo getcolor('colorcode',$avlcolor1); ?>"><?php echo getcolor('color',$avlcolor1); ?></a>
    <?php } ?>
    </li>   
<?php } ?>

<!-- <li class="active"><a class="options-color tt-color-bg-10" href="#"></a></li> -->

</ul>
</div>
</div>  <?php if($pageu['stock']=='1') { ?>
                                <div class="tt-wrapper">
                                    <form method="post" id="reviewform">
                                    <div class="tt-row-custom-01">
                                        <div class="col-item">
                                            
                                            <?php
                                            $getqtyc = DB_QUERY("SELECT * FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$pageu['pid']."' AND `size`='".getsize('size',$pageu['size'])."' ");
                                            if($getqtyc['id']!='') { 
                                                $tempqty=$getqtyc['qty'];
                                               
                                            }
                                            else
                                            {
                                               $tempqty  =1;  
                                            }
                                            ?>
                                                <input type="hidden" name="product_id[]" value="<?php echo md5($prod); ?>" />
                                        <div class="tt-input-counter style-01"><span class="minus-btn"></span> 
                                                     <input size="100"  title="Numbers Only" name="qty[]" id="qty1"  value="<?php echo $tempqty; ?>" type="number"/>
                                                    <span class="plus-btn" data-max="100"></span></div> </div>
                                        <div class="col-item">
                                            <input type="hidden" name="sizeid" id="sizeid" value="<?php echo getsize('size',$pageu['size']); ?>">
                                      
                                        <?php if(isset($_SESSION['FUID'])) { 
                                        $_SESSION['RED_URL']='';
                                        ?>             
                                        <button type="button" data-pdid="<?php echo $pageu['pid'].'-'.getsize('size',$pageu['size']); ?>"  title="Add to Cart" class="btn btn-lg add_cart_this_product"  data-go-cart='1'>
                                       ADD TO CART
                                        </button>
                                        <?php } else { 
                                         $_SESSION['RED_URL']=$fsitename.'view/'.$_REQUEST['pid'].'.htm';
                                        ?>
                                        <nav class="main-nav">
                                        <button type="button" title="Add to Cart" class="btn btn-lg cd-signin">
                                       ADD TO CART
                                        </button></nav>
                                        <?php } ?>
                                        
                                        </div>
                                    </div>
                             </form>
                                </div>
                                <?php } ?>
                                <div class="tt-wrapper">
                                   
                                      <!-- ==========ask me======= -->
                                    <br><br>
             <input class="btn btn-xl" onclick="myFunction()" type="submit" value="Ask me?"> 
<p id="askresponse"></p>
 <form method="post" id="askme" style="margin-top: 20px; display: none;">
     <input type="hidden" name="productid" id="productid" value="<?php echo $pageu['pid']; ?>">
     <input type="hidden" name="vendorid" id="vendorid" value="<?php echo getvendor('uid',getusers('usergroup',$pageu['vendor'])); ?>">
      <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['FUID']; ?>">
<textarea id="w3review" placeholder="ask me" name="askmessage" rows="12" cols="50" style="border:2px solid #2879fe;padding:5px;"></textarea>
  <br><br>
  <input class="btn btn-xl" type="submit" value="Submit">
</form>


            </div>
             
            <script>
function myFunction() {
  var x = document.getElementById("askme");

  if (x.style.display === "none") {
    x.style.display = "block";
    x.style.background = "none";
  } else {
    x.style.display = "none";

  }
}
</script>

<!-- ================================= -->
                                </div>
                               
								
                               </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="container-indent wrapper-social-icon">
                
                <div class="container">
                    <ul class="tt-social-icon justify-content-center">
                        <li><a class="icon-g-64" href="http://www.facebook.com/"></a></li>
                        <li><a class="icon-h-58" href="http://www.facebook.com/"></a></li>
                        <li><a class="icon-g-66" href="http://www.twitter.com/"></a></li>
                        <li><a class="icon-g-67" href="http://www.google.com/"></a></li>
                        <li><a class="icon-g-70" href="https://instagram.com/"></a></li>
                    </ul>
                </div>
            </div> -->
            <div class="container-indent">
            <div class="container">
                <div class="tt-collapse-block">
                    <div class="tt-item">
                        <div class="tt-collapse-title">DESCRIPTION</div>
                        <div class="tt-collapse-content" style="display: block;">
                       <?php echo $pageu['description']; ?>
                    </div>
                    </div>
                         <div class="tt-item">
                                        <div class="tt-collapse-title tt-poin-comments">Write a review</div>
                                        <div class="tt-collapse-content">
                                            <div class="tt-review-block">
                                               
                                               
                                               
                                                    <div class="tt-review-form">
                                                    <div class="tt-message-info">BE THE FIRST TO REVIEW <span>“<?php echo $pageu['productname']; ?>”</span></div>
                                                    <p>Your email address will not be published. Required fields are marked *</p>
                                                    <p id="reviewresponse"></p>
                                                      
                                                    <div class="tt-rating-indicator">
                                                        <div class="tt-title">YOUR RATING *</div>
                                                        <div class="tt-rating">
                                                           
                                                            <div class="rating-opt">
															<div class="jr-ratenode jr-rating"></div>
															<div class="jr-ratenode jr-rating"></div>
															<div class="jr-ratenode jr-rating"></div>
															<div class="jr-ratenode jr-rating"></div>
															<div class="jr-ratenode jr-nomal"></div>
														</div>
                                                        </div>
                                                    </div>
                                                  <br>
                                                   <form class="form-default" method="post" id="reviewform">
                                                   <input type="hidden" id="info" name="ratingval" value="4">
                                                            <input type="hidden" name="rproduct" value="<?php echo $pageu['pid']; ?>">
                                                        <div class="form-group"><label for="inputName" class="control-label">YOUR NAME *</label> <input type="text" class="form-control" id="inputName" name="rname" placeholder="Enter your name"  required="required"/></div>
                                                        <div class="form-group">
                                                            <label for="inputEmail" class="control-label">E-MAIL *</label> <input type="email" class="form-control" id="inputEmail" name="remail" placeholder="Enter your e-mail"  required="required"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="textarea" class="control-label">YOUR REVIEW *</label> <textarea class="form-control" name="rcomment" id="textarea" placeholder="Enter your review" rows="8" required="required"></textarea>
                                                        </div>
                                                        <div class="form-group"><button type="submit" class="btn" name="rewsubmit" onsubmit="javascript:alert('success');">SUBMIT</button></div>
                                                   </form>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                               
                      <div class="tt-item">
                          <?php $review = DB("SELECT * FROM `productreview` WHERE `product`='".$pageu['pid']."' AND `status`='1' ");
$rcount = mysqli_num_rows($review); ?>

                                        <div class="tt-collapse-title tt-poin-comments">REVIEWS (<?php echo $rcount; ?>)</div>
                                        <div class="tt-collapse-content">
                                            <div class="tt-review-block">
                                                <div class="tt-row-custom-02">
                                                    <!--<div class="col-item"><h2 class="tt-title">REVIEW FOR VARIABLE PRODUCT</h2></div>-->
                                                    <!--<div class="col-item"><a href="#">Write a review</a></div>-->
                                                </div>
                                                <div class="tt-review-comments">
                                                    <?php
if ($rcount != '0') {
while ($reviewlist = mysqli_fetch_array($review)) {
?>
                                                    <div class="tt-item">
                                                        <!--<div class="tt-avatar">-->
                                                        <!--    <a href="#"><img data-src="images/product/single/review-comments-img-01.jpg" alt="" /></a>-->
                                                        <!--</div>-->
                                                        <div class="tt-content">
                                                            <div class="tt-rating">
                                                                <?php for($i=1;$i<=$reviewlist['rating'];$i++) { ?>
                                                                <i class="icon-star"></i>
                                                                <?php } ?>
                                                                </div>
                                                            <div class="tt-comments-info">
                                                                <span class="username">by <span><?php echo $reviewlist['name']; ?></span></span> <span class="time">on <?php echo date('F, d Y',strtotime($reviewlist['date'])); ?></span>
                                                            </div>
                                                            <!--<div class="tt-comments-title">Very Good!</div>-->
                                                            <p><?php echo $reviewlist['comments']; ?></p>
                                                        </div>
                                                    </div>
                                                 
                                                 <?php } } ?>
                                                </div>
                                           
                                           
                                            </div>
                                        </div>
                                    </div>
                               
                </div>
            </div>
        </div>
        <?php
            $relatedproduct = DB("SELECT * FROM `product` WHERE `status`='1' AND `cid`='".$pageu['cid']."' AND `sid`='".$pageu['sid']."' AND `innerid`='".$pageu['innerid']."' AND `pid`!='".$pageu['pid']."' ORDER BY `order` ASC ");
            $rcount = mysqli_num_rows($relatedproduct);
            if ($rcount != '0') {
             ?>
           <!--  <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-block-title text-left"><h3 class="tt-title-small">RELATED PRODUCT</h3></div>
                    <div class="tt-carousel-products row arrow-location-right-top tt-alignment-img tt-layout-product-item slick-animated-show-js">
                    <?php while ($relatedlist = mysqli_fetch_array($relatedproduct)) {
                         if($relatedlist['image']!='')  {
                            $imgpath = $fsitename.'images/product/' . $relatedlist['imagefolder'] . '/';   
                            $imgs= explode(',',$relatedlist['image']);
                            $imgres=$imgpath.$imgs['0'];
                            }
                           else {
                              $imgres=$fsitename.'images/noimage.png'; 
                           }

                        ?>
                                       
                    <div class="col-2 col-md-4 col-lg-3">
                            <div class="tt-product thumbprod-center">
                                <div class="tt-image-box">
                                    <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                                    <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a> <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                                    <a href="<?php echo $fsitename; ?>view/<?php echo $relatedlist['link']; ?>.htm">
                                        <span class="tt-img"><img src="<?php echo $imgres; ?>" alt="" /></span><span class="tt-img-roll-over"><img src="<?php echo $imgres; ?>" alt="" /></span>
                                    </a>
                                </div>
                                <div class="tt-description">
                                    <div class="tt-row">
                                        <ul class="tt-add-info">
                                            <li><a href="#"><?php echo getusers('name',$relatedlist['vendor']); ?></a></li>
                                        </ul>
                                    </div>
                                    <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $relatedlist['link']; ?>.htm"><?php echo $relatedlist['productname']; ?></a></h2>
                                    
                                     <?php if($relatedlist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $relatedlist['sprice']; ?></span> <span class="old-price">$ <?php echo $relatedlist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $relatedlist['price']; ?></span></div>
                                    <?php } ?>
                                    
                                     <br>
                                          <div class="tt-add-info">
                                        <ul>
                                            <li><span>Size:</span> <?php echo getsize('size',$relatedlist['size']); ?></li>
                                            <li><span>Color:</span> <?php echo getcolor('color',$relatedlist['color']); ?></li>
                                        </ul>
                                    </div>
                                    <div class="tt-product-inside-hover">
                                        
                                        <div class="tt-row-btn">
                                             <?php if(isset($_SESSION['FUID'])) { 
                                        $_SESSION['RED_URL']='';
                                        ?>   
                                            <a href="#" class="tt-btn-addtocart thumbprod-button-bg add_cart_this_product" data-pdid="<?php echo $relatedlist['pid'].'-'.getsize('size',$relatedlist['size']); ?>"  data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                                            <?php } else { 
                                             $_SESSION['RED_URL']=$fsitename.'view/'.$_REQUEST['pid'].'.htm';
                                            ?>
                                             <nav class="main-nav">
                                       
                                       
                                        
                                             <a href="#" class="tt-btn-addtocart thumbprod-button-bg cd-signin"  data-toggle="modal">ADD TO CART</a>
                                             </nav>
                                            <?php } ?>
                                            </div>
                                        <div class="tt-row-btn">
                                            <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a> <a href="#" class="tt-btn-wishlist"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div -->>
            <?php } ?>
        </div>

  <!-- ===slider -->

 <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'><link rel="stylesheet" href="./style.css">


<!-- partial:index.partial.html -->
<div class="tcb-product-slider">
        <div class="container">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
              
               <?php
                $i=0; $j=1;
              //  echo "SELECT * FROM `product` WHERE `status`='1' AND `cid`='".$pageu['cid']."' AND `sid`='".$pageu['sid']."' AND `innerid`='".$pageu['innerid']."' AND `pid`!='".$pageu['pid']."' ORDER BY `order` ASC ";

              $relatedproduct = DB("SELECT * FROM `product` WHERE `status`='1' AND `cid`='".$pageu['cid']."' AND `sid`='".$pageu['sid']."' AND `innerid`='".$pageu['innerid']."' AND `pid`!='".$pageu['pid']."' ORDER BY `order` ASC ");
               $rcount = mysqli_num_rows($relatedproduct);
            if ($rcount != '0') {
                while ($relatedlist = mysqli_fetch_array($relatedproduct)) {
                    if($relatedlist['image']!='')  {
                       $imgpath = $fsitename.'images/product/' . $relatedlist['imagefolder'] . '/';   
                       $imgs= explode(',',$relatedlist['image']);
                       $imgres=$imgpath.$imgs['0'];
                       }
                      else {
                         $imgres=$fsitename.'images/noimage.png'; 
                      }

             ?>
          <?php if($i%4==0) { ?> <div class="item <?php if($j==1) { ?>active<?php } ?>">
                        <div class="row">  
<?php } ?>  
<div class="col-2 col-md-4 col-lg-3">
                           <div class="tt-product thumbprod-center">
                               <div class="tt-image-box">
                                   <!-- <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                                   <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a> <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                                    -->
                                   <a href="<?php echo $fsitename; ?>view/<?php echo $relatedlist['link']; ?>.htm">
                                       <span class="tt-img"><img src="<?php echo $imgres; ?>" alt="" /></span><span class="tt-img-roll-over"><img data-lazy="<?php echo $imgres; ?>" alt="" /></span>
                                   </a>
                               </div>
                               <div class="tt-description">
                                   <div class="tt-row">
                                       <ul class="tt-add-info">
                                           <li><a href="#"><?php echo getusers('name',$relatedlist['vendor']); ?></a></li>
                                       </ul>
                                   </div>
                                   <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $relatedlist['link']; ?>.htm"><?php echo $relatedlist['productname']; ?></a></h2>
                                   
                                    <?php if($relatedlist['sprice']!='') { ?>
                                  <div class="tt-price"><span class="new-price">$<?php echo $relatedlist['sprice']; ?></span> <span class="old-price">$ <?php echo $relatedlist['price']; ?></span></div>
                                   <?php } else { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $relatedlist['price']; ?></span></div>
                                   <?php } ?>
                                   
                                    <br>
                                         <!-- <div class="tt-add-info">
                                       <ul>
                                           <li><span>Size:</span> <?php echo getsize('size',$relatedlist['size']); ?></li>
                                           <li><span>Color:</span> <?php echo getcolor('color',$relatedlist['color']); ?></li>
                                       </ul>
                                   </div> -->
                                   <div class="tt-product-inside-hover">
                                       
                                       <div class="tt-row-btn">
                                            <?php if(isset($_SESSION['FUID'])) { 
                                       $_SESSION['RED_URL']='';
                                       ?>   
                                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg add_cart_this_product" data-pdid="<?php echo $relatedlist['pid'].'-'.getsize('size',$relatedlist['size']); ?>"  data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                                           <?php } else { 
                                            $_SESSION['RED_URL']=$fsitename.'view/'.$_REQUEST['pid'].'.htm';
                                           ?>
                                            <nav class="main-nav">
                                      
                                      
                                       
                                            <a href="#" class="tt-btn-addtocart thumbprod-button-bg cd-signin"  data-toggle="modal">ADD TO CART</a>
                                            </nav>
                                           <?php } ?>
                                           </div>
                                       <div class="tt-row-btn">
                                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a> <a href="#" class="tt-btn-wishlist"></a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
       
<?php  $i++;  if($i%4==0) { ?>
 </div>
 </div>   
<?php } ?>    
                    <?php $j++; } } ?>   
                   
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div> <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>


<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>

  <!-- slider -->





  <!-- partial:index.partial.html -->
<!-- <div class="container"> -->
 <!--  <div class="row">
    <div id="carousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="d-lg-block">
            <div class="slide-box">
                <div class="row">
                    <div class="col-lg-3">
                     <div class="tt-product thumbprod-center">
    <div class="tt-image-box">
        <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
        <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a> <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
        <a href="http://localhost/tai/view/Testcheckedred.htm">
            <span class="tt-img"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
            <span class="tt-img-roll-over"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
        </a>
    </div>
    <div class="tt-description">
        <div class="tt-row">
            <ul class="tt-add-info">
                <li><a href="#">Priyan</a></li>
            </ul>
        </div>
        <h2 class="tt-title"><a href="http://localhost/tai/view/Testcheckedred.htm">Test Checked-Red</a></h2>

        <div class="tt-price"><span class="new-price">$900</span> <span class="old-price">$ 1000</span></div>

        <br />
        <div class="tt-add-info">
            <ul>
                <li><span>Size:</span> XXL</li>
                <li><span>Color:</span> red</li>
            </ul>
        </div>
        <div class="tt-product-inside-hover">
            <div class="tt-row-btn">
                <nav class="main-nav">
                    <a href="#" class="tt-btn-addtocart thumbprod-button-bg cd-signin" data-toggle="modal">ADD TO CART</a>
                </nav>
            </div>
            <div class="tt-row-btn"><a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a> <a href="#" class="tt-btn-wishlist"></a></div>
        </div>
    </div>
</div>
 </div>
<div class="col-lg-3">
  <div class="tt-product thumbprod-center">
    <div class="tt-image-box">
        <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
        <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a> <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
        <a href="http://localhost/tai/view/Testcheckedred.htm">
            <span class="tt-img"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
            <span class="tt-img-roll-over"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
        </a>
    </div>
    <div class="tt-description">
        <div class="tt-row">
            <ul class="tt-add-info">
                <li><a href="#">Priyan</a></li>
            </ul>
        </div>
        <h2 class="tt-title"><a href="http://localhost/tai/view/Testcheckedred.htm">Test Checked-Red</a></h2>

        <div class="tt-price"><span class="new-price">$900</span> <span class="old-price">$ 1000</span></div>

        <br />
        <div class="tt-add-info">
            <ul>
                <li><span>Size:</span> XXL</li>
                <li><span>Color:</span> red</li>
            </ul>
        </div>
        <div class="tt-product-inside-hover">
            <div class="tt-row-btn">
                <nav class="main-nav">
                    <a href="#" class="tt-btn-addtocart thumbprod-button-bg cd-signin" data-toggle="modal">ADD TO CART</a>
                </nav>
            </div>
            <div class="tt-row-btn"><a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a> <a href="#" class="tt-btn-wishlist"></a></div>
        </div>
    </div>
</div>
   </div>
 <div class="col-lg-3">
<div class="tt-product thumbprod-center">
    <div class="tt-image-box">
        <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
        <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a> <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
        <a href="http://localhost/tai/view/Testcheckedred.htm">
            <span class="tt-img"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
            <span class="tt-img-roll-over"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
        </a>
    </div>
    <div class="tt-description">
        <div class="tt-row">
            <ul class="tt-add-info">
                <li><a href="#">Priyan</a></li>
            </ul>
        </div>
        <h2 class="tt-title"><a href="http://localhost/tai/view/Testcheckedred.htm">Test Checked-Red</a></h2>

        <div class="tt-price"><span class="new-price">$900</span> <span class="old-price">$ 1000</span></div>

        <br />
        <div class="tt-add-info">
            <ul>
                <li><span>Size:</span> XXL</li>
                <li><span>Color:</span> red</li>
            </ul>
        </div>
        <div class="tt-product-inside-hover">
            <div class="tt-row-btn">
                <nav class="main-nav">
                    <a href="#" class="tt-btn-addtocart thumbprod-button-bg cd-signin" data-toggle="modal">ADD TO CART</a>
                </nav>
            </div>
            <div class="tt-row-btn"><a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a> <a href="#" class="tt-btn-wishlist"></a></div>
        </div>
    </div>
</div>
 </div>
<div class="col-lg-3">
<div class="tt-product thumbprod-center">
    <div class="tt-image-box">
        <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
        <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a> <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
        <a href="http://localhost/tai/view/Testcheckedred.htm">
            <span class="tt-img"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
            <span class="tt-img-roll-over"><img src="http://localhost/tai/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="" class="loading" data-was-processed="true" /></span>
        </a>
    </div>
    <div class="tt-description">
        <div class="tt-row">
            <ul class="tt-add-info">
                <li><a href="#">Priyan</a></li>
            </ul>
        </div>
        <h2 class="tt-title"><a href="http://localhost/tai/view/Testcheckedred.htm">Test Checked-Red</a></h2>

        <div class="tt-price"><span class="new-price">$900</span> <span class="old-price">$ 1000</span></div>

        <br />
        <div class="tt-add-info">
            <ul>
                <li><span>Size:</span> XXL</li>
                <li><span>Color:</span> red</li>
            </ul>
        </div>
        <div class="tt-product-inside-hover">
            <div class="tt-row-btn">
                <nav class="main-nav">
                    <a href="#" class="tt-btn-addtocart thumbprod-button-bg cd-signin" data-toggle="modal">ADD TO CART</a>
                </nav>
            </div>
            <div class="tt-row-btn"><a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a> <a href="#" class="tt-btn-wishlist"></a></div>
        </div>
    </div>
</div></div>
</div>
</div>
</div>

<div class="d-none d-md-block d-lg-none">
            <div class="slide-box">
              <img src="https://picsum.photos/240/200/?image=0&random" alt="First slide">
              <img src="https://picsum.photos/240/200/?image=1&random" alt="First slide">
              <img src="https://picsum.photos/240/200/?image=2&random" alt="First slide">
            </div>
          </div> -->
    <!--       <div class="d-none d-sm-block d-md-none">
            <div class="slide-box">
              <img src="https://picsum.photos/270/200/?image=0&random" alt="First slide">
              <img src="https://picsum.photos/270/200/?image=1&random" alt="First slide">
            </div>
          </div>
          <div class="d-block d-sm-none">
            <img class="d-block w-100" src="https://picsum.photos/600/400/?image=0&random" alt="First slide">
          </div>
        </div>
        <div class="carousel-item">
          <div class="d-none d-lg-block">
            <div class="slide-box">
              <img src="https://picsum.photos/285/200/?image=4&random" alt="Second slide">
              <img src="https://picsum.photos/285/200/?image=5&random" alt="Second slide">
              <img src="https://picsum.photos/285/200/?image=6&random" alt="Second slide">
              <img src="https://picsum.photos/285/200/?image=7&random" alt="Second slide">
            </div>
          </div>
          <div class="d-none d-md-block d-lg-none">
            <div class="slide-box">
              <img src="https://picsum.photos/240/200/?image=3&random" alt="Second slide">
              <img src="https://picsum.photos/240/200/?image=4&random" alt="Second slide">
              <img src="https://picsum.photos/240/200/?image=5&random" alt="Second slide">
            </div>
          </div>
          <div class="d-none d-sm-block d-md-none">
            <div class="slide-box">
              <img src="https://picsum.photos/270/200/?image=2&random" alt="Second slide">
              <img src="https://picsum.photos/270/200/?image=3&random" alt="Second slide">
            </div>
          </div>
          <div class="d-block d-sm-none">
            <img class="d-block w-100" src="https://picsum.photos/600/400/?image=1&random" alt="Second slide">
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div> -->
  <!-- </div> --> 
<!-- </div> -->
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js'></script>
 

     <div class="modal fade" id="ModalSubsribeGood" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-xs"><div class="modal-content"><div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
             <span class="icon icon-clear"></span></button></div>
             <div class="modal-body">
                 <div class="tt-modal-subsribe-good"><i class="icon-f-68"></i> You have successfully signed!</div></div></div></div></div><!-- modal (size guid) -->
     <div class="modal fade" id="modalProductInfo" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
             <span class="icon icon-clear"></span></button></div>
             <div class="modal-body">
                 <div class="tt-layout-product-info"><h6 class="tt-title">SIZE GUIDE</h6>
                 This is an approximate conversion table to help you find your size.<div class="tt-table-responsive-md">
                     
                 <img src="<?php echo $fsitename; ?>images/sizechart/<?php echo getinnetcat('image',$pageu['innerid']); ?>">
                 </div></div></div></div></div></div><!-- modal (size guid) --><div class="modal fade" id="modalProductInfo-02" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                         <span class="icon icon-clear"></span></button></div>
                         <div class="modal-body"><div class="tt-layout-product-info-02">
                             <h6 class="tt-title">RETURN POLICY</h6>
                            <?php echo getprofile('return_policy','1'); ?>
                            </div></div></div></div></div>
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                            
                            
                             <script src="<?php echo $fsitename; ?>js/jquery-rating.js"></script>
                             
                             
										
		<script>
			 $('.rating-opt').start(function(cur){
			 $('#info').val(cur);
			});
        </script>
        <script>
      $(document).ready(function(){
          setTimeout(function() {
              <?php
              if($msg!='') {
                  echo 'alert("Review sent successfully");';
              }
              
              
              ?>
              }, 500);
      });
  </script>

		<script>
     $(document).ready(function(){
$('#askme').submit(function(){
// show that something is loading
$('#askresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
    $('#askresponse').html(data);
    document.getElementById("askme").style.display="none";
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});
});
</script>

<?php include "require/footer.php"; ?>

	<script>
		function getrating(a)
		{
		   	 $('#ratingval').val(a);
		}
			 $('.rating-opt').start(function(cur){
				console.log(cur);
				 $('#info').text(cur);
			});
		</script>
<script>
function getprice(sizeid,sprice,price){
  
    $('#sizeid').val(sizeid); 
    $('#sprice1').html('$ '+sprice);   
    $('#price1').html('$ '+price);  
}    
</script>    

 <script src="<?php echo $fsitename; ?>separate-include/single-product/single-product.js"></script>