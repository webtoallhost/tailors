<?php
require_once __DIR__ . '/vendor/autoload.php';
include '../admin/config/config.inc.php';

//if ($_SESSION['CART_TOTAL'] != '') {
//print_r($_POST); print_r($_REQUEST); exit;
// $orderid = $_SESSION['last_order_id'];
$general = FETCH_all("SELECT * FROM `generalsettings` WHERE `generalid` = ?", '1');
$am = FETCH_all("SELECT * FROM `norder` WHERE `oid`=?", $_REQUEST['id']);
//$general = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');

$to = $general['recoveryemail'];
$subject = "Your Click2buy Order Confirmation " . $am['order_id'];
$subject1 = "You have new order " . $am['order_id'];

if ($am['anonymously'] == '1') {
    $anony = 'Send this as Anonymous';
} else {
    $anony = '';
}

if ($am['promotional_type'] == '1') {
    $ptype = $am['promotional_code'] . ' (' . $am['promotional_discount'] . ' %)';
} elseif ($am['promotional_type'] == '2') {
    $ptype = $am['promotional_code'] . ' (' . $am['promotional_discount'] . ' ' . $am['currency'] . ')';
} else {
    $ptype = '-';
}

if ($am['guest'] == '1') {
    $cemail = getguest1('email', $am['CusID']);
} else {
    $cemail = getcustomer1('E-mail', $am['CusID']);
}

if ($am['order_status'] == '0') {
    $ostatus = 'Awaiting for Payment';
} elseif ($am['order_status'] == '1') {
    $ostatus = 'Awaiting Fulfillment';
} elseif ($am['order_status'] == '2') {
    $ostatus = 'Completed';
} elseif ($am['order_status'] == '3') {
    $ostatus = 'Cancelled';
} elseif ($am['order_status'] == '4') {
    $ostatus = 'Declined';
} elseif ($am['order_status'] == '5') {
    $ostatus = 'Refunded';
} elseif ($am['order_status'] == '6') {
    $ostatus = 'Partially Refunded';
}

$message = ' <table style="width:100%; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width:100%; border-bottom:1px solid #cc6600;text-align:center;"><h2 style="color:#cc6600;" >Invoice / Receipt</h2></td>
                </tr>
                </table>
                <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40%" align="left" >
					
					' . $general['about'] . '</td>
                    <td width="60%" align="right"><table><tbody>	
                        <tr><td></td><td></td><td></td></tr>
                        <tr><td></td><td></td><td></td></tr>
			<tr>
				<td style="vertical-align:top"><small><strong>Invoice Number</strong></small></td>
				<td style="vertical-align:top"><small>:</small></td>
				<td style="vertical-align:top"><small><strong>' . $am['order_id'] . '</strong></small></td>
			</tr>
			<tr>
				<td style="vertical-align:top"><small>Order Date</small></td>
				<td style="vertical-align:top"><small>:</small></td>
				<td style="vertical-align:top"><small>' . date('d-M-Y H:i:s A', strtotime($am['datetime'])) . '</small></td>
			</tr>	
                        <tr>
		       <td style="vertical-align:top"><small><strong>Order Status</strong></small></td>
		       <td style="vertical-align:top"><small>:</small></td>
		       <td style="vertical-align:top"><small><strong>' . $ostatus . '</strong></small></td>
		       </tr></tbody></table>
                          </td>
                  </tr> </table>
                  <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                          <tr>                            
                             <td width="50%;">
                                 <h4>Shipping Address</h4>
                                 <p>' . $am['shipping_address'] . '</p>
                             </td>
                            </tr>
                   </table> 
                    <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="100%;">
                                    <p><b>Order Comments : </b> ' . $am['order_comments'] . '</p>
                                    <p><b>Card Message : </b> ' . $am['card_message'] . '</p>
                                    <p>' . $anony . '</p>
                                </td>
                            </tr>
                        </table>
               <table style="width:100%; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%">
                        <h4>Your Order Contains</h4>
                       
                    </td>
                </tr>
            </table>   ' . mailcart($am['oid']) . '
             <table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" valign="top">
                        <h4>Payment Method : ' . $am['payment_mode'] . '</h4></td>
                    <td width="50%" align="right">
                        <table style="width:100%; font-size:13px;">
                            <tr>
                                <td style="border-bottom:1px dotted #cc6600;"><b>SubTotal :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right">&#8377;&nbsp;' . number_format($am['subtotal'], '2', '.', '') . '</td>
                            </tr>
							<tr>
                                <td style="border-bottom:1px dotted #cc6600;"><b>Discount :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right">&#8377;&nbsp;' . number_format($am['discounted_amount'], '2', '.', '') . '</td>
                            </tr>
							<tr>
                                <td style="border-bottom:1px dotted #cc6600;"><b>Shipping Cost :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right">&#8377;&nbsp;' . number_format($am['shipping_charge'], '2', '.', '') . '</td>
                            </tr>
							<tr>
                                <td style="border-bottom:1px dotted #cc6600;"><b>Total :</b></td><td style="border-bottom:1px dotted #cc6600;" align="right">&#8377;&nbsp;' . number_format($am['over_all_total'], '2', '.', '') . '</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>';

// echo $message;

$mpdf = new mPDF();
$mpdf->SetDisplayMode('default');
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
$filename = "test.txt";

$file = fopen($filename, "w");
fwrite($file, $message);
$mpdf->SetTitle('Quotation Report');
$mpdf->keep_table_proportions = false;
$mpdf->shrink_this_table_to_fit = 0;
$mpdf->SetAutoPageBreak(true, 10);
$mpdf->WriteHTML(file_get_contents($filename));
$mpdf->setAutoBottomMargin = 'stretch';
//$mpdf->setHTMLFooter('<div style="text-align:center;">THANK YOU FOR YOUR ENQUIRY</div><div style="border-top: 0.1mm solid #000000;text-align:center;padding:5px 0 5px 0;"><small>'.$address.'</samll></div>');
$mpdf->Output('yourFileName.pdf', 'I');
?>
