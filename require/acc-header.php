<div class="row">
    <div class="col-md-9 col-md-push-3 ">
        <div class="widget">
            <div class="widget-body">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide testimonial">
                            <blockquote style="float:left; width:100%; height:100%; padding: 53px 45px 20px 60px;">
                                <div class="col-md-9">
                                    <h2>Welcome <?php echo getcustomer('FirstName', $_SESSION['FUID']); ?></h2>
                                    <?php
                                    if (getcustomerlog('Logintime', $_SESSION['FUID']) != '') {
                                        ?>
                                        <h4>Last Login at : <?php echo date('d / M / Y h:i:s A', strtotime(getcustomerlog('Logintime', $_SESSION['FUID']))); ?></h4>
                                    <?php } ?>
                                    <ul class="category-list">
                                        <?php $onum=FETCH_all("SELECT COUNT(`CusID`) AS `num` FROM `norder` WHERE `CusID`=?",$_SESSION['FUID']); ?>
                                        <li>Total Orders : <?php echo $onum['num']; ?></li>
                                    </ul>

                                </div>
                                <div class="col-md-3">
                                    <figure>
                                        <?php if (getcustomer('Image', $_SESSION['FUID']) != '') { ?>
                                            <img src="<?php echo $fsitename; ?>images/profile/<?php echo getcustomer('Image', $_SESSION['FUID']); ?>" alt="<?php echo getcustomer('FirstName', $_SESSION['FUID']); ?>" style="width:100%;" />
                                        <?php } else { ?>
                                            <img src="<?php echo $fsitename; ?>assets/images/products/product1.jpg" alt="Mark" style="width:100%;" />
                                        <?php } ?>
                                    </figure>
                                </div>
                            </blockquote>

                        </div><!-- End .testimonial -->
                    </div><!-- End .swiper-wrapper -->
                </div><!-- end .swiper-container -->
            </div><!-- End .widget-body -->
        </div><!-- End .widget -->


    </div><!-- End .col-md-9 -->

    <aside class="col-md-3 col-md-pull-9 sidebar">
        <div class="widget widget-category">
            <h3 class="widget-title">
                Quick Links
                <i class="icon cat-title-icon"></i>
            </h3>
            <ul class="category-list">
                <li>
                    <a href="<?php echo $fsitename; ?>pages/myaccount.htm" <?php echo $acactive1; ?>>
                        My Account
                        <i class="cat-icon fa fa-user"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $fsitename; ?>pages/myprofile.htm" <?php echo $acactive2; ?>>
                        Edit Profile
                        <i class="cat-icon fa fa-user-plus"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $fsitename; ?>pages/changepassword.htm" <?php echo $acactive3; ?>>
                        Change Password
                        <i class="cat-icon fa fa-key"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $fsitename; ?>pages/manageaddress.htm" <?php echo $acactive5; ?>>
                        Manage Address
                        <i class="cat-icon fa fa-address-book"></i>
                    </a>
                </li>
				 <li>
                    <a href="<?php echo $fsitename; ?>pages/mywishlist.htm" <?php echo $acactive6; ?>>
                        View Wishlist
                        <i class="cat-icon fa fa-shopping-cart"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $fsitename; ?>pages/myorders.htm" <?php echo $acactive4; ?>>
                        View Orders
                        <i class="cat-icon fa fa-shopping-cart"></i>
                    </a>
                </li>
                <li>
                    <a href="<?php echo $fsitename; ?>pages/logout.htm">
                        Logout
                        <i class="cat-icon fa fa-sign-out"></i>
                    </a>
                </li>
            </ul>
        </div><!-- End .widget -->

    </aside>
</div><!-- End .row -->