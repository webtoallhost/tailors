<aside class="col-md-3 col-md-pull-9 sidebar">
    <?php
    if ($catid == '1') {
        $cat = $db->prepare("SELECT `sid`,`subcategory`,`link` FROM `subcategory` WHERE `status`= ? AND FIND_IN_SET (?,`cid`)");
        $cat->execute(array('1', $pageu['cid']));
        $count = $cat->rowCount();
        if ($count > 0) {
            ?>
            <div class="widget widget-category">
                <h3 class="widget-title">
                    Sub-Categories
                    <i class="icon cat-title-icon"></i>
                </h3>
                <ul class="category-list">
                    <?php
                    while ($cate = $cat->fetch(PDO::FETCH_ASSOC)) {
                        $pnum = $db->prepare("SELECT `pid` FROM `product` WHERE `status`= ? AND FIND_IN_SET (?,`sid`) AND FIND_IN_SET (?,`cid`) ");
                        $pnum->execute(array('1', $cate['sid'],  $pageu['cid']));
                        $pcount = $pnum->rowCount();
                        ?>
                        <li>
                            <a href="<?php echo $fsitename . getcategoryy('link', $_REQUEST['cid']) . '/' . $cate['link'] . '.htm'; ?>">
                                <?php echo stripslashes($cate['subcategory']); ?>
                                <i class="cat-icon" style="background:#00aeff; color:#FFF; font-weight:bold; border-radius:20px; z-index: 99; padding: 3px 0px 0px 0px; text-align: center; font-size: 12px;"><?php echo $pcount; ?></i>
                            </a>
                        </li>
                    <?php } ?>

                </ul>
            </div>
            <?php
        }
    } elseif ($subid == '1') {
        $cat = $db->prepare("SELECT `innerid`,`innername`,`link` FROM `innercategory` WHERE `status`= ? AND FIND_IN_SET (?,`cid`) AND FIND_IN_SET (?,`subcategory`)");
        $cat->execute(array('1', getcategoryy('cid', $_REQUEST['cid']), $pageu['sid']));
        $count = $cat->rowCount();
        if ($count > 0) {
            ?>
            <div class="widget widget-category">
                <h3 class="widget-title">
                    Inner-Categories
                    <i class="icon cat-title-icon"></i>
                </h3>
                <ul class="category-list">
                    <?php
                    while ($cate = $cat->fetch(PDO::FETCH_ASSOC)) {
                        $pnum = $db->prepare("SELECT `pid` FROM `product` WHERE `status`= ? AND FIND_IN_SET (?,`innerid`)");
                        $pnum->execute(array('1', $cate['innerid']));
                        $pcount = $pnum->rowCount();
                        ?>
                        <li>
                            <a href="<?php echo $fsitename . getcategoryy('link', $_REQUEST['cid']) . '/' . getsubcategoryy('link', $_REQUEST['sid']) . '/' . $cate['link'] . '.htm'; ?>">
                                <?php echo stripslashes($cate['innername']); ?>
                                <i class="cat-icon" style="background:#00aeff; color:#FFF; font-weight:bold; border-radius:20px; z-index: 99; padding: 3px 0px 0px 0px; text-align: center; font-size: 12px;"><?php echo $pcount; ?></i>
                            </a>
                        </li>
                    <?php } ?>

                </ul>
            </div><!-- End .widget -->
            <?php
        }
    } else {
        ?>
        <div class="widget widget-category">
            <h3 class="widget-title">
                Shop By Categories
                <i class="icon cat-title-icon"></i>
            </h3>
            <ul class="category-list">
                <?php
                $cat = $db->prepare("SELECT * FROM `category` WHERE `status`= ?");
                $cat->execute(array('1'));
                while ($cate = $cat->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <li>
                        <a href="<?php echo $fsitename . $cate['link'] . '.htm'; ?>">
                            <?php echo stripslashes($cate['category']); ?>
                            <i class="cat-icon <?php echo $cate['icon']; ?>"></i>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div><!-- End .widget -->
    <?php } /* <div class="widget filter-box-widget">
      <h3 class="widget-title">Color filter</h3>
      <div class="widget-body">
      <div class="filter-color-container">
      <div class="row">
      <a href="#" class="filter-color-box" style="background-color: #fff;"></a>
      <a href="#" class="filter-color-box" style="background-color: #f3eaa2;"></a>
      <a href="#" class="filter-color-box" style="background-color: #f1dcb1;"></a>
      <a href="#" class="filter-color-box" style="background-color: #f8b34f;"></a>
      <a href="#" class="filter-color-box" style="background-color: #aaffff;"></a>
      <a href="#" class="filter-color-box" style="background-color: #79c5d3;"></a>
      <a href="#" class="filter-color-box" style="background-color: #a4a9f2;"></a>
      <a href="#" class="filter-color-box" style="background-color: #6e82ce;"></a>
      <a href="#" class="filter-color-box" style="background-color: #ad7abd;"></a>
      <a href="#" class="filter-color-box" style="background-color: #c8e472;"></a>
      <a href="#" class="filter-color-box" style="background-color: #74d893;"></a>
      <a href="#" class="filter-color-box" style="background-color: #6cb34d;"></a>
      <a href="#" class="filter-color-box" style="background-color: #fdb9b9;"></a>
      <a href="#" class="filter-color-box" style="background-color: #fc7f7f;"></a>
      <a href="#" class="filter-color-box" style="background-color: #fc568d;"></a>
      <a href="#" class="filter-color-box" style="background-color: #dcdcdc;"></a>
      <a href="#" class="filter-color-box" style="background-color: #937c61;"></a>
      <a href="#" class="filter-color-box" style="background-color: #434343;"></a>
      </div><!-- End .row -->
      </div><!-- End .filter-color-container -->
      </div><!-- End .widget-body -->
      </div><!-- End .widget -->

      <div class="widget filter-box-widget">
      <h3 class="widget-title">Size filter</h3>
      <div class="widget-body">
      <div class="filter-color-container">
      <div class="row">
      <a href="#" class="filter-size-box">6</a>
      <a href="#" class="filter-size-box">8</a>
      <a href="#" class="filter-size-box">10</a>
      <a href="#" class="filter-size-box">12</a>
      <a href="#" class="filter-size-box">14</a>
      <a href="#" class="filter-size-box">16</a>
      <a href="#" class="filter-size-box">XS</a>
      <a href="#" class="filter-size-box">S</a>
      <a href="#" class="filter-size-box">M</a>
      <a href="#" class="filter-size-box">ML</a>
      <a href="#" class="filter-size-box">L</a>
      <a href="#" class="filter-size-box">XL</a>
      </div><!-- End .row -->
      </div><!-- End .filter-color-container -->
      </div><!-- End .widget-body -->
      </div><!-- End .widget --> */ ?>

    <div class="widget">
        <h3 class="widget-title">Price filter</h3>

        <div class="widget-body">
            <div id="filter-range-details" class="row">
                <div class="col-xs-6">
                    <div class="filter-price-container">
                        <input type="text" id="price-range-low" class="form-control">
                        <div class="price-label">
                            <span class="hidden-md">From: </span><span id="low-price-val"></span>
                        </div><!-- End .price-label -->
                    </div><!-- End .filter-price-container -->
                </div><!-- End .col-xs-6 -->
                <div class="col-xs-6">
                    <div class="filter-price-container">
                        <input type="text" id="price-range-high" class="form-control">
                        <div class="price-label">
                            <span class="hidden-md">To: </span><span id="high-price-val"></span>
                        </div><!-- End .price-label -->
                    </div><!-- End .filter-price-container -->
                </div><!-- End .col-xs-6 -->
            </div><!-- End #filter-range-details -->

            <div id="price-slider"></div><!-- End #price-slider -->

            <div class="filter-price-action">
                <a href="#" class="btn btn-border" id="price_filter">Ok</a>
                <!--<a href="#" class="btn btn-border">Clear</a>-->
            </div><!-- End #filter-price-action -->
        </div><!-- End .widget-body -->
    </div><!-- End .widget -->

</aside>