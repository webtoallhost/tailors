  <div class="col-lg-3 col-md-3 col-sidebar">

                        <!-- Block  Breadcrumb-->
                        
                        
                       <!-- Block  Breadcrumb-->

                        <!-- block filter products -->
                        <div id="layered-filter-block" class="block-sidebar block-filter no-hide" style="margin-top:10px;">
                            <div class="close-filter-products"><i class="fa fa-times" aria-hidden="true"></i></div>
                            <div class="block-title">
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;MY ACCOUNT</strong>
                            </div>
                            <div class="block-content">

                                <!-- Filter Item  categori-->
                                <div class="filter-options-item filter-options-categori">
                                    <div class="filter-options-title"> Quick Links</div>
                                    <div class="filter-options-content">
                                        <ol class="items">
							         
								<li class="item ">
								<label>
								<a href="<?php echo $fsitename; ?>pages/myaccount.htm" <?php if($acactive=='1') { ?>class="active" <?php } ?>><i class="cat-icon fa fa-user"></i>&nbsp;&nbsp;My Account
								</a></label>
								</li>
								<li class="item ">
								<label>
								<a href="<?php echo $fsitename; ?>pages/myprofile.htm" <?php if($acactive=='2') { ?>class="active" <?php } ?>><i class="cat-icon fa fa-user-plus"></i>&nbsp;&nbsp;Edit Profile
								</a></label>
								</li>
								<li class="item ">
								<label>
								<a href="<?php echo $fsitename; ?>pages/changepassword.htm" <?php if($acactive=='3') { ?>class="active" <?php } ?>><i class="cat-icon fa fa-key"></i>&nbsp;&nbsp;Change Password
								</a></label>
								</li>
								<li class="item ">
								<label>
								<a href="<?php echo $fsitename; ?>pages/mywishlist.htm" <?php if($acactive=='5') { ?>class="active" <?php } ?>><i class="cat-icon fa fa-key"></i>&nbsp;&nbsp;View Wishlist
								</a></label>
								</li>
								<li class="item ">
								<label>
								<a href="<?php echo $fsitename; ?>pages/myorders.htm" <?php if($acactive=='4') { ?>class="active" <?php } ?>><i class="cat-icon fa fa-shopping-cart"></i>&nbsp;&nbsp;View Orders
								</a></label>
								</li>
								
								<li class="item ">
								<label>
								<a href="<?php echo $fsitename; ?>pages/logout.htm" <?php echo $acactive6; ?>><i class="cat-icon fa fa-sign-out"></i>&nbsp;&nbsp;Logout
								</a></label>
								</li>
                                        </ol>
                                    </div>
                                </div><!-- Filter Item  categori-->

                           
                            </div>
                        </div><!-- Filter -->

                       </div><!-- Sidebar -->
