<?php

include_once("../admin/config/config.inc.php");
if ($_POST['cartid'] != '' && $_POST['type'] == 'add') {
    $Cart = new Cart();
    $count = $Cart->AddCart($_POST['cartid'], $_POST['count'],$_POST['sizeid']);
    echo json_encode(array(
        $count,
        $Cart->Quick_Cart()
    ));
} elseif ($_POST['cartid'] != '' && $_POST['type'] == 'update') {
    $Cart = new Cart();
    $count = $Cart->UpdateCart_Ajax($_POST['cartid'], $_POST['count'], $_POST['sizeid']);
    echo json_encode(array(
        $count[0],
        $Cart->Quick_Cart(),        
        number_format($count[1], '2', '.', ','),       
        number_format($_SESSION['CART_TOTAL'], '2', '.', ',')	
    ));
} elseif ($_POST['cartid'] != '' && $_POST['type'] == 'delete') {
    $Cart = new Cart();
    $count = $Cart->DeleteCart($_POST['cartid']);
    echo json_encode(array(
        $count,
        $Cart->Quick_Cart(),
        $_SESSION['CART_TOTAL']
    ));
} elseif ($_POST['code'] != '') {
    $Cart = new Cart();
    $res = $Cart->Coupon_Code($_POST['code'],$_POST['shipprice']);
    echo json_encode(array(
        $Cart->cart_count,
        $res,
        $Cart->Quick_Cart(),
        number_format($_SESSION['CART_TOTAL'], '2', '.', ','),
        number_format($_SESSION['CART_TOTAL_FINAL'], '2', '.', ','),
        number_format($_SESSION['CART_DISCOUNT'], '2', '.', ','),
        number_format($_SESSION['SHIP_AMOUNT'], '2', '.', ',')
        
    ));
} elseif ($_POST['code_remove'] == '1') {
    $Cart = new Cart();
    $res = $Cart->Coupon_Code_Remove();
    echo json_encode(array(
        $Cart->cart_count,
        $res,
        $Cart->Quick_Cart(),
        number_format($_SESSION['CART_TOTAL'], '2', '.', ','),
        number_format($_SESSION['CART_TOTAL_FINAL'], '2', '.', ','),
        number_format($_SESSION['CART_DISCOUNT'], '2', '.', ','),
        number_format($_SESSION['SHIP_AMOUNT'], '2', '.', ',')	
    ));
} 
elseif ($_POST['reward_point'] != '') {

    $Cart = new Cart();
    $res = $Cart->Reward_point($_POST['reward_point']);
    print_r(json_encode(array(
        $res,
        $_SESSION['CART_TOTAL'],       
        number_format($_SESSION['CART_TOTAL_FINAL'], '2', '.', ','),	
        $_SESSION['CART_DISCOUNT'],
        $_SESSION['SHIP_AMOUNT']
    )));
} 
elseif ($_POST['remove_reward_point'] != '') {
    $Cart = new Cart();
    $res = $Cart->Remove_Reward_point($_POST['remove_reward_point']);
    print_r(json_encode(array(
        $res,
        $_SESSION['CART_TOTAL'],
         number_format($_SESSION['CART_TOTAL_FINAL'], '2', '.', ','),	
        $_SESSION['CART_DISCOUNT'],
        $_SESSION['SHIP_AMOUNT']
    )));
}
elseif ($_POST['shippi_state']!='') {
	
    $Cart = new Cart();
    $res = $Cart->addshipamt($_POST['shippi_state'],$_POST['shippi_postcode']);
    echo json_encode(array(
        $Cart->cart_count,
        $res,
        $Cart->Quick_Cart(),
        $_SESSION['CART_TOTAL'],
        $_SESSION['CART_TOTAL_FINAL'],
        $_SESSION['CART_DISCOUNT'],
        $_SESSION['SHIP_AMOUNT']
    ));
}
elseif ($_POST['detshippi_postcode']!='') {
$Cart = new Cart();
    $res =$Cart->detaddshipamt($_POST['detshippi_postcode'],$_POST['prodid']);
    echo json_encode(array(
        $res
        
    ));
}
/* elseif ($_POST['detshippi_state']!='') {
$Cart = new Cart();
    $res =$Cart->detaddshipamt($_POST['detshippi_state'],$_POST['detshippi_postcode'],$_POST['prodid']);
    echo json_encode(array(
        $res
        
    ));
} */
?>
