<style type="text/css">
	.cart-navigation {
		text-align:center;
		margin:15px;
	}
	.cart-navigation li{
		display: inline-block;
		margin:0 100px;
	}
	.cart-navigation li span.cart-item-step{
		display:block;
		width: 46px;
		border: 1px solid #ddd;
		padding: 10px;
		border-radius: 59px;
		color: #ddd;
	}
	.cart-navigation li span.cart-item-step.selected{
		color:#fff;
		border: 1px solid #00AEFF;
		background: #00AEFF;
	}
	.cart-navigation li span.cart-item-text.selected{
		font-weight:bold;
	}
	.cart-item-line{
		position: relative;
		top: 38px;
		width: 453px;
		background: #ddd;
		height: 1px;
		margin: 0 auto;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="cart-item-line"></div>
		<ul class="cart-navigation">
			<li>
				<span class="cart-item-step <?php echo $selected1; ?>">1</span>
				<br />
				<span class="cart-item-text <?php echo $selected1; ?>">Cart</span>
			</li>
			<li>
				<span class="cart-item-step <?php echo $selected2; ?>">2</span>
				<br />
				<span class="cart-item-text <?php echo $selected2; ?>">Checkout</span>
			</li>
			<li>
				<span class="cart-item-step <?php echo $selected3; ?>">3</span>
				<br />
				<span class="cart-item-text <?php echo $selected3; ?>">Payment</span>
			</li>
		</ul>
	</div>
</div>
