<?php
error_reporting(0);
if (($dynamic == 1)||($fdynamic == 1)) {

} else {
include 'admin/config/config.inc.php';
}

$general = FETCH_all("SELECT * FROM `generalsettings` WHERE `generalid` = ?", '1');

$manageprofile = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
$content = FETCH_all("SELECT * FROM `static_pages` WHERE `stid`=?", $page);
@extract($content);
$content = str_replace("../", $fsitename, $content);

if ($dynamic == '1') {
$t = stripslashes($pageu['metatitle']);
$kfd = stripslashes($pageu['metakeywords']);
$das = stripslashes($pageu['metadescription']);
} else {
$t = stripslashes($metatitle);
$kfd = stripslashes($metakeywords);
$das = stripslashes($metadescription);
}
if ($selid == '1') {
$menu1 = ' class="open"';
}
if ($selid == '2') {
$menu2 = ' class="open"';
}
if ($selid == '3') {
$menu3 = ' class="open"';
}
if ($selid == '4') {
$menu4 = ' open';
}
if ($selid == '5') {
$menu5 = ' class="open"';
}
if ($selid == '6') {
$menu6 = ' class="open"';
}

if ($acactive == '1') {
$acactive1 = ' class="active"';
}
if ($acactive == '2') {
$acactive2 = ' class="active"';
}
if ($acactive == '3') {
$acactive3 = ' class="active"';
}
if ($acactive == '4') {
$acactive4 = ' class="active"';
}
if ($acactive == '5') {
$acactive5 = ' class="active"';
}


if ($image != '') {
$file = $fsitename . 'staticimg/' . $image;
} elseif ($pageu['image'] != '') {
$file = $fullimage;
} else {
$file = $fsitename . 'images/kpmlogo-big.png';
}
if ($file != '') {
//$image_type = image_type_to_mime_type(exif_imagetype($file));
//list($width, $height, $type, $attr) = getimagesize($file);
}

$social1 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='1'");
$social2 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='2'");
$social3 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='3'");
$social4 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='4'");
$social5 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='5'");
$social6 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='6'");
$social7 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='7'");
$social8 = DB_QUERY("SELECT * FROM `socialmedia` WHERE `sid`='8'");

$advertisement = FETCH_all("SELECT * FROM `homebanners` WHERE `hbid` = ?", '1');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $t; ?></title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="<?php echo $kfd; ?>" />
        <meta name="description" content="<?php echo $das; ?>" />
        <meta property="og:title" content="<?php echo $t; ?>" />
        <meta property="og:description" content="<?php echo $das; ?>" />
        <meta property="og:url" content="<?php echo $actual_link; ?>" />
        <link rel="canonical" href="<?php echo $actual_link; ?>" />
        <meta property="og:image" content="<?php echo $file; ?>" />
        <meta property="og:image:width" content="<?php echo $width; ?>" />
        <meta property="og:image:height" content="<?php echo $height; ?>" />
        <meta property="og:image:type" content="<?php //echo getImagetype($file);        ?>" />
        <meta property="twitter:image" content="<?php echo $file; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php echo stripslashes($general['og_tag']); ?>

        <?php echo stripslashes($general['beforehead']); ?>
        <!-- Style CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo $fsitename; ?>css/style.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBt6oed2MygyllnIceHSIOziCnPWOyTkTk" async defer></script>
        <script>
            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    alert("Geolocation is not supported by this browser.");
                }
            }
            function showPosition(position) {
                var geocoder = new google.maps.Geocoder();
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var city = '';
                var state = '';
                var country = '';
                var postal_code = '';
                var latlng = new google.maps.LatLng(lat, lng);
                geocoder.geocode({'location': latlng}, function (results) {
                    var comp = results[0].address_components;
                    $.each(comp, function (i, val) {
                        if (val.types[0] == 'administrative_area_level_2') {
                            city = val.long_name;
                        }
                        if (val.types[0] == 'administrative_area_level_1') {
                            state = val.long_name;
                        }
                        if (val.types[0] == 'country') {
                            country = val.long_name;
                        }
                        if (val.types[0] == 'postal_code') {
                            postal_code = val.long_name;
                        }
                    });
                    // assignWidthToPHP
                    var xhttp = new XMLHttpRequest();
                    xhttp.open("GET", "http://localhost/click2buy-newhtml/getcountry.php?country=" + country, false);
                    xhttp.send();

                    // alert("Latitude: " + lat + "\n" + "Longitude: " + lng + "\n" + "City: " + city + "\n" + "State: " + state + "\n" + "Country: " + country + "\n" + "Postal code: " + postal_code);

                    $(".box-group").prepend('<input type="hidden" name="Latitude" id="" value="' + lat + '"/>' +
                            '<input type="hidden" name="lng" id="lng" value="' + lng + '"/>' +
                            '<input type="hidden" name="city" id="city" value="' + city + '"/>' +
                            '<input type="hidden" name="state" id="state" value="' + state + '"/>' +
                            '<input type="hidden" name="country" id="country" value="' + country + '"/>' +
                            '<input type="hidden" name="postal_code" id="postal_code" value="' + postal_code + '"/>');
                });



            }
            window.onload = getLocation;
        </script>

        <style>
            .block-service .item .icon {
                vertical-align: middle;
                color: #ffff;
                font-size: xx-large;
                width: 84px;
                text-align: center;
                line-height: 80px;
                height: 84px;
                background-color: #0091fc;
                border-radius: 100%;
                display: block;
                position: absolute;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="cms-index-index index-opt-5">

        <div class="wrapper">
            <!-- alert banner top -->
            <div role="alert" class="qc-top-site qc-top-site10 alert  fade in" style="background-image: url(<?php echo $fsitename; ?>images/homebanners/<?php echo $advertisement['image1']; ?>"> 
                <div class="container">
                    <button  class="close" type="button"><span aria-hidden="true">×</span></button> 
                    <div class="description">
                        <span class="title"><?php echo stripslashes($advertisement['title1']); ?></span>
                        <?php if($advertisement['content1'] != '') { ?>
                        <?php echo stripslashes($advertisement['content1']); ?>
                        <?php } ?>
                        <?php if($advertisement['link1'] != '') { ?>
                        <a href="<?php echo $advertisement['link1']; ?>" target="_blank" class="btn">shop now</a>
                        <?php } ?>
                    </div>
                </div>
            </div><!-- alert banner top -->
            <!-- HEADER -->
            <header class="site-header header-opt-5">

                <!-- header-top -->
                <div class="header-top">
                    <div class="container">

                        <!-- hotline -->
                        <ul class="hotline nav-left" >
                            <li ><span><i class="fa fa-phone" aria-hidden="true"></i>+ <?php
                                    if($manageprofile['phonenumber'] != '') { echo $manageprofile['phonenumber'];
                                    }
                                    ?></span></li>
                            <li ><span><i class="fa fa-envelope" aria-hidden="true"></i> Contact us today !</span></li>
                        </ul><!-- hotline -->

                        <!-- heder links -->
<?php if (!isset($_SESSION['FUID']) && $_SESSION['FUID'] == '') { ?>

                        <ul class="links nav-right"><li ><a href="<?php echo $fsitename; ?>pages/login.htm">Login/Register</a></li>
                            <li><a href="<?php echo $fsitename; ?>pages/contact-us.htm" >Support</a></li>
                        </ul>                           
<?php } else { ?>

                        <ul class="links nav-right"> 
                            <li><a href="<?php echo $fsitename; ?>pages/myaccount.htm"><b>Welcome <?php echo getcustomer('FirstName', $_SESSION['FUID']); ?></b></a></li>

                            <li class="dropdown setting">
                                <a data-toggle="dropdown" role="button" href="#" class="dropdown-toggle "><span>Settings</span> <i aria-hidden="true" class="fa fa-angle-down"></i></a>
                                <div class="dropdown-menu ">

                                    <ul class="account">
                                        <li><a href="<?php echo $fsitename; ?>pages/mywishlist.htm">My Wishlist</a></li>
                                        <li><a href="<?php echo $fsitename; ?>pages/myaccount.htm">My Account</a></li>
                                        <li><a href="<?php echo $fsitename; ?>pages/checkout.htm">Checkout</a></li>
                                        <li><a href="<?php echo $fsitename; ?>pages/logout.htm">Logout</a></li>
                                    </ul>

                                </div>
                            </li>
                            <li><a href="<?php echo $fsitename; ?>pages/contact-us.htm" >Support</a></li>
                        </ul>
<?php } ?>

                    </div>
                </div> <!-- header-top -->

                <!-- header-content -->
                <div class="header-content">
                    <div class="container">

                        <div class="row">

                            <div class="col-md-3 nav-left">
                                <!-- logo -->
                                <strong class="logo">
                                    <a href="<?php echo $fsitename; ?>"><img src="<?php echo $fsitename; ?>admin/pages/profile/image/<?php echo $manageprofile['image']; ?>" alt="<?php echo $manageprofile['Company_name']; ?>"></a>
                                </strong><!-- logo -->
                            </div>

                            <div class="col-md-6 nav-mind">

                                <!-- block search -->
                                <div class="block-search">
                                    <div class="block-title">
                                        <span>Search</span>
                                    </div>
                                    <div class="block-content">
                                        <form action="<?php echo $fsitename; ?>listings.htm" method="get"> <div class="categori-search  ">
                                                <select data-placeholder="All Categories" class="categori-search-option" name="srchcategory" id="srchcategory">
                                                    <option value="">All Categories</option>
                                                    <?php
                                                    $b = '1';
                                                    $catlist = $db->prepare("SELECT * FROM `category` WHERE `status`= ? ORDER BY `order` ASC");
                                                    $catlist->execute(array('1'));
                                                    while ($fcat = $catlist->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>
                                                    <option value="<?php echo $fcat['cid']; ?>" <?php if($_REQUEST['srchcategory'] == $fcat['cid']) { ?> selected="selected" <?php } ?>><?php echo stripslashes($fcat['category']); ?></option>
<?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-search">

                                                <div class="box-group">
                                                    <input type="text" class="form-control" placeholder="Search here..." name="keyword" id="keyword" value="<?php echo $_REQUEST['keyword']; ?>" required>
                                                    <button class="btn btn-search" type="submit" title="Search"><span>search</span></button>
                                                </div>

                                            </div></form>
                                    </div>
                                </div><!-- block search -->

                            </div>

                            <div class="col-md-3 nav-right">

                                <!-- block mini cart -->
                                <div class="block-minicart dropdown">
                                    <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <span class="cart-icon"></span>
                                        <span class="cart-text">cart</span>
                                        <span class="counter qty">
                                            <!--<span class="counter-numbercounter-number">6</span>
                                            --><span class="counter-label">6 <span>Item(s)</span></span>
                                            <span class="counter-price">$75.00</span>
                                        </span>
                                    </a>
                                    <div class="dropdown-menu" id="quick_cart">

                                        <?php
                                        $carts = new Cart();
                                        echo $carts->Quick_Cart();
                                        ?>
                                    </div>

                                </div><!-- block mini cart -->

                                <!-- link  wishlish-->
                                <input type="hidden" name="sess_userid" id="sess_userid" value="<?php echo $_SESSION['FUID']; ?>">
                                <a href="<?php echo $fsitename; ?>pages/mywishlist.htm" class="link-wishlist"><span>wishlish</span></a>
                                <!-- link  wishlish-->

                                <!-- link  wishlish-->
                                <!--<a href="" class="link-compare"><span>compare</span></a>-->
                                <!-- link  wishlish-->

                            </div>

                        </div>                    

                    </div>
                </div><!-- header-content -->
                <div class="  header-nav mid-header" <?php if($getstatic == '1') { ?>style="background-color:#0091fc;" <?php } ?>>
                    <div class="container">
                        <div class="box-header-nav">

                            <span data-action="toggle-nav-cat" class="nav-toggle-menu nav-toggle-cat"><span>Categories</span><i aria-hidden="true" class="fa fa-bars"></i></span>

                            <div class="block-nav-categori">
                                <div class="block-title">
                                    <span>Categories</span>
                                </div>
                                <div class="block-content">
                                    <ul class="ui-categori">
                                        <?php
                                        $b = '1';
                                        $catlist = $db->prepare("SELECT * FROM `category` WHERE `status`= ? ORDER BY `order` ASC");
                                        $catlist->execute(array('1'));
                                        while ($fcat = $catlist->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <li class="parent">
                                            <a href="<?php echo $fsitename . str_replace(' ', '_', $fcat['link']) . '.htm'; ?>">
                                                <span class="icon"><i class="<?php echo stripslashes($fcat['icon']); ?>" aria-hidden="true"></i></span>
                                            <?php echo stripslashes($fcat['category']); ?>
                                            </a>
                                            <span class="toggle-submenu"></span>
                                            <?php
                                            $a = '1';
                                            $subcatlist = $db->prepare("SELECT * FROM `subcategory` WHERE `status`= ? and `cid`=? and `menu`=? ORDER BY `order` ASC");
                                            $subcatlist->execute(array('1', $fcat['cid'], '1'));
                                            $scount = $subcatlist->rowcount();
                                            if($scount != '0')
                                            {
                                            ?>
                                            <div class="submenu">
                                                <ul class="categori-list clearfix">
                                                    <?php
                                                    while ($fsubcat = $subcatlist->fetch(PDO::FETCH_ASSOC)) {
                                                    ?>



                                                    <li class="col-sm-3">
                                                        <strong class="title"><a href="<?php echo $fsitename . str_replace(' ', '_', $fcat['link']) . '/' . str_replace(' ', '_', $fsubcat['link']) . '.htm'; ?>"><?php echo stripslashes($fsubcat['subcategory']); ?></a></strong>
                                                        <?php
                                                        $innercatlist = $db->prepare("SELECT * FROM `innercategory` WHERE `status`= ? and `cid`=? and `subcategory`=? ORDER BY `order` ASC");
                                                        $innercatlist->execute(array('1', $fcat['cid'], $fsubcat['sid']));
                                                        $icount = $innercatlist->rowcount();
                                                        if($icount != '0')
                                                        {
                                                        ?>	
                                                        <ul>
                                                            <?php
                                                            while ($finner = $innercatlist->fetch(PDO::FETCH_ASSOC)) {
                                                            ?>					
                                                            <li><a href="<?php echo $fsitename . str_replace(' ', '_', $fcat['link']) . '/' . str_replace(' ', '_', $fsubcat['link']) . '/' . str_replace(' ', '_', $finner['link']) . '.htm'; ?>"><?php echo stripslashes($finner['innername']); ?></a></li>
                                                        <?php } ?>                      
                                                        </ul>
<?php } ?>					
                                                    </li>



                                                    <?php
                                                    $a++;
                                                    if ($a == 5) {
                                                    $a = 1;
                                                    echo '</ul><ul class="categori-list clearfix">';
                                                    } }
                                                    ?>  


                                                </ul>
                                            </div><?php } ?> </li>
<?php } ?>  
                                    </ul>

                                    <div class="view-all-categori">
                                        <a class="open-cate btn-view-all">All Categories</a>
                                    </div>
                                </div>

                            </div>

                            <!-- menu -->
                            <div class="block-nav-menu">
                                <ul class="ui-menu">
                                    <li>
                                        <a href="<?php echo $fsitename; ?>">Home   </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $fsitename; ?>pages/about-us.htm">About Us</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $fsitename; ?>listings.htm">Products</a>
                                    </li>
                                    <li><a href="<?php echo $fsitename; ?>blog.htm">Blog</a></li>
                                    <li><a href="<?php echo $fsitename; ?>contact-us.htm">Contact Us</a></li>
                                </ul>
                            </div><!-- menu -->

                            <span data-action="toggle-nav" class="nav-toggle-menu"><span>Menu</span><i aria-hidden="true" class="fa fa-bars"></i></span>

                            <div class="block-minicart dropdown ">
                                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <span class="cart-icon"></span>
                                </a>
                                <div class="dropdown-menu">
                                    <form>
                                        <div  class="minicart-content-wrapper" >
                                            <div class="subtitle">
                                                You have <?php echo $_SESSION['CART_COUNT']; ?> item(s) in your cart
                                            </div>
                                            <div class="minicart-items-wrapper">
                                                <?php
                                                $carts = new Cart();
                                                echo $carts->Quick_Cart();
                                                ?>
                                            </div>
<?php if($_SESSION['CART_COUNT'] != '0') { ?>
                                            <div class="subtotal">
                                                <span class="label">Total</span>
                                                <span class="price">&#8377;&nbsp;<?php echo $_SESSION['CART_TOTAL_FINAL']; ?></span>
                                            </div><?php } ?>
                                            <div class="actions">
                                                <a class="btn btn-viewcart" href="<?php echo $fsitename; ?>pages/cart.htm">
                                                    <span>Shopping Cart</span>
                                                </a>

                                                <a href="<?php echo $fsitename; ?>pages/checkout.htm">
                                                    <button class="btn btn-checkout" type="button" title="Check Out">
                                                        <span>Check out</span>
                                                    </button>
                                                </a>

                                            </div><?php } ?>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="block-search">
                                <div class="block-title">
                                    <span>Search</span>
                                </div>
                                <div class="block-content">
                                    <div class="form-search">
                                        <form action="<?php echo $fsitename; ?>listings.htm" method="post">
                                            <div class="box-group">
                                                <input type="text" class="form-control" placeholder="Search here..." name="keyword" id="keyword" value="<?php echo $_REQUEST['keyword']; ?>">
                                                <button class="btn btn-search" type="submit" title="Search"><span>search</span></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
<?php if (isset($_SESSION['FUID'])) {
    ?>							  
                                <div class="dropdown setting">

                                    <a data-toggle="dropdown" role="button" href="<?php echo $fsitename; ?>pages/myaccount.htm" class="dropdown-toggle "><span>Settings</span> <i aria-hidden="true" class="fa fa-user"></i></a>
                                    <div class="dropdown-menu  ">
                                        <!--<div class="switcher  switcher-language">
                                            <strong class="title">Select language</strong>
                                            <ul class="switcher-options ">
                                                <li class="switcher-option">
                                                    <a href="#">
                                                        <img class="switcher-flag" alt="flag" src="<?php //$fsitename;    ?>images/flags/flag_french.png">
                                                    </a>
                                                </li>
                                                <li class="switcher-option">
                                                    <a href="#">
                                                        <img class="switcher-flag" alt="flag" src="<?php //$fsitename;    ?>images/flags/flag_germany.png">
                                                    </a>
                                                </li>
                                                <li class="switcher-option">
                                                    <a href="#">
                                                        <img class="switcher-flag" alt="flag" src="<?php //$fsitename;    ?>images/flags/flag_english.png">
                                                    </a>
                                                </li>
                                                <li class="switcher-option switcher-active">
                                                    <a href="#">
                                                        <img class="switcher-flag" alt="flag" src="<?php //$fsitename;    ?>images/flags/flag_spain.png">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                                                        <div class="switcher  switcher-currency">
                                            <strong class="title">SELECT CURRENCIES</strong>
                                            <ul class="switcher-options ">
                                                <li class="switcher-option">
                                                    <a href="#">
                                                        <i class="fa fa-usd" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li class="switcher-option switcher-active">
                                                    <a href="#">
                                                        <i class="fa fa-eur" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li class="switcher-option">
                                                    <a href="#">
                                                        <i class="fa fa-gbp" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                               
                                            </ul>
                                        </div>-->
                                        <ul class="account">
                                            <?php if ($_SESSION['FUID'] != '') { ?>
                                                <li><a href="<?php echo $fsitename; ?>wishlist.htm">Wishlist</a></li>
    <?php } else { ?>
                                                <li><a href="<?php echo $fsitename; ?>login.htm">Wishlist</a></li>
    <?php } ?>
                                            <li><a href="<?php echo $fsitename; ?>pages/myaccount.htm">My Account</a></li>
                                            <li><a href="<?php echo $fsitename; ?>pages/checkout.htm">Checkout</a></li>
                                            <li><a href="<?php echo $fsitename; ?>pages/logout.htm">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
<?php } ?>
                        </div>
                    </div>
                </div>

            </header><!-- end HEADER -->     