<style type="text/css">

        .theme-background-color {
    background-color: #100bb4!important;
}
.cd-user-modal-container {
    position: relative;
    width: 90%;
    max-width: 600px;
    background: #121212;
    
}
footer .tt-footer-custom .tt-box-copyright {
    font-weight: 400;
    padding: 0;
    color: #000;
    }
.copydiv ul{
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: flex-end;
    align-content: center;
    align-items: center;
}
footer .tt-color-scheme-01 .tt-collapse-content {
    color: #fff;
}
footer .tt-color-scheme-01 .tt-collapse-title, footer .tt-color-scheme-01 .tt-collapse-title a {
    color: #fff;
}
footer .tt-color-scheme-01 address span {
    color: #fff;
    font-weight: bold;
}
footer .tt-color-scheme-01 {
    background: #222222;
    color: #fff;
}
.cd-form label {
color:#fff;
}

.cd-user-modal-container .cd-switcher a.selected {
    background: #121212;
    color: #ffffff;
}
footer .tt-color-scheme-01 .tt-list li a, footer .tt-color-scheme-01 .tt-mobile-collapse .tt-collapse-content .tt-list li a {
    color: #fff;
}
.cd-user-modal-container .cd-switcher a {
    display: block;
    width: 100%;
    height: 50px;
    line-height: 50px;
    background: #d2d8d8;
    color: #010101;
}
.letter-box input{
    border-radius: 5px;
    border: 0;
    padding: 6px 8px;
    margin-top: 10px;
}
.logo-image {
    text-align: center;
    padding-bottom: 15px;
}
footer .logo-image .tt-logo img {
    max-height: 63px;
    display: inline-block;
}
.letter-box .btn {
    margin-top: 10px;
    width: 100%;
}
.letter-box form{
    background: none;
}
#loader .dot {
    -webkit-animation: 3s ease-in-out infinite loader;
    animation: 3s ease-in-out infinite loader;
    height: 15px;
    width: 15px;
    border-radius: 50%;
    position: absolute;
    background: #111111!important;
}
.cd-form input[type=submit] {
    background: #d2d8d8b5;
    color: #fff;
}
.cd-form input.full-width {
    color: #000;
    font-size: 16px;
    font-weight: bolder;
}
.no-touch .cd-form input[type=submit]:hover, .no-touch .cd-form input[type=submit]:focus {
    background: #ffffff;
    outline: none;
    color: black;
    border: 1px solid grey;
    box-shadow: 0px 0px 5px #ffffff;
}
.copydiv img{
    max-width: 68px;
}
.copydiv ul{
    margin: 0;
    text-align: right;
}
footer .tt-footer-custom .tt-logo-col+.tt-col-item .tt-box-copyright {
    padding: 0;
    }
   /*=============*/
   
footer .tt-color-scheme-01 .tt-list li a:hover, footer .tt-color-scheme-01 .tt-mobile-collapse .tt-collapse-content .tt-list li a:hover{
    text-decoration: none;
}
    form {
        /*background-image: url(images/popup/overlay.png);*/
          background-image: url(https://i.ytimg.com/vi/rBhnxN74edw/maxresdefault.jpg);
        background-position: center;
        background-repeat: no-repeat;
        background-size: inherit;
        width: 100%;
        height: 100%;
    }
    fieldset.scheduler-border {
        border: 1px groove #DDD !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow: 0px 0px 0px 0px #000;
        box-shadow: 0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        width:inherit; / Or auto /
        padding:0 10px; / To give a bit of padding on the left and right /
        border-bottom:none;
    }
    .transbox {
      /*margin: 30px;*/
      background-color: #ffffff;
      border: 1px solid black;
      opacity: 0.6;
    }

    .transbox p {
      margin: 5%;
      font-weight: bold;
      color: #000000;
    }
    .suq
    {
        font-size: 13px;
        color: #fff;
        text-transform: uppercase;
        /*border-bottom: 1px solid #ff5e5ef0;*/
    }
</style> 
 <footer id="tt-footer">
     <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<ul class="cd-switcher">
				<li><a href="#0">Sign in</a></li>
				<li><a href="#0">New account</a></li>
			</ul>

			<div id="cd-login"> <!-- log in form -->
			<p id="logresponse"></p>
				<form class="cd-form" method="post" id="popuplogin">
					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input class="full-width has-padding has-border" id="loginemail" name="loginemail" type="email" required="required" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Password</label>
						<input class="full-width has-padding has-border" id="logpassword" name="logpassword" type="text"  required="required" placeholder="Password">
						<a href="#0" class="hide-password">Hide</a>
						<span class="cd-error-message">Error message here!</span>
					</p>

					<!--<p class="fieldset">-->
					<!--	<input type="checkbox" id="remember-me" checked>-->
					<!--	<label for="remember-me">Remember me</label>-->
					<!--</p>-->

					<p class="fieldset">
						<input class="full-width" type="submit" name="loginsubmit" id="loginsubmit" value="Login">
					</p>
                                    <p class="cd-form-bottom-message forgot-text"><a href="#0">Forgot your password?</a></p>
				</form>
				
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->

			<div id="cd-signup"> <!-- sign up form -->
				<form class="cd-form" method="post" id="popupregister">
				    <p id="regresponse"></p>
					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-username">Username</label>
						<input required="required" class="full-width has-padding has-border" id="signup-username" name="fname" type="text" placeholder="Username">
					
					
					</p>

					<p class="fieldset">
						<label class="image-replace cd-email" for="signup-email">E-mail</label>
						<input required="required" class="full-width has-padding has-border" id="signup-email" name="emailid" type="email" placeholder="E-mail">
						
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Password</label>
						<input required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
  title="Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters" class="full-width has-padding has-border" id="signup-password" name="password" type="text"  placeholder="Password">
 
						<a href="#0" class="hide-password">Hide</a>
						 
					</p>
<span class="suq">Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more characters</span>
					<p class="fieldset">
						<input type="checkbox" id="accept-terms">
						<label for="accept-terms">I agree to the <a href="<?php echo $fsitename; ?>pages/terms.htm" target="_blank">Terms</a></label>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" name="create" value="Create account">
					</p>
				</form>

				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-signup -->

			<div id="cd-reset-password"> <!-- reset password form -->
				<p class="cd-form-message">Lost your password? Please enter your email address. You will receive a password to login.</p>

				<form class="cd-form" method="post" id="popupforgot">
				    <p id="forgotresponse"></p>
					<p class="fieldset">
						<label class="image-replace cd-email" for="reset-email">E-mail</label>
						<input class="full-width has-padding has-border" id="reset-email" name="forgotemail" type="email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" name="forgotsubmit" value="Reset password">
					</p>
				</form>

				<p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
			</div> <!-- cd-reset-password -->
			<a href="#0" class="cd-close-form">Close</a>
		</div> <!-- cd-user-modal-container -->
	</div> 
            
            <div class="tt-footer-col tt-color-scheme-01">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-2 col-xl-2">
                            <div class="tt-logo-container logo-image">
                     <!-- mobile logo -->                            <a class="tt-logo tt-logo-alignment" href="<?php echo $fsitename; ?>"><img src="<?php echo $fsitename; ?>images/logo/logo1.png" alt="" /></a>                            <!-- /mobile logo -->                        
                  </div>
                        <ul class="tt-social-icon">
                                <li><a class="icon-g-64" target="_blank" href="<?php echo getmedia('link',1); ?>" title="<?php echo getmedia('sname',1); ?>" alt="<?php echo getmedia('sname',1); ?>"></a></li>
                                <li><a class="icon-h-58" target="_blank" href="<?php echo getmedia('link',2); ?>" title="<?php echo getmedia('sname',2); ?>" alt="<?php echo getmedia('sname',2); ?>"></a></li>
                                <li><a class="icon-g-66" target="_blank" href="<?php echo getmedia('link',3); ?>" title="<?php echo getmedia('sname',3); ?>" alt="<?php echo getmedia('sname',3); ?>"></a></li>
                                <li><a class="icon-g-67" target="_blank" href="<?php echo getmedia('link',5); ?>" title="<?php echo getmedia('sname',5); ?>" alt="<?php echo getmedia('sname',5); ?>"></a></li>
                                <li><a class="icon-g-70" target="_blank" href="<?php echo getmedia('link',4); ?>" title="<?php echo getmedia('sname',4); ?>" alt="<?php echo getmedia('sname',4); ?>"></a></li>
                            </ul>
                        </div>
                         <div <?php if(isset($_SESSION['FUID'])) { ?> class="col-md-6 col-lg-2 col-xl-3" <?php } else { ?> class="col-md-6 col-lg-2 col-xl-3" <?php } ?>>
                            <div class="tt-newsletter">
                                <div class="tt-mobile-collapse">
                                    <h4 class="tt-collapse-title">CONTACT US</h4>
                                    <div class="tt-collapse-content">
                                        <address>
                                            <p><?php echo getprofile('address',1); ?></p>
                                            <p><span>+<?php echo getprofile('phonenumber',1); ?></span></p>
                                            <p>Info@gmail.com</p>
                                            <!-- <p><span>E-mail:</span> <a href="mailto:<?php //echo getprofile('recoveryemail',1); ?>"><?php //echo getprofile('recoveryemail',1); ?></a></p> -->
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-2 col-xl-2">
                            <div class="tt-mobile-collapse">
                                <h4 class="tt-collapse-title">INFORMATION</h4>
                                <div class="tt-collapse-content">
                                    <ul class="tt-list">
                                        <li><a href="">About Us</a></li>
                                        <li><a href="">Blog</a></li>
                                        <li><a href="">Men's Fashion</a></li>
                                        <li><a href="">Women's Fashion</a></li>
                                        <li><a href="">Kids Fashion</a></li>
                                        <li><a href="">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-6 col-lg-2 col-xl-2">
                            <div class="tt-mobile-collapse">
                                <h4 class="tt-collapse-title">MY ACCOUNT</h4>
                                <div class="tt-collapse-content">
                                    <ul class="tt-list">
                                        <li><a href="">Orders</a></li>
                                        <li><a href="">Compare</a></li>
                                        <li><a href="">Wishlist</a></li>
                                        <li><a href="">Login</a></li>
                                        <li><a href="">Register</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
<!-- <div <?php if(isset($_SESSION['FUID'])) { ?> class="col-md-6 col-lg-2 col-xl-3" <?php } else { ?> class="col-md-6 col-lg-2 col-xl-4" <?php } ?>>
                            <div class="tt-mobile-collapse">
                                <h4 class="tt-collapse-title">CATEGORIES</h4>
                                <div class="tt-collapse-content">
                                    <ul class="tt-list">
                                        <?php
                                        $catlist = DB("SELECT * FROM `category` WHERE `status`='1' ORDER BY `order` ASC ");
                                         $ccount = mysqli_num_rows($catlist);
                                         while ($catlisting = mysqli_fetch_array($catlist)) {  
                                        ?>
                                        <li><a href="<?php echo $fsitename. getmtype('link',$catlisting['type']) . '/' . $catlisting['link'] . '.htm'; ?>"><?php echo $catlisting['category']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-md-6 col-lg-2 col-xl-2">
                            <div class="tt-mobile-collapse">
                                <h4 class="tt-collapse-title">MY ACCOUNT</h4>
                                <div class="tt-collapse-content">
                                    <ul class="tt-list">
                                        <li><a>Orders</a></li>
                                        <li><a>Compare</a></li>
                                        <li><a>Wishlist</a></li>
                                        <li><a>Login</a></li>
                                        <li><a>Register</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-2 col-xl-3">
                            <div class="tt-mobile-collapse letter-box">
                                <h4 class="tt-collapse-title">NEWS LETTER</h4>
                                <div class="tt-collapse-content">
                                    <p>Get latest company news and offers</p> 
                                    <form>
                                        <input type="email" name="email" placeholder="Email" class="w-100">
                                        <button class="btn btn-primary">Subscribe</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
            <div class="tt-footer-custom">
                <div class="container">
<!--                     <div class="tt-row row copydiv">
 -->                           
                                <!-- copyright -->
                                <!-- <div class="tt-box-copyright col-md-6 col-lg-6 col-xl-6">&copy; <?php echo date('Y'); ?>. All Rights Reserved Terms and Conditions - Privacy and Policy</div>
                                <div class="col-md-6 col-lg-6 col-xl-6 text-right">
                                <ul>
                                    <li><img src="images/logo/Stripe_logo.png"></li>
                                    <li><img src="images/logo/paypal.png"></li>
                                    <li><img src="images/logo/visa-and-mastercard-logo-26.png"></li>
                                    <li><img src="images/logo/discover.png"></li>
                                    <li><img src="images/logo/express.png"></li>
                                </ul>
                            </div> -->
                                <!-- /copyright -->
                        <!--<div class="tt-col-right">-->
                        <!--    <div class="tt-col-item">-->
                                <!-- payment-list -->
                        <!--        <ul class="tt-payment-list">-->
                        <!--            <li>-->
                        <!--                <a href="page404.html">-->
                        <!--                    <span class="icon-Stripe">-->
                        <!--                        <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span>-->
                        <!--                        <span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span>-->
                        <!--                    </span>-->
                        <!--                </a>-->
                        <!--            </li>-->
                        <!--            <li>-->
                        <!--                <a href="page404.html">-->
                        <!--                    <span class="icon-paypal-2">-->
                        <!--                        <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span>-->
                        <!--                    </span>-->
                        <!--                </a>-->
                        <!--            </li>-->
                        <!--            <li>-->
                        <!--                <a href="page404.html">-->
                        <!--                    <span class="icon-visa"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>-->
                        <!--                </a>-->
                        <!--            </li>-->
                        <!--            <li>-->
                        <!--                <a href="page404.html">-->
                        <!--                    <span class="icon-mastercard">-->
                        <!--                        <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span>-->
                        <!--                        <span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span>-->
                        <!--                    </span>-->
                        <!--                </a>-->
                        <!--            </li>-->
                        <!--            <li>-->
                        <!--                <a href="page404.html">-->
                        <!--                    <span class="icon-discover">-->
                        <!--                        <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span>-->
                        <!--                        <span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span>-->
                        <!--                        <span class="path14"></span><span class="path15"></span><span class="path16"></span>-->
                        <!--                    </span>-->
                        <!--                </a>-->
                        <!--            </li>-->
                        <!--            <li>-->
                        <!--                <a href="page404.html">-->
                        <!--                    <span class="icon-american-express">-->
                        <!--                        <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span>-->
                        <!--                        <span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span>-->
                        <!--                    </span>-->
                        <!--                </a>-->
                        <!--            </li>-->
                        <!--        </ul>-->
                                <!-- /payment-list -->
                        <!--    </div>-->
                        <!--</div>-->
<!--                     </div>
 -->  
 <div class="tt-row">
                <div class="tt-col-left">
                    <div class="tt-col-item tt-logo-col">
                        <!-- logo -->
                        <!-- /logo -->
                    </div>
                    <div class="tt-col-item">
                        <!-- copyright -->
                        <div class="tt-box-copyright">
                            &copy; <?php echo date('Y'); ?>. All Rights Reserved Terms and Conditions - Privacy and Policy
                        </div>
                        <!-- /copyright -->
                    </div>
                </div>
                <div class="tt-col-right">
                    <div class="tt-col-item">
                        <!-- payment-list -->
                        <ul class="tt-payment-list">
                            <li><a href="page404.html"><span class="icon-Stripe"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span>
                            </span></a></li>
                            <li><a href="page404.html"> <span class="icon-paypal-2">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-visa">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-mastercard">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-discover">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-american-express">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span>
                            </span></a></li>
                        </ul>
                        <!-- /payment-list -->
                    </div>
                </div>
            </div>              
</div>
            </div>
        </footer>
        <div class="tt-promo-fixed" id="js-tt-promo-fixed">
            <button class="tt-btn-close"></button>
            <?php $lastorder = FETCH_all("SELECT * FROM `order` WHERE `oid`!=? ORDER BY `oid` DESC", 0);
             if(getproduct('image',$lastorder['product_id'])!='')  {
                              $imgpath = $fsitename.'images/product/' . getproduct('imagefolder',$lastorder['product_id']) . '/';   
                              $imgs= explode(',',getproduct('image',$lastorder['product_id']));
                              $imgres=$imgpath.$imgs['0'];
                              }
             if($imgres!='') { 
             ?>
            <div class="tt-img">
                <a href="product.html"><img src="<?php echo $fsitename; ?>images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo getproduct('productname',$lastorder['product_id']); ?>" /></a>
            </div>
            <?php } ?>
            <div class="tt-description">
                <div class="tt-box-top">
                    <p>Someone purchased a</p>
                    <p><a href="<?php echo $fsitename; ?>view/<?php echo getproduct('link',$lastorder['product_id']); ?>.htm"><?php echo $lastorder['product_name']; ?></a></p>
                </div>
                <!--<div class="tt-info">14 Minutes ago from New York, USA</div>-->
            </div>
        </div>
        <!-- modal (AddToCartProduct) -->
        <div class="modal fade" id="modalAddToCartProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-addtocart mobile">
                            <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                            <a href="#" class="btn-link btn-close-popup">CONTINUE SHOPPING</a> <a href="shopping_cart_02.html" class="btn-link">VIEW CART</a> <a href="page404.html" class="btn-link">PROCEED TO CHECKOUT</a>
                        </div>
                        <div class="tt-modal-addtocart desctope">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                                    <div class="tt-modal-product">
                                        <div class="tt-img"><img src="images/loader.svg" data-src="images/product/product-01.jpg" alt="" /></div>
                                        <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                                        <div class="tt-qty">QTY: <span>1</span></div>
                                    </div>
                                    <div class="tt-product-total">
                                        <div class="tt-total">TOTAL: <span class="tt-price">$324</span></div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <a href="#" class="tt-cart-total">
                                        There are 1 items in your cart
                                        <div class="tt-total">TOTAL: <span class="tt-price">$324</span></div>
                                    </a>
                                    <a href="#" class="btn btn-border btn-close-popup">CONTINUE SHOPPING</a> <a href="shopping_cart_02.html" class="btn btn-border">VIEW CART</a> <a href="#" class="btn">PROCEED TO CHECKOUT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal (quickViewModal) -->
        <div class="modal fade" id="ModalquickView" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-6">
                                    <div class="tt-mobile-product-slider arrow-location-center">
                                        <div><img src="#" data-lazy="images/product/product-01.jpg" alt="" /></div>
                                        <div><img src="#" data-lazy="images/product/product-01-02.jpg" alt="" /></div>
                                        <div><img src="#" data-lazy="images/product/product-01-03.jpg" alt="" /></div>
                                        <div><img src="#" data-lazy="images/product/product-01-04.jpg" alt="" /></div>
                                        <!--
								//video insertion template
								<div>
									<div class="tt-video-block">
										<a href="#" class="link-video"></a>
										<video class="movie" src="video/video.mp4" poster="video/video_img.jpg"></video>
									</div>
								</div> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-6">
                                    <div class="tt-product-single-info">
                                        <div class="tt-add-info">
                                            <ul>
                                                <li><span>SKU:</span> 001</li>
                                                <li><span>Availability:</span> 40 in Stock</li>
                                            </ul>
                                        </div>
                                        <h2 class="tt-title">Cotton Blend Fleece Hoodie</h2>
                                        <div class="tt-price"><span class="new-price">$29</span> <span class="old-price"></span></div>
                                        <div class="tt-review">
                                            <div class="tt-rating"><i class="icon-star"></i> <i class="icon-star"></i> <i class="icon-star"></i> <i class="icon-star-half"></i> <i class="icon-star-empty"></i></div>
                                            <a href="#">(1 Customer Review)</a>
                                        </div>
                                        <div class="tt-wrapper">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</div>
                                        <div class="tt-swatches-container">
                                            <div class="tt-wrapper">
                                                <div class="tt-title-options">SIZE</div>
                                                <form class="form-default">
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>21</option>
                                                            <option>25</option>
                                                            <option>36</option>
                                                        </select>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tt-wrapper">
                                                <div class="tt-title-options">COLOR</div>
                                                <form class="form-default">
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option>Red</option>
                                                            <option>Green</option>
                                                            <option>Brown</option>
                                                        </select>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tt-wrapper">
                                                <div class="tt-title-options">TEXTURE:</div>
                                                <ul class="tt-options-swatch options-large">
                                                    <li>
                                                        <a class="options-color" href="#">
                                                            <span class="swatch-img"><img src="images/loader.svg" data-src="images/custom/texture-img-01.jpg" alt="" /> </span><span class="swatch-label color-black"></span>
                                                        </a>
                                                    </li>
                                                    <li class="active">
                                                        <a class="options-color" href="#">
                                                            <span class="swatch-img"><img src="images/loader.svg" data-src="images/custom/texture-img-02.jpg" alt="" /> </span><span class="swatch-label color-black"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="options-color" href="#">
                                                            <span class="swatch-img"><img src="images/loader.svg" data-src="images/custom/texture-img-03.jpg" alt="" /> </span><span class="swatch-label color-black"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="options-color" href="#">
                                                            <span class="swatch-img"><img src="images/loader.svg" data-src="images/custom/texture-img-04.jpg" alt="" /> </span><span class="swatch-label color-black"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="options-color" href="#">
                                                            <span class="swatch-img"><img src="images/loader.svg" data-src="images/custom/texture-img-05.jpg" alt="" /> </span><span class="swatch-label color-black"></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tt-wrapper">
                                            <div class="tt-row-custom-01">
                                                <div class="col-item">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn"></span> <input type="text" value="1" size="5" /> <span class="plus-btn"></span></div>
                                                </div>
                                                <div class="col-item">
                                                    <a href="#" class="btn btn-lg"><i class="icon-f-39"></i>ADD TO CART</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- modalVideoProduct -->
        <div class="modal fade" id="modalVideoProduct" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-video">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body"><div class="modal-video-content"></div></div>
                </div>
            </div>
        </div>
        <!-- modal (ModalSubsribeGood) -->
        <div class="modal fade" id="ModalSubsribeGood" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-subsribe-good"><i class="icon-f-68"></i> You have successfully signed!</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal (newsletter) --><!-- <div class="modal  fade"  id="Modalnewsletter" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true"  data-pause=2000>
	<div class="modal-dialog modal-sm">
		<div class="modal-content ">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
			</div>
			<form>
				<div class="modal-body no-background">
					<div class="tt-modal-newsletter">
						<div class="tt-modal-newsletter-promo">
							<div class="tt-title-small">BE THE FIRST<br> TO KNOW ABOUT</div>
							<div class="tt-title-large">WOKIEE</div>
							<p>
								HTML FASHION DROPSHIPPING THEME
							</p>
						</div>
						<p>
							By subscribe, you accept the terms &amp; privacy policy<br>
						</p>
						<div class="subscribe-form form-default">
							<div class="row-subscibe">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Enter your e-mail">
									<button type="submit" class="btn">JOIN US</button>
								</div>
							</div>
							<div class="checkbox-group">
								<input type="checkbox" id="checkBox1">
								<label for="checkBox1">
									<span class="check"></span>
									<span class="box"></span>
									Don�t Show This Popup Again
								</label>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div> -->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5fd1d73ca1d54c18d8f23da8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
        <script src="<?php echo $fsitename; ?>ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
   <script>
     $(document).ready(function(){
$('#popupregister').submit(function(){
// show that something is loading
$('#regresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
    $('#regresponse').html(data);
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});



$('#popuplogin').submit(function(){
//alert('tetst');
// show that something is loading
$('#logresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
      // alert(data);
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
    $('#logresponse').html(data);
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});
$('#popupforgot').submit(function(){
//alert('tetst');
// show that something is loading
$('#forgotresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
      // alert(data);
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
 	
		//login_selected();
    $('#forgotresponse').html(data);
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});

});  
   </script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script>
  	jQuery(document).ready(function($){
	var $form_modal = $('.cd-user-modal'),
		$form_login = $form_modal.find('#cd-login'),
		$form_signup = $form_modal.find('#cd-signup'),
		$form_forgot_password = $form_modal.find('#cd-reset-password'),
		$form_modal_tab = $('.cd-switcher'),
		$tab_login = $form_modal_tab.children('li').eq(0).children('a'),
		$tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
		$forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
		$back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
		$main_nav = $('.main-nav');
		$hai = $('.haiclick');

	//open modal
	$main_nav.on('click', function(event){

		if( $(event.target).is($main_nav) ) {
			// on mobile open the submenu
			$(this).children('ul').toggleClass('is-visible');
		} else {
			// on mobile close submenu
			$main_nav.children('ul').removeClass('is-visible');
			//show modal layer
			$form_modal.addClass('is-visible');	
			//show the selected form
			( $(event.target).is('.cd-signup') ) ? signup_selected() : login_selected();
		}

	});



    //open modal2
    $hai.on('click', function(event){
        if( $(event.target).is($main_nav) ) {
            // on mobile open the submenu
            $(this).children('ul').toggleClass('is-visible');
        } else {
            // on mobile close submenu
            $main_nav.children('ul').removeClass('is-visible');
            //show modal layer
            $form_modal.addClass('is-visible'); 
            //show the selected form
            ( $(event.target).is('.cd-signup') ) ? signup_selected() : login_selected();
        }

    });
	//close modal
	$('.cd-user-modal').on('click', function(event){
		if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
			$form_modal.removeClass('is-visible');
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$form_modal.removeClass('is-visible');
	    }
    });

	//switch from a tab to another
	$form_modal_tab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
	});

	//hide or show password
	$('.hide-password').on('click', function(){
		var $this= $(this),
			$password_field = $this.prev('input');
		
		( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
		( 'Hide' == $this.text() ) ? $this.text('Show') : $this.text('Hide');
		//focus and move cursor to the end of input field
		$password_field.putCursorAtEnd();
	});

	//show forgot-password form 
	$forgot_password_link.on('click', function(event){
		event.preventDefault();
		forgot_password_selected();
	});

	//back to login from the forgot-password form
	$back_to_login_link.on('click', function(event){
		event.preventDefault();
		login_selected();
	});

	function login_selected(){
		$form_login.addClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.addClass('selected');
		$tab_signup.removeClass('selected');
	}

	function signup_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.addClass('is-selected');
		$form_forgot_password.removeClass('is-selected');
		$tab_login.removeClass('selected');
		$tab_signup.addClass('selected');
	}

	function forgot_password_selected(){
		$form_login.removeClass('is-selected');
		$form_signup.removeClass('is-selected');
		$form_forgot_password.addClass('is-selected');
	}

	//REMOVE THIS - it's just to show error messages 
// 	$form_login.find('input[type="submit"]').on('click', function(event){
// 		event.preventDefault();
// 	//	$form_login.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
// 	});
// 	$form_signup.find('input[type="submit"]').on('click', function(event){
// 		event.preventDefault();
// 		//$form_signup.find('input[type="email"]').toggleClass('has-error').next('span').toggleClass('is-visible');
// 	});


	//IE9 placeholder fallback
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}

});


//credits https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	// If this function exists...
    	if (this.setSelectionRange) {
      		// ... then use it (Doesn't work in IE)
      		// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      		var len = $(this).val().length * 2;
      		this.setSelectionRange(len, len);
    	} else {
    		// ... otherwise replace the contents with itself
    		// (Doesn't work in Google Chrome)
      		$(this).val($(this).val());
    	}
	});
};
  </script>
        <script>
            window.jQuery || document.write('<script src="<?php echo $fsitename; ?>external/jquery/jquery.min.js"></script>
    
        </script>

        <script>
          $(".closebtn").onclick(function(){
            // alert('test');
       $(".closebtn").css("display", "block");
       });    
        </script>
        <script defer="defer" src="<?php echo $fsitename; ?>js/bundle.js"></script>
        <script src="<?php echo $fsitename; ?>js/cart.js"></script>
        <script src="<?php echo $fsitename; ?>js/toastr.js"></script>
        <link href="<?php echo $fsitename; ?>css/toastr.css" rel="stylesheet"/>
   </div>
    </body>
 
</html>
