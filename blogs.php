<?php
$page = '24';
$selid = '4';
include "require/header.php";
include "require/paging1.php";
$alink = strtok($actual_link, '?');
$alink = $alink . '?filter';
?>
<style>
  .tt-listing-post:not(.tt-half) .tt-post .tt-post-img + .tt-post-content {
    background: none;
    padding: 50px 0 0;
     border-radius: 0px;
    box-shadow: none;

}
.tt-listing-post:not(.tt-half) .tt-post .tt-post-img {
    flex: 0 0 calc(45% - 39px);
    max-width: calc(59% - 39px);
    width: calc(45% - 39px);
    margin-right: 39px;
}
.tt-listing-post .tt-post .tt-post-content .tt-title a {
    font-size: 17px;
    color: #191919;
    display: inline-block;
    line-height: 27px;
    text-transform: uppercase;
    font-weight: 800;
}
.tt-listing-post .tt-post .tt-post-content .tt-description {
    margin-top: 0px;
}
body .tt-listing-post .tt-post .tt-post-content .tt-description p {
    color: gray;
    margin-top: 10px;
}
body .tt-listing-post .tt-post .tt-post-content .tt-btn {
    margin-top: 15px;
}
.tt-title-subpages:not(.text-left):not(.text-right) {
    text-align: left;
    font-weight: bold;
    font-size: 23px;
    padding-bottom: 30px;
}
.tt-listing-post .tt-post .tt-post-img img {
    border-radius: 34px;
}
.tt-listing-post .tt-post .tt-post-content .tt-btn  .btn:not(:disabled):not(.disabled) {
    cursor: pointer;
    background: transparent;
    color: #2879fe;
    padding: 0;
    text-decoration: underline;
    font-weight: 600;
}
  @media (min-width: 1025px){


    .rightColumn {
    padding: 0 39px;
    border-radius: 50px;

}


.btn:not(:disabled):not(.disabled) {
    cursor: pointer;
    background: #2879fe;
    color: #fff;
}
.tt-listing-post .tt-post .tt-post-content .tt-description p{
color: #fff;
}
.tt-aside-post .item .tt-title {
    color: #ffffff;
    }
    .tt-aside-post .item p {
    color: #fff;
}
.tt-list-row li a {
    color: #0000ff;
}
.tt-aside-post .item .tt-tag a {
    color: #0000ff;
    text-shadow: 0px 0px 1px #0000ff;
}
}
.input-group>.form-control:not(:last-child) {
    border: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    background: #f7f6f6;
    box-shadow: none;
}
.input-group .btn:not(:disabled):not(.disabled) {
    padding: 5px 10px;
    cursor: pointer;
    background: #f7f6f6;
    color: #000;
}
body .tt-block-aside .tt-aside-title{
  border-bottom: 0 !important;
  padding-bottom: 0;
}
.input-group .btn:not(.tt-icon-right) [class^=icon-] {
    font-size: 20px;
    margin-right: 0px;
}
.tt-aside-post .item .tt-tag a {
    font-size: 15px;
    color: #2879fe;
    text-shadow: none;
    font-weight: 500;
}
.tt-aside-post .item .tt-title {
    color: #191919;
}
.tt-aside-post .item p {
    color: gray;
}
.tt-block-aside:not(:first-child) {
    margin-top: 30px;
}
.tt-list-row li a {
    border-bottom: 1px solid #ddd;
    color: #777;
    padding-bottom: 10px;
    padding-top: 10px;
}
.tt-list-row li:last-child a {
    padding-bottom: 0;
    border: 0;
}

  

</style>

<div class="tt-breadcrumb">
         <div class="container">
            <ul>
               <li><a href="<?php echo $fsitename; ?>">Home</a></li>
               <li>Blog</li>
            </ul>
         </div>
      </div>
      <div id="tt-pageContent">
         <div class="container-indent">
            <div class="container container-fluid-custom-mobile-padding">
               <h1 class="tt-title-subpages noborder">Blog</h1>
               <div class="row">
                  <div class="col-sm-12 col-md-8 col-lg-9">
            <?php
            $s = '';
                        if ($_REQUEST['bid'] != '' && $_REQUEST['bid'] != 'pages') {
                            $bcategory = FETCH_all("SELECT * FROM `blogcategory` WHERE `link` = ?", $_REQUEST['bid']);
                            $bcid = $bcategory['bcid'];
                            $s = " AND FIND_IN_SET($bcid,`category`)";
                        }
                        $sql = "SELECT * FROM `blog` WHERE `status`=?" . $s;
                        $paging = new paging($sql, '5');
                        $sqlt = $paging->sql;
                        $spro = $db->prepare($sqlt);
                        $spro->execute(array('1'));
                        $bcount = $spro->rowcount();

                        if ($bcount != '0') { ?>
                     <div class="tt-listing-post">
                          <?php  while ($bloglist = $spro->fetch()) {
                            $bimage=$fsitename.'images/blog/'.$bloglist['image'];
                            ?>
                        <div class="tt-post">
                           <div class="tt-post-img full-hgt-img"><a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><img src="images/loader.svg" data-src="<?php echo $bimage; ?>" alt="<?php echo stripslashes($fblogs['title']); ?>"></a></div>
                           <div class="tt-post-content">
                              <div class="tt-tag">
                               <?php
                               $expcat=explode(',',$bloglist['category']);
                               foreach($expcat as $expcat1)
                               {
                               $links[]='<a href="'.$fsitename . getblogcategory("link",$expcat1) . '/blogs.htm">'.getblogcategory("category",$expcat1).'</a>';    
                               }
                               $impl=implode(',',array_unique($links));
                               echo $impl;
                               ?>  
                           
                             </div>
                              <h2 class="tt-title"><a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><?php echo stripslashes($bloglist['title']); ?></a></h2>
                              <div class="tt-description"><?php echo stripslashes(substr($bloglist['description'], 0, 120)); ?></div>
                              <div class="tt-btn"><a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm" class="btn">READ MORE</a></div>
                           </div>
                        </div>
                          <?php } ?>
                       <div class="tt-pagination">
                           <?php echo $paging->show_paging($fsitename . 'pages/blogs.htm?filter'); ?>       
                           <!-- <a href="#" class="btn-pagination btn-prev"></a> -->
                          
                        </div>
                     </div>
            <?php } ?>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-3 rightColumn">
                    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search this blog">
    <div class="input-group-append">
      <button class="btn btn-secondary" type="button">
        <i class="icon-f-85"></i>
      </button>
    </div>
  </div>
  
                     <div class="tt-block-aside">
                        <h3 class="tt-aside-title">CATEGORIES</h3>
                        <div class="tt-aside-content">
                           <ul class="tt-list-row">
                                <?php
                                $b = '1';
                                $blogs = $db->prepare("SELECT * FROM `blogcategory` WHERE `status`= ? ORDER BY `order` ASC");
                                $blogs->execute(array('1'));
                                $bcount = $blogs->rowcount();
                                if ($bcount != '0') {
                                while ($blogsfet = $blogs->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                               <li>
                              <a href="<?php echo $fsitename . $blogsfet['link'] . '/blogs'; ?>.htm" <?php if ($blogsfet['link'] == $_REQUEST['bid']) { ?> style="font-weight:bold;" <?php } ?>>
                                                <?php echo $blogsfet['category']; ?>

                                            </a>
                               </li>
                                <?php } } ?>
                             
                           </ul>
                        </div>
                     </div>
                     <div class="tt-block-aside">
                        <h3 class="tt-aside-title">RECENT POST</h3>
                        <div class="tt-aside-content">
                           <div class="tt-aside-post">
                                <?php
                                $b = '1';
                                $blogs = $db->prepare("SELECT * FROM `blog` WHERE `status`= ? ORDER BY `bid` DESC");
                                $blogs->execute(array('1'));
                                $bcount = $blogs->rowcount();
                                if ($bcount != '0') {
                                while ($blogsfet = $blogs->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                              <div class="item">
                                 <div class="tt-tag">
                                 <?php
                               $expcat=explode(',',$blogsfet['category']);
                               foreach($expcat as $expcat1)
                               {
                               $links[]='<a href="'.$fsitename . getblogcategory("link",$expcat1) . '/blogs.htm">'.getblogcategory("category",$expcat1).'</a>';    
                               }
                               $impl=implode(',',array_unique($links));
                               echo $impl;
                               ?>  

                                 </div>
                                 <a href="<?php echo $fsitename . 'view-' . $blogsfet['link']; ?>/blog.htm">
                                    <div class="tt-title"><?php echo stripslashes($blogsfet['title']); ?></div>
                                    <div class="tt-description"><?php echo stripslashes(substr($blogsfet['description'], 0, 120)); ?></div>
                                 </a>
                                 
                                 
                              </div>
                                <?php } } ?>
                             </div>
                        </div>
                     </div>
                   </div>
               </div>
            </div>
         </div>
      </div>
      
<?php 
include "require/footer.php";
?>