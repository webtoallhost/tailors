<?php include 'require/header.php';

if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'login.htm'.'">';
    exit;
}


if (isset($_POST['guest-checkout'])) {
    if ($_SESSION['GUEST_ID'] != '') {
        $ds = $db->prepare("DELETE FROM `guest` WHERE `id`=?");
        $ds->execute(array($_SESSION['GUEST_ID']));
        $ds = $db->prepare("DELETE FROM `bill_ship_address` WHERE `Guest_ID`=?");
        $ds->execute(array($_SESSION['GUEST_ID']));
    }
    $sg = $db->prepare("INSERT INTO `guest` SET `email`=?,`ip`=?");
    $sg->execute(array($_POST['guest'], $_SERVER['REMOTE_ADDR']));
    $lastid = $db->lastInsertId();

    $sg = $db->prepare("INSERT INTO `bill_ship_address` SET `Guest_ID`=?,`bemail`=?");
    $sg->execute(array($lastid, $_POST['guest']));
    $_SESSION['GUEST'] = '1';
    $_SESSION['GUEST_ID'] = $lastid;
    if ($_POST['hiddenagent'] != '') {
        $_SESSION['AGENT_CODE'] = $_POST['hiddenagent'];
    }
    header("location:" . $fsitename . "checkout.htm");
    exit;
}
?>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Shopping Cart</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container">
                    <h1 class="tt-title-subpages noborder">SHOPPING CART</h1>
                    <div class="row">
                        <div class="col-sm-12 col-xl-12">
                            <div class="tt-shopcart-table">
                                <?php 
                               // print_r($_SESSION);
                                ?>
                                <table>
                                    <tbody>
                                        
                                        <?php 
                                        $user_id = $_SESSION['FUID']; 
                                         $tempcart = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=?");
         $tempcart->execute(array($user_id));
		 if ($tempcart->rowCount() > 0) {
		 while($tempcartfetch = $tempcart->fetch()) {
		       $im = explode(",", getproduct('image', $tempcartfetch['productid']));
                                        ?>
                                         <tr>
                                            <td>
                                            <a href="#" class="tt-btn-close delete_cart_this_product" data-rem_par="1" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" ></a>
                                            </td>
                                            <td>
                                                <input type="hidden" name="sizeid" id="sizeid" value="<?php echo $tempcartfetch['size']; ?>">
                                                <div class="tt-product-img"><a href="<?php echo $fsitename . 'view/' . getproduct('link', $tempcartfetch['productid']) . '.htm'; ?>">
                                                                <?php if($im[0]!='') { ?>
                                                                <img src="<?php echo $fsitename . 'images/product/' . getproduct('imagefolder', $tempcartfetch['productid']) . '/' . $im[0]; ?>" alt="<?php echo getproduct('productname', $tempcartfetch['productid']); ?>" style="width:100px; float:left;" />
<?php } else { ?>
                                                                <img src="<?php echo $fsitename . 'images/noimage1.png'; ?>" alt="<?php echo getproduct('productname', $tempcartfetch['productid']); ?>" style="width:100px; float:left;" />
                                                                <?php } ?>
                                                            </a></div>
                                            </td>
                                            <td>
                                                <h2 class="tt-title"><a href="<?php echo $fsitename . 'view/' . getproduct('link', $tempcartfetch['productid']) . '.htm'; ?>"><?php echo getproduct('productname', $tempcartfetch['productid']); ?></a>
                                        <br>
                                        <i>Size : <?php echo $tempcartfetch['size']; ?></i>
                                                </h2>
                                                <ul class="tt-list-parameters">
                                                    <li><div class="tt-price"> <?php
                                                    
                                                    $as = $db->prepare("SELECT * FROM `sizeprice` WHERE `size`=? AND `product_id`=?");
         $as->execute(array($tempcartfetch['size'],$tempcartfetch['productid']));
		 if ($as->rowCount() > 0) {
		      $asd = $as->fetch();
		   $sprice = $asd['sprice'];
           
            $price = $asd['price'];  
		 }
		 else
		 {
		    $sprice = getproduct('sprice', $tempcartfetch['productid']);
           
            $price = getproduct('price', $tempcartfetch['productid']);   
		 }
                                                  
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></li>
                                                    <li><div class="detach-quantity-mobile"></div></li>
                                                    <li><div class="tt-price subtotal">$124</div></li>
                                                </ul>
                                            </td>
                                            <td><div class="tt-price"><?php
                           
                           
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></td>
                                            <td>
                                                <div class="detach-quantity-desctope">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn" id="cart-minues" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"></span> 
                                                     <input size="100"  title="Numbers Only" name="qty[]" id="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"   value="<?php echo $tempcartfetch['qty']; ?>" />
                                                    <span class="plus-btn" id="cart-plus" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" id="temp" data-max="100"></span></div>
                                                </div>
                                                <input type="hidden" name="product_id[]" value="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" />
                                            </td>
                                            <td><div class="tt-price subtotal">$&nbsp;<span class="subtotal-col cart-id-<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"><?php
                                            if ($sprice > 0) {
                                                echo number_format($sprice * $tempcartfetch['qty'], '2', '.', '');
                                                   $cartftotoal+=number_format($sprice * $tempcartfetch['qty'], '2', '.', '');
                                            } else {
                                                echo number_format($price * $tempcartfetch['qty'], '2', '.', '');
                                                   $cartftotoal+=number_format($price * $tempcartfetch['qty'], '2', '.', '');
                                            }
                                            
                                                    ?></span></div></td>
                                        </tr>
                                        <?php } } else { 
                                            
                                    $prod_weight = '';
                                    $_SESSION['prod_weight'] = '';
                                    $qtys = array_count_values($_SESSION['CART_PRODUCTS']);

                                    foreach (array_unique($_SESSION['CART_PRODUCTS']) as $prod) {
                                        
                                        $prod11=explode('-',$prod);
                                        
                                        $im = explode(",", getproduct('image', $prod11['0']));


                                        ?>
                                        <tr>
                                            <td>
                                            <a href="#" class="tt-btn-close delete_cart_this_product" data-rem_par="1" data-pdid="<?php echo $prod; ?>" ></a>
                                            </td>
                                            <td>
                                                <input type="hidden" name="sizeid" id="sizeid" value="<?php echo $prod11['1']; ?>">
                                                <div class="tt-product-img"><a href="<?php echo $fsitename . 'view-' . getproduct('link', $prod11['0']) . '.htm'; ?>">
                                                                <?php if($im[0]!='') { ?>
                                                                <img src="<?php echo $fsitename . 'images/product/' . getproduct('imagefolder', $prod11['0']) . '/' . $im[0]; ?>" alt="<?php echo getproduct('productname', $prod11['0']); ?>" style="width:100px; float:left;" />
<?php } else { ?>
                                                                <img src="<?php echo $fsitename . 'images/noimage1.png'; ?>" alt="<?php echo getproduct('productname', $prod11['0']); ?>" style="width:100px; float:left;" />
                                                                <?php } ?>
                                                            </a></div>
                                            </td>
                                            <td>
                                                <h2 class="tt-title"><a href="<?php echo $fsitename . 'view-' . getproduct('link', $prod11['0']) . '.htm'; ?>"><?php echo getproduct('productname', $prod11['0']); ?></a></h2>
                                                <ul class="tt-list-parameters">
                                                    <li><div class="tt-price"> <?php
                                                    $cartftotoal=0;
                                                    $as = $db->prepare("SELECT * FROM `sizeprice` WHERE `size`=? AND `product_id`=?");
         $as->execute(array($prod11['1'],$prod11['0']));
		 if ($as->rowCount() > 0) {
		      $asd = $as->fetch();
		   $sprice = $asd['sprice'];
           
            $price = $asd['price'];  
		 }
		 else
		 {
		    $sprice = getproduct('sprice', $prod11['0']);
           
            $price = getproduct('price', $prod11['0']);   
		 }
                                                  
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></li>
                                                    <li><div class="detach-quantity-mobile"></div></li>
                                                    <li><div class="tt-price subtotal">$124</div></li>
                                                </ul>
                                            </td>
                                            <td><div class="tt-price"><?php
                           
                           
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></td>
                                            <td>
                                                <div class="detach-quantity-desctope">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn" data-pdid="<?php echo $prod; ?>"></span> 
                                                     <input size="100"  title="Numbers Only" name="qty[]" id="<?php echo $prod; ?>"   value="<?php echo $qtys[$prod]; ?>" />
                                                    <span class="plus-btn" data-pdid="<?php echo $prod; ?>" id="temp" data-max="100"></span></div>
                                                </div>
                                                <input type="hidden" name="product_id[]" value="<?php echo $prod; ?>" />
                                            </td>
                                            <td><div class="tt-price subtotal">$&nbsp;<span class="subtotal-col cart-id-<?php echo $prod; ?>"><?php
                                            if ($sprice > 0) {
                                                echo number_format($sprice * $qtys[$prod], '2', '.', '');
                                                $cartftotoal+=number_format($sprice * $qtys[$prod], '2', '.', '');
                                            } else {
                                                echo number_format($price * $qtys[$prod], '2', '.', '');
                                                  $cartftotoal+=number_format($price * $qtys[$prod], '2', '.', '');
                                            }
                                                    ?></span></div></td>
                                        </tr>
                                        <?php } } ?>
                                        </tbody>
                                </table>
                                <div class="tt-shopcart-btn">
                                    <div class="col-left">
                                        <a class="btn-link" href="<?php echo $fsitename; ?>listings.htm"><i class="icon-e-19"></i>CONTINUE SHOPPING</a>
                                    </div>
                                    <!--<div class="col-right">-->
                                    <!--    <a class="btn-link" href="#"><i class="icon-h-02"></i>CLEAR SHOPPING CART</a> <a class="btn-link" href="#"><i class="icon-h-48"></i>UPDATE CART</a>-->
                                    <!--</div>-->
                                </div>
                            </div>
                        </div>
                      
                    </div>
                    <div class="row">
                         <div class="col-sm-12 col-xl-4">&nbsp;</div>
                          <div class="col-sm-12 col-xl-4">&nbsp;</div>
                          <div class="col-sm-12 col-xl-4">
                            <div class="tt-shopcart-wrapper">
                             
                                <div class="tt-shopcart-box tt-boredr-large">
                                    <table class="tt-shopcart-table01">
                                        <tbody>
                                            <tr>
                                                <th>SUBTOTAL</th>
                                                <td>$<span id="subtotal-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>GRAND TOTAL</th>
                                                <td>$<span id="total-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <a href="<?php echo $fsitename; ?>pages/orderconfirmation.htm" class="btn btn-lg"><span class="icon icon-check_circle"></span>PLACE ORDER</a>
                                </div>
                            </div>
                        </div></div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>
        
        <script>
             $(document).ready(function(){
$('#shipingform').submit(function(){
// show that something is loading
$('#shpresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
 var result=data.split('#');
 
    $('#sipprice').html(result['0']);
    
     $('#subtotal-shiptds').html(result['0']);
     $('#total-tds').html(result['1']);
    
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});

   });
   
   function getstate(a)
    {
        alert(a);
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {country: a},
            success: function (data) {
              $("#state").html(data);
              
            }
        });
    }    
        </script>