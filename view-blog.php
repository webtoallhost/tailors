<?php $dynamic = '1';

$selid = '4';

include 'admin/config/config.inc.php';



$pageu = FETCH_all("SELECT * FROM `blog` WHERE `link` =?", $_REQUEST['bid']);



$getCategoryname = pFETCH("SELECT category FROM `blogcategory` WHERE FIND_IN_SET(`bcid`, ?)", $pageu['category']);



//$pageu2 = FETCH_all("SELECT category FROM `blogcategory` WHERE FIND_IN_SET(`bcid`, ?)", $pageu['category']);

if ($pageu['bid'] == '') {

    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';

    exit;

}



include "require/header.php";

?>

<style>
  .tt-listing-post:not(.tt-half) .tt-post .tt-post-img + .tt-post-content {
    background: none;
    padding: 50px 0 0;
     border-radius: 0px;
    box-shadow: none;

}
.tt-social-icon li a {
    color: #b6adad;
    }
.tt-listing-post:not(.tt-half) .tt-post .tt-post-img {
    flex: 0 0 calc(45% - 39px);
    max-width: calc(59% - 39px);
    width: calc(45% - 39px);
    margin-right: 39px;
}
.tt-listing-post .tt-post .tt-post-content .tt-title a {
    font-size: 17px;
    color: #191919;
    display: inline-block;
    line-height: 27px;
    text-transform: uppercase;
    font-weight: 800;
}
.tt-listing-post .tt-post .tt-post-content .tt-description {
    margin-top: 0px;
}
body .tt-listing-post .tt-post .tt-post-content .tt-description p {
    color: gray;
    margin-top: 10px;
}
body .tt-listing-post .tt-post .tt-post-content .tt-btn {
    margin-top: 15px;
}
.tt-title-subpages:not(.text-left):not(.text-right) {
    text-align: left;
    font-weight: bold;
    font-size: 23px;
    padding-bottom: 30px;
}
.tt-listing-post .tt-post .tt-post-img img {
    border-radius: 34px;
}
.tt-listing-post .tt-post .tt-post-content .tt-btn  .btn:not(:disabled):not(.disabled) {
    cursor: pointer;
    background: transparent;
    color: #2879fe;
    padding: 0;
    text-decoration: underline;
    font-weight: 600;
}
  @media (min-width: 1025px){


    .rightColumn {
    padding: 0 39px;
    border-radius: 50px;

}


.btn:not(:disabled):not(.disabled) {
    cursor: pointer;
    background: #2879fe;
    color: #fff;
}
.tt-listing-post .tt-post .tt-post-content .tt-description p{
color: #fff;
}
.tt-aside-post .item .tt-title {
    color: #ffffff;
    }
    .tt-aside-post .item p {
    color: #fff;
}
.tt-list-row li a {
    color: #0000ff;
}
.tt-aside-post .item .tt-tag a {
    color: #0000ff;
    text-shadow: 0px 0px 1px #0000ff;
}
}
.input-group>.form-control:not(:last-child) {
    border: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    background: #f7f6f6;
    box-shadow: none;
}
.input-group .btn:not(:disabled):not(.disabled) {
    padding: 5px 10px;
    cursor: pointer;
    background: #f7f6f6;
    color: #000;
}
body .tt-block-aside .tt-aside-title{
  border-bottom: 0 !important;
  padding-bottom: 0;
}
.input-group .btn:not(.tt-icon-right) [class^=icon-] {
    font-size: 20px;
    margin-right: 0px;
}
.tt-aside-post .item .tt-tag a {
    font-size: 15px;
    color: #2879fe;
    text-shadow: none;
    font-weight: 500;
}
.tt-aside-post .item .tt-title {
    color: #191919;
}
.tt-aside-post .item p {
    color: gray;
}
.tt-block-aside:not(:first-child) {
    margin-top: 30px;
}
.tt-list-row li a {
    border-bottom: 1px solid #ddd;
    color: #777;
    padding-bottom: 10px;
    padding-top: 10px;
}
.tt-list-row li:last-child a {
    padding-bottom: 0;
    border: 0;
}
.tt-post-single .tt-post-content img{
  margin-top: 0
}
.tt-post-single .tt-tag{
  justify-content: flex-start;
}
  

</style>
      <div class="tt-breadcrumb">
         <div class="container">
            <ul>
               <li><a href="index.html">Home</a></li>
               <li>Blog</li>
            </ul>
         </div>
      </div>
      <div id="tt-pageContent">
         <div class="container-indent">
            <div class="container container-fluid-custom-mobile-padding">
               <div class="row justify-content-center">
                  <div class="col-xs-12 col-md-10 col-lg-8 col-md-auto">
                     <div class="tt-post-single">
                       
                         <div class="tt-post-content">
<!--                            <img src="images/loader.svg" data-src="images/blog/blog-single-img-01.jpg" alt="">
 -->                           <div class="row">
                              <div class="col-xs-12 col-sm-12"><img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/blog/<?php echo $pageu['image']; ?>" alt=""></div>        
                           </div>
                           <h2 class="tt-title"><?php echo $pageu['title']; ?></h2>
                            <div class="tt-tag">
                           
                        <?php
                               $expcat=explode(',',$pageu['category']);
                               foreach($expcat as $expcat1)
                               {
                               $links[]='<a href="'.$fsitename . getblogcategory("link",$expcat1) . '/blogs.htm">'.getblogcategory("category",$expcat1).'</a>';    
                               }
                               $impl=implode(',',array_unique($links));
                               echo $impl;
                               ?>  
                        </div>
                           <p><?php echo stripslashes($pageu['description']); ?></p>
                           <div class="row">
                           <div class="col-sm-12 col-md-6">
                            <ul class="tt-social-icon">
                                <li><a class="icon-g-64" target="_blank" href="https://www.facebook.com/" title="Facebook" alt="Facebook"></a></li>
                                <li><a class="icon-h-58" target="_blank" href="https://twitter.com" title="Twiiter" alt="Twiiter"></a></li>
                                <li><a class="icon-g-66" target="_blank" href="https://plus.google.com/" title="Google Plus" alt="Google Plus"></a></li>
                                <li><a class="icon-g-67" target="_blank" href="http://www.instagram.com" title="Instagram" alt="Instagram"></a></li>
                                <li><a class="icon-g-70" target="_blank" href="https://in.pinterest.com/" title="Pinterest" alt="Pinterest"></a></li>
                            </ul>
                           </div>
                          <div class="col-sm-12 col-md-6 text-right">
                            <p style="margin: 0">posted by admin</p>
                          </div>
                        </div>
                            </div>
                         </div>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-4 rightColumn">
                    <div class="input-group">
    <input type="text" class="form-control" placeholder="Search this blog">
    <div class="input-group-append">
      <button class="btn btn-secondary" type="button">
        <i class="icon-f-85"></i>
      </button>
    </div>
  </div>
  
                     <div class="tt-block-aside">
                        <h3 class="tt-aside-title">CATEGORIES</h3>
                        <div class="tt-aside-content">
                           <ul class="tt-list-row">
                                <?php
                                $b = '1';
                                $blogs = $db->prepare("SELECT * FROM `blogcategory` WHERE `status`= ? ORDER BY `order` ASC");
                                $blogs->execute(array('1'));
                                $bcount = $blogs->rowcount();
                                if ($bcount != '0') {
                                while ($blogsfet = $blogs->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                               <li>
                              <a href="<?php echo $fsitename . $blogsfet['link'] . '/blogs'; ?>.htm" <?php if ($blogsfet['link'] == $_REQUEST['bid']) { ?> style="font-weight:bold;" <?php } ?>>
                                                <?php echo $blogsfet['category']; ?>

                                            </a>
                               </li>
                                <?php } } ?>
                             
                           </ul>
                        </div>
                     </div>
                     <div class="tt-block-aside">
                        <h3 class="tt-aside-title">RECENT POST</h3>
                        <div class="tt-aside-content">
                           <div class="tt-aside-post">
                                <?php
                                $b = '1';
                                $blogs = $db->prepare("SELECT * FROM `blog` WHERE `status`= ? ORDER BY `bid` DESC");
                                $blogs->execute(array('1'));
                                $bcount = $blogs->rowcount();
                                if ($bcount != '0') {
                                while ($blogsfet = $blogs->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                              <div class="item">
                                 <div class="tt-tag">
                                 <?php
                               $expcat=explode(',',$blogsfet['category']);
                               foreach($expcat as $expcat1)
                               {
                               $links[]='<a href="'.$fsitename . getblogcategory("link",$expcat1) . '/blogs.htm">'.getblogcategory("category",$expcat1).'</a>';    
                               }
                               $impl=implode(',',array_unique($links));
                               echo $impl;
                               ?>  

                                 </div>
                                 <a href="<?php echo $fsitename . 'view-' . $blogsfet['link']; ?>/blog.htm">
                                    <div class="tt-title"><?php echo stripslashes($blogsfet['title']); ?></div>
                                    <div class="tt-description"><?php echo stripslashes(substr($blogsfet['description'], 0, 120)); ?></div>
                                 </a>
                                 
                                 
                              </div>
                                <?php } } ?>
                             </div>
                        </div>
                     </div>
                   </div>
               </div>
            </div>
         </div>
      </div>
    <?php include 'require/footer.php'; ?>