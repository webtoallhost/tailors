<?php
$page = '14';
$getstatic = '1';
include 'require/header.php';
if ($_SESSION['FUID'] != '') {
    header("location:" . $fsitename . "pages/myaccount.htm");
    exit;
}

if (isset($_REQUEST['create'])) {
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
  
    $chk = FETCH_all("SELECT * FROM `customer` WHERE `emailid`=?", $emailid);
$chk1 = FETCH_all("SELECT * FROM `customer` WHERE `mobileno`=?", $mobileno);
    if($chk['emailid'] != ''){
     $smsg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Already a user registered with this email</h4></div>';
    } elseif($chk1['mobileno'] != ''){
     $smsg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Already a user registered with this mobile no</h4></div>';
    
}

        elseif ($password == $cpassword) {
            $smsg = signupp($fname,$lname,$emailid,$mobileno,$password,$country,$state,$city,$address,$pincode,$gender,$collar,$sholder,$height,$hip_size,$ip);
            
              //  echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/login.htm' . '">';
                header("location:" . $fsitename . "pages/login.htm?su=reg");
                exit;
            
        } else {
            $smsg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Passwords are not Match</h4></div>';
        }
  
}
?>
<style>
    #forgotid {
        cursor: pointer;
    }
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Register</li>
                </ul>
            </div>
        </div>
          <div id="tt-pageContent">
        <div class="container-indent">
            <form name="regform" id="regform" method="post">
            <div class="container">
                <h1 class="tt-title-subpages noborder">CREATE AN ACCOUNT</h1>
                <div class="row">
                <div class="col-md-6"> <div class="tt-login-form">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-12">
                            <div class="tt-item">
                                <h2 class="tt-title">PERSONAL INFORMATION</h2>
                                <div class="form-default">
                                 
                                        <div class="form-group">
                                            <label for="loginInputName">FIRST NAME *</label>
                                            <div class="tt-required">* Required Fields</div>
                                            <input type="text" name="fname" required="required" class="form-control" id="loginInputName" placeholder="Enter First Name">
                                        </div>
                                        <div class="form-group"><label for="loginLastName">LAST NAME *</label>
                                        <input type="text" name="lname" required="required" class="form-control" id="loginLastName" placeholder="Enter Last Name"></div>
                                        <div class="form-group"><label for="loginInputEmail">E-MAIL *</label><span style="color:red;" id="emailerror"></span>
                                        <input type="email" name="emailid" required="required" class="form-control" id="emailid" placeholder="Enter E-mail"></div>
                                        <div class="form-group"><label for="loginInputEmail">Mobile No *</label><span style="color:red;" id="mblerror"></span>
                                        <input type="number" name="mobileno" required="required" class="form-control" id="mobileno" placeholder="Enter Mobileno"></div>
                                        <div class="form-group"><label for="loginInputPassword">PASSWORD *</label> 
                                        <input type="text" name="password" required="required" class="form-control" id="loginInputPassword" placeholder="Enter Password"></div>
                                         <div class="form-group"><label for="loginInputPassword">CONFIRM PASSWORD *</label> 
                                        <input type="text" name="cpassword" required="required" class="form-control" id="loginInputPassword" placeholder="Enter Confirm Password"></div>
                                        
                                     
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div>
                <div class="col-md-6"> <div class="tt-login-form">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-12">
                            <div class="tt-item">
                                <h2 class="tt-title">ADDRESS DETAILS</h2>
                                <div class="form-default">
                                   
                                        <div class="form-group">
                                            <label for="loginInputName">COUNTRY *</label>
                                            <div class="tt-required">* Required Fields</div>
                                            <select name="country" id="country" class="form-control" required="required" onchange="getstate(this.value)">
                                            <option value="">Select Country</option>   
                                              <?php
                                            $country = DB("SELECT * FROM `countries` ORDER BY `name` ASC");
                                            $ccount = mysqli_num_rows($trending);
                                            if ($ccount != '0') {
                                            while ($countrylist = mysqli_fetch_array($country)) {
                                                ?>
                                                <option value="<?php echo $countrylist['id']; ?>"><?php echo $countrylist['name']; ?></option>
                                            <?php } } ?>    
                                            </select>
                                           
                                        </div>
                                        <div class="form-group"><label for="loginLastName">STATE *</label> 
                                        <select name="state" id="state" class="form-control" required="required" onchange="getcity(this.value)">
                                            <option value="">Select State</option>   
                                           </select> 
                                        </div>
                                        <div class="form-group"><label for="loginInputEmail">CITY *</label> 
                                        <select name="city" id="city" class="form-control" required="required">
                                            <option value="">Select City</option>    
                                            </select>
                                        </div>
                                        <div class="form-group"><label for="loginInputPassword">ADDRESS *</label> 
                                        <textarea name="address" class="form-control" required="required" rows="4" style="height:100px;"></textarea>
                                       
                                       </div>
                                         <div class="form-group"><label for="loginInputPassword">PINCODE *</label> 
                                         <input type="text" name="pincode" required="required" class="form-control" id="loginInputPassword" placeholder="Enter Pincode">
                                       </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div>
                </div>
                <br>
               <div class="row">
                      <div class="col-md-12"> <div class="tt-login-form">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-12">
                            <div class="tt-item">
                                <h2 class="tt-title">MEASUREMENT DETAILS</h2>
                                <div class="form-default">
                                   <div class="row">
                                       <div class="col-md-6"> <div class="form-group"><label for="loginInputPassword">Gender </label> 
                                         <select class="form-control" name="gender">
                                              <option value="">Select</option>   
                                         <option value="Male">Male</option>   
                                         <option value="Female">Female</option>
                                         </select>
                                       </div></div> 
                                       <div class="col-md-6"> <div class="form-group"><label for="loginInputPassword">Collar </label> 
                                        <input type="text" name="collar" class="form-control" id="loginInputPassword" placeholder="Enter Collar Size">
                                       </div></div> 
                                       </div>
                                         <div class="row">
                                       <div class="col-md-6"> <div class="form-group"><label for="loginInputPassword">Shoulder </label> 
                                        <input type="text" name="shoulder" class="form-control" id="loginInputPassword" placeholder="Enter Shoulder Size">
                                       </div></div> 
                                       <div class="col-md-6"> <div class="form-group"><label for="loginInputPassword">Height </label> 
                                        <input type="text" name="height" class="form-control" id="loginInputPassword" placeholder="Enter Height">
                                       </div></div> 
                                       </div>
                                        <div class="row">
                                       <div class="col-md-6"> <div class="form-group"><label for="loginInputPassword">Hip Size </label> 
                                          <input type="text" name="hip_size" class="form-control" id="loginInputPassword" placeholder="Enter Hip Size">
                                       </div></div> 
                                     
                                       </div>
                                        <div class="row">
                                            <div class="col-auto">
                                                <div class="form-group"><button class="btn btn-border" type="submit" name="create" id="create">CREATE</button></div>
                                            </div>
                                            <div class="col-auto align-self-center">
                                                <!--<div class="form-group">-->
                                                <!--    <ul class="additional-links">-->
                                                <!--        <li>or <a href="#">Return to Store</a></li>-->
                                                <!--    </ul>-->
                                                <!--</div>-->
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div> 
               </div>
            </div>
            </form>
        </div>
    </div>
     <script src="<?php echo $fsitename; ?>js/jquery.min.js"></script>
        <script>
        $(document).ready(function(){
  $("#emailid").keyup(function(){
       var emailid = $(this).val();
  $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {emailid: emailid},
            success: function (data) {
              $("#emailerror").html(data);
              
            }
        });    
 
  });
    $("#mobileno").keyup(function(){
       var mobileno = $(this).val();
  $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {mobileno: mobileno},
            success: function (data) {
              $("#mblerror").html(data);
              
            }
        });    
 
  });
});
        function logchange(a) {
        if (a == 1) {
            $("#getforgot").css("display", "none");
            $("#getlogin").css("display", "block");
            $('#login').hide();
            $('#forgotpassword').fadeToggle();
        } else {
            $("#getlogin").css("display", "none");
            $("#getforgot").css("display", "block");
            $('#forgotpassword').hide();
            $('#login').fadeToggle();

        }
    }
     function getstate(a)
    {
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {country: a},
            success: function (data) {
              $("#state").html(data);
              
            }
        });
    }
     function getcity(a)
    {
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {state: a},
            success: function (data) {
              $("#city").html(data);
              
            }
        });
    }
    </script>
  
   
        <?php include 'require/footer.php';?>