<?php  

include 'admin/config/config.inc.php';

session_destroy();

session_unset();

echo "<script> function() {
  document.getElementById('sign-out').addEventListener('click', function() {
    firebase.auth().signOut();
  });</script>";
header("location:".$fsitename);

exit;

?>