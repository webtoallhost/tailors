<?php include 'require/header.php';
if (isset($_REQUEST['submit'])) {
    
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
    
    
        $msg = addcontact($name, $email, $mobile, $subject, $message, $ip);
    
}

?>
<style>
/*21.01.2021*/
.tt-contact-info:not(.text-left):not(.text-right) {
    background: #000000a1;
    box-shadow: 0px 0px 7px #ffffff70;
    border-radius: 34px;
}

/*21.01.2021*/

    .form-default .form-control {
    border: 1px solid #5e9afe;
}
#contactform1 {
  padding-bottom: 20px;
    background: none;
    /* padding: 36px; */
    border-radius: 50px;
    box-shadow: none;
    margin-top: 0;
    padding-left: 0;
    padding-top: 20px;
}
.form-default .form-control {
    font-weight: 500;
    color: #000;
    background: #ffffff;
    border: 0;
}
/*04.01.2021*/
.tt-block-title .tt-description {
    font-size: 14px;
    line-height: 24px;
    margin-top: 10px;
}
.tt-block-title .tt-title {
    /* margin-bottom: 14px; */
    font-size: 24px;
    font-family: system-ui;
    font-weight: bold;
    letter-spacing: .04em;
    margin: 0;
    padding: 0;
    color: #191919;
    margin-top: 30px;
}

#bdd{
    background: #2879fe!important;
    color: white;
}
.addresstable{
    margin-right: 20px;
    padding: 21px;
}
.colorbg{
    padding: 20px 30px;
    height: 100%;
    resize: none;
}
h2:not(.small):not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]) {
    font-family: unset;
    font-size: 32px;
    line-height: 44px;
    font-weight: bold;
    letter-spacing: .03em;
    padding-bottom: 23px;
    padding: 0;
}
.adddiv{
    display: flex;
    align-items: center;
}
.contact-form textarea {
    height: 120px;
}
.adddiv i{
    position: relative;
    color: #2879fe;
    /* margin-top: 7px; */
    top: 2px;
    font-size: 26px;
    margin-right: 10px;
}
h4:not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]) {
    font-size: 24px;
    line-height: 34px;
    font-weight: 500;
    letter-spacing: .03em;
    padding-bottom: 10px;
    margin-top: 30px;
}
.form-default .form-control {
    font-weight: 500;
    color: #000;
    background: #ffffff;
    border: 0;
}
.tt-social-icon li a {
    color: #8e8b8b;
    }
/*04.01.2021*/

</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li>Contact</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="tt-block-title">
            <h1 class="tt-title">Contact Us</h1>
            <div class="tt-description">Any question or remarks?just write as a message!</div>
         </div>
        <div class="container">
         <div class="row">
            <div class="col-sm-12 col-xl-4">
                <div class="colorbg">
                <h2>Get In Touch</h2>
                    <address>
                                    <p class="adddiv"><span><i class="icon-g-35"></i></span>  <?php echo getprofile('address', 1); ?></p>
                                    <p class="adddiv"><span><i class="icon-h-35"></i></span>+<?php echo getprofile('phonenumber', 1); ?></p>
                                    <p class="adddiv"><span><i class="icon-g-51"></i></span> <a href="mailto:info@mydomain.com">info@mydomain.com</a></p>
                                </address>

                                <h4>Social Media</h4>
                                <ul class="tt-social-icon">
                                <li><a class="icon-g-64" target="_blank" href="https://www.facebook.com/" title="Facebook" alt="Facebook"></a></li>
                                <li><a class="icon-h-58" target="_blank" href="https://twitter.com" title="Twiiter" alt="Twiiter"></a></li>
                                <li><a class="icon-g-66" target="_blank" href="https://plus.google.com/" title="Google Plus" alt="Google Plus"></a></li>
                                <li><a class="icon-g-67" target="_blank" href="http://www.instagram.com" title="Instagram" alt="Instagram"></a></li>
                                <li><a class="icon-g-70" target="_blank" href="https://in.pinterest.com/" title="Pinterest" alt="Pinterest"></a></li>
                            </ul>
            </div>
        </div>
             <div class="col-sm-12 col-xl-8">
                                <div class="colorbg">
                <h2>Let's Talk</h2>
                <form id="contactform1" class="contact-form form-default" method="post">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name (required)" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Mobile" required="required" />
                                </div>
                                
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Your Email (required)" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject" required="required" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" rows="7" placeholder="Your Message" id="message" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="text-left">
                            <button type="submit" name="submit" class="btn" id="bdd">SUBMIT</button>
                        </div>
                    </form>
            </div>
            </div>
         </div>
     </div>
           <!--  <div class="container-indent nomargin">
                <div class="tt-contact-box">
                    <img class="img-mobile" src="images/custom/contact-img-01.jpg" alt="" />
                    <div class="container container-fluid-custom-mobile-padding">
                        <h1 class="tt-title">
                            WE ARE LOOKING FORWARD<br />
                            TO HEARING FROM YOU
                        </h1>
                        <address>
                            Address: <?php echo getprofile('address', 1); ?>,<br />
                            Phone: +<?php echo getprofile('phonenumber', 1); ?>
                        </address>
                    </div>
                </div>
            </div> -->
            <div class="container-indent">
                <div class="container">
                    <!-- <div class="contact-map"><div id="map"></div></div> -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125766.12980003307!2d78.05278255426747!3d9.91799869040521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b00c582b1189633%3A0xdc955b7264f63933!2sMadurai%2C%20Tamil%20Nadu!5e0!3m2!1sen!2sin!4v1604738507494!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-contact02-col-list">
                        <div class="row">
                           <!--  <div class="col-sm-12 col-md-6 ml-sm-auto mr-sm-auto">
                                <div class="tt-contact-info">
                                    <i class="tt-icon icon-f-93"></i>
                                    <h6 class="tt-title">LET’S HAVE A CHAT!</h6>
                                    <address>
                                        +<?php echo getprofile('phonenumber', 1); ?>
                                    </address>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="tt-contact-info">
                                    <i class="tt-icon icon-f-24"></i>
                                    <h6 class="tt-title">VISIT OUR LOCATION</h6>
                                    <address>
                                       <?php echo getprofile('address', 1); ?>
                                    </address>
                                </div>
                            </div> -->
                            <!--<div class="col-sm-6 col-md-4">-->
                            <!--    <div class="tt-contact-info">-->
                            <!--        <i class="tt-icon icon-f-92"></i>-->
                            <!--        <h6 class="tt-title">WORK TIME</h6>-->
                            <!--        <address>-->
                            <!--            7 Days a week<br />-->
                            <!--            from 10 AM to 6 PM-->
                            <!--        </address>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    
                </div>
            </div> -->
        </div>
        <?php include 'require/footer.php';?>
         <script>
      $(document).ready(function(){
          setTimeout(function() {
              <?php
              if($msg!='') {
                  echo 'alert("Message sent successfully");';
              }
              
              
              ?>
              }, 500);
      });
  </script>

