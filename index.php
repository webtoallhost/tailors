

<?php  include "require/header.php"; ?>
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap" rel="stylesheet">
<div id="tt-pageContent">
   <?php
      $banner = DB("SELECT * FROM `banner` WHERE `status`='1' AND `image`!='' ORDER BY `order` ASC ");
      $bcount = mysqli_num_rows($banner);
      if ($bcount != '0') {
       ?>
   <div class="container-indent nomargin">
      <div class="container-fluid">
         <div class="row">
            <div class="slider-revolution revolution-default">
               <div class="tp-banner-container">
                  <div class="tp-banner revolution">
                     <ul>
                        <?php while ($bannerlist = mysqli_fetch_array($banner)) { ?>
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                           <img
                              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAMgAQMAAAD4P+14AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAPdJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABg9uBAAAAAAADI/7URVFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVWFPTgQAAAAAADyf20EVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVpDw4JAAAAAAT9f+0JIwAAAAAAAAAAALAJ8T4AAZAZiOkAAAAASUVORK5CYII="
                              data-lazyload="<?php echo $fsitename.'images/banner/'.$bannerlist['image']; ?>"
                              alt="slide1"
                              data-bgposition="center center"
                              data-bgfit="cover"
                              data-bgrepeat="no-repeat"
                              />
                           <div
                              class="tp-caption tp-caption1 lft stb"
                              data-x="center"
                              data-y="center"
                              data-hoffset="0"
                              data-voffset="0"
                              data-speed="600"
                              data-start="900"
                              data-easing="Power4.easeOut"
                              data-endeasing="Power4.easeIn"
                              >
                              <div class="tp-caption1-wd-1 tt-base-color"><?php echo $bannerlist['title']; ?>
                              </div>
                              <div class="tp-caption1-wd-3" style="font-size:20px;"><?php echo $bannerlist['content']; ?></div>
                              <?php if($bannerlist['link']!='') { ?>
                              <div class="tp-caption1-wd-4"><a href="<?php echo $bannerlist['link']; ?>" target="_blank" class="btn btn-xl" data-text="SHOP NOW!">SHOP NOW!</a></div>
                              <?php }  ?>
                           </div>
                        </li>
                        <?php } ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
   <div class="container-indent0">
      <div class="container-fluid">
         <div class="row tt-layout-promo-box">
            <div class="col-sm-12 col-md-6">
               <div class="row">
                  <div class="col-sm-6">
                     <!--  <a href="<?php echo getquickbanner('link',1); ?>" class="tt-promo-box tt-one-child hover-type-2">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',1); ?>" alt="<?php echo getquickbanner('title',1); ?>" />
                        <div class="tt-description">
                           <div class="tt-description-wrapper">
                              <div class="tt-background"></div>
                              <div class="tt-title-small"><?php echo getquickbanner('title',1); ?></div>
                           </div>
                        </div>
                        </a> -->
                     <a  class="tt-promo-box tt-one-child hover-type-2">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',1); ?>" alt="<?php echo getquickbanner('title',1); ?>" />
                        <div class="tt-description newstyle">
                           <div class="tt-description-wrapper">
                              <h4>Men's</h4>
                              <h2>Fshion</h2>
                              <!-- <div class="tt-background"></div>
                                 <div class="tt-title-small"><?php echo getquickbanner('title',1); ?></div> -->
                           </div>
                        </div>
                     </a>
                     <a href="<?php echo getquickbanner('link',2); ?>" class="tt-promo-box tt-one-child hover-type-2">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',2); ?>" alt="<?php echo getquickbanner('title',2); ?>" />
                        <div class="tt-description newstyle">
                           <div class="tt-description-wrapper">
                              <h4 style="color: #FB0301">Sale 50%</h4>
                              <h2>offer</h2>
                              <!-- <div class="tt-background"></div>
                                 <div class="tt-title-small"><?php echo getquickbanner('title',1); ?></div> -->
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="col-sm-6">
                     <a href="<?php echo getquickbanner('link',3); ?>" class="tt-promo-box tt-one-child hover-type-2">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',3); ?>" alt="<?php echo getquickbanner('title',3); ?>"/>
                        <div class="tt-description newstyle centerbox">
                           <div class="tt-description-wrapper">
                              <h4>Women's</h4>
                              <h2>Collection</h2>
                              <button class="btn tt-title-small" style="background: #FB0301"><?php echo getquickbanner('title',3); ?></button>
                           </div>
                        </div>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-sm-12 col-md-6">
               <div class="row">
                  <div class="col-sm-6">
                     <a href="<?php echo getquickbanner('link',4); ?>" class="tt-promo-box tt-one-child hover-type-2">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',4); ?>" alt="<?php echo getquickbanner('title',4); ?>" />
                        <div class="tt-description newstyle">
                           <div class="tt-description-wrapper">
                              <h4>Kids's</h4>
                              <h2>Collection</h2>
                              <!-- <button class="btn tt-title-small" style="background: #FB0301"><?php echo getquickbanner('title',4); ?></button> -->
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="col-sm-6">
                     <a href="<?php echo getquickbanner('link',5); ?>" class="tt-promo-box tt-one-child hover-type-2">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',5); ?>" alt="<?php echo getquickbanner('title',5); ?>"/>
                        <div class="tt-description newstyle">
                           <div class="tt-description-wrapper">
                              <h4>2021's</h4>
                              <h2>Collection</h2>
                              <!-- <button class="btn tt-title-small" style="background: #FB0301"><?php echo getquickbanner('title',5); ?></button> -->
                           </div>
                        </div>
                     </a>
                  </div>
                  <div class="col-sm-12">
                     <a href="<?php echo getquickbanner('link',6); ?>" class="tt-promo-box tt-one-child">
                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',6); ?>" alt="<?php echo getquickbanner('title',6); ?>" />
                        <div class="tt-description newstyle rightbox">
                           <div class="tt-description-wrapper">
                              <h4>New arrivals</h4>
                              <button class="btn tt-title-small"><?php echo getquickbanner('title',6); ?></button>
                           </div>
                        </div>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <style>
      .tt-base-color{
      color: #fff;
      }
      .sizebox.tt-promo-box .tt-description .tt-title-small {
      font-weight: bold;
      font-family: Hind,sans-serif;
      color: #f34f04;
      }
      .newstyle h4{
      font-family: 'Dancing Script', cursive;
      }
      .sizebox.tt-promo-box:not(.tt-one-child) .tt-description .tt-description-wrapper {
      width: 100%;
      padding: 10px;
      text-align: center;
      max-width: 100%;
      min-width: 52%;
      }
      .sizebox.tt-promo-box .tt-description {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      width: 100%;
      height: 100%;
      padding: 20px 10px;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      justify-content: center;
      align-content: center;
      align-items: flex-end;
      }
      .tt-promo-box .tt-description.newstyle.centerbox{
      top: 0;
      height: auto;
      }
      .tt-promo-box .tt-description.newstyle.rightbox{
      top: 0;
      left: auto;
      right: 0;
      bottom: 0;
      width: auto;
      height: 100%;
      }
      .slider-revolution .tp-caption1-wd-3{
      color: #fff;
      }
      .slider-revolution [class^=btn] {
      position: inherit;
      background-color: #000;
      }
      .tt-block-title .tt-description {
      background: #2879fe;
      font-size: 16px;
      line-height: 24px;
      font-weight: 500;
      letter-spacing: .04em;
      font-family: Hind,sans-serif;
      color: #191919;
      margin: 0;
      padding: 0;
      height: 4px;
      width: 60px;
      display: inline-block;
      }
      .tt-desctop-menu:not(.tt-hover-02) li.dropdown>a {
      text-decoration: none;
      }
      .tt-desktop-header .container {
      max-width: 1190px;
      }
      .boxtype.tt-blog-thumb .tt-img+.tt-title-description{
      margin-top: 0
      }
      .tt-product:not(.tt-view) .tt-image-box .tt-btn-quickview+.tt-btn-wishlist+.tt-btn-compare {
      top: 145px;
      }
      .modal .modal-dialog {
    min-width: 70%;
}
.tt-product-vertical-layout .tt-product-single-img img{
    min-height: 500px
}
      .tt-product:not(.tt-view) .tt-image-box .tt-btn-quickview+.tt-btn-compare, .tt-product:not(.tt-view) .tt-image-box .tt-btn-quickview+.tt-btn-wishlist {
      top: 90px;
      }
      .tt-product:not(.tt-view) .tt-image-box .tt-label-location{
      right: 15px !important;
      left: auto;
      }
      .tt-options-swatch:not(.options-large):not(.options-middle) li a:not(.options-color) {
      border: 1px solid #ccc;
      }
      .tt-product:not(.tt-view).thumbprod-center {
      border: 1px solid #f3f2f2;
      }
      .tt-product:not(.tt-view) .tt-image-box .tt-btn-quickview{
      top: 37px
      }
      .carousel-control-prev-icon {
      background-image: url(images/logo/left-arrow-icon.png);
      }
      .carousel-control-prev-icon {
      background-image: url(images/logo/right-arrow-icon.png);
      }
      .tt-product:not(.tt-view) .tt-image-box .tt-label-location .tt-label-new {
      background: #10099b;
      color: #fff;
      padding: 3px 4px 3px;
      }
      .tt-product:not(.tt-view) .tt-image-box .tt-label-location {
      position: absolute;
      top: 7px;
      right: 8px;
      }
      .tt-product.thumbprod-center.product-nohover:hover{
      box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
      }
      .boxtype.tt-blog-thumb {
      border: 1px solid #ede7e7;
      height: 100%;
      position: relative;
      /* box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%); */
      padding-bottom: 30px;
      }
      .newstyle h4:not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]) {
      color: #fff;
      font-size: 47px;
      line-height: 34px;
      font-weight: 500;
      letter-spacing: .03em;
      padding-bottom: 0;
      }
      .newstyle h2:not(.small):not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]) {
      color: #fff;
      font-size: 34px;
      line-height: 44px;
      font-weight: 500;
      text-transform: uppercase;
      letter-spacing: .03em;
      padding-bottom: 0;
      }
      .tt-promo-box .tt-description.newstyle {
      display: inline-block;
      position: absolute;
      top: auto;
      left: 0;
      right: 0;
      bottom: 0;
      width: auto;
      height: 46%;
      padding: 20px;
      display: flex;
      flex-direction: row;
      flex-wrap: nowrap;
      justify-content: center;
      align-content: center;
      align-items: center;
      color: #777;
      transition: color .2s linear;
      }
      .tt-promo-box.tt-one-child .tt-description.newstyle .tt-description-wrapper {
      z-index: 10;
      background-color: rgb(40 23 37 / 1%);
      padding: 10px 20px;
      text-align: center;
      max-width: 409px;
      position: relative;
      display: inline-block;
      }
      .boxtype.tt-blog-thumb:hover{
      box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
      }
      .boxtype.tt-product {
      padding-bottom: 10px;
      position: relative;
      box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
      }
      .boxtype.tt-product:not(.tt-view) .tt-description .tt-title{
      font-size: 22px 
      }
      .tt-blog-thumb .tt-title-description .tt-title a {
      text-overflow: ellipsis;
      overflow: hidden;
      /* white-space: nowrap; */
      display: -webkit-box !important;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      }
      .owl-prev::before {
      position: absolute;
      font-size: 30px;
      line-height: 1;
      font-family: wokiee;
      font-weight: 500;
      color: #ccc;
      content: "\e90d";
      left: -27px;
      }
      .owl-carousel .owl-nav.disabled {
      position: absolute;
      display: block;
      width: 100%;
      display: flex;
      justify-content: space-between;
      top: 50%;
      }
      .padingspace{
      padding: 20px;
      margin-bottom: 45px;
      }
      .owl-theme .owl-nav [class*=owl-]:hover {
      background: transparent;
      color: transparent;
      text-decoration: none;
      }
      .owl-next::before {
      right: -27px;
      width: 100%;
      content: "\e90e";
      font-size: 30px;
      line-height: 1;
      font-family: wokiee;
      font-weight: 500;
      color: #ccc;
      position: absolute;
      }
      .tt-blog-thumb:hover .tt-title-description .tt-background {
      top: 0;
      }
      .owl-theme .owl-nav [class*=owl-] {
      color: transparent;
      font-size: 14px;
      margin: 5px;
      position: relative;
      padding: 4px 7px;
      background: transparent;
      display: inline-block;
      cursor: pointer;
      border-radius: 3px;
      }
      .slider-revolution.revolution-default .tp-leftarrow.default, .slider-revolution.revolution-default .tp-rightarrow.default{
      color: #fff
      }
      @media(min-width: 1125px) and (max-width: 1400px){
      .tt-layout-promo-box:not(.nomargin){
      padding: 0 75px;
      }
      }
      .tt-promo-box.tt-one-child .tt-description .tt-description-wrapper .tt-background{
      background: #FB0301;
      }
      .tt-promo-box .tt-description .tt-title-small {
      font-family: Hind,sans-serif;
      color: #fff;
      }
      header .tt-search .tt-dropdown-menu form{
      background: none;
      }
      .linktext{
      font-weight: 600;
      position: absolute;
      bottom: -10px;
      left: 0;
      right: 0;
      color: #191919;
      text-decoration: underline;
      }
      #js-tt-stuck-nav #js-include-desktop-menu nav .tt-megamenu-submenu {
      margin-left: 0px !important;
      }   
      .slider-revolution .tp-caption1 {
      left: auto !important;
      /* float: right; */
      width: 25%;
      right: 20%;
      text-align: left;
      color: #191919;
      }        
      @media (max-width: 480px){
      .via{
      position: unset !important;
      left: 0 !important;
      margin-top: 0 !important;
      }
      .mp{
      width: 140px;
      margin-left: 7px;
      }
      .hert{
      position: absolute;
      margin-top: 39px !important;
      }
      #w3review{
      width:262px;
      }
      }
      .via{
      position: absolute;
      left: 75%;
      margin-top: -57px;
      text-transform: uppercase;
      }
      @media (max-width: 1024px){
      .tt-product-design02:not(.tt-view) .tt-description, .tt-product:not(.tt-view) .tt-description {
      margin-top: 18px;
      position: absolute;
      }
      }  
      .tt-services-block .tt-col-icon [class^=icon-]{
      position: relative;
      top: 0;
      }
      .right-all{
      position: relative;
      }
      .right-all .viewdiv{
      top: 20px;
      position: absolute;
      right: 17px;
      color: #000;
      font-weight: 600;
      }
      .right-all .viewdiv a{
      color: #000;
      }
      .tt-services-block .tt-col-icon {
      border-radius: 50%;
      align-self: center;
      font-size: 45px;
      color: #000;
      background: #f1f0f0;
      min-width: 60px;
      height: 60px;
      display: flex;
      justify-content: center;
      align-items: center;
      }
      .slider-revolution .tp-caption1-wd-3{
        margin-top: 5px;
      }
      .slider-revolution [class^=btn] {
    height: auto;
    padding-bottom: 2px
}
.slider-revolution .tp-caption1-wd-4 {
    margin-top: 15px;
}
body .slider-revolution .tp-bullets{
    display: none;
}
      #Modalsizebox .modal-xs .modal-header .close {
      right: -91px;
      top: -45px;
      font-weight: 500;
      }
      #Modalsizebox .carousel-indicators{
        margin-left: 10%;
        right: auto;
        left: 20%;
        margin: 0;
        bottom: -10px
      }
     #Modalsizebox .carousel-indicators .active {
    background-color: #000;
}
#Modalsizebox .carousel-indicators li {
    border-radius: 50%;
    height: 10px;
    width: 10px;
    background-color: #ccc
}
      #Modalsizebox .tt-wrapper.dblock{
        display: none;
      }
      #Modalsizebox.modal .modal-content {
      padding: 20px;
      border-radius: 20px;
      border: none;
      }
      #Modalsizebox.modal .modal-header {
      border: none;
      padding: 0;
      margin: 0;
      height: 0;
      position: relative;
      }
      .tt-product:not(.tt-view).thumbprod-center.boxshape{
      box-shadow: none;
      }
      .tt-product:not(.tt-view).thumbprod-center.boxshape:hover{
      box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);
      }
      .tt-product:not(.tt-view).thumbprod-center.boxshape:hover .bg-dark{
      background-color: #007bff !important;
      }
      .module {
      background: 
      linear-gradient(
      rgba(0, 0, 0, 0.6),
      rgba(0, 0, 0, 0.6)
      ),
      url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/skyscrapers.jpg);
      background-size: cover;
      position: relative;
      border-radius: 8px;
      }
      .tt-product-single-info .tt-title {
      font-size: 30px;
      line-height: 40px;
      font-weight: 600;
      margin-top: 5px;
      color: #191919;
      margin-bottom: 5px;
      }
      .tt-product-single-info {
      padding-left: 10px;
      margin: -5px 0 0;
      text-align: left;
      }
      .tt-product-single-info .tt-price {
      font-size: 30px;
      line-height: 40px;
      font-family: Hind,sans-serif;
      font-weight: 500;
      color: #000;
      }
      .tt-product-single-info .tt-price .old-price, .tt-product-single-info .tt-price .old-price .money {
      color: gray;
      text-decoration: line-through;
      font-size: 17px;
      }
      .tt-rating {
      display: inline-flex;
      }
      .boxmodal .tt-wrapper {
      margin: 0 !important;
      }
      .boxmodal {
      width: 70%;
      display: flex;
      justify-content: space-between;
      }
      .tt-input-counter.style-01 input {
      background: transparent;
      border: 1px solid #e3e4e7;
      }
      .modal .modal-body:not(.no-background) {
      background-color: #fff;
      border-radius: 6px;
      }
      .tt-product-vertical-layout .tt-product-single-img {
      float: left;
      width: 100%;
      position: relative;
      }
      /* @media (max-width: 1024px){
      .tt-product-design02:not(.tt-view) .tt-image-box .tt-img img, .tt-product:not(.tt-view) .tt-image-box .tt-img img {
      height: 250px !important;
      }
      }*/
   </style>
   <!-- ==================new section -strar-->
   <div class="container-indent">
      <div class="container container-fluid-custom-mobile-padding right-all">
         <div class="tt-block-title">
            <h1 class="tt-title">SHOWROOMS</h1>
            <div class="tt-description"></div>
            <div class="viewdiv">
               <a href="<?php echo $fsitename; ?>pages/listing1.htm">View All</a>
            </div>
         </div>
         <div class="row tt-layout-product-item">
            <?php
               $trending = DB("SELECT * FROM `vendor` WHERE `status`='1' ORDER BY `uid` DESC LIMIT 4");
               $tcount = mysqli_num_rows($trending);
               if ($tcount != '0') {
               while ($trendinglist = mysqli_fetch_array($trending)) {
                   $link22 = FETCH_all("SELECT * FROM `users` WHERE `usergroup`=? AND `type`=? ", $trendinglist['uid'],'vendor');
                 if($trendinglist['image']!='')  {
                 $imgres = $fsitename.'images/vendor/' . $trendinglist['image'];   
               
                 }
                else {
                   $imgres=$fsitename.'images/noimage.png'; 
                }
               ?>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product boxtype thumbprod-center boxshape">
                  <div class="tt-image-box">
                     <!--<a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>-->
                     <a href="<?php echo $fsitename; ?><?php echo $link22['id']; ?>.htm">
                     <span class="tt-img"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['name']; ?>" /></span>
                     <span class="tt-img-roll-over"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['name']; ?>" /></span>
                     </a>
                  </div>
                  <div class="tt-description border-top mt-4 mx-3">
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </p>
                     <!-- <h2 class="tt-title"><a href="<?php echo $fsitename; ?><?php echo $link22['id']; ?>.htm" class="fontbolder"><?php echo $trendinglist['name']; ?></a></h2> -->
                     <div class="tp-caption1-wd-4 mt-3 mb-3"><a href="<?php echo $bannerlist['link']; ?>" target="_blank" class="btn bg-dark" data-text="SHOP NOW!">SHOP NOW!</a></div>
                  </div>
               </div>
            </div>
            <?php } } ?>
         </div>
      </div>
   </div>
   <div class="container-indent">
      <div class="container container-fluid-custom-mobile-padding right-all">
         <div class="module mid">
            <div class="tt-description newstyle">
               <div class="tt-description-wrapper text-center p-5">
                  <h4 class="text-primary">Custom</h4>
                  <h2>YOUR CLOTH</h2>
                  <button class="btn bg-primary mt-2" style="background: #FB0301">SHOP NOW</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- new section===end========= -->
   <div class="container-indent">
      <div class="container container-fluid-custom-mobile-padding right-all">
         <div class="tt-block-title">
            <h1 class="tt-title">RECENT ARRIVALS</h1>
            <div class="tt-description"></div>
            <div class="viewdiv">
               <a href="<?php echo $fsitename; ?>listing.htm?offer=new">View All</a>
            </div>
         </div>
         <div class="row tt-layout-product-item">
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row" data-toggle="modal" data-target="#Modalsizebox">
                        <ul class="tt-add-info">
                           <li><a href="#" >T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                        </div>
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <!-- <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                           </div> -->
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <!-- <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                           </div> -->
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <!-- <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                           </div> -->
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="container-indent">
      <div class="container-fluid-custom">
         <div class="row tt-layout-promo-box sizebox">
            <div class="col-sm-6 col-md-4">
               <a href="<?php echo getofferbanner('link',1); ?>" class="tt-promo-box">
                  <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',1); ?>" alt="<?php echo getofferbanner('title',1); ?>" />
                  <div class="tt-description">
                     <div class="tt-description-wrapper">
                        <div class="tt-background"></div>
                        <div class="tt-title-small clchang"><?php echo getofferbanner('title',1); ?></div>
                        <div class="tt-title-large"><?php echo getofferbanner('content',1); ?></div>
                     </div>
                  </div>
               </a>
            </div>
            <div class="col-sm-6 col-md-4">
               <a href="<?php echo getofferbanner('link',2); ?>" class="tt-promo-box">
                  <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',2); ?>" alt="<?php echo getofferbanner('title',2); ?>" />
                  <div class="tt-description">
                     <div class="tt-description-wrapper">
                        <div class="tt-background"></div>
                        <div class="tt-title-small  clchang"><?php echo getofferbanner('title',2); ?></div>
                        <div class="tt-title-large"><?php echo getofferbanner('content',2); ?></div>
                     </div>
                  </div>
               </a>
            </div>
            <div class="col-sm-6 col-md-4">
               <a href="<?php echo getofferbanner('link',3); ?>" class="tt-promo-box">
                  <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',3); ?>" alt="<?php echo getofferbanner('title',3); ?>" />
                  <div class="tt-description">
                     <div class="tt-background"></div>
                     <div class="tt-description-wrapper">
                        <div class="tt-background "></div>
                        <div class="tt-title-small clchang"><?php echo getofferbanner('title',3); ?></div>
                        <div class="tt-title-large"><span class="tt-base-color clchang"><?php echo getofferbanner('content',3); ?></span></div>
                     </div>
                  </div>
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="container-indent">
      <div class="container container-fluid-custom-mobile-padding right-all">
         <div class="tt-block-title">
            <h2 class="tt-title">POPULAR SEARCH</h2>
            <div class="tt-description"></div>
            <div class="viewdiv">
               <a href="<?php echo $fsitename; ?>listing.htm?offer=deal">View All</a>
            </div>
         </div>
         <div class="row tt-layout-product-item">
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                        </div>
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <!-- <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                           </div> -->
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <!-- <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                           </div> -->
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-6 col-md-4 col-lg-3">
               <div class="tt-product thumbprod-center product-nohover">
                  <div class="tt-image-box">
                     <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView" data-tooltip="Quick View" data-tposition="left"></a>
                     <a href="#" class="tt-btn-wishlist" data-tooltip="Add to Wishlist" data-tposition="left"></a>
                     <a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>
                     <a href="product.html">
                     <span class="tt-img"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-img-roll-over"><img src="images/product/product-03-02.jpg" data-src="images/product/product-03-02.jpg" alt="" class="loaded" data-was-processed="true"></span>
                     <span class="tt-label-location">
                     <span class="tt-label-new">New</span>
                     </span>
                     </a>
                  </div>
                  <div class="tt-description">
                     <div class="tt-row">
                        <ul class="tt-add-info">
                           <li><a href="#">T-SHIRTS</a></li>
                        </ul>
                        <div class="tt-rating">
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star"></i>
                           <i class="icon-star-half"></i>
                           <i class="icon-star-empty"></i>
                        </div>
                     </div>
                     <h2 class="tt-title"><a href="product.html">Flared Shift Dress</a></h2>
                     <div class="tt-price">
                        $12
                     </div>
                     <div class="tt-option-block">
                        <ul class="tt-options-swatch js-change-img">
                           <li class="active"><a href="#" class="options-color-img" data-src="images/product/product-03.jpg" data-src-hover="images/product/product-03-02.jpg" data-tooltip="Blue" data-tposition="top"><img src="images/product/product-03.jpg" data-src="images/product/product-03.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-05.jpg" data-src-hover="images/product/product-03-05-hover.jpg" data-tooltip="Light Blue" data-tposition="top"><img src="images/product/product-03-05.jpg" data-src="images/product/product-03-05.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-06.jpg" data-src-hover="images/product/product-03-06-hover.jpg" data-tooltip="Green" data-tposition="top"><img src="images/product/product-03-06.jpg" data-src="images/product/product-03-06.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-07.jpg" data-src-hover="images/product/product-03-07-hover.jpg" data-tooltip="Pink" data-tposition="top"><img src="images/product/product-03-07.jpg" data-src="images/product/product-03-07.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                           <li><a href="#" class="options-color-img" data-src="images/product/product-03-08.jpg" data-src-hover="images/product/product-03-08-hover.jpg" data-tooltip="Orange" data-tposition="top"><img src="images/product/product-03-08.jpg" data-src="images/product/product-03-08.jpg" alt="" class="loaded" data-was-processed="true"></a></li>
                        </ul>
                     </div>
                     <div class="tt-product-inside-hover">
                        <!-- <div class="tt-row-btn">
                           <a href="#" class="tt-btn-addtocart thumbprod-button-bg" data-toggle="modal" data-target="#modalAddToCartProduct">ADD TO CART</a>
                           </div> -->
                        <div class="tt-row-btn">
                           <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView"></a>
                           <a href="#" class="tt-btn-wishlist"></a>
                           <a href="#" class="tt-btn-compare"></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php
      $blog = DB("SELECT * FROM `blog` WHERE `status`='1' AND `image`!='' ORDER BY `order` ASC LIMIT 3 ");
      $bcount = mysqli_num_rows($blog);
      if ($bcount != '0') { ?>
   <div class="container-indent">
      <div class="container">
         <div class="tt-block-title">
            <h2 class="tt-title">LATEST FROM BLOG</h2>
            <div class="tt-description"></div>
         </div>
         <div class="tt-blog-thumb-list">
            <div class="row">
               <div class="owl-carousel owl-theme">
                  <div class="item">
                     <!--  <div class="tt-blog-thumb-list">
                        <div class="row">
                           <?php  while ($bloglist = mysqli_fetch_array($blog)) { 
                           $bimage=$fsitename.'images/blog/'.$bloglist['image'];
                           ?>
                           <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                              <div class="tt-blog-thumb bgback boxtype">
                                 <div class="tt-img">
                                    <a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><img src="images/loader.svg" data-src="<?php echo $bimage; ?>" alt="<?php echo stripslashes($fblogs['title']); ?>" /></a>
                                 </div>
                                 <div class="tt-title-description">
                                    <div class="tt-background"></div>
                                    <div class="tt-tag"><a href="<?php echo $fsitename . getblogcategory('link',$bloglist['category']) . '/blogs'; ?>.htm"><?php echo getblogcategory('category',$bloglist['category']); ?></a></div>
                                    <div class="tt-title"><a href="<?php echo $fsitename . 'view/' . $bloglist['link']; ?>/blog.htm"><?php echo stripslashes($bloglist['title']); ?></a></div>
                                    <p><?php echo stripslashes(substr($bloglist['description'], 0, 120)); ?></p>
                                    <a class="linktext" href="">Read more</a>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                        </div> -->
                     <div class="padingspace">
                        <div class="tt-blog-thumb bgback boxtype">
                           <div class="tt-img">
                              <a href=""><img  src="images/blog/blog2.jpg" /></a>
                           </div>
                           <div class="tt-title-description">
                              <div class="tt-background"></div>
                              <div class="tt-tag"><a href="">5 MARCH 2021</a></div>
                              <div class="tt-title"><a href="">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod</a></div>
                              <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                              <a class="linktext" href="">Read more</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="padingspace">
                        <div class="tt-blog-thumb bgback boxtype">
                           <div class="tt-img">
                              <a href=""><img  src="images/blog/blog2.jpg" /></a>
                           </div>
                           <div class="tt-title-description">
                              <div class="tt-background"></div>
                              <div class="tt-tag"><a href="">5 MARCH 2021</a></div>
                              <div class="tt-title"><a href="">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod</a></div>
                              <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                              <a class="linktext" href="">Read more</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="padingspace">
                        <div class="tt-blog-thumb bgback boxtype">
                           <div class="tt-img">
                              <a href=""><img  src="images/blog/blog2.jpg" /></a>
                           </div>
                           <div class="tt-title-description">
                              <div class="tt-background"></div>
                              <div class="tt-tag"><a href="">5 MARCH 2021</a></div>
                              <div class="tt-title"><a href="">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod</a></div>
                              <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </p>
                              <a class="linktext" href="">Read more</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php } ?>
<div class="container-indent">
   <div class="container">
      <div class="row tt-services-listing">
         <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#" class="tt-services-block">
               <div class="tt-col-icon"><i class="icon-f-48"></i></div>
               <div class="tt-col-description">
                  <h4 class="tt-title">FREE SHIPPING</h4>
                  <p class="w-75 text-truncate">Free shipping on all US order or order above $99</p>
               </div>
            </a>
         </div>
         <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#" class="tt-services-block">
               <div class="tt-col-icon"><i class="icon-f-35"></i></div>
               <div class="tt-col-description">
                  <h4 class="tt-title">SUPPORT 24/7</h4>
                  <p class="w-75 text-truncate">Contact us 24 hours a day, 7 days a week</p>
               </div>
            </a>
         </div>
         <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#" class="tt-services-block">
               <div class="tt-col-icon"><i class="icon-e-09"></i></div>
               <div class="tt-col-description">
                  <h4 class="tt-title">30 DAYS RETURN</h4>
                  <p class="w-75 text-truncate">Simply return it within 24 days for an exchange.</p>
               </div>
            </a>
         </div>
      </div>
   </div>
</div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<div class="modal fade" id="Modalsizebox" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-xs">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <span class="icon icon-clear"></span></button>
         </div>
         <div class="modal-body">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
               <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
               </ol>
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <div class="row">
                        <div class="col-6 hidden-xs">
                           <div class="tt-product-vertical-layout">
                              <div class="tt-product-single-img">
                                 <div>
                                     <img class="zoom-product" src="http://localhost/tailor/images/product/Test-Checked-Red/1609830610210833930.jpg" data-zoom-image="http://localhost/tailor/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="">
                                    <div id="custom-product-item"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6">
                           <div class="tt-product-single-info">
                              <div class="tt-add-info">
                                 <ul>
                                    <li><span>SKU:</span>Test Checked</li>
                                    <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                 </ul>
                              </div>
                              <h1 class="tt-title">Test Checked-Red</h1>
                              <div id="pprice"></div>
                              <div class="tt-price"><span class="new-price" id="sprice1">$ 900</span>&nbsp;&nbsp;<span id="price1" class="old-price">$  1000</span></div>
                              <div class="tt-row">
                                 <ul class="tt-add-info">
                                    <li><a href="#"></a></li>
                                 </ul>
                                 <div class="tt-rating">
                                    <i class="icon-star"></i> 
                                    <i class="icon-star"></i> 
                                    <i class="icon-star"></i> 
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                 </div>
                                 <a href="" style="color: blue">(0 Customer Review)</a>
                              </div>
                              <br>
                              <!-- <div class="tt-review">
                                 <div class="tt-rating">
                                                                             </div>
                                 <a class="product-page-gotocomments-js"></a>
                                 </div> -->
                              <div class="tt-wrapper" style="margin-top: 10px">
                                 <div class="tt-add-info">
                                    <ul>
                                       <!-- <li><span style="color:#2879fe;">Seller:</span>&nbsp;&nbsp;Priyan</li>
                                          <li><span style="color:#2879fe;">Contact Number:</span>&nbsp;&nbsp;7200300343</li>
                                          -->
                                       <li style="color:gray;" class="position-relative"><span style="color:#000;">Type:</span>&nbsp;&nbsp;Mens Clothing</li>
                                       <li style="color:gray;"><span style="color:#000;">Size:</span>&nbsp;&nbsp;XXL</li>
                                       <li style="color:gray;"><span style="color:#000;">Color:</span>&nbsp;&nbsp;red</li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tt-wrapper">
                                 <p>Test Checked</p>
                                 <br>
                                 <!--    -->
                              </div>
                              <div class="tt-swatches-container boxmodal">
                                 <div class="tt-wrapper">
                                    <div class="tt-title-options">AVAILABLE SIZE:</div>
                                    <ul class="tt-options-swatch options-large">
                                       <li class="active"><a href="#" onclick="getprice('XXL',900,1000);">XXL</a></li>
                                       <li><a href="#" onclick="getprice('XL',950,1000);">XL</a></li>
                                       <li><a href="#" onclick="getprice('L',850,1000);">L</a></li>
                                       <!-- <li class="active"><a href="#">6</a></li> -->
                                    </ul>
                                 </div>
                                 <!-- <div class="tt-wrapper product-information-buttons">
                                    <a data-toggle="modal" data-target="#modalProductInfo" href="#">Size Guide</a> 
                                    <a data-toggle="modal" data-target="#modalProductInfo-02" href="#">Return Policies</a>
                                    </div> -->
                                 <div class="tt-wrapper">
                                    <div class="tt-title-options">AVAILABLE COLOR:</div>
                                    <ul class="tt-options-swatch options-large">
                                       <li class="active">
                                          <a class="options-color tt-color-bg-09" style="background-color:#f61335">red</a>
                                       </li>
                                       <li>
                                          <a onclick="window.location.href = 'http://localhost/tailor/view/TestCheckedPink.htm'; " class="options-color tt-color-bg-09" style="background-color:#f51689">Pink</a>
                                       </li>
                                       <!-- <li class="active"><a class="options-color tt-color-bg-10" href="#"></a></li> -->
                                    </ul>
                                 </div>
                              </div>
                              <div class="tt-wrapper">
                                 <form method="post" id="reviewform" style="background: none">
                                    <div class="tt-row-custom-01">
                                       <div class="col-item">
                                          <input type="hidden" name="product_id[]" value="d41d8cd98f00b204e9800998ecf8427e">
                                          <div class="tt-input-counter style-01"><span class="minus-btn"></span> 
                                             <input size="100" title="Numbers Only" name="qty[]" id="qty1" value="1" type="number">
                                             <span class="plus-btn" data-max="100"></span>
                                          </div>
                                       </div>
                                       <div class="col-item">
                                          <input type="hidden" name="sizeid" id="sizeid" value="XXL">
                                          <button type="button" data-pdid="42-XXL" title="Add to Cart" class="btn btn-lg add_cart_this_product" data-go-cart="1">
                                          ADD TO CART
                                          </button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              <div class="tt-wrapper dblock">
                                 <!-- ==========ask me======= -->
                                 <br><br>
                                 <!--              <input class="btn btn-xl" onclick="myFunction()" type="submit" value="Ask me?"> 
                                    -->
                                 <p id="askresponse"></p>
                                 <form method="post" id="askme" style="margin-top: 20px; display: none;">
                                    <input type="hidden" name="productid" id="productid" value="42">
                                    <input type="hidden" name="vendorid" id="vendorid" value="9">
                                    <input type="hidden" name="userid" id="userid" value="2">
                                    <textarea id="w3review" placeholder="ask me" name="askmessage" rows="12" cols="50" style="border:2px solid #2879fe;padding:5px;"></textarea>
                                    <br><br>
                                    <input class="btn btn-xl" type="submit" value="Submit">
                                 </form>
                              </div>
                              <script>
                                 function myFunction() {
                                   var x = document.getElementById("askme");
                                 
                                   if (x.style.display === "none") {
                                     x.style.display = "block";
                                     x.style.background = "none";
                                   } else {
                                     x.style.display = "none";
                                 
                                   }
                                 }
                              </script>
                              <!-- ================================= -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="row">
                        <div class="col-6 hidden-xs">
                           <div class="tt-product-vertical-layout">
                              <div class="tt-product-single-img">
                                 <div>
                                     <img class="zoom-product" src="http://localhost/tailor/images/product/Test-Checked-Red/1609830610210833930.jpg" data-zoom-image="http://localhost/tailor/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="">
                                    <div id="custom-product-item"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6">
                           <div class="tt-product-single-info">
                              <div class="tt-add-info">
                                 <ul>
                                    <li><span>SKU:</span>Test Checked</li>
                                    <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                 </ul>
                              </div>
                              <h1 class="tt-title">Test Checked-Red</h1>
                              <div id="pprice"></div>
                              <div class="tt-price"><span class="new-price" id="sprice1">$ 900</span>&nbsp;&nbsp;<span id="price1" class="old-price">$  1000</span></div>
                              <div class="tt-row">
                                 <ul class="tt-add-info">
                                    <li><a href="#"></a></li>
                                 </ul>
                                 <div class="tt-rating">
                                    <i class="icon-star"></i> 
                                    <i class="icon-star"></i> 
                                    <i class="icon-star"></i> 
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                 </div>
                                 <a href="" style="color: blue">(0 Customer Review)</a>
                              </div>
                              <br>
                              <!-- <div class="tt-review">
                                 <div class="tt-rating">
                                                                             </div>
                                 <a class="product-page-gotocomments-js"></a>
                                 </div> -->
                              <div class="tt-wrapper" style="margin-top: 10px">
                                 <div class="tt-add-info">
                                    <ul>
                                       <!-- <li><span style="color:#2879fe;">Seller:</span>&nbsp;&nbsp;Priyan</li>
                                          <li><span style="color:#2879fe;">Contact Number:</span>&nbsp;&nbsp;7200300343</li>
                                          -->
                                       <li style="color:gray;" class="position-relative">
                                          <span style="color:#000;">Type:</span>&nbsp;&nbsp;Mens Clothing 
                                          
                                       </li>
                                       <li style="color:gray;"><span style="color:#000;">Size:</span>&nbsp;&nbsp;XXL</li>
                                       <li style="color:gray;"><span style="color:#000;">Color:</span>&nbsp;&nbsp;red</li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tt-wrapper">
                                 <p>Test Checked</p>
                                 <br>
                                 <!--    -->
                              </div>
                              <div class="tt-swatches-container boxmodal">
                                 <div class="tt-wrapper">
                                    <div class="tt-title-options">AVAILABLE SIZE:</div>
                                    <ul class="tt-options-swatch options-large">
                                       <li class="active"><a href="#" onclick="getprice('XXL',900,1000);">XXL</a></li>
                                       <li><a href="#" onclick="getprice('XL',950,1000);">XL</a></li>
                                       <li><a href="#" onclick="getprice('L',850,1000);">L</a></li>
                                       <!-- <li class="active"><a href="#">6</a></li> -->
                                    </ul>
                                 </div>
                                 <!-- <div class="tt-wrapper product-information-buttons">
                                    <a data-toggle="modal" data-target="#modalProductInfo" href="#">Size Guide</a> 
                                    <a data-toggle="modal" data-target="#modalProductInfo-02" href="#">Return Policies</a>
                                    </div> -->
                                 <div class="tt-wrapper">
                                    <div class="tt-title-options">AVAILABLE COLOR:</div>
                                    <ul class="tt-options-swatch options-large">
                                       <li class="active">
                                          <a class="options-color tt-color-bg-09" style="background-color:#f61335">red</a>
                                       </li>
                                       <li>
                                          <a onclick="window.location.href = 'http://localhost/tailor/view/TestCheckedPink.htm'; " class="options-color tt-color-bg-09" style="background-color:#f51689">Pink</a>
                                       </li>
                                       <!-- <li class="active"><a class="options-color tt-color-bg-10" href="#"></a></li> -->
                                    </ul>
                                 </div>
                              </div>
                              <div class="tt-wrapper">
                                 <form method="post" id="reviewform" style="background: none">
                                    <div class="tt-row-custom-01">
                                       <div class="col-item">
                                          <input type="hidden" name="product_id[]" value="d41d8cd98f00b204e9800998ecf8427e">
                                          <div class="tt-input-counter style-01"><span class="minus-btn"></span> 
                                             <input size="100" title="Numbers Only" name="qty[]" id="qty1" value="1" type="number">
                                             <span class="plus-btn" data-max="100"></span>
                                          </div>
                                       </div>
                                       <div class="col-item">
                                          <input type="hidden" name="sizeid" id="sizeid" value="XXL">
                                          <button type="button" data-pdid="42-XXL" title="Add to Cart" class="btn btn-lg add_cart_this_product" data-go-cart="1">
                                          ADD TO CART
                                          </button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                             
                              <div class="tt-wrapper dblock">
                                 <!-- ==========ask me======= -->
                                 <br><br>
                                 <!--              <input class="btn btn-xl" onclick="myFunction()" type="submit" value="Ask me?"> 
                                    -->
                                 <p id="askresponse"></p>
                                 <form method="post" id="askme" style="margin-top: 20px; display: none;">
                                    <input type="hidden" name="productid" id="productid" value="42">
                                    <input type="hidden" name="vendorid" id="vendorid" value="9">
                                    <input type="hidden" name="userid" id="userid" value="2">
                                    <textarea id="w3review" placeholder="ask me" name="askmessage" rows="12" cols="50" style="border:2px solid #2879fe;padding:5px;"></textarea>
                                    <br><br>
                                    <input class="btn btn-xl" type="submit" value="Submit">
                                 </form>
                              </div>
                              <script>
                                 function myFunction() {
                                   var x = document.getElementById("askme");
                                 
                                   if (x.style.display === "none") {
                                     x.style.display = "block";
                                     x.style.background = "none";
                                   } else {
                                     x.style.display = "none";
                                 
                                   }
                                 }
                              </script>
                              <!-- ================================= -->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="row">
                        <div class="col-6 hidden-xs">
                           <div class="tt-product-vertical-layout">
                              <div class="tt-product-single-img">
                                 <div>
                                     <img class="zoom-product" src="http://localhost/tailor/images/product/Test-Checked-Red/1609830610210833930.jpg" data-zoom-image="http://localhost/tailor/images/product/Test-Checked-Red/1609830610210833930.jpg" alt="">
                                    <div id="custom-product-item"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-6">
                           <div class="tt-product-single-info">
                              <div class="tt-add-info">
                                 <ul>
                                    <li><span>SKU:</span>Test Checked</li>
                                    <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                 </ul>
                              </div>
                              <h1 class="tt-title">Test Checked-Red</h1>
                              <div id="pprice"></div>
                              <div class="tt-price"><span class="new-price" id="sprice1">$ 900</span>&nbsp;&nbsp;<span id="price1" class="old-price">$  1000</span></div>
                              <div class="tt-row">
                                 <ul class="tt-add-info">
                                    <li><a href="#"></a></li>
                                 </ul>
                                 <div class="tt-rating">
                                    <i class="icon-star"></i> 
                                    <i class="icon-star"></i> 
                                    <i class="icon-star"></i> 
                                    <i class="icon-star-empty"></i>
                                    <i class="icon-star-empty"></i>
                                 </div>
                                 <a href="" style="color: blue">(0 Customer Review)</a>
                              </div>
                              <br>
                              <!-- <div class="tt-review">
                                 <div class="tt-rating">
                                                                             </div>
                                 <a class="product-page-gotocomments-js"></a>
                                 </div> -->
                              <div class="tt-wrapper" style="margin-top: 10px">
                                 <div class="tt-add-info">
                                    <ul>
                                       <!-- <li><span style="color:#2879fe;">Seller:</span>&nbsp;&nbsp;Priyan</li>
                                          <li><span style="color:#2879fe;">Contact Number:</span>&nbsp;&nbsp;7200300343</li>
                                          -->
                                       <li style="color:gray;" class="position-relative">
                                          <span style="color:#000;">Type:</span>&nbsp;&nbsp;Mens Clothing 
                                          
                                       </li>
                                       <li style="color:gray;"><span style="color:#000;">Size:</span>&nbsp;&nbsp;XXL</li>
                                       <li style="color:gray;"><span style="color:#000;">Color:</span>&nbsp;&nbsp;red</li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="tt-wrapper">
                                 <p>Test Checked</p>
                                 <br>
                                 <!--    -->
                              </div>
                              <div class="tt-swatches-container boxmodal">
                                 <div class="tt-wrapper">
                                    <div class="tt-title-options">AVAILABLE SIZE:</div>
                                    <ul class="tt-options-swatch options-large">
                                       <li class="active"><a href="#" onclick="getprice('XXL',900,1000);">XXL</a></li>
                                       <li><a href="#" onclick="getprice('XL',950,1000);">XL</a></li>
                                       <li><a href="#" onclick="getprice('L',850,1000);">L</a></li>
                                       <!-- <li class="active"><a href="#">6</a></li> -->
                                    </ul>
                                 </div>
                                 <!-- <div class="tt-wrapper product-information-buttons">
                                    <a data-toggle="modal" data-target="#modalProductInfo" href="#">Size Guide</a> 
                                    <a data-toggle="modal" data-target="#modalProductInfo-02" href="#">Return Policies</a>
                                    </div> -->
                                 <div class="tt-wrapper">
                                    <div class="tt-title-options">AVAILABLE COLOR:</div>
                                    <ul class="tt-options-swatch options-large">
                                       <li class="active">
                                          <a class="options-color tt-color-bg-09" style="background-color:#f61335">red</a>
                                       </li>
                                       <li>
                                          <a onclick="window.location.href = 'http://localhost/tailor/view/TestCheckedPink.htm'; " class="options-color tt-color-bg-09" style="background-color:#f51689">Pink</a>
                                       </li>
                                       <!-- <li class="active"><a class="options-color tt-color-bg-10" href="#"></a></li> -->
                                    </ul>
                                 </div>
                              </div>
                              <div class="tt-wrapper">
                                 <form method="post" id="reviewform" style="background: none">
                                    <div class="tt-row-custom-01">
                                       <div class="col-item">
                                          <input type="hidden" name="product_id[]" value="d41d8cd98f00b204e9800998ecf8427e">
                                          <div class="tt-input-counter style-01"><span class="minus-btn"></span> 
                                             <input size="100" title="Numbers Only" name="qty[]" id="qty1" value="1" type="number">
                                             <span class="plus-btn" data-max="100"></span>
                                          </div>
                                       </div>
                                       <div class="col-item">
                                          <input type="hidden" name="sizeid" id="sizeid" value="XXL">
                                          <button type="button" data-pdid="42-XXL" title="Add to Cart" class="btn btn-lg add_cart_this_product" data-go-cart="1">
                                          ADD TO CART
                                          </button>
                                       </div>
                                    </div>
                                 </form>
                              </div>
                              
                              <div class="tt-wrapper dblock">
                                 <!-- ==========ask me======= -->
                                 <br><br>
                                 <!--              <input class="btn btn-xl" onclick="myFunction()" type="submit" value="Ask me?"> 
                                    -->
                                 <p id="askresponse"></p>
                                 <form method="post" id="askme" style="margin-top: 20px; display: none;">
                                    <input type="hidden" name="productid" id="productid" value="42">
                                    <input type="hidden" name="vendorid" id="vendorid" value="9">
                                    <input type="hidden" name="userid" id="userid" value="2">
                                    <textarea id="w3review" placeholder="ask me" name="askmessage" rows="12" cols="50" style="border:2px solid #2879fe;padding:5px;"></textarea>
                                    <br><br>
                                    <input class="btn btn-xl" type="submit" value="Submit">
                                 </form>
                              </div>
                              <script>
                                 function myFunction() {
                                   var x = document.getElementById("askme");
                                 
                                   if (x.style.display === "none") {
                                     x.style.display = "block";
                                     x.style.background = "none";
                                   } else {
                                     x.style.display = "none";
                                 
                                   }
                                 }
                              </script>
                              <!-- ================================= -->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include "require/footer.php"; ?>
<script>
   $(document).ready(function() {
   // alert('hai');
    $('.owl-carousel').owlCarousel({
   
   loop:true,
   margin:10,
   nav:true,
   responsive:{
   0:{
   items:1
   },
   600:{
   items:3
   
   },
   1000:{
   items:3
   }
   }
   })
   
     // alert('hai');
   })
</script>
<script type="text/javascript" src="js/vendors/jqueryy.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script src="js/highlight.js"></script>
<script src="js/app.js"></script>