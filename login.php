<?php include 'require/header.php';

if($_SESSION['RED_URL']!='' && $_SESSION['FUID']!='')
{
    $url=$_SESSION['RED_URL'];
 echo "string";
     echo "<script type='text/javascript'>
window.location.href = '$url';
</script>";    
}
if ($_SESSION['FUID'] != '') {
    $url=$fsitename . "myaccount.htm?uid=".$_SESSION['FUID'];
//header("location:" . $fsitename . "myaccount.htm?uid=".$_SESSION['FUID']);
    echo "string";
     echo "<script type='text/javascript'>
window.location.href = '$url';
</script>";
    exit;
}

if (isset($_REQUEST['loginsubmit'])) {
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
        $url = $fsitename . 'pages/myaccount.htm';
        // $url = $fsitename;
   $lmsg = checklogin($loginemail, $logpassword, $ip, $url);
   //echo $lmsg;
}
if (isset($_REQUEST['forgotsubmit'])) {
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
   $url = $fsitename . 'myaccount.htm';
    // $url = $fsitename;
    $lmsg = forgetpassword($forgotemail,  $ip);
   //echo $lmsg;
}

?>
<style>
    #forgotid {
        cursor: pointer;
    }
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li>Register</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container">
                    <h1 class="tt-title-subpages noborder">ALREADY REGISTERED?</h1>
                 
                   
                    <div class="tt-login-form">
                        
                        <div class="row justify-content-center">
                            
                            <!-- <div class="col-xs-12 col-md-6">
                                <div class="tt-item">
                                    <h2 class="tt-title">NEW CUSTOMER</h2>
                                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                                    <div class="form-group"><a href="<?php echo $fsitename;?>register.php" class="btn btn-top btn-border">CREATE AN ACCOUNT</a></div>
                                </div>
                            </div> -->
                            <div class="col-xs-12 col-md-6">
                                  <?php echo $lmsg;
            
            if($_SESSION['msg'])
            {
                echo $_SESSION['msg'];
                $_SESSION['msg']='';
            }

            ?>
<br>
                                <div class="tt-item" id="login">
                                    <h2 class="tt-title">LOGIN</h2>
                                    If you have an account with us, please log in.
                                    <div class="form-default form-top">
                                        <form id="customer_login1" name="login" method="post">
                                            <div class="form-group">
                                                <label for="loginemail">USERNAME OR E-MAIL *</label>
                                                <div class="tt-required">* Required Fields</div>
                                                <input type="email" required name="loginemail"  class="form-control" id="loginemail" placeholder="Enter Username or E-mail" />
                                            </div>
                                            <div class="form-group">
                                                <label for="logpassword">PASSWORD *</label> 
                                                <input type="password" required name="logpassword"  class="form-control" id="logpassword" placeholder="Enter Password" /></div>
                                            <div class="row">
                                                <div class="col-auto mr-auto">
                                                    <div class="form-group">
                                                        <button class="btn btn-border" type="submit" name="loginsubmit" id="loginsubmit">LOGIN</button></div>
                                                </div>
                                                <div class="col-auto mr-auto">
                                                    <div class="form-group"><a href="<?php echo $fsitename;?>register.php" class="btn btn-border">CREATE AN ACCOUNT</a></div>
                                                </div>
                                                <div class="col-auto align-self-end">
                                                    <div class="form-group">
                                                        <ul class="additional-links">
                                                            <li><a id="forgotid" onclick="logchange('1')">Lost your password?</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tt-item" id="forgotpassword" style="display: none;">
                                    <h2 class="tt-title">Forget Password </h2>
                                    <div class="form-default form-top">
                                        <form id="customer_login2" name="forgotpassword" method="post" action="" >
                                            <div class="form-group">
                                                <label for="forgotemail">USERNAME OR E-MAIL *</label>
                                                <div class="tt-required">* Required Fields</div>
                                                <input type="email" required name="forgotemail" class="form-control" id="forgotemail" placeholder="Enter Username or E-mail" />
                                            </div>
                                            <div class="row">
                                                <div class="col-auto mr-auto">
                                                    <div class="form-group">
                                                        <button class="btn btn-border" type="submit" name="forgotsubmit" id="forgotsubmit">RESET PASSWORD</button></div>
                                                </div>
                                                <div class="col-auto align-self-end">
                                                    <div class="form-group">
                                                        <ul class="additional-links">
                                                            <li><a id="forgotid"onclick="logchange('0')">Back to Login</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <br><br>
        <script>
        function logchange(a) {
        if (a == 1) {
            $("#getforgot").css("display", "none");
            $("#getlogin").css("display", "block");
            $('#login').hide();
            $('#forgotpassword').fadeToggle();
        } else {
            $("#getlogin").css("display", "none");
            $("#getforgot").css("display", "block");
            $('#forgotpassword').hide();
            $('#login').fadeToggle();

        }
    }
    </script>
        <?php include 'require/footer.php';?>