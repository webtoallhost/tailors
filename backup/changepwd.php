<?php include 'require/demoheader.php';
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'login.htm'.'">';
    exit;
}
//echo $_SESSION['FUID'];
if (isset($_REQUEST['updatepassword'])) {
    

     @extract($_REQUEST);

    $ip = $_SERVER['REMOTE_ADDR'];

    $pass = getcustomer('password', $_SESSION['FUID']);

    if ($pass == $currentpassword) {

        if ($newpassword == $conpassword) {

            $smsg = changepassword($newpassword, $ip, $_SESSION['FUID']);

        } else {
       
         $smsg = '<div class="erroralert"><span class="closebtn" onclick="this.parentElement.style.display='."'".none."'".';">&times;</span><strong>Passwords are Mismatch</strong></div>';
    
           }

    } else {
   $smsg = '<div class="erroralert"><span class="closebtn" onclick="this.parentElement.style.display='."'".none."'".';">&times;</span><strong>Current Password not Match</strong></div>';
     
    }
 

}
?>

<style>
    .Categories-list ul li {
    padding: 8px 0;
    display: block;
}
.Categories-list ul li a {
    color: #292929;
    font-size: 18px;
    line-height: 18px;
    text-decoration: none;
    text-transform: capitalize;
    transition: all 0.3s ease 0s;
}
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li>Account</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <h1 class="tt-title-subpages noborder">ACCOUNT</h1>
                    <div class="tt-shopping-layout">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 col-12"style="border: 1px solid #80808096;">
                                <h2 class="tt-title">User Settings</h2>
                                <div class="Categories-list">
                                     <ul>
                                            
                                            <li><a href="<?php $fsitename; ?>myaccount.htm?uid=<?php echo $_SESSION['FUID'];?>"><i class="icon-f-94"></i>&nbsp;My Account</a></li>
                                            <li><a href="<?php $fsitename; ?>editaccount.htm"><i class="icon-f-94"></i>&nbsp;Edit Profile</a></li>
                                             <li><a href="<?php $fsitename; ?>editmeasurement.htm"><i class="icon-f-94"></i>&nbsp;Edit Measurement</a></li>
                                            <li><a href="<?php $fsitename; ?>changepwd.htm" style="font-weight:bold;"><i class="icon-f-77"></i>&nbsp;Change Password</a></li>
                                            <li><a href="<?php $fsitename; ?>mywishlist.htm"><i class="icon-f-77"></i>&nbsp;My Wishlist</a></li>
                                            <li><a href="<?php $fsitename; ?>myorders.htm"><i class="icon-f-68"></i>&nbsp;View Orders</a></li>
                                            <li><a href="<?php $fsitename; ?>logout.htm"><i class="icon-f-77"></i>&nbsp;Logout</a></li>
                                            
                                        </ul>
                                       
                                       
                                    </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-12">
                                <div class="tt-wrapper">
                            <h6 class="tt-title">CHANGE PASSWORD</h6>
                            <?php echo $smsg; ?>
                            <br>
                            <?php
                if ($_SESSION['FUID'] != '') {
                $customer = FETCH_all("SELECT * FROM `customer` WHERE `CusID` = ?", $_SESSION['FUID']);
                //print_R($customer);
                }
            ?>   
                            <div class="form-default">
                                <form action="" method="post" autocomplete="off" onsubmit="return validatePassword();">
                                    <div class="form-group">
                                        <label for="currentpassword" class="control-label">ACCOUNT PASSWORD *</label> 
                                        <input type="password" class="form-control" name="currentpassword" id="currentpassword"  required="required" minlength="6" maxlength="55" value="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="newpassword" class="control-label">NEW PASSWORD *</label> 
                                        <input type="password" class="form-control" name="newpassword" id="newpassword"  required="required" minlength="6" maxlength="55" value="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="conpassword" class="control-label">CONFIRM PASSWORD *</label> 
                                        <input type="password" class="form-control" name="conpassword" id="conpassword"  required="required" value="" />
                                    </div>
                                    
                                    
                                    <div class="row tt-offset-21">
                                        <div class="col-auto">
                                            <button type="submit" class="btn" name="updatepassword" id="updatepassword">CHANGE PASSWORD</button>
                                        </div>
                                        <!-- <div class="col-auto align-self-center">or <a href="#" class="tt-link">Cancel</a></div> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>

        <script type="text/javascript">
        function validatePassword()
    {
        var password = document.getElementById("password"), conpassword = document.getElementById("confirmpassword");
        if (password.value != conpassword.value) {
            conpassword.setCustomValidity("Passwords Can't Match");
        } else {
            conpassword.setCustomValidity('');
        }
    }
    </script>