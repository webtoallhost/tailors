<?php include 'require/header.php';
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}
if (isset($_REQUEST['signupsubmit'])) {

    @extract($_REQUEST);

    $prodid = $_REQUEST['prodid'];

    $htry = $db->prepare("DELETE FROM `wishlist` WHERE `id`=?");

    $htry->execute(array($prodid));

    $msg = '1';

    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/mywishlist.htm' . '">';

    exit;
}
?>

<style>
    .Categories-list ul li {
    padding: 8px 0;
    display: block;
}
.Categories-list ul li a {
    color: #292929;
    font-size: 18px;
    line-height: 18px;
    text-decoration: none;
    text-transform: capitalize;
    transition: all 0.3s ease 0s;
}
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li>Account</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <h1 class="tt-title-subpages noborder">ACCOUNT</h1>
                    <div class="tt-shopping-layout">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 col-12"
                            style="border: 1px solid #80808096;">
                                <h2 class="tt-title">User Settings</h2>
                                <div class="Categories-list">
                                     <ul>
                                            
                                            <li><a href="<?php $fsitename; ?>myaccount.htm?uid=<?php echo $_SESSION['FUID'];?>"><i class="icon-f-94"></i>&nbsp;My Account</a></li>
                                            <li><a href="<?php $fsitename; ?>editaccount.htm"><i class="icon-f-94"></i>&nbsp;Edit Profile</a></li>
                                             <li><a href="<?php $fsitename; ?>editmeasurement.htm"><i class="icon-f-94"></i>&nbsp;Edit Measurement</a></li>
                                            <li><a href="<?php $fsitename; ?>changepwd.htm"><i class="icon-f-77"></i>&nbsp;Change Password</a></li>
                                            <li><a href="<?php $fsitename; ?>mywishlist.htm" style="font-weight:bold;"><i class="icon-f-77"></i>&nbsp;My Wishlist</a></li>
                                            <li><a href="<?php $fsitename; ?>myorders.htm"><i class="icon-f-68"></i>&nbsp;View Orders</a></li>
                                            <li><a href="<?php $fsitename; ?>logout.htm"><i class="icon-f-77"></i>&nbsp;Logout</a></li>
                                            
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-12">
                                <div class="tt-wrapper">
                            <h6 class="tt-title">MY WISHLIST</h6>
                           <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container">
                  
                   <?php
            $wlist = DB("SELECT * FROM `wishlist` WHERE `userid`='".$_SESSION['FUID']."' ORDER BY `id` DESC ");
            $wcount = mysqli_num_rows($wlist);
            if ($wcount != '0') {
             ?>
                    <div class="tt-wishlist-box" id="js-wishlist-removeitem">
                        <div class="tt-wishlist-list">
                             <?php while ($wlistng = mysqli_fetch_array($wlist)) { 
                             if(getproduct('image',$wlistng['wishlistid'])!='')  {
                              $imgpath = $fsitename.'images/product/' . getproduct('imagefolder',$wlistng['wishlistid']) . '/';   
                              $imgs= explode(',',getproduct('image',$wlistng['wishlistid']));
                              $imgres=$imgpath.$imgs['0'];
                              }
                             else {
                                $imgres=$fsitename.'images/noimage.png'; 
                             }
                             ?>
                            <div class="tt-item" >
                                <div class="tt-col-description">
                                    <div class="tt-img"><img src="<?php echo $imgres; ?>" alt="<?php echo getproduct('productname',$wlistng['wishlistid']); ?>" /></div>
                                    <div class="tt-description" style="margin-left: -36px;">
                                        <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo getproduct('link',$wlistng['wishlistid']); ?>.htm"><?php echo getproduct('productname',$wlistng['wishlistid']); ?>
                                             <div class="tt-price" style="padding-left: 25px;    margin-top: 10px;">
                                             <?php if(getproduct('sprice',$wlistng['wishlistid'])!='') { ?>
                                   <span class="new-price">$ <?php echo getproduct('sprice',$wlistng['wishlistid']); ?></span> <span class="old-price">$ <?php echo getproduct('price',$wlistng['wishlistid']); ?></span>
                                    <?php } else { ?>
                                    <span class="new-price">$ <?php echo getproduct('price',$wlistng['wishlistid']); ?></span>
                                 
                                    <?php } ?>
                                        </div>

                                        </a></h2>
                                       
                                    </div>
                                </div> <form name="cartdet" method="post" style="width: 701px;">
                            <input type="hidden" name="prodid" value="<?php echo $wlistng['id']; ?>">
                                <div class="tt-col-btn">
                                    <a href="#" class="tt-btn-addtocart add_cart_this_product" data-pdid="<?php echo $wlistng['wishlistid'].'-'.getsize('size',getproduct('size',$wlistng['wishlistid'])); ?>" data-toggle="modal" data-target="#modalAddToCartProduct<?php echo $wlistng['wishlistid']; ?>"><i class="icon-f-39"></i>ADD TO CART</a>
                                    <a class="btn-link" href="#" data-toggle="modal" data-target="#ModalquickView<?php echo $wlistng['wishlistid']; ?>"><i class="icon-f-73"></i>SEE PRODUCT</a>
                                    
                            <button type="submit" name="signupsubmit" class="btn-link js-removeitem" style="border:none;"><i class="icon-h-02"></i>REMOVE
                                </button>
                                  
                                </div>  </form>
                            </div>
                             <div class="modal fade" id="modalAddToCartProduct<?php echo $wlistng['wishlistid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>


                    <div class="modal-body">
                         <?php 
                                    $useid=$_SESSION['FUID'];
                                   $asd = DB_QUERY("SELECT SUM(`totprice`) AS `fftotprice`,`qty`,`totprice`,COUNT(*) AS totrecd FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$wlistng['wishlistid']."' AND `size`='".getsize('size',getproduct('size',$wlistng['wishlistid']))."' ");
                                 $asd1 = DB_QUERY("SELECT COUNT(*) AS totrecd,  SUM(`totprice`) AS `fftotprice` FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' ");
                                
                                 ?>
                        <div class="tt-modal-addtocart mobile">
                            <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                            <a href="<?php echo $fsitename; ?>listings.htm" class="btn-link btn-close-popup">CONTINUE SHOPPING</a> <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn-link">VIEW CART</a>
                        </div>
                        <div class="tt-modal-addtocart desctope">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                                    <div class="tt-modal-product">
                                        <div class="tt-img"><img src="<?php echo $fsitename; ?>images/loader.svg" data-src="<?php echo $imgres; ?>" alt="" /></div>
                                        <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo getproduct('link',$wlistng['wishlistid']); ?>.htm"><?php echo getproduct('productname',$wlistng['wishlistid']); ?></a></h2>
                                        <?php if($asd['qty']!='') { ?>
                                        <div class="tt-qty">QTY: <span><?php echo $asd['qty']; ?></span></div>
                                        <?php } ?>
                                    </div>
                                    <?php if($asd['fftotprice']!='') { ?>
                                    <div class="tt-product-total">
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd['fftotprice']; ?></span></div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-lg-6">
                                <?php if($asd1['fftotprice']!='') { ?>
                                    <a href="#" class="tt-cart-total">
                                        There are <?php echo $asd1['totrecd']; ?> items in your cart
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd1['fftotprice']; ?></span></div>
                                    </a>
                                <?php } ?>
                                    <a href="<?php echo $fsitename; ?>listings.htm" class="btn btn-border btn-close-popup">CONTINUE SHOPPING</a> 
                                    <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn">VIEW CART</a> 
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                          <div class="modal fade" id="ModalquickView<?php echo $wlistng['wishlistid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-6">
                                    <div class="tt-mobile-product-slider arrow-location-center">
                                    <?php foreach($imgs as $imgs11) { 
                                         $imgres=$imgpath.$imgs11;
                                        ?>    
                                    <div><img src="#" data-lazy="<?php echo $imgres; ?>" alt="" /></div>
                                    <?php } ?>
                                        <!--
								//video insertion template
								<div>
									<div class="tt-video-block">
										<a href="#" class="link-video"></a>
										<video class="movie" src="video/video.mp4" poster="video/video_img.jpg"></video>
									</div>
								</div> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-6">
                                    <div class="tt-product-single-info">
                                        <div class="tt-add-info">
                                            <ul>
                                                <li><span>SKU:</span><?php echo getproduct('item_code',$wlistng['wishlistid']); ?></li>
                                                <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                            </ul>
                                        </div>
                                        <h2 class="tt-title"><?php echo getproduct('productname',$wlistng['wishlistid']); ?></h2>
                                     <?php if(getproduct('sprice',$wlistng['wishlistid'])!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo getproduct('sprice',$wlistng['wishlistid']); ?></span> <span class="old-price">$ <?php echo getproduct('price',$wlistng['wishlistid']); ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo getproduct('price',$wlistng['wishlistid']); ?></span></div>
                                    <?php } ?>
                                        <!--<div class="tt-review">-->
                                        <!--    <div class="tt-rating"><i class="icon-star"></i> <i class="icon-star"></i> <i class="icon-star"></i> <i class="icon-star-half"></i> <i class="icon-star-empty"></i></div>-->
                                        <!--    <a href="#">(1 Customer Review)</a>-->
                                        <!--</div>-->
                                          <div class="tt-add-info">
                                        <ul>
                                            <li><span>Size:</span> <?php echo getsize('size',getproduct('size',$wlistng['wishlistid'])); ?></li>
                                            <li><span>Color:</span> <?php echo getcolor('color',getproduct('color',$wlistng['wishlistid'])); ?></li>
                                        </ul>
                                    </div>
                                        <div class="tt-wrapper"><?php echo getproduct('sortdescription',$wlistng['wishlistid']); ?></div>
                                     
                                      <?php if(getproduct('stock',$wlistng['wishlistid'])=='1') { ?>
                                        <div class="tt-wrapper">
                                            <div class="tt-row-custom-01">
                                                <div class="col-item">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn"></span> <input type="text" value="<?php echo $tempqty; ?>" size="5"  id="qty1" /> <span class="plus-btn"></span></div>
                                                </div>
                                                <div class="col-item">
                                            <?php if(isset($_SESSION['FUID'])) {
                                            $_SESSION['RED_URL']='';
                                            ?> 
                                            <a href="#" class="btn btn-lg add_cart_this_product" data-pdid="<?php echo $wlistng['wishlistid'].'-'.getsize('size',getproduct('size',$wlistng['wishlistid'])); ?>"><i class="icon-f-39"></i>ADD TO CART</a>
                                               <?php } else { 
                                                $_SESSION['RED_URL']=$fsitename;
                                               ?>
                                               <a href="<?php echo $fsitename; ?>pages/login.htm" class="btn btn-lg"><i class="icon-f-39"></i>ADD TO CART</a>
                                               <?php } ?>
                                               
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                           
                           <?php } ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="tt-wishlist-box" id="js-wishlist-removeitem">
                        <p align="center">No Records Available</p>
                    </div>    
                    <?php } ?>
                </div>
            </div>
        </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>