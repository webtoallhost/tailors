<?php include 'require/header.php';
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}
$norder = FETCH_all("SELECT * FROM `norder` WHERE `oid`=?", $_REQUEST['oid']);

if ($norderfetch['order_status'] == '0') {
    $odstatus = 'Order Placed';
} elseif ($norderfetch['order_status'] == '1') {
    $odstatus = 'Delivered';
} elseif ($norderfetch['order_status'] == '2') {
    $odstatus = 'Return Replacement';
} 
?>

<style>
    .Categories-list ul li {
    padding: 8px 0;
    display: block;
}
.Categories-list ul li a {
    color: #292929;
    font-size: 18px;
    line-height: 18px;
    text-decoration: none;
    text-transform: capitalize;
    transition: all 0.3s ease 0s;
}
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li>Account</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <h1 class="tt-title-subpages noborder">ORDER</h1>
                    <div class="tt-shopping-layout">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 col-12"style="border: 1px solid #80808096;">
                                <h2 class="tt-title">User Settings</h2>
                                <div class="Categories-list">
                                         <ul>
                                            
                                            <li><a href="<?php $fsitename; ?>myaccount.htm?uid=<?php echo $_SESSION['FUID'];?>"><i class="icon-f-94"></i>&nbsp;My Account</a></li>
                                            <li><a href="<?php $fsitename; ?>editaccount.htm"><i class="icon-f-94"></i>&nbsp;Edit Profile</a></li>
                                             <li><a href="<?php $fsitename; ?>editmeasurement.htm"><i class="icon-f-94"></i>&nbsp;Edit Measurement</a></li>
                                            <li><a href="<?php $fsitename; ?>changepwd.htm"><i class="icon-f-77"></i>&nbsp;Change Password</a></li>
                                            <li><a href="<?php $fsitename; ?>mywishlist.htm"><i class="icon-f-77"></i>&nbsp;My Wishlist</a></li>
                                            <li><a href="<?php $fsitename; ?>myorders.htm" style="font-weight:bold;"><i class="icon-f-68"></i>&nbsp;View Orders</a></li>
                                            <li><a href="<?php $fsitename; ?>logout.htm"><i class="icon-f-77"></i>&nbsp;Logout</a></li>
                                            
                                        </ul>
                                      
                                      
                                    </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-12">
                                <div class="tt-wrapper">
                                <div class="tt-table-responsive">
                                    <div class="row">
                                    <div class="col-md-12" align="right"><a href="<?php echo $fsitename; ?>pages/myorders.htm"><h5>Back to Listing</h5></a></div>

</div>
                                <div class="panel-heading">
                            <h5>Customer Details </h5>
                        </div>
                        <div class="panel-body">
                            <!--<div class="row">-->
                            <!--    <div class="col-md-12" style="padding-bottom: 10px;">-->
                            <!--        <a href="<?php echo $fsitename ?>MPDF/<?php echo $norder['oid']; ?>/orderinvoice.htm" target="_blank" style="font-size: 21px;float:right">View Invoice</a>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                             <tr>
                                                <td colspan="2">
                                                    <?php echo $norder['cus_name']; ?>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td colspan="2">
                                                    <?php echo $norder['cus_email']; ?>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td colspan="2">
                                                    <?php echo $norder['cus_mobile']; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <?php echo $norder['billing_address']; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <table   class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Order ID : 
                                                </td>
                                                <td>
                                                    <?php echo $norder['order_id']; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Date : 
                                                </td>
                                                <td>
                                                    <?php echo date("d-M-Y h:i:s A", strtotime($norder['datetime'])) ?>
                                                </td>
                                            </tr>
                                            
                                            <!--<tr>
                                                <td>
                                                    Currency : 
                                                </td>
                                                <td>
                                                    <?php //echo $norder['currency']; ?>
                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>
                                                    Order Status : 
                                                </td>
                                                <td>
<?php if ($norder['order_status'] == '0') { echo "Order Placed"; }
					   else if ($norder['order_status'] == '1') { echo "Delivered";}
					   else if ($norder['order_status'] == '2') { echo "Return Replacement";}
					   
?>
                                                </td>
                                            </tr>
                                            <!--<tr>
                                                <td>
                                                    Agent Code : 
                                                </td>
                                                <td>
                                                    <?php //echo $norder['agent_code']; ?>
                                                </td>
                                            </tr>-->

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <!-- <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            <h5>Other Informations</h5>
                        </div>
                        <div class="panel-body">  
                            <div class="col-md-6">
                                <label>Order Comments</label><br>
                                <?php echo stripslashes($norder['order_comments']); ?>          
                            </div>
                         
                        </div>
                    </div>    
                   <br> -->
                    <div class="panel panel-info" id="comp_details_fields">
                        <div class="panel-heading">
                            <h5>Product Information</h5>
                        </div>
                        <div class="panel-body">  
                            <div class="row">
                            <div class="col-md-12">
                            <table class="tt-table-shop-01" style="margin-top:0px;">
                                 
                                    <thead>
                                        <tr>
                                        <th width="25%">Product</th>
                                            <th width="25%">Product Name</th>
                                            <th width="15%" align="right">Price</th>
                                            <th width="10%" align="center">Qty</th>
                                            <th width="15%" align="right">Amount</th>
                                        </tr>           	
                                    </thead>
                                    <tbody>
                                        <?php
                                        $row = pFETCH("SELECT * FROM `order` WHERE `Order_id`=?", $norder['oid']);
                                        while ($frow = $row->fetch(PDO::FETCH_ASSOC)) {
                                            $im = explode(",", getproduct('image', $frow['product_id']));
                   
                                            ?>
                                            <tr> 
                                            <td style="vertical-align:top">
                                            <?php if($im[0]!='') { ?>
                                                                <img src="<?php echo $fsitename . 'images/product/' . getproduct('imagefolder', $frow['product_id']) . '/' . $im[0]; ?>" alt="<?php echo getproduct('productname', $frow['product_id']); ?>" style="width:100px; float:left;" />
<?php } else { ?>
                                                                <img src="<?php echo $fsitename . 'images/noimage1.png'; ?>" alt="<?php echo getproduct('productname', $frow['product_id']); ?>" style="width:100px; float:left;" />
                                                                <?php } ?>
                                        </td>
                                                
                                                <td style="vertical-align:top"><?php echo $frow['product_name']; ?></td>
                                                <td style="vertical-align:top"> $&nbsp;<?php echo number_format($frow['product_price'], 2, '.', '') ?></td>
                                                <td align="left" style="vertical-align:top"><?php echo $frow['product_qty']; ?></td>
                                                <td align="left" style="vertical-align:top;padding-right: 10px;">  $&nbsp;<?php echo number_format($frow['product_total_price'], 2, '.', '') ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                            <div class="col-md-6">

                                <h5>Payment Method : <?php echo $norder['payment_type'].'('.$norder['payment_mode'].')'; ?></h5>
<?php  if ($ptype != '-'){?>
                                <p>Applied Promo Code : <?php     echo $norder['promotional_code'];
                                ?> </p>
<?php  }  ?>

                            </div>
                            <div class="col-md-6">
                                <td width="50%;" align="right">
                                    <table style="width:100%; font-size:13px;">
                                        <tr>
                                            <td style="border-bottom:1px dotted #cc6600;"><b>SubTotal ( $) :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right"><?php echo number_format($norder['subtotal'], '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:1px dotted #cc6600;"><b>Discount ( $) :</b></td><td style="border-bottom:1px dotted #cc6600;" align="right"><?php echo number_format($norder['discounted_amount'], '2', '.', ''); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:1px dotted #cc6600;"><b>Shipping ( $) :</b></td><td  style="border-bottom:1px dotted #cc6600;"align="right"><?php echo str_replace('$','',$norder['shipping_charge']); ?></td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:1px dotted #cc6600;"><b>Total ( $) :</b></td><td style="border-bottom:1px dotted #cc6600;" align="right"><?php echo number_format($norder['over_all_total'], '2', '.', ''); ?></td>
                                        </tr>
                                    </table>
                                </td>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>