<?php include 'require/header.php';
if (isset($_REQUEST['submit'])) {
    
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
    
    
        $msg = addcontact($name, $email, $mobile, $subject, $message, $ip);
    
}

?>
<style>
    .form-default .form-control {
    border: 1px solid #5e9afe;
}


/*04.01.2021*/

#contactform1 {
   background: black;
   
}
#bdd{
    background: #2879fe!important;
    color: white;
}
/*04.01.2021*/

</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li>Contact</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent nomargin">
                <div class="tt-contact-box">
                    <img class="img-mobile" src="images/custom/contact-img-01.jpg" alt="" />
                    <div class="container container-fluid-custom-mobile-padding">
                        <h1 class="tt-title">
                            WE ARE LOOKING FORWARD<br />
                            TO HEARING FROM YOU
                        </h1>
                        <address>
                            Address: <?php echo getprofile('address', 1); ?>,<br />
                            Phone: +<?php echo getprofile('phonenumber', 1); ?>
                        </address>
                    </div>
                </div>
            </div>
            <div class="container-indent">
                <div class="container">
                    <!-- <div class="contact-map"><div id="map"></div></div> -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125766.12980003307!2d78.05278255426747!3d9.91799869040521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b00c582b1189633%3A0xdc955b7264f63933!2sMadurai%2C%20Tamil%20Nadu!5e0!3m2!1sen!2sin!4v1604738507494!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-contact02-col-list">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 ml-sm-auto mr-sm-auto">
                                <div class="tt-contact-info">
                                    <i class="tt-icon icon-f-93"></i>
                                    <h6 class="tt-title">LET’S HAVE A CHAT!</h6>
                                    <address>
                                        +<?php echo getprofile('phonenumber', 1); ?>
                                    </address>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="tt-contact-info">
                                    <i class="tt-icon icon-f-24"></i>
                                    <h6 class="tt-title">VISIT OUR LOCATION</h6>
                                    <address>
                                       <?php echo getprofile('address', 1); ?>
                                    </address>
                                </div>
                            </div>
                            <!--<div class="col-sm-6 col-md-4">-->
                            <!--    <div class="tt-contact-info">-->
                            <!--        <i class="tt-icon icon-f-92"></i>-->
                            <!--        <h6 class="tt-title">WORK TIME</h6>-->
                            <!--        <address>-->
                            <!--            7 Days a week<br />-->
                            <!--            from 10 AM to 6 PM-->
                            <!--        </address>-->
                            <!--    </div>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <form id="contactform1" class="contact-form form-default" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name (required)" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Your Email (required)" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Mobile" required="required" />
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject" required="required" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" rows="7" placeholder="Your Message" id="message" required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" name="submit" class="btn" id="bdd">SEND MESSAGE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>
         <script>
      $(document).ready(function(){
          setTimeout(function() {
              <?php
              if($msg!='') {
                  echo 'alert("Message sent successfully");';
              }
              
              
              ?>
              }, 500);
      });
  </script>

