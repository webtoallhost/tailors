<?php include 'require/header.php';
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}

?>

<style>
    .Categories-list ul li {
    padding: 8px 0;
    display: block;
}
.Categories-list ul li a {
    color: #292929;
    font-size: 18px;
    line-height: 18px;
    text-decoration: none;
    text-transform: capitalize;
    transition: all 0.3s ease 0s;
}
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li>Account</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <h1 class="tt-title-subpages noborder">ORDER</h1>
                    <div class="tt-shopping-layout">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 col-12"style="border: 1px solid #80808096;">
                                <h2 class="tt-title">User Settings</h2>
                                <div class="Categories-list">
                                         <ul>
                                            
                                            <li><a href="<?php $fsitename; ?>myaccount.htm?uid=<?php echo $_SESSION['FUID'];?>"><i class="icon-f-94"></i>&nbsp;My Account</a></li>
                                            <li><a href="<?php $fsitename; ?>editaccount.htm"><i class="icon-f-94"></i>&nbsp;Edit Profile</a></li>
                                             <li><a href="<?php $fsitename; ?>editmeasurement.htm"><i class="icon-f-94"></i>&nbsp;Edit Measurement</a></li>
                                            <li><a href="<?php $fsitename; ?>changepwd.htm"><i class="icon-f-77"></i>&nbsp;Change Password</a></li>
                                            <li><a href="<?php $fsitename; ?>mywishlist.htm"><i class="icon-f-77"></i>&nbsp;My Wishlist</a></li>
                                            <li><a href="<?php $fsitename; ?>myorders.htm" style="font-weight:bold;"><i class="icon-f-68"></i>&nbsp;View Orders</a></li>
                                            <li><a href="<?php $fsitename; ?>logout.htm"><i class="icon-f-77"></i>&nbsp;Logout</a></li>
                                            
                                        </ul>
                                      
                                      
                                    </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-12">
                                <div class="tt-wrapper">
                                <div class="tt-table-responsive">
                                     <?php
            $wlist = DB("SELECT * FROM `norder` WHERE `CusID`='".$_SESSION['FUID']."' ORDER BY `oid` DESC ");
            $wcount = mysqli_num_rows($wlist);
            if ($wcount != '0') {
             ?>
                                <table class="tt-table-shop-01" style="margin-top:0px;">
                                    <thead>
                                        <tr>
                                            <th>ORDER</th>
                                            <th>DATE</th>
                                            <th>DELIVERY STATUS</th>
                                            <th>ORDER STATUS</th>
                                            <th>TOTAL</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php while ($wlistng = mysqli_fetch_array($wlist)) { 
                                        $chk = FETCH_all("SELECT count(*) as crecod FROM `order` WHERE `Order_id`=?", $wlistng['oid']);
                                       ?>
                                        <tr>
                                            <td>#<?php echo $wlistng['oid']; ?></td>
                                            <td><?php echo date('d-M-Y',strtotime($wlistng['datetime'])); ?></td>
                                            <td><?php echo $wlistng['delivered_status']; ?></td>
                                            <td>
                                                <?php
                                                if ($wlistng['order_status'] == '0') { echo "Order Placed"; }
                                                else if ($wlistng['order_status'] == '1') { echo "Delivered";}
                                                else if ($wlistng['order_status'] == '2') { echo "Return Replacement";}
                                                
                                                ?>
                                            </td>
                                            <td>$<?php echo $wlistng['over_all_total']; ?> for <?php echo $chk['crecod']; ?> item</td>
                                          <td><a href="<?php echo $fsitename; ?>pages/vieworder.htm?oid=<?php echo $wlistng['oid']; ?>">View</a></td>
                                        </tr>
                                       <?php } ?>
                                    </tbody>
                                </table>
                                <?php } else { ?>
                                <p align="center">No Orders Found</p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>