<?php include 'require/header.php';

$_SESSION['FUID']=$_REQUEST['uid'];
//echo $_SESSION['FUID'];
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}

?>

<style>
    .Categories-list ul li {
    padding: 8px 0;
    display: block;
}
.Categories-list ul li a {
    color: #292929;
    font-size: 18px;
    line-height: 18px;
    text-decoration: none;
    text-transform: capitalize;
    transition: all 0.3s ease 0s;
}
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Account</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <h1 class="tt-title-subpages noborder">ACCOUNT</h1>
                    <div class="tt-shopping-layout">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 col-12"style="border: 1px solid #80808096;">
                                <h2 class="tt-title">User Settings</h2>
                                <div class="Categories-list">
                                     <ul>
                                            
                                            <li><a href="<?php $fsitename; ?>myaccount.htm?uid=<?php echo $_SESSION['FUID'];?>" style="font-weight:bold;"><i class="icon-f-94"></i>&nbsp;My Account</a></li>
                                            <li><a href="<?php $fsitename; ?>editaccount.htm"><i class="icon-f-94"></i>&nbsp;Edit Profile</a></li>
                                             <li><a href="<?php $fsitename; ?>editmeasurement.htm"><i class="icon-f-94"></i>&nbsp;Edit Measurement</a></li>
                                            <li><a href="<?php $fsitename; ?>changepwd.htm"><i class="icon-f-77"></i>&nbsp;Change Password</a></li>
                                            <li><a href="<?php $fsitename; ?>mywishlist.htm"><i class="icon-f-77"></i>&nbsp;My Wishlist</a></li>
                                            <li><a href="<?php $fsitename; ?>myorders.htm"><i class="icon-f-68"></i>&nbsp;View Orders</a></li>
                                            <li><a href="<?php $fsitename; ?>logout.htm"><i class="icon-f-77"></i>&nbsp;Logout</a></li>
                                            
                                        </ul>
                                     
                                     
                                    </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-12" >
                                   <div class="tt-wrapper">
                                <?php
                if ($_SESSION['FUID'] != '') {
                $customer = FETCH_all("SELECT * FROM `customer` WHERE `CusId` = ?", $_SESSION['FUID']);
                //print_R($customer);
                }
            ?>   
                                <h2 class="tt-title">Welcome <?php echo $customer['fname'];?> :)</h2>
                                <!-- <h2 class="tt-title">Last Login at : <?php echo date('M d, Y', strtotime(getcustomerlog('Logintime', $_SESSION['FUID']))); ?></h2>
                                <div class="tt-wrapper">
                                    <a href="#" class="btn">ADD A NEW ADDRESS</a><br />
                                    <a href="account.html" class="tt-link-back"><i class="icon-h-46"></i> RETURN TO ACCOUNT PAGE</a>
                                </div> -->
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>