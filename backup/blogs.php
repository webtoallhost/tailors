<?php
$page = '24';
$selid = '4';
include "require/header.php";
include "require/paging1.php";
$alink = strtok($actual_link, '?');
$alink = $alink . '?filter';
?>
<div class="tt-breadcrumb">
         <div class="container">
            <ul>
               <li><a href="<?php echo $fsitename; ?>">Home</a></li>
               <li>Blog</li>
            </ul>
         </div>
      </div>
      <div id="tt-pageContent">
         <div class="container-indent">
            <div class="container container-fluid-custom-mobile-padding">
               <h1 class="tt-title-subpages noborder">THE BLOG</h1>
               <div class="row">
                  <div class="col-sm-12 col-md-8 col-lg-9">
            <?php
            $s = '';
                        if ($_REQUEST['bid'] != '' && $_REQUEST['bid'] != 'pages') {
                            $bcategory = FETCH_all("SELECT * FROM `blogcategory` WHERE `link` = ?", $_REQUEST['bid']);
                            $bcid = $bcategory['bcid'];
                            $s = " AND FIND_IN_SET($bcid,`category`)";
                        }
                        $sql = "SELECT * FROM `blog` WHERE `status`=?" . $s;
                        $paging = new paging($sql, '5');
                        $sqlt = $paging->sql;
                        $spro = $db->prepare($sqlt);
                        $spro->execute(array('1'));
                        $bcount = $spro->rowcount();

                        if ($bcount != '0') { ?>
                     <div class="tt-listing-post">
                          <?php  while ($bloglist = $spro->fetch()) {
                            $bimage=$fsitename.'images/blog/'.$bloglist['image'];
                            ?>
                        <div class="tt-post">
                           <div class="tt-post-img"><a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><img src="images/loader.svg" data-src="<?php echo $bimage; ?>" alt="<?php echo stripslashes($fblogs['title']); ?>"></a></div>
                           <div class="tt-post-content">
                              <div class="tt-tag">
                               <?php
                               $expcat=explode(',',$bloglist['category']);
                               foreach($expcat as $expcat1)
                               {
                               $links[]='<a href="'.$fsitename . getblogcategory("link",$expcat1) . '/blogs.htm">'.getblogcategory("category",$expcat1).'</a>';    
                               }
                               $impl=implode(',',array_unique($links));
                               echo $impl;
                               ?>  
                           
                             </div>
                              <h2 class="tt-title"><a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><?php echo stripslashes($bloglist['title']); ?></a></h2>
                              <div class="tt-description"><?php echo stripslashes(substr($bloglist['description'], 0, 120)); ?></div>
                              <div class="tt-btn"><a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm" class="btn">READ MORE</a></div>
                           </div>
                        </div>
                          <?php } ?>
                       <div class="tt-pagination">
                           <?php echo $paging->show_paging($fsitename . 'pages/blogs.htm?filter'); ?>       
                           <!-- <a href="#" class="btn-pagination btn-prev"></a> -->
                          
                        </div>
                     </div>
            <?php } ?>
                  </div>
                  <div class="col-sm-12 col-md-4 col-lg-3 rightColumn">
                     <div class="tt-block-aside">
                        <h3 class="tt-aside-title">CATEGORIES</h3>
                        <div class="tt-aside-content">
                           <ul class="tt-list-row">
                                <?php
                                $b = '1';
                                $blogs = $db->prepare("SELECT * FROM `blogcategory` WHERE `status`= ? ORDER BY `order` ASC");
                                $blogs->execute(array('1'));
                                $bcount = $blogs->rowcount();
                                if ($bcount != '0') {
                                while ($blogsfet = $blogs->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                               <li>
                              <a href="<?php echo $fsitename . $blogsfet['link'] . '/blogs'; ?>.htm" <?php if ($blogsfet['link'] == $_REQUEST['bid']) { ?> style="font-weight:bold;" <?php } ?>>
                                                <?php echo $blogsfet['category']; ?>

                                            </a>
                               </li>
                                <?php } } ?>
                             
                           </ul>
                        </div>
                     </div>
                     <div class="tt-block-aside">
                        <h3 class="tt-aside-title">RECENT POST</h3>
                        <div class="tt-aside-content">
                           <div class="tt-aside-post">
                                <?php
                                $b = '1';
                                $blogs = $db->prepare("SELECT * FROM `blog` WHERE `status`= ? ORDER BY `bid` DESC");
                                $blogs->execute(array('1'));
                                $bcount = $blogs->rowcount();
                                if ($bcount != '0') {
                                while ($blogsfet = $blogs->fetch(PDO::FETCH_ASSOC)) {
                                    ?>
                              <div class="item">
                                 <div class="tt-tag">
                                 <?php
                               $expcat=explode(',',$blogsfet['category']);
                               foreach($expcat as $expcat1)
                               {
                               $links[]='<a href="'.$fsitename . getblogcategory("link",$expcat1) . '/blogs.htm">'.getblogcategory("category",$expcat1).'</a>';    
                               }
                               $impl=implode(',',array_unique($links));
                               echo $impl;
                               ?>  

                                 </div>
                                 <a href="<?php echo $fsitename . 'view-' . $blogsfet['link']; ?>/blog.htm">
                                    <div class="tt-title"><?php echo stripslashes($blogsfet['title']); ?></div>
                                    <div class="tt-description"><?php echo stripslashes(substr($blogsfet['description'], 0, 120)); ?></div>
                                 </a>
                                 
                                 
                              </div>
                                <?php } } ?>
                             </div>
                        </div>
                     </div>
                   </div>
               </div>
            </div>
         </div>
      </div>
      
<?php 
include "require/footer.php";
?>