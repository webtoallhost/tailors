<?php include "require/blackheader.php"; ?>
        <div id="tt-pageContent">
             <?php
            $banner = DB("SELECT * FROM `banner` WHERE `status`='1' AND `image`!='' ORDER BY `order` ASC ");
            $bcount = mysqli_num_rows($banner);
            if ($bcount != '0') {
             ?>
             
             
<!--changes overlayer full site css     -->
             <style>
             #loader-wrapper {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 77;
    background-image: url("https://wallpaper-house.com/data/out/8/wallpaper2you_251840.jpg");
    background-size:1520px 800px;
    background-color: white;
    opacity: 1;
    display: block;
}

.tt-product:not(.tt-view) .tt-image-box .tt-btn-wishlist:before {
    content: '\eb16';
    font-family: wokiee;
    font-size: 16px;
    line-height: 1;
    margin-top: 1px;
   
}
/*==================banner css             */
.slider-revolution .tp-dottedoverlay {
    border-radius: 57px;
}
      .tp-simpleresponsive>ul>li {

    border-radius: 69px;
}    
@media (min-width: 1025px){
.tt-layout-product-item:not(.tt-view) .tt-collection-item, .tt-layout-product-item:not(.tt-view) .tt-product, .tt-layout-product-item:not(.tt-view) .tt-product-design02 {
    margin-top: 38px;
    border-radius: 35px;
}
    .tt-product:not(.tt-view) .tt-image-box .tt-btn-wishlist.active, .tt-product:not(.tt-view) .tt-image-box .tt-btn-wishlist:hover {
    background: #110c09;
    color: #fff;
}
    
}
.tt-blog-thumb .tt-img img {
    border-radius: 22px;
}
.tt-blog-thumb .tt-title-description>:nth-child(1) {
    border-radius: 23px;
}
/*=============radius border===========*/
.tt-promo-box {
    border-radius: 39px;
}
.tt-promo-box:hover{
    border-radius: 39px;
        box-shadow: 4px -4px 8px #818291;
}

/*===============image hover==== */
.btn {
 background: #110c09;
    font-family: "Rubik",sans-serif;
    border: 1px solid #fff;
    color: #fff;
    font-size: 14px;
    line-height: 1;
    font-weight: 400;
    letter-spacing: .03em;
    position: relative;
    outline: 0;
    padding: 6px 31px 4px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    height: 40px;
    cursor: pointer;
    border-radius: 6px;
    transition: color .2s linear,background-color .2s linear;
}
.btn:hover {
    background: transparent;
    color: #fff;
    outline: 0;
    box-shadow:0px 0px 4px #fff;
}
.containerimgbton {
  position: relative;
  width: 50%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

/*====button*/

.slider-revolution [class^=btn] {
    position: inherit;
    background-color: transparent;
    color: #fff;
    border: 1px solid;
    font-size: 20px;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.slider-revolution [class^=btn]:hover {
    background-color: #110c09;
    color: #fff;
}
.containerimgbton:hover .image {
  opacity: 0.3;
}

.containerimgbton:hover .middle {
  opacity: 1;
}

.text {
  background-color: #4CAF50;
  color: white;
  font-size: 16px;
  padding: 16px 32px;
}

::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    #909;
}
/*===============image hover==== */
             
            body {
    font-family: "Rubik",sans-serif !important;
    font-size: 14px;
    line-height: 22px;
    font-weight: 300;
    color: #2c67c8;
    background-image: url(../../images/bodybg/background1.jpg);
    background-size: cover;
    /*background: center 0 no-repeat #110c09d9;*/
    margin: 0;
    overflow-x: hidden;
    direction: ltr;
} 

.header-popup-bg {
    z-index: 0;
}
/*buttoncss */

.tt-base-color {
    color: #fefefe;
}
.tt-promo-box.tt-one-child .tt-description .tt-description-wrapper .tt-background {
   background: rgb(52 48 45 / 0%);
    color: #110c09;
    border: 1px solid;
}
.tt-promo-box.tt-one-child .tt-description .tt-description-wrapper .tt-background:hover {
    /*background: rgb(40 121 254) !important;*/
    /*color:#fff;*/
    background: rgb(17 12 9) !important;
    color: #fff;
    
}

.tt-promo-box .tt-description .tt-title-small {
    /*font-family: Hind,sans-serif;*/
    /*color: #ffffff;*/
        color: #110c09;
    font-size: 18px;
    font-weight: 700;
}

.tt-promo-box .tt-description .tt-title-small:hover {
    /*font-family: Hind,sans-serif;*/
     /*background: rgb(40 121 254) !important;*/
    color: #ffffff;
}
@media (min-width: 790px){
footer .tt-color-scheme-01 {
    background: #110c0921;
    color: #f7f8fa;
}

footer .tt-color-scheme-01 .tt-collapse-content {
    color: #fff;
}

@media (min-width: 790px){
footer .tt-color-scheme-01 .tt-collapse-content a {
    color: #dadada;
}
footer .tt-footer-custom .tt-logo-col+.tt-col-item .tt-box-copyright {
    color: white;
}
}
footer .tt-color-scheme-01 .tt-collapse-title, footer .tt-color-scheme-01 .tt-collapse-title a {
    color: #ffffff;
}
}   

@media (min-width: 790px){
footer .tt-color-scheme-01 .tt-list li a, footer .tt-color-scheme-01 .tt-mobile-collapse .tt-collapse-content .tt-list li a {
    color: #fff;
}
footer .tt-color-scheme-01 address span {
    color: #ffffff;
}
}

.tt-services-block .tt-col-icon {
    align-self: flex-start;
    font-size: 50px;
    color: #ffffff;
}


@media (min-width: 790px){
footer .tt-color-scheme-02 {
    background: #ffffff;
    color: #000;
}

footer .tt-color-scheme-02 .tt-social-icon li a {
    color: #000;
}
     }
     @media (min-width: 1025px){
.tt-desktop-header {
    display: block;
    background: #110c09;
}
   .stuck.tt-stuck-nav {
    background: #110c09;
    border-bottom: 1px solid #ffffff;
}                  
}

header .tt-dropdown-obj .tt-dropdown-toggle {
    color: #ffffff;
}

.tt-services-block .tt-col-description .tt-title {
    color: #ffffff;
    
}
.tt-services-block .tt-col-description {
    flex: 2 1 auto;
    line-height: 1.3;
    color: #fff;
}
.tt-block-title .tt-title {
    color: #f4f4f4;
}
.tt-block-title .tt-description {
    color: #f4f4f4;
}
/*====card show index=*/
.tt-promo-fixed {
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: flex-start;
    align-content: stretch;
    align-items: stretch;
    background-color: #1e1d1eed;
    box-shadow: 0 0 10px rgb(255 255 255 / 74%);
    padding: 10px;
    overflow: hidden;
    position: fixed;
    bottom: 20px;
    left: 20px;
    z-index: 6;
    margin-right: 20px;
    max-width: 360px;
    border-radius: 5px;
    opacity: 0;
}

.tt-promo-fixed .tt-description a {
    color: #f5f4f4;
    display: inline-block;
    transition: color .2s linear;
}

.tt-promo-fixed .tt-description {
    color: #f7f8fa;
}
.tt-promo-fixed .tt-btn-close{
    color: #fff;
}

/*=======card section end======*/
element.style {
}
.tt-stuck-nav .tt-desctop-menu:not(.tt-hover-02) nav>ul li.dropdown:first-child>a {
    padding-left: 0;
}
.tt-desctop-menu:not(.tt-hover-02) li.dropdown.active>a, .tt-desctop-menu:not(.tt-hover-02) li.dropdown.selected>a, .tt-desctop-menu:not(.tt-hover-02) li.dropdown>a:hover {
    color: #ffffff;
    background: 0 0;
}

.tt-desctop-menu:not(.tt-hover-02) li.dropdown>a {
    color: #ffffff;
    background: 0 0;
}
.slider-revolution .tp-dottedoverlay {
    z-index: 1;
    background: #000000a1;
}
.slider-revolution .tp-caption1 {
    text-align: center;
    color: #ffffff;
}
             </style>
            <div class="container-indent nomargin">
                <div class="container-fluid">
                    <div class="row">
                        <div class="slider-revolution revolution-default">
                            <div class="tp-banner-container">
                                <div class="tp-banner revolution">
                                    <ul>
                                        <?php while ($bannerlist = mysqli_fetch_array($banner)) { ?>
                                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                                            <img
                                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAAMgAQMAAAD4P+14AAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAPdJREFUeNrswYEAAAAAgKD9qRepAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABg9uBAAAAAAADI/7URVFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVWFPTgQAAAAAADyf20EVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVpDw4JAAAAAAT9f+0JIwAAAAAAAAAAALAJ8T4AAZAZiOkAAAAASUVORK5CYII="
                                                data-lazyload="<?php echo $fsitename.'images/banner/'.$bannerlist['image']; ?>"
                                                alt="slide1"
                                                data-bgposition="center center"
                                                data-bgfit="cover"
                                                data-bgrepeat="no-repeat"
                                            />
                                          <div
                                                class="tp-caption tp-caption1 lft stb"
                                                data-x="center"
                                                data-y="center"
                                                data-hoffset="0"
                                                data-voffset="0"
                                                data-speed="600"
                                                data-start="900"
                                                data-easing="Power4.easeOut"
                                                data-endeasing="Power4.easeIn"
                                            >
                                                <div class="tp-caption1-wd-1 tt-base-color"><?php echo $bannerlist['title']; ?>
                                                </div>
                                               
                                              
                                              <div class="tp-caption1-wd-3" style="font-size:20px;"><?php echo $bannerlist['content']; ?></div>
                                              <?php if($bannerlist['link']!='') { ?>
                                                <div class="tp-caption1-wd-4"><a href="<?php echo $bannerlist['link']; ?>" target="_blank" class="btn btn-xl" data-text="SHOP NOW!">SHOP NOW!</a></div>
                                              <?php }  ?>
                                          </div>
                                            
                                        </li>
                                        <?php } ?>
                                       </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="container-indent0">
                <div class="container-fluid">
                    <div class="row tt-layout-promo-box">
                        <div class="col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',1); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',1); ?>" alt="<?php echo getquickbanner('title',1); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',1); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?php echo getquickbanner('link',2); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',2); ?>" alt="<?php echo getquickbanner('title',2); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',2); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',3); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',3); ?>" alt="<?php echo getquickbanner('title',3); ?>"/>
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',3); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',4); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',4); ?>" alt="<?php echo getquickbanner('title',4); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',4); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="<?php echo getquickbanner('link',5); ?>" class="tt-promo-box tt-one-child hover-type-2">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',5); ?>" alt="<?php echo getquickbanner('title',5); ?>"/>
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',5); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-12">
                                    <a href="<?php echo getquickbanner('link',6); ?>" class="tt-promo-box tt-one-child">
                                        <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/quickbanner/<?php echo getquickbanner('image',6); ?>" alt="<?php echo getquickbanner('title',6); ?>" />
                                        <div class="tt-description">
                                            <div class="tt-description-wrapper">
                                                <div class="tt-background"></div>
                                                <div class="tt-title-small"><?php echo getquickbanner('title',6); ?></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
                    <style>
                    
   @media (max-width: 480px){
    .via{
                     position: unset !important;
    left: 0 !important;
    margin-top: 0 !important;
                }
                
                .mp{
           width: 140px;
        margin-left: 7px;
                }
                .hert{
     position: absolute;
    margin-top: 39px !important;
                }
                #w3review{
                    width:262px;
                }
}
     .via{
                 position: absolute;
                 left: 75%;
                 margin-top: -57px;
                }
                media (max-width: 1024px){
.tt-product-design02:not(.tt-view) .tt-description, .tt-product:not(.tt-view) .tt-description {
    margin-top: 18px;
    position: absolute;
}
      }  
      
      @media (max-width: 1024px){
.tt-product-design02:not(.tt-view) .tt-image-box .tt-img img, .tt-product:not(.tt-view) .tt-image-box .tt-img img {
   
    height: 250px !important;
}
}
      </style>
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-block-title">
                        <h1 class="tt-title">RECENT ARRIVALS</h1>
                        <div class="tt-description">TOP VIEW IN THIS WEEK</div>
                         <a href="<?php echo $fsitename; ?>listing.htm?offer=new"><input class="btn btn-xl via"  type="button" value="View All"></a>
                    </div>
                    
                    <div class="row tt-layout-product-item">
                         <?php
                            $trending = DB("SELECT * FROM `product` WHERE `status`='1' AND `new`='1' ORDER BY `order` ASC  LIMIT 4");
                            $tcount = mysqli_num_rows($trending);
                            if ($tcount != '0') {
                            while ($trendinglist = mysqli_fetch_array($trending)) {
                              if($trendinglist['image']!='')  {
                              $imgpath = $fsitename.'images/product/' . $trendinglist['imagefolder'] . '/';   
                              $imgs= explode(',',$trendinglist['image']);
                              $imgres=$imgpath.$imgs['0'];
                              }
                             else {
                                $imgres=$fsitename.'images/noimage.png'; 
                             }
                            ?>
                          <div class="col-6 col-md-4 col-lg-3">
                            <div class="tt-product thumbprod-center">
                                <div class="tt-image-box ">
                                    <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $trendinglist['pid']; ?>" data-tooltip="Quick View" data-tposition="left"></a>
                                   <?php if(isset($_SESSION['FUID'])) { 
                                    $_SESSION['RED_URL']='';
                                     $getwish = DB_QUERY("SELECT * FROM `wishlist` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `wishlistid`='".$trendinglist['pid']."' ");
                                     if($getwish['id']!='') { $wcls="tt-btn-wishlist add_wishlist_product active";  } else { $wcls='tt-btn-wishlist add_wishlist_product'; }    
                                    ?>
                                    <a href="#" class="<?php echo $wcls; ?>" data-pdid="<?php echo $trendinglist['pid']; ?>" data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } else {
                                     $_SESSION['RED_URL']=$fsitename;
                                    ?>
                                    <!--onclick="window.location.href = '<?php echo $fsitename; ?>pages/login.htm';"-->
                                     <a  class="tt-btn-wishlist haiclick"  data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } ?>
                                    <!--<a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>-->
                                    <a href="<?php echo $fsitename; ?>view/<?php echo $trendinglist['link']; ?>.htm">
                                        <span class="tt-img"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['productname']; ?>" /></span>
                                        <span class="tt-img-roll-over"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $trendinglist['productname']; ?>" /></span>
                                         <?php if($trendinglist['discounttype']=='percentage') { ?>
                                         <span class="tt-label-location"><span class="tt-label-sale">Discount <?php echo $trendinglist['discount']; ?>%</span></span>
                                         <?php } ?>
                                    </a>

                                </div>
                                <div class="tt-description">
                                    <div class="tt-row">
                                        <ul class="tt-add-info">
                                            <li><a href="<?php echo $fsitename . getmtype('link',getcategory('type',$trendinglist['cid'])) .'/'. getcategory('link',$trendinglist['cid']).'.htm'; ?>"><?php echo getcategory('category',$trendinglist['cid']); ?></a></li>
                                        </ul>
                                       <div class="tt-rating">
                                                        <?php   $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $trendinglist['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                  $emptrat=5-$avgrat;
                  ?>
                                                <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>     </div>
                                    </div>
                                    <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $trendinglist['link']; ?>.htm"><?php echo $trendinglist['productname']; ?></a></h2>
                                    <?php if($trendinglist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$ <?php echo $trendinglist['sprice']; ?></span> <span class="old-price">$ <?php echo $trendinglist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$ <?php echo $trendinglist['price']; ?></span></div>
                                 
                                    <?php } ?>
                                    <div class="tt-product-inside-hover">
                                        
                                        <?php
                                            $getqtyc = DB_QUERY("SELECT * FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$trendinglist['pid']."' AND `size`='".getsize('size',$trendinglist['size'])."' ");
                                            if($getqtyc['id']!='') { 
                                                $tempqty=$getqtyc['qty'];
                                               
                                            }
                                            else
                                            {
                                               $tempqty  =1;  
                                            }
                                            ?>
                                             
                                            
                                        <div class="tt-row-btn">
                                            <?php if($trendinglist['stock']=='1') { ?>
                                             <input size="100"  title="Numbers Only" name="qty[]" id="qty1"  value="<?php echo $tempqty; ?>" type="hidden"/>
                                        <?php if(isset($_SESSION['FUID'])) { 
                                        $_SESSION['RED_URL']='';
                                        ?> 
                                             <button type="button" data-pdid="<?php echo $trendinglist['pid'].'-'.getsize('size',$trendinglist['size']); ?>"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg add_cart_this_product" data-toggle="modal" data-target="#modalAddToCartProduct<?php echo $trendinglist['pid']; ?>" data-go-cart='1'>
                                       ADD TO CART
                                        </button>
                                        <?php } else {
                                        $_SESSION['RED_URL']=$fsitename;
                                        ?>
                                         <button type="button"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg haiclick mp" data-toggle="modal">
                                       ADD TO CART
                                        </button>
                                        <?php } ?>
                                          <!-- modal (AddToCartProduct) -->
                                          <?php } ?>
                                        <div class="tt-row-btn hert">
                                            
                                            <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $trendinglist['pid']; ?>"></a> <a href="#" class="tt-btn-wishlist"></a> 
                                            <!--<a href="#" class="tt-btn-compare"></a>-->
                                        </div>
                                              
                                              </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modalAddToCartProduct<?php echo $trendinglist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                         <?php 
                                    $useid=$_SESSION['FUID'];
                                   $asd = DB_QUERY("SELECT SUM(`totprice`) AS `fftotprice`,`qty`,`totprice`,COUNT(*) AS totrecd FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$trendinglist['pid']."' AND `size`='".getsize('size',$trendinglist['size'])."' ");
                                 $asd1 = DB_QUERY("SELECT COUNT(*) AS totrecd,  SUM(`totprice`) AS `fftotprice` FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' ");
                                
                                 ?>
                        <div class="tt-modal-addtocart mobile">
                            <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                           <a href="<?php echo $fsitename; ?>listings.htm" class="btn-link btn-close-popup">CONTINUE SHOPPING</a> 
                           <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn-link">VIEW CART</a>
                        </div>
                        <div class="tt-modal-addtocart desctope">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                                    <div class="tt-modal-product">
                                        <div class="tt-img"><img src="<?php echo $fsitename; ?>images/loader.svg" data-src="<?php echo $imgres; ?>" alt="" /></div>
                                        <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $trendinglist['link']; ?>.htm"><?php echo $trendinglist['productname']; ?></a></h2>
                                        <div class="tt-qty">QTY: <span><?php echo $asd['qty']; ?></span></div>
                                    </div>
                                    <div class="tt-product-total">
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd['fftotprice']; ?></span></div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <a href="#" class="tt-cart-total">
                                        There are <?php echo $asd1['totrecd']; ?> items in your cart
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd1['fftotprice']; ?></span></div>
                                    </a>
                                    <a href="<?php echo $fsitename; ?>listings.htm" class="btn btn-border btn-close-popup">CONTINUE SHOPPING</a> 
                                    <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn">VIEW CART</a> 
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                        <div class="modal fade" id="ModalquickView<?php echo $trendinglist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-6">
                                    <div class="tt-mobile-product-slider arrow-location-center">
                                    <?php foreach($imgs as $imgs11) { 
                                         $imgres=$imgpath.$imgs11;
                                        ?>    
                                    <div><img src="#" data-lazy="<?php echo $imgres; ?>" alt="" /></div>
                                    <?php } ?>
                                        <!--
								//video insertion template
								<div>
									<div class="tt-video-block">
										<a href="#" class="link-video"></a>
										<video class="movie" src="video/video.mp4" poster="video/video_img.jpg"></video>
									</div>
								</div> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-6">
                                    <div class="tt-product-single-info">
                                        <div class="tt-add-info">
                                            <ul>
                                                <li><span>SKU:</span><?php echo $trendinglist['item_code']; ?></li>
                                                <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                            </ul>
                                        </div>
                                        <h2 class="tt-title"><?php echo $trendinglist['productname']; ?></h2>
                                     <?php if($trendinglist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $trendinglist['sprice']; ?></span> <span class="old-price">$ <?php echo $trendinglist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $trendinglist['price']; ?></span></div>
                                    <?php } ?>
                                       <div class="tt-rating">
                <?php   $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $trendinglist['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                  $emptrat=5-$avgrat;
                  ?>
                                                <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>     </div>
                                          <div class="tt-add-info">
                                        <ul>
                                            <li><span>Size:</span> <?php echo getsize('size',$trendinglist['size']); ?></li>
                                            <li><span>Color:</span> <?php echo getcolor('color',$trendinglist['color']); ?></li>
                                        </ul>
                                    </div>
                                        <div class="tt-wrapper"><?php echo $trendinglist['sortdescription']; ?></div>
                                     
                                      <?php if($trendinglist['stock']=='1') { ?>
                                        <div class="tt-wrapper">
                                            <div class="tt-row-custom-01">
                                                <div class="col-item">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn"></span> <input type="text" value="<?php echo $tempqty; ?>" size="5"  id="qty1" /> <span class="plus-btn"></span></div>
                                                </div>
                                                <div class="col-item">
                                            <?php if(isset($_SESSION['FUID'])) {
                                            $_SESSION['RED_URL']='';
                                            ?> 
                                            <a href="#" class="btn btn-lg add_cart_this_product" data-pdid="<?php echo $trendinglist['pid'].'-'.getsize('size',$trendinglist['size']); ?>"><i class="icon-f-39"></i>ADD TO CART</a>
                                               <?php } else { 
                                                $_SESSION['RED_URL']=$fsitename;
                                               ?>
                                               <a href="#" class="btn btn-lg haiclick"><i class="icon-f-39"></i>ADD TO CART</a>
                                               <?php } ?>
                                               
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                            <?php } } ?>
                        
                    </div>
                </div>
            </div>
            <div class="container-indent">
                <div class="container-fluid-custom">
                    <div class="row tt-layout-promo-box">
                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo getofferbanner('link',1); ?>" class="tt-promo-box">
                                <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',1); ?>" alt="<?php echo getofferbanner('title',1); ?>" />
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small"><?php echo getofferbanner('title',1); ?></div>
                                        <div class="tt-title-large"><?php echo getofferbanner('content',1); ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo getofferbanner('link',2); ?>" class="tt-promo-box">
                                <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',2); ?>" alt="<?php echo getofferbanner('title',2); ?>" />
                                <div class="tt-description">
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small"><?php echo getofferbanner('title',2); ?></div>
                                        <div class="tt-title-large"><?php echo getofferbanner('content',2); ?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <a href="<?php echo getofferbanner('link',3); ?>" class="tt-promo-box">
                                <img src="images/loader.svg" data-src="<?php echo $fsitename; ?>images/offer/<?php echo getofferbanner('image',3); ?>" alt="<?php echo getofferbanner('title',3); ?>" />
                                <div class="tt-description">
                                    <div class="tt-background"></div>
                                    <div class="tt-description-wrapper">
                                        <div class="tt-background"></div>
                                        <div class="tt-title-small"><?php echo getofferbanner('title',3); ?></div>
                                        <div class="tt-title-large"><span class="tt-base-color"><?php echo getofferbanner('content',3); ?></span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <div class="tt-block-title">
                        <h2 class="tt-title">POPULAR SEARCH</h2>
                        <div class="tt-description">TOP SALE IN THIS WEEK</div>
                        <a href="<?php echo $fsitename; ?>listing.htm?offer=deal"> <input class="btn btn-xl via"  type="button" value="View All"></a>
                    </div>
                    <div class="row tt-layout-product-item">
                         <?php
                            $bestseller = DB("SELECT * FROM `product` WHERE `status`='1' AND `deal`='1' ORDER BY `order` ASC  LIMIT 4");
                            $bcount = mysqli_num_rows($bestseller);
                            if ($bcount != '0') {
                            while ($bestsellerlist = mysqli_fetch_array($bestseller)) {
                              if($bestsellerlist['image']!='')  {
                              $imgpath = $fsitename.'images/product/' . $bestsellerlist['imagefolder'] . '/';   
                              $imgs= explode(',',$bestsellerlist['image']);
                              $imgres=$imgpath.$imgs['0'];
                              }
                             else {
                                $imgres=$fsitename.'images/noimage.png'; 
                             }
                            ?>
                            <?php
                                            $getqtyc = DB_QUERY("SELECT * FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$bestsellerlist['pid']."' AND `size`='".getsize('size',$bestsellerlist['size'])."' ");
                                            if($getqtyc['id']!='') { 
                                                $tempqty=$getqtyc['qty'];
                                               
                                            }
                                            else
                                            {
                                               $tempqty  =1;  
                                            }
                                            ?>
                                           
                        <div class="col-6 col-md-4 col-lg-3">
                            <div class="tt-product thumbprod-center">
                                <div class="tt-image-box">
                                      <input size="100"  title="Numbers Only" name="qty[]" id="qty1"  value="<?php echo $tempqty; ?>" type="hidden"/>
                                    <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $bestsellerlist['pid']; ?>" data-tooltip="Quick View" data-tposition="left"></a>
                                     <?php if(isset($_SESSION['FUID'])) { 
                                    $_SESSION['RED_URL']='';
                                     $getwish = DB_QUERY("SELECT * FROM `wishlist` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `wishlistid`='".$bestsellerlist['pid']."' ");
                                     if($getwish['id']!='') { $wcls="tt-btn-wishlist add_wishlist_product active";  } else { $wcls='tt-btn-wishlist add_wishlist_product'; }    
                                    ?>
                                    <a href="#" class="<?php echo $wcls; ?>" data-pdid="<?php echo $bestsellerlist['pid']; ?>" data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } else {
                                     $_SESSION['RED_URL']=$fsitename;
                                    ?>
                                    <!--onclick="window.location.href = '<?php echo $fsitename; ?>pages/login.htm';"-->
                                     <a  class="tt-btn-wishlist haiclick"  data-tooltip="Add to Wishlist" data-tposition="left"></a> 
                                    <?php } ?>
                                    <!--<a href="#" class="tt-btn-compare" data-tooltip="Add to Compare" data-tposition="left"></a>-->
                                    <a href="<?php echo $fsitename; ?>view/<?php echo $bestsellerlist['link']; ?>.htm">
                                        <span class="tt-img"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $bestsellerlist['productname']; ?>" /></span>
                                        <span class="tt-img-roll-over"><img src="images/loader.svg" data-src="<?php echo $imgres; ?>" alt="<?php echo $bestsellerlist['productname']; ?>" /></span>
                                        <?php if($bestsellerlist['discounttype']=='percentage') { ?>
                                         <span class="tt-label-location"><span class="tt-label-sale">Discount <?php echo $bestsellerlist['discount']; ?>%</span></span>
                                         <?php } ?>
                                    </a>
                                    
                                </div>
                                <div class="tt-description">
                                    <div class="tt-row">
                                        <ul class="tt-add-info">
                                            <li><a href="<?php echo $fsitename . getmtype('link',getcategory('type',$bestsellerlist['cid'])) .'/'. getcategory('link',$bestsellerlist['cid']).'.htm'; ?>"><?php echo getcategory('category',$bestsellerlist['cid']); ?></a></li>
                                        </ul>
                                        <div class="tt-rating">
                                           
                                            <?php   $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $bestsellerlist['pid'],'1');
                                                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                                                  $emptrat=5-$avgrat;
                                                  ?>
                                                <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>     </div>
                                    </div>
                                    <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $bestsellerlist['link']; ?>.htm"><?php echo $bestsellerlist['productname']; ?></a></h2>
                                   <?php if($bestsellerlist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['sprice']; ?></span> <span class="old-price">$ <?php echo $bestsellerlist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['price']; ?></span></div>
                                 
                                    <?php } ?>
                                    <div class="tt-product-inside-hover">
                                         <?php if($bestsellerlist['stock']=='1') { ?>
                                        <div class="tt-row-btn">
                                          <?php if(isset($_SESSION['FUID'])) {
                                          $_SESSION['RED_URL']='';
                                          ?> 
                                        <button type="button" data-pdid="<?php echo $bestsellerlist['pid'].'-'.getsize('size',$bestsellerlist['size']); ?>"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg add_cart_this_product" data-toggle="modal" data-target="#modalAddToCartProduct<?php echo $bestsellerlist['pid']; ?>" data-go-cart='1'>
                                       ADD TO CART
                                        </button>
                                        <?php } else { 
                                         $_SESSION['RED_URL']=$fsitename;
                                        ?>
                                         <button type="button"  title="Add to Cart" class="tt-btn-addtocart thumbprod-button-bg haiclick"  data-toggle="modal">
                                       ADD TO CART
                                        </button>
                                        <?php } ?>
                                        </div>
                                        <?php } ?>
                                        <div class="tt-row-btn">
                                            <a href="#" class="tt-btn-quickview" data-toggle="modal" data-target="#ModalquickView<?php echo $bestsellerlist['pid']; ?>"></a> <a href="#" class="tt-btn-wishlist"></a> 
                                            <!--<a href="#" class="tt-btn-compare"></a>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
 <div class="modal fade" id="modalAddToCartProduct<?php echo $trendinglist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                         <?php 
                                    $useid=$_SESSION['FUID'];
                                   $asd = DB_QUERY("SELECT SUM(`totprice`) AS `fftotprice`,`qty`,`totprice`,COUNT(*) AS totrecd FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' AND `productid`='".$bestsellerlist['pid']."' AND `size`='".getsize('size',$bestsellerlist['size'])."' ");
                                 $asd1 = DB_QUERY("SELECT COUNT(*) AS totrecd,  SUM(`totprice`) AS `fftotprice` FROM `tempcart` WHERE `userid`='" . $_SESSION['FUID'] . "' ");
                                
                                 ?>
                        <div class="tt-modal-addtocart mobile">
                            <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                           <a href="<?php echo $fsitename; ?>listings.htm" class="btn-link btn-close-popup">CONTINUE SHOPPING</a> 
                           <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn-link">VIEW CART</a>
                        </div>
                        <div class="tt-modal-addtocart desctope">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="tt-modal-messages"><i class="icon-f-68"></i> Added to cart successfully!</div>
                                    <div class="tt-modal-product">
                                        <div class="tt-img"><img src="<?php echo $fsitename; ?>images/loader.svg" data-src="<?php echo $imgres; ?>" alt="" /></div>
                                        <h2 class="tt-title"><a href="<?php echo $fsitename; ?>view/<?php echo $bestsellerlist['link']; ?>.htm"><?php echo $bestsellerlist['productname']; ?></a></h2>
                                        <div class="tt-qty">QTY: <span><?php echo $asd['qty']; ?></span></div>
                                    </div>
                                    <div class="tt-product-total">
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd['fftotprice']; ?></span></div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <a href="#" class="tt-cart-total">
                                        There are <?php echo $asd1['totrecd']; ?> items in your cart
                                        <div class="tt-total">TOTAL: <span class="tt-price">$<?php echo $asd1['fftotprice']; ?></span></div>
                                    </a>
                                    <a href="<?php echo $fsitename; ?>listings.htm" class="btn btn-border btn-close-popup">CONTINUE SHOPPING</a> 
                                    <a href="<?php echo $fsitename; ?>pages/cart.htm" class="btn">VIEW CART</a> 
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                      
                         <!-- modal (quickViewModal) -->
        <div class="modal fade" id="ModalquickView<?php echo $bestsellerlist['pid']; ?>" tabindex="-1" role="dialog" aria-label="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="icon icon-clear"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="tt-modal-quickview desctope">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-6">
                                    <div class="tt-mobile-product-slider arrow-location-center">
                                    <?php foreach($imgs as $imgs11) { 
                                         $imgres=$imgpath.$imgs11;
                                        ?>    
                                    <div><img src="#" data-lazy="<?php echo $imgres; ?>" alt="" /></div>
                                    <?php } ?>
                                        <!--
								//video insertion template
								<div>
									<div class="tt-video-block">
										<a href="#" class="link-video"></a>
										<video class="movie" src="video/video.mp4" poster="video/video_img.jpg"></video>
									</div>
								</div> -->
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-6">
                                    <div class="tt-product-single-info">
                                        <div class="tt-add-info">
                                            <ul>
                                                <li><span>SKU:</span><?php echo $bestsellerlist['item_code']; ?></li>
                                                <!-- <li><span>Availability:</span> 40 in Stock</li> -->
                                            </ul>
                                        </div>
                                        <h2 class="tt-title"><?php echo $bestsellerlist['productname']; ?></h2>
                                     <?php if($bestsellerlist['sprice']!='') { ?>
                                   <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['sprice']; ?></span> <span class="old-price">$ <?php echo $bestsellerlist['price']; ?></span></div>
                                    <?php } else { ?>
                                    <div class="tt-price"><span class="new-price">$<?php echo $bestsellerlist['price']; ?></span></div>
                                    <?php } 
                                          $reviewdetails = FETCH_all("SELECT count(*) as avgg,sum(`rating`) as clrate,count(`comments`) as totcmts FROM `productreview` WHERE `product` = ? AND `status`=? ", $bestsellerlist['pid'],'1');
                  $avgrat=$reviewdetails['clrate']/$reviewdetails['avgg']; 
                  $emptrat=5-$avgrat;
                                    ?>
                                    <div class="tt-review">
                                    <div class="tt-rating">
                                    <?php for($i=1;$i<=$avgrat;$i++) { ?>    
                                    <i class="icon-star"></i> 
                                    <?php } ?>
                                      <?php for($i=1;$i<=$emptrat;$i++) { ?>  
                                    <i class="icon-star-empty"></i>
                                    <?php } ?>
                                    </div>
                                    <a class="product-page-gotocomments-js">(<?php echo $reviewdetails['totcmts']; ?> Customer Review)</a>
                                </div>
                                        <div class="tt-add-info">
                                        <ul>
                                            <li><span>Size:</span> <?php echo getsize('size',$bestsellerlist['size']); ?></li>
                                            <li><span>Color:</span> <?php echo getcolor('color',$bestsellerlist['color']); ?></li>
                                        </ul>
                                    </div>
                                        <div class="tt-wrapper"><?php echo $bestsellerlist['sortdescription']; ?></div>
                                     <?php if($bestsellerlist['stock']=='1') { ?>
                                        <div class="tt-wrapper">
                                            <div class="tt-row-custom-01">
                                                <div class="col-item">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn"></span> <input type="text" value="<?php echo $tempqty; ?>" id="qty1" size="5" /> <span class="plus-btn"></span></div>
                                                </div>
                                                <div class="col-item">
                                              
                                               <?php if(isset($_SESSION['FUID'])) {
                                               $_SESSION['RED_URL']='';
                                               ?> 
                                                    <a href="#" class="btn btn-lg add_cart_this_product" data-pdid="<?php echo $bestsellerlist['pid'].'-'.getsize('size',$bestsellerlist['size']); ?>"><i class="icon-f-39"></i>ADD TO CART</a>
                                                    <?php } else {
                                                     $_SESSION['RED_URL']=$fsitename;
                                                    ?>
                                                    <a href="#" class="btn btn-lg haiclick"><i class="icon-f-39"></i>ADD TO CART</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                            <?php } } ?>
                    </div>
                </div>
            </div>
            <?php
            $blog = DB("SELECT * FROM `blog` WHERE `status`='1' AND `image`!='' ORDER BY `order` ASC LIMIT 3 ");
            $bcount = mysqli_num_rows($blog);
            if ($bcount != '0') { ?>
            <div class="container-indent">
                <div class="container">
                    <div class="tt-block-title">
                        <h2 class="tt-title">LATES FROM BLOG</h2>
                        <div class="tt-description">THE FRESHEST AND MOST EXCITING NEWS</div>
                    </div>
                    <div class="tt-blog-thumb-list">
                        <div class="row">
                            <?php  while ($bloglist = mysqli_fetch_array($blog)) { 
                            $bimage=$fsitename.'images/blog/'.$bloglist['image'];
                            ?>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="tt-blog-thumb">
                                    <div class="tt-img">
                                        <a href="<?php echo $fsitename . 'view-' . $bloglist['link']; ?>/blog.htm"><img src="images/loader.svg" data-src="<?php echo $bimage; ?>" alt="<?php echo stripslashes($fblogs['title']); ?>" /></a>
                                    </div>
                                    <div class="tt-title-description">
                                        <div class="tt-background"></div>
                                        <div class="tt-tag"><a href="<?php echo $fsitename . getblogcategory('link',$bloglist['category']) . '/blogs'; ?>.htm"><?php echo getblogcategory('category',$bloglist['category']); ?></a></div>
                                        <div class="tt-title"><a href="<?php echo $fsitename . 'view/' . $bloglist['link']; ?>/blog.htm"><?php echo stripslashes($bloglist['title']); ?></a></div>
                                        <p><?php echo stripslashes(substr($bloglist['description'], 0, 120)); ?></p>
                                       
                                        
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                          </div>
                    </div>
                </div>
            </div>
            <?php } ?>
<div class="container-indent">
                <div class="container">
                    <div class="row tt-services-listing">
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <a href="#" class="tt-services-block">
                                <div class="tt-col-icon"><i class="icon-f-48"></i></div>
                                <div class="tt-col-description">
                                    <h4 class="tt-title">FREE SHIPPING</h4>
                                    <p>Free shipping on all US order or order above $99</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <a href="#" class="tt-services-block">
                                <div class="tt-col-icon"><i class="icon-f-35"></i></div>
                                <div class="tt-col-description">
                                    <h4 class="tt-title">SUPPORT 24/7</h4>
                                    <p>Contact us 24 hours a day, 7 days a week</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4">
                            <a href="#" class="tt-services-block">
                                <div class="tt-col-icon"><i class="icon-e-09"></i></div>
                                <div class="tt-col-description">
                                    <h4 class="tt-title">30 DAYS RETURN</h4>
                                    <p>Simply return it within 24 days for an exchange.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "require/blackfooter.php"; ?>      
<?php 
// include "require/footer.php";
?> 