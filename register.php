<?php
$page = '14';
$getstatic = '1';
include 'require/header.php';
if ($_SESSION['FUID'] != '') {
    header("location:" . $fsitename . "pages/myaccount.htm");
    exit;
}

if (isset($_REQUEST['create'])) {
    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
  
    $chk = FETCH_all("SELECT * FROM `customer` WHERE `emailid`=?", $emailid);
    if($chk['emailid'] != ''){
     $smsg = '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-close"></i> Already a user registered with this email</h4></div>';
    }  else {
            $smsg = popupsignupp($fname,$emailid,$password);
            
              //  echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/login.htm' . '">';
                header("location:" . $fsitename . "pages/login.htm?su=reg");
                exit;
        }
      
  
}
?>
<style>
    #forgotid {
        cursor: pointer;
    }
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Register</li>
                </ul>
            </div>
        </div>
          <div id="tt-pageContent">
        <div class="container-indent">
            <form name="regform" id="regform" method="post">
            <div class="container">
                <h1 class="tt-title-subpages noborder">CREATE AN ACCOUNT</h1>
                <div class="row">
                <div class="col-md-12"> <div class="tt-login-form">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-6">
                            <div class="tt-item">
                              
                                <div class="form-default">
                                 
                                        <div class="form-group">
                                            <label for="loginInputName">USER NAME *</label>
                                            <div class="tt-required">* Required Fields</div>
                                            <input type="text" name="fname" required="required" class="form-control" id="loginInputName" placeholder="Enter First Name">
                                        </div>
                                        <div class="form-group"><label for="loginInputEmail">E-MAIL *</label><span style="color:red;" id="emailerror"></span>
                                        <input type="email" name="emailid" required="required" class="form-control" id="emailid" placeholder="Enter E-mail"></div>
                                        <div class="form-group"><label for="loginInputPassword">PASSWORD *</label> 
                                        <input type="text" name="password" required="required" class="form-control" id="loginInputPassword" placeholder="Enter Password"></div>
                                      <div class="col-auto">
                                                <div class="form-group"><button class="btn btn-border" type="submit" name="create" id="create">CREATE</button></div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div>
                
                </div>
                <br>
             
            </div>
            </form>
        </div>
    </div>
     <script src="<?php echo $fsitename; ?>js/jquery.min.js"></script>
        <script>
        $(document).ready(function(){
  $("#emailid").keyup(function(){
       var emailid = $(this).val();
  $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {emailid: emailid},
            success: function (data) {
              $("#emailerror").html(data);
              
            }
        });    
 
  });
    $("#mobileno").keyup(function(){
       var mobileno = $(this).val();
  $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {mobileno: mobileno},
            success: function (data) {
              $("#mblerror").html(data);
              
            }
        });    
 
  });
});
        function logchange(a) {
        if (a == 1) {
            $("#getforgot").css("display", "none");
            $("#getlogin").css("display", "block");
            $('#login').hide();
            $('#forgotpassword').fadeToggle();
        } else {
            $("#getlogin").css("display", "none");
            $("#getforgot").css("display", "block");
            $('#forgotpassword').hide();
            $('#login').fadeToggle();

        }
    }
     function getstate(a)
    {
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {country: a},
            success: function (data) {
              $("#state").html(data);
              
            }
        });
    }
     function getcity(a)
    {
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {state: a},
            success: function (data) {
              $("#city").html(data);
              
            }
        });
    }
    </script>
  
   
        <?php include 'require/footer.php';?>