$(function() {

    $('form[name="cart-form"] input').on('keypress', function(e) {
        if (e.which === 13) {
            e.preventDefault();
            //  $('#updtj').trigger('click');
        }
    });

    //Checkout same bill address


    $('#keyword1').keyup(function() {
        setTimeout(function() {
            //var a = $('input[name="searchBox"]').val();
            var a = $('#keyword1').val();
            var srchcategory = $('#srchcategory1').val();
            if (a != '') {
                //alert(a);
                $.ajax({
                    type: "POST",
                    url: sitename + "getproduct.php", //Local
                    data: { a: a, srchcategory: srchcategory },
                    beforeSend: function() {
                        $(".autocomplete-suggestions").css("display", "block").html('<i class="fa fa-spinner fa-spin"></i>');
                        /*$("#autocomplete-suggestions").html('<div class="autocomplete-suggestions" id="autocomplete-suggestions" style="position: absolute; max-height: 300px; z-index: 9999; top: 70px; left: 566.5px; width: 360px; display: none;">
                         <div class="autocomplete-suggestions" data-index="0"></div></div>');*/
                    },
                    success: function(data) {
                        if (a != '') {
                            $("#autocomplete-suggestionss").css("display", "block");
                            $("#autocomplete-suggestionss").html(data);
                        }
                    }
                });
            } else {
                $("#autocomplete-suggestionss").css("display", "none");
                //$(".autocomplete-suggestions").html(data);
            }
        }, 500);
    });

    $('#keyword').keyup(function() {
        setTimeout(function() {
            //var a = $('input[name="searchBox"]').val();
            var a = $('#keyword').val();
            var srchcategory = $('#srchcategory').val();
            if (a != '') {
                //alert(a);
                $.ajax({
                    type: "POST",
                    url: sitename + "getproduct.php", //Local
                    data: { a: a, srchcategory: srchcategory },
                    beforeSend: function() {
                        $(".autocomplete-suggestions").css("display", "block").html('<i class="fa fa-spinner fa-spin"></i>');
                        /*$("#autocomplete-suggestions").html('<div class="autocomplete-suggestions" id="autocomplete-suggestions" style="position: absolute; max-height: 300px; z-index: 9999; top: 70px; left: 566.5px; width: 360px; display: none;">
                         <div class="autocomplete-suggestion" data-index="0"></div></div>');*/
                    },
                    success: function(data) {
                        if (a != '') {
                            $("#autocomplete-suggestions").css("display", "block");
                            $("#autocomplete-suggestions").html(data);
                        }
                    }
                });
            } else {
                $("#autocomplete-suggestions").css("display", "none");
                //$(".autocomplete-suggestions").html(data);
            }
        }, 500);
    });

    $('input[name="sameaddress"]').change(function() {

        if ($("input#sameaddress11").is(':checked')) {

            var fname = $(this).data('val', $('#sfirstname').val());
            var saddress1 = $(this).data('address', $('#saddress1').val());
            var saddress2 = $(this).data('address1', $('#saddress2').val());
            var spostcode = $(this).data('postcode', $('#spostcode').val());
            var state = $(this).data('state', $('#state').val());
            var shipcity = $(this).data('shipcity', $('#shipcity').val());
            var sphone = $(this).data('sphone', $('#sphone').val());
            var mobile = $(this).data('smobile', $('#smobile').val());
            var country = $(this).data('scountry', $('#scountry').val());
            var ss = $(this).data('samt', $('#shamt').val());
            $('#sfirstname').val($('#fname').val());
            $('#saddress1').val($('#address1').val());
            $('#saddress2').val($('#address2').val());
            $('#spostcode').val($('#cupostcode').val());
            $('#state').val($('#shipstate').val());
            $('#shipcity').val($('#city').val());
            $('#sphone').val($('#cuphone').val());
            $('#smobile').val($('#cumobile').val());
            $('#scountry').val($('#country').val());
            var ss = $('#shamt').val();
            $('#sa').html(ss);

            if ($('#state').val() == '') {
                $('#sa').html('0.00');
                var cd = $('#cd').html().replace(/,/g, ''),
                    cd = +cd;
                var cs = $('#cs').html().replace(/,/g, ''),
                    cs = +cs;
                val = cs - cd;
                $('#ct').html(parseFloat(val).toFixed(2));
                toastr.error('Select Shipping State.');
            }

            postcode = $('#cupostcode').val();
            var state1 = $('#shipstate').val();

            $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { shippi_state: state1, shippi_postcode: $('#cupostcode').val() }, function(data) {
                //alert(data);
                if (data[1] == 'zero') {
                    if (data[5] == null) {
                        $('#cd').html('0.00');
                    } else {
                        $('#cd').html(data[5]);
                    }
                    $('#cs').html(data[3]);
                    $('#ct').html(data[4]);
                    $('#sa').html(data[6]);
                    toastr.success('Hurray!! Your Shipping was Free');
                } else {
                    //alert(data[5]);
                    if (data[5] == null) {
                        $('#cd').html('0.00');
                    } else {
                        $('#cd').html(data[5]);
                    }
                    $('#cs').html(data[3]);
                    $('#ct').html(data[4]);
                    $('#sa').html(data[6]);
                    toastr.success('Shipping Amount Added');
                }
            }, 'json');


        } else {
            //Clear on uncheck
            $('#sfirstname').val($(this).data('val'));
            $('#saddress1').val($(this).data('address'));
            $('#saddress2').val($(this).data('address1'));
            $('#state').val($(this).data('state'));
            $('#shipcity').val($(this).data('shipcity'));
            $('#spostcode').val($(this).data('postcode'));
            $('#sphone').val($(this).data('sphone'));
            $('#smobile').val($(this).data('smobile'));
            $('#scountry').val($('#country').val());
            if (ss != '') {
                $('#sa').html($(this).data('samt'));
            } else {
                $('#sa').html('0.00');
            }

            if ($('#state').val() == '') {
                $('#sa').html('0.00');
                var cd = $('#cd').html().replace(/,/g, ''),
                    cd = +cd;
                var cs = $('#cs').html().replace(/,/g, ''),
                    cs = +cs;
                val = cs - cd;
                $('#ct').html(parseFloat(val).toFixed(2));
                toastr.error('Select Shipping State.');
            }

            var state1 = $(this).data('state');

            $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { shippi_state: state1, shippi_postcode: $(this).data('postcode') }, function(data) {
                //alert(data);
                if (data[1] == 'zero') {
                    if (data[5] == null) {
                        $('#cd').html('0.00');
                    } else {
                        $('#cd').html(data[5]);
                    }
                    $('#cs').html(data[3]);
                    $('#ct').html(data[4]);
                    $('#sa').html(data[6]);
                    toastr.success('Hurray!! Your Shipping was Free');
                } else {
                    //alert(data[5]);
                    if (data[5] == null) {
                        $('#cd').html('0.00');
                    } else {
                        $('#cd').html(data[5]);
                    }
                    $('#cs').html(data[3]);
                    $('#ct').html(data[4]);
                    $('#sa').html(data[6]);
                    toastr.success('Shipping Amount Added');
                }
            }, 'json');
        };

    });



    $('input[name="same_bill_status"]').change(function() {
        var this_one = $(this);
        var type = ['b', 's'];
        var values = ['firstname', 'lastname', 'email', 'phone', 'address1', 'address2', 'city', 'state', 'country', 'postcode'];
        values.forEach(function(value, key) {
            if (this_one.prop('checked')) {
                $('#' + type[1] + value).focus();
                $('#' + type[1] + value).val($('#' + type[0] + value).val());
            } else {
                $('#' + type[1] + value).val('');
                $('#' + type[1] + value).focus();
                $('#' + type[1] + value).blur();
            }
        });
    });

    $('#almembr').click(function(e) {
        $('#cartcheckout').hide();
        $('#cartlogin').show(100);
    })

    $('#chekgust').click(function(e) {
        $('#cartlogin').hide();
        $('#cartcheckout').show(100);
    });

    $('#whatsthisyms').click(function(e) {
        $('#Message_why').show();
    });

    $('#whatsthisyms_1').click(function(e) {
        $('#Message_why').hide();
    });

    $('#agentcode').keyup(function(e) {
        $('.hiddenagent').val($(this).val());
    });


    var type = ['b', 's'];
    var values = ['firstname', 'lastname', 'email', 'phone', 'address1', 'address2', 'city', 'state', 'country', 'postcode'];
    values.forEach(function(value, key) {
        type.forEach(function(tvalue, tkey) {
            if ($('#' + tvalue + value).val() !== '') {
                $('#' + tvalue + value).focus();
                $('#' + tvalue + value).blur();
            }
        });
    });

    $('.add_cart_this_product').click(function() {
      var prd = $(this).data('pdid');
        var count = $('#qty1').val() || '1';
        var sizeid = $('#sizeid').val();
   
   
        var $ds = $(this);
        $('.quick_cart').html('<i class="fa fa-spinner fa-spin"></i>');
        $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { cartid: prd, type: 'add', count: count ,sizeid: sizeid }, function(data) {
         
            $('.dcart-total-count').html(data[0]);
            $('.quick_cart').html(data[1]);
            $('.delete_cart_this_product').unbind('click');
            deleteCart();
            toastr.success('Item added to your cart.');
            //if ($ds.data('go-cart') == '1') {
            // setTimeout(function () {
            //  window.location.href='cart.htm';
            //  call();
            //}, 500);
            // }
        }, 'json');
    });

    $('.add_wishlist_product').click(function() {
        var prd = $(this).data('pdid');
        var $ds = $(this);
        // $('.quick_cart').html('<i class="fa fa-spinner fa-spin"></i>');
        $.post("e6b9b2f6b88425/", { prodid: prd, type: 'add' }, function(data) {

            if (data[0] == '1') {
                msg = data[1];
                toastr.success(msg);
            } else {
                msg = data[1];
                toastr.error(msg);
            }
        }, 'json');
    });


    $('#cart-minues,#cart-plus,.cart-field').on('click keyup', function(e) {
        //$('.cart-minus,.cart-plus').on('click keyup', function (e) {
        //alert('hi');
          var sizeid = $('#sizeid').val() || 'M';
        
        
        if (e.type == 'click' && $(this).hasClass('cart-field')) {

        } else {
            var prd = $(this).data('pdid');
         
         
            if (e.type != 'click') {
                var c = $('#' + prd).val().length;
                if (c > 0) {
                    var count = parseInt($('#' + prd).val());
                } else {
                    var count = '1';
                    $('#' + prd).val('1');
                }

            } else {
                var count = parseInt($('#' + prd).val()) || parseInt('0');
                 if ($(this).hasClass('plus-btn')) {
                    count = count ;
                } else {
                    if (count > 1) {
                        count = count ;
                    }
                }
                $('#' + prd).val(count);
            }

            if (count <= 0) {
                toastr.error('Minimum qty is 1.');
            } else if (count === '') {
                toastr.error('Minimum qty is 1.');
            } else {
                $('.quick_cart').html('<i class="fa fa-spinner fa-spin"></i>');
                 $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { cartid: prd, type: 'update', count: count ,sizeid: sizeid }, function(data) {
                    $('.dcart-total-count').html(data[0]);
                    $('.quick_cart').html(data[1]);
                    $('.cart-id-' + prd).html(data[2]);
                    $('#subtotal-tds,#total-tds').html(data[3]);
                    $('.delete_cart_this_product').unbind('click');
                    deleteCart();
                    toastr.success('Your cart updated.');
                }, 'json');
            }
        }
    });
})

function deleteCart() {

    $('.delete_cart_this_product_s').click(function() {
        //  evt.preventDefault();
        // alert('a');
        var prd = $(this).data('pdid');
        if ($(this).data('rem_par') == '1') {
            $(this).parent().parent().remove();
        }
        $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { cartid: prd, type: 'delete' }, function(data) {

            $('.dcart-total-count').html(data[0]);
            $('.quick_cart').html(data[1]);
            $('#subtotal-tds,#total-tds').html(data[2]);
            if (data[2] == '0.00') {
                //$('#img').html('<div class="col-md-12" align="center"><img id="theImg" src="http://192.168.1.18/dappakadai/images/emptycart.png" class="img-responsive" style="width: 300px;" /><br /><b>Your Cart is Empty</b></div>');    
                //$('.subtotal-col').html('<br /><b>Your Cart is Empty</b>');
                $('#img').html('<div class="col-md-12" align="center"><b>Your Cart is Empty</b></div>');
                $('#demo').show();
                $('#check').hide();
                $('#cartcheckout').hide();
            }
            toastr.error('Item removed from your cart.');
            call();
        }, 'json');
    });

    $('.delete_cart_this_product').click(function() {
        //  evt.preventDefault();
        // alert('a');
        var prd = $(this).data('pdid');
        if ($(this).data('rem_par') == '1') {
            $(this).parent().parent().remove();
        }
        $('.quick_cart').html('<i class="fa fa-spinner fa-spin"></i>');
        $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { cartid: prd, type: 'delete' }, function(data) {

            $('.dcart-total-count').html(data[0]);
            $('.quick_cart').html(data[1]);
            $('#subtotal-tds,#total-tds').html(data[2]);
            if (data[2] == '0.00') {
                //$('#img').html('<div class="col-md-12" align="center"><img id="theImg" src="http://192.168.1.18/dappakadai/images/emptycart.png" class="img-responsive" style="width: 300px;" /><br /><b>Your Cart is Empty</b></div>');    
                //$('.subtotal-col').html('<br /><b>Your Cart is Empty</b>');
                $('#img').html('<div class="col-md-12" align="center"><b>Your Cart is Empty</b></div>');
                $('#demo').show();
                $('#check').hide();
                $('#cartcheckout').hide();
            }

            $('.delete_cart_this_product').unbind('click');
            deleteCart();
            toastr.error('Item removed from your cart.');
            call();
        }, 'json');



    });
    $('.delete_cart_this_products').click(function() {
        //alert('b');
        var prd = $(this).data('pdid');
        if ($(this).data('rem_par') == '1') {
            $(this).parent().parent().remove();
        }
        $('.quick_cart').html('<i class="fa fa-spinner fa-spin"></i>');
        $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { cartid: prd, type: 'delete' }, function(data) {
            $('.dcart-total-count').html(data[0]);
            $('.quick_cart').html(data[1]);
            $('#subtotal-tds,#total-tds').html(data[2]);
            if (data[2] == '0.00') {
                //$('#img').html('<div class="col-md-12" align="center"><img id="theImg" src="http://192.168.1.18/dappakadai/images/emptycart.png" class="img-responsive" style="width: 300px;" /><br /><b>Your Cart is Empty</b></div>');    
                //$('.subtotal-col').html('<br /><b>Your Cart is Empty</b>');
                $('#img').html('<div class="col-md-12" align="center"><b>Your Cart is Empty</b></div>');
            }
            $('.delete_cart_this_product').unbind('click');
            deleteCart();
            toastr.error('Item removed from your cart.');
            call();
        }, 'json');

    });
}
/* function detship_calc() {
 
 shipstate = $('#administrative_area_level_1').val();
 
 postcode = $('#postal_code').val();
 
 prodid = $('#prodid').val();
 
 $.post("e6b9b2300665c9e6c169c6e2f6b88425/", {detshippi_state: shipstate, detshippi_postcode: postcode,prodid:prodid}, function (data) {
 // alert(data);
 if(data!='' || data!='0.00')
 {
 $('#sa11').html("<strong style='color:#0091fc;'>Shipping Cost&nbsp;&nbsp;&nbsp;</strong>  &#8377;&nbsp;"+data);
 }
 else{
 $('#sa11').html('<strong style="color:#ff0000;">Available Only in India</strong>');
 }		   // toastr.success('Hurray!! Your Shipping was Free');
 
 }, 'json');
 
 
 }
 */
function detship_calc() {

    postcode = $('#shpostcode').val();

    prodid = $('#prodid').val();
    if (postcode == '') {
        $('#sa11').html("<strong style='color:#ff0000;'>Enter your postcode</strong>");

    } else {
        $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { detshippi_postcode: postcode, prodid: prodid }, function(data) {
            if (data != '') {
                $('#sa11').html("<strong style='color:#0091fc;'>Shipping Cost&nbsp;&nbsp;&nbsp;</strong>  &#8377;&nbsp;" + data);
            } else {
                $('#sa11').html("<strong style='color:#0091fc;'>Shipping Cost&nbsp;&nbsp;&nbsp;</strong>  &#8377;&nbsp;0.00");
            } // toastr.success('Hurray!! Your Shipping was Free');

        }, 'json');
    }


}


function ship_calc() {
    $('#spostcode').on('keyup', function() {
        var lengthh = $(this).val().length;
        var sded = $(this);
        var state = $('#state').val();
        if (lengthh >= 6) {
            if ($('#state').val() == '') {
                $('#sa').html('0.00');
                var cd = $('#cd').html().replace(/,/g, ''),
                    cd = +cd;
                var cs = $('#cs').html().replace(/,/g, ''),
                    cs = +cs;
                val = cs - cd;
                $('#ct').html(parseFloat(val).toFixed(2));
                toastr.error('Select Shipping State.');
            } else {
                postcode = $('#spostcode').val();

                $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { shippi_state: state, shippi_postcode: $('#spostcode').val() }, function(data) {
                    //alert(data);
                    if (data[1] == 'zero') {
                        if (data[5] == null) {
                            $('#cd').html('0.00');
                        } else {
                            $('#cd').html(data[5]);
                        }
                        $('#cs').html(data[3]);
                        $('#ct').html(data[4]);
                        $('#sa').html(data[6]);
                        toastr.success('Hurray!! Your Shipping was Free');
                    } else {
                        //alert(data[5]);
                        if (data[5] == null) {
                            $('#cd').html('0.00');
                        } else {
                            $('#cd').html(data[5]);
                        }
                        $('#cs').html(data[3]);
                        $('#ct').html(data[4]);
                        $('#sa').html(data[6]);
                        toastr.success('Shipping Amount Added');
                    }
                }, 'json');
            }
        }
    });



    //$('#state').onchange(function (e) {
    $('#state').on('change', function() {
        var sded = $(this);
        var state = $('#state').val();
        if ($('#state').val() == '') {
            $('#sa').html('0.00');
            cs = parseFloat($('#cs').html());
            cd = parseFloat($('#cd').html());
            var cd = $('#cd').html().replace(/,/g, ''),
                cd = +cd;
            var cs = $('#cs').html().replace(/,/g, ''),
                cs = +cs;
            val = cs - cd;
            $('#ct').html(parseFloat(val).toFixed(2));
            toastr.error('Select Shipping State.');
        } else {
            postcode = $('#spostcode').val();

            $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { shippi_state: state, shippi_postcode: $('#spostcode').val() }, function(data) {
                //alert(data);
                if (data[1] == 'zero') {
                    if (data[5] == null) {
                        $('#cd').html('0.00');
                    } else {
                        $('#cd').html(data[5]);
                    }
                    $('#cs').html(data[3]);
                    $('#ct').html(data[4]);
                    $('#sa').html(data[6]);
                    toastr.success('Hurray!! Your Shipping was Free');
                } else {
                    //alert(data[5]);
                    if (data[5] == null) {
                        $('#cd').html('0.00');
                    } else {
                        $('#cd').html(data[5]);
                    }
                    $('#cs').html(data[3]);
                    $('#ct').html(data[4]);
                    $('#sa').html(data[6]);
                    toastr.success('Shipping Amount Added');
                }
            }, 'json');
        }
    });
}



function coupon_add() {

    $('#coupon_code_btn').click(function(e) {
        var sded = $(this);
        if ($('#coupon_code').val() == '') {
            toastr.error('Coupon code is empty.');
        } else {
          
            $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { code: $('#coupon_code').val(),shipprice: $('#ship_price1').val() }, function(data) {
                if (data[1] == 'Code Invalid') {
                    $('#cd').html(data[5]);
                    $('#discounted_amount').val(data[5]);
                    
                    $('#subtotal-tds').html(data[3]);
                    $('#subtotal-shiptds').html(data[6]);
                    $('#total-tds').html(data[4]);
                    toastr.error('Coupon code is Invalid.');
                } else {
                   
                   
                    $('#cd').html(data[5]);
                    $('#discounted_amount').val(data[5]);
                    $('#subtotal-shiptds').html(data[6]);
                    $('#subtotal-tds').html(data[3]);
                    $('#total-tds').html(data[4]);
                    toastr.success('Coupon code accepted.');
                    sded.attr({ 'id': 'coupon_code_btn_remove', 'class': 'btn btn-red min-width-sm' });
                    sded.html('Remove');
                    $('#coupon_code_btn_remove').unbind('click');
                    coupon_remove();
                }
            }, 'json');
        }
    });
}

function coupon_remove() {
    $('#coupon_code_btn_remove').click(function(e) {
        var sded = $(this);
        $.post("e6b9b2300665c9e6c169c6e2f6b88425/", { code_remove: '1' }, function(data) {
           
           
            $('#coupon_code').val('');
            $('#cd').html(data[5]);
            $('#discounted_amount').val(data[5]);
             $('#subtotal-shiptds').html(data[6]);
            $('#subtotal-tds').html(data[3]);
            $('#total-tds').html(data[4]);
            toastr.error('Coupon code is removed.');
            sded.attr({ 'id': 'coupon_code_btn', 'class': 'btn btn-custom min-width-sm' });
            sded.html('Apply Now');
            $('#coupon_code_btn').unbind('click');
            coupon_add();
        }, 'json');

    });
}





ship_calc();
coupon_add();
coupon_remove();
deleteCart();
reward_add();