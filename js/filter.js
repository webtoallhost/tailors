function getfprice(pfrom, pto, position) {
    $('#fpfrom').val(pfrom);
    $('#fpto').val(pto);
    $('#price' + position).addClass("active");
    getattrval();
}

function getfsizeid(a, b) {

    if ($('#fizeid' + b).is(':checked')) {
        $('#fizeid').val($('#fizeid').val() + ',' + b);

    }


    var sort_by = $("#sort_by option:selected").val();
    var size = $("#fizeid").val();



    var color = '';
    $('input[name="fclrid"]:checked').each(function() {
        color += this.value + ',';
    });

    var cid = $('input[name="cid"]').val();
    var sid = $('input[name="sid"]').val();
    var iid = $('input[name="iid"]').val();
    var typeid = $('input[name="typeid"]').val();
    var pfrom = $('input[name="pfrom"]').val();
    var pto = $('input[name="pto"]').val();

    $.ajax({
        type: "POST",
        async: false,
        cache: false,
        timeout: 30000,
        url: "http://13.234.94.153/product_ajax.php",
        data: { size: size, color: color, pricefrom: pfrom, priceto: pto, cid: cid, sid: sid, iid: iid, sort_by: sort_by },
        beforeSend: function() {

            $("#filterresult").html('<div class="loader-icon" style="text-align:center; width:100%;"><img src="http://13.234.94.153/images/loader.svg" style="padding:25% 0 0 0; width:10%;" /></div>');
        },
        success: function(data) {
            var arr = size.split(',');
            for (var i = 0; i < arr.length; i++) {
                $("#sizesel" + arr[i]).prop('checked', true);
            }
            $("#filterresult").html(data);

        }
    });
}

function getfcolorid(a, b) {

    $('#fclrid' + a).val(b);
    getattrval();
}

function getsort() {
    var sort_by = $("#sort_by option:selected").val();
    var cid = $('input[name="cid"]').val();
    var sid = $('input[name="sid"]').val();
    var iid = $('input[name="iid"]').val();
    $.ajax({
        type: "POST",
        url: sitename + "product_ajax.php",
        data: { cid: cid, sid: sid, iid: iid, sort_by: sort_by },
        beforeSend: function() {
            $("#filterresult").html('<div class="loader-icon" style="text-align:center; width:100%;"><img src="http://www.click2buy.in/demo/images/loading.gif" style="padding:25% 0 0 0; width:10%;" /></div>');
        },
        success: function(data) {
            $("#filterresult").html(data);
        }
    });
}

function getcatwise() {
    var a = '';
    $('input[name="subcategory"]:checked').each(function() {
        a += this.value + ',';
    });
    var a = a.substring(0, a.length - 1);
    var b = '';
    $('input[name="brand"]:checked').each(function() {
        b += this.value + ',';
    });
    var b = b.substring(0, b.length - 1);
    var pfrom1 = $('input[name="pfrom"]').val();
    var pto1 = $('input[name="pto"]').val();
    var cid = $('input[name="cid"]').val();
    $.ajax({
        type: "POST",
        url: sitename + "product_ajax.php",
        data: { subcategory: a, brand: b, pricefrom: pfrom1, priceto: pto1, cid: cid },
        beforeSend: function() {
            $("#filterresult").html('<div class="loader-icon" style="text-align:center; width:100%;"><img src="http://www.click2buy.in/demo/images/loading.gif" style="padding:25% 0 0 0; width:10%;" /></div>');
        },
        success: function(data) {
            $("#filterresult").html(data);
        }
    });
}

function getattrval() {

    var sort_by = $("#sort_by option:selected").val();


    var size = '';
    $('input[name="filtersize"]:checked').each(function() {
        size += this.value + ',';
    });


    var color = '';
    $('input[name="filtercolor"]:checked').each(function() {
        color += this.value + ',';
    });

    var cid = $('input[name="cid"]').val();
    var sid = $('input[name="sid"]').val();
    var iid = $('input[name="iid"]').val();
    var typeid = $('input[name="typeid"]').val();
    var pfrom = $('#fpfrom').val();
    var pto = $('#fpto').val();

    $.ajax({
        type: "POST",
        async: false,
        cache: false,
        timeout: 30000,
        url: "http://13.234.94.153/product_ajax.php",
        data: { size: size, color: color, pricefrom: pfrom, priceto: pto, cid: cid, sid: sid, iid: iid, typeid: typeid, sort_by: sort_by },
        beforeSend: function() {
            $("#filterresult").html('<div class="loader-icon" style="text-align:center; width:100%;"><img src="http://13.234.94.153/images/loader.svg" style="padding:25% 0 0 0; width:10%;" /></div>');
        },
        success: function(data) {
            $("#filterresult").html(data);
        }
    });
}