<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<?php include 'require/header.php';
$_SESSION['orconfres']='';
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}


if (isset($_POST['guest-checkout'])) {
    if ($_SESSION['GUEST_ID'] != '') {
        $ds = $db->prepare("DELETE FROM `guest` WHERE `id`=?");
        $ds->execute(array($_SESSION['GUEST_ID']));
        $ds = $db->prepare("DELETE FROM `bill_ship_address` WHERE `Guest_ID`=?");
        $ds->execute(array($_SESSION['GUEST_ID']));
    }
    $sg = $db->prepare("INSERT INTO `guest` SET `email`=?,`ip`=?");
    $sg->execute(array($_POST['guest'], $_SERVER['REMOTE_ADDR']));
    $lastid = $db->lastInsertId();

    $sg = $db->prepare("INSERT INTO `bill_ship_address` SET `Guest_ID`=?,`bemail`=?");
    $sg->execute(array($lastid, $_POST['guest']));
    $_SESSION['GUEST'] = '1';
    $_SESSION['GUEST_ID'] = $lastid;
    if ($_POST['hiddenagent'] != '') {
        $_SESSION['AGENT_CODE'] = $_POST['hiddenagent'];
    }
    header("location:" . $fsitename . "checkout.htm");
    exit;
}
?>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Shopping Cart</li>
                </ul>
            </div>
        </div>

        <style>
            .tt-shopcart-table .tt-title{
                color: #f7f8fa;
            }
            .tt-shopcart-table .tt-title a {
    color: #f7f8fa;
}
.tt-shopcart-table .tt-price {
    color: #f7f8fa;

}
.tt-shopcart-wrapper {
    border: 1px solid #e9e7e7;
    background: #110c09b0;
}
.btn-link, .btn-link:focus {
    display: inline-block;
    font-size: 14px;
    color: #2879fe;
    text-decoration: none;
    background: #f7f8fa;
    font-weight: 500;
    letter-spacing: 0.02em;
    font-family: Rubik;
    transition: 0.2s linear;
}
@media (min-width: 1230px){
    .col-xl-12 {
    background: #110c09c7;
}
.tt-shopcart-table {
    margin-right: 20px;
    padding: 21px;
}
}

.tt-shopcart-table01 td, .tt-shopcart-table01 th {
    color: #f7f8fa;
}
.tt-shopcart-table .tt-btn-close:before {
    color: #fff;
}
.tt-title-subpages:not(.text-left):not(.text-right) {
    text-align: left;
    text-transform: capitalize;
    padding-left: 0;
}
.tt-shopcart-table table tr {
    border-top: 0;
    border-bottom: 1px solid #e9e7e7;
    }
    .tt-shopcart-table .tt-btn-close:before {
    color: gray;
}
.colorbg {
    border: none;
    background: #f7f6f6;
    border-radius: 8px;
}
.tt-shopcart-table01 td, .tt-shopcart-table01 th {
    color: #000;
}
.tt-shopcart-table .tt-price {
    font-weight: 400;
    color: gray;
}
.tt-input-counter.style-01 {
    position: relative;
    max-width: 132px;
    min-width: 110px;
}
.tt-shopcart-table table td:nth-child(4) {
    width: 8px;
}
.tt-shopcart-table table td:nth-child(3) {
    width: 18px;
}
.tt-shopcart-table table td:nth-child(2) {
    width: 458px;
    padding-left: 20px;
}
.tt-shopcart-table table td:nth-child(1) {
    width: 1px;
}
.col-xl-4{
    padding-left: 20px;
}
.col-xl-4 .btn{
    width: 100%
}
.markbg{
    text-align: center;
    padding-top: 29px;
    padding-bottom: 10px;
}
.markbg a{
    text-decoration: underline;
}
.tt-input-counter.style-01 input {
    background: #fff;
}
.tt-shopcart-table table td:nth-child(5) {
    width: 18px;
}
.tt-shopcart-table table td:nth-child(6) {
    width: 15px;
    text-align: right;
}
.tt-shopcart-table01 tfoot tr td {
    font-size: 18px;
    color: #000;
    }
.form-control{
    border:0;
}
h3.codetext:not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]) {
    font-size: 18px;
    line-height: 25px;
    font-weight: 600;
    padding-bottom: 15px;
}
.input-group-btn .btn{
    border-radius: 0 6px 6px 0px;
}
h2.small:not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]), h3:not([class^=tt-title]):not([class^=tt-collapse]):not([class^=tt-aside]) {
    font-size: 18px;
    line-height: 25px;
    font-weight: 400;
    padding-bottom: 1px;
}
.tt-shopcart-table .tt-title {
    color: gray;
}
.tt-shopcart-table .tt-title a {
    color: gray;
}
        </style>
        
        <div id="tt-pageContent"> 
        <?php
                      $user_id = $_SESSION['FUID']; 
                                         $tempcart = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=?");
         $tempcart->execute(array($user_id));
		 if ($tempcart->rowCount() > 0) {
                    ?>
            <div class="container-indent">
                <div class="container">
                    <h1 class="tt-title-subpages noborder">Shopping cart</h1>
                   
                    <div class="row">
                        <div class="col-sm-12 col-xl-8">
                            <div class="tt-shopcart-table colorbg">
                                <?php 
                               // print_r($_SESSION);
                                ?>
                                <table>
                                    <tbody>
                                        
                                        <?php 
                                      
		 while($tempcartfetch = $tempcart->fetch()) {
		       $im = explode(",", getproduct('image', $tempcartfetch['productid']));
                                        ?>
                                         <tr>
                                            
                                            <td>
                                                <input type="hidden" name="sizeid" id="sizeid" value="<?php echo $tempcartfetch['size']; ?>">
                                                <div class="tt-product-img"><a href="<?php echo $fsitename . 'view/' . getproduct('link', $tempcartfetch['productid']) . '.htm'; ?>">
                                                                <?php if($im[0]!='') { ?>
                                                                <img src="<?php echo $fsitename . 'images/product/' . getproduct('imagefolder', $tempcartfetch['productid']) . '/' . $im[0]; ?>" alt="<?php echo getproduct('productname', $tempcartfetch['productid']); ?>" style="width:100px; float:left;" />
<?php } else { ?>
                                                                <img src="<?php echo $fsitename . 'images/noimage1.png'; ?>" alt="<?php echo getproduct('productname', $tempcartfetch['productid']); ?>" style="width:100px; float:left;" />
                                                                <?php } ?>
                                                            </a></div>
                                            </td>
                                            <td>
                                               <h3>Test shirt</h3>
                                                <h2 class="tt-title">
                                                                                            <p style="margin: 0">Size : <?php echo $tempcartfetch['size']; ?></p>
                                        <a href="<?php echo $fsitename . 'view/' . getproduct('link', $tempcartfetch['productid']) . '.htm'; ?>"><?php echo getproduct('productname', $tempcartfetch['productid']); ?></a>
                                                </h2>
                                                <ul class="tt-list-parameters">
                                                    <li><div class="tt-price"> <?php
                                                    
                                                    $as = $db->prepare("SELECT * FROM `sizeprice` WHERE `size`=? AND `product_id`=?");
         $as->execute(array($tempcartfetch['size'],$tempcartfetch['productid']));
		 if ($as->rowCount() > 0) {
		      $asd = $as->fetch();
		   $sprice = $asd['sprice'];
           
            $price = $asd['price'];  
		 }
		 else
		 {
		    $sprice = getproduct('sprice', $tempcartfetch['productid']);
           
            $price = getproduct('price', $tempcartfetch['productid']);   
		 }
                                                  
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></li>
                                                    <li><div class="detach-quantity-mobile"></div></li>
                                                    <li><div class="tt-price subtotal">$124</div></li>
                                                </ul>
                                            </td>
                                            <td><div class="tt-price"><?php
                           
                           
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></td>
                                            <td>
                                                <div class="detach-quantity-desctope">
                                                    <div class="tt-input-counter style-01"><span class="minus-btn" id="cart-minues" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"></span> 
                                                     <input size="100"  title="Numbers Only" name="qty[]" id="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"   value="<?php echo $tempcartfetch['qty']; ?>" />
                                                    <span class="plus-btn" id="cart-plus" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" id="temp" data-max="100"></span></div>
                                                </div>
                                                <input type="hidden" name="product_id[]" value="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" />
                                            </td>
                                            <td><div class="tt-price subtotal">$&nbsp;<span class="subtotal-col cart-id-<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"><?php
                                            if ($sprice > 0) {
                                                echo number_format($sprice * $tempcartfetch['qty'], '2', '.', '');
                                                   $cartftotoal+=number_format($sprice * $tempcartfetch['qty'], '2', '.', '');
                                            } else {
                                                echo number_format($price * $tempcartfetch['qty'], '2', '.', '');
                                                   $cartftotoal+=number_format($price * $tempcartfetch['qty'], '2', '.', '');
                                            }
                                            
                                                    ?></span></div></td>
                                                    <td>
                                            <a href="#" class="tt-btn-close delete_cart_this_product" data-rem_par="1" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" ></a>
                                            </td>
                                        </tr>
                                        <?php }  ?>
                                        </tbody>
                                </table>
                               
                            </div>
                            <div class="row tt-shopcart-table">
                                <div class="col-md-6">
                             <a href="#" class="tt-btn-close delete_cart_this_product" data-rem_par="1" data-pdid="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" ></a>
                             <a style="text-decoration: underline;">Clear shopping cart</a>
                         </div>
                          <div class="col-md-6" style="text-align: right;">
                             <a style="text-decoration: underline;"><i class="fa fa-refresh" aria-hidden="true"></i>
Update cart</a>
                         </div>
                     </div>
                        </div>

                        <div class="col-sm-12 col-xl-4">
                            <div class="tt-shopcart-wrapper colorbg">
                             <h3 class="codetext">DISCOUNT CODE</h3>

                             <div class="input-group">
                            <input type="text" class="form-control" placeholder="Code">
                            <span class="input-group-btn">
                           <button class="btn btn-default" type="submit">
                            APLLY
                           </button>
                         </span>
                             </div>
                                <div class="tt-shopcart-box tt-boredr-large">
                                    <table class="tt-shopcart-table01">
                                        <tbody>
                                            <tr>
                                                <th>Sub Total</th>
                                                <td>$<span id="subtotal-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                             <tr>
                                                <th>GST</th>
                                                <td>$<span id="subtotal-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                            <tr>
                                                <th style="padding-bottom: 20px;">Shipping</th>
                                                <td style="padding-bottom: 20px;">$<span id="subtotal-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr style="border-top: 1px solid #e9e7e7;">
                                                <th>TOTAL</th>
                                                <td>$<span id="total-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                        </tfoot>

                                    </table>


                                    
                                </div>
                            </div>
                            <div class="markbg">
                            <a  href="<?php echo $fsitename; ?>listings.htm">CONTINUE SHOPPING</a>
                        </div>
                        <div style="text-align: center;">
                                    <a href="<?php echo $fsitename; ?>pages/orderconfirmation.htm" class="btn btn-lg"><span class="icon icon-check_circle"></span>PROCEED TO CHECKOUT</a>
                                </div>
                        </div>
                      
                    </div>
                       
                </div>
            </div> <?php } else { ?>
                      <div class="tt-empty-cart">
                <span class="tt-icon icon-f-39"></span>
                <h1 class="tt-title">SHOPPING CART IS EMPTY</h1>
                <p>You have no items in your shopping cart.</p>
                <a href="<?php echo $fistename; ?>listings.htm" class="btn">CONTINUE SHOPPING</a>
            </div>
                        <?php } ?>
        </div>
        <?php include 'require/footer.php';?>
        
        <script>
             $(document).ready(function(){
$('#shipingform').submit(function(){
// show that something is loading
$('#shpresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
 var result=data.split('#');
 
    $('#sipprice').html(result['0']);
    
     $('#subtotal-shiptds').html(result['0']);
     $('#total-tds').html(result['1']);
    
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});

   });
   
   function getstate(a)
    {
        alert(a);
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {country: a},
            success: function (data) {
              $("#state").html(data);
              
            }
        });
    }    
        </script>