<?php include 'require/header.php';
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}
//echo $_SESSION['FUID'];
if (isset($_REQUEST['updateprofile'])) {

    @extract($_REQUEST);
    $ip = $_SERVER['REMOTE_ADDR'];
    
    $editmsg = updatemeasurement($gender,$collar,$shoulder,$height,$hip_size,$_SESSION['FUID']);
    
}
?>

<style>
    .Categories-list ul li {
    padding: 8px 0;
    display: block;
}
.Categories-list ul li a {
    color: #292929;
    font-size: 18px;
    line-height: 18px;
    text-decoration: none;
    text-transform: capitalize;
    transition: all 0.3s ease 0s;
}
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="<?php echo $fsitename; ?>">Home</a></li>
                    <li>Account</li>
                </ul>
            </div>
        </div>
        <div id="tt-pageContent">
            <div class="container-indent">
                <div class="container container-fluid-custom-mobile-padding">
                    <h1 class="tt-title-subpages noborder">ACCOUNT</h1>
                    <div class="tt-shopping-layout">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 col-12"style="border: 1px solid #80808096;">
                                <h2 class="tt-title">User Settings</h2>
                                <div class="Categories-list">
                                       <ul>
                                            
                                            <li><a href="<?php $fsitename; ?>myaccount.htm?uid=<?php echo $_SESSION['FUID'];?>"><i class="icon-f-94"></i>&nbsp;My Account</a></li>
                                            <li><a href="<?php $fsitename; ?>editaccount.htm"><i class="icon-f-94"></i>&nbsp;Edit Profile</a></li>
                                             <li><a href="<?php $fsitename; ?>editmeasurement.htm" style="font-weight:bold;"><i class="icon-f-94"></i>&nbsp;Edit Measurement</a></li>
                                            <li><a href="<?php $fsitename; ?>changepwd.htm"><i class="icon-f-77"></i>&nbsp;Change Password</a></li>
                                            <li><a href="<?php $fsitename; ?>mywishlist.htm"><i class="icon-f-77"></i>&nbsp;My Wishlist</a></li>
                                            <li><a href="<?php $fsitename; ?>myorders.htm"><i class="icon-f-68"></i>&nbsp;View Orders</a></li>
                                            <li><a href="<?php $fsitename; ?>logout.htm"><i class="icon-f-77"></i>&nbsp;Logout</a></li>
                                            
                                        </ul>
                                    </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12 col-12">
                                <div class="tt-wrapper">
                            <h6 class="tt-title">EDIT MEASUREMENT</h6>
                            <?php echo $editmsg; ?>
                            <br>
                            <?php
                if ($_SESSION['FUID'] != '') {
                $customer = FETCH_all("SELECT * FROM `customer` WHERE `CusId` = ?", $_SESSION['FUID']);
                //print_R($customer);
                }
            ?>   
                            <div class="form-default">
                                <form action="" method="post" autocomplete="off" id="reviewform">
                                    <div class="form-group">
                                        <label for="fname" class="control-label">GENDER *</label> 
                                        <select name="gender" class="form-control">
                                        <option value="">Select</option>
                                        <option value="Male" <?php if($customer['gender']=='Male') { ?> selected="selected" <?php } ?>>Male</option>
                                        <option value="Female" <?php if($customer['gender']=='Female') { ?> selected="selected" <?php } ?>>Female</option>
                                        </select>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="lname" class="control-label">COLLAR</label> 
                                        <input type="text" class="form-control" name="collar" id="collar" value="<?php echo $customer['collar']; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="signupemail" class="control-label">SHOULDER</label> 
                                        <input type="text" class="form-control" name="shoulder" id="shoulder"  value="<?php echo $customer['shoulder']; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="phoneno" class="control-label">HEIGHT</label> 
                                        <input type="text" class="form-control" name="height" id="height"  value="<?php echo $customer['height']; ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label for="address1" class="control-label">HIP SIZE</label> 
                                        <input type="text" class="form-control" name="hip_size" id="hip_size" value="<?php echo $customer['hip_size']; ?>" />
                                    </div>
                                 
                                    <div class="row tt-offset-21">
                                        <div class="col-auto">
                                            <button type="submit" class="btn" name="updateprofile" id="updateprofile">UPDATE</button>
                                        </div>
                                        <!-- <div class="col-auto align-self-center">or <a href="#" class="tt-link">Cancel</a></div> -->
                                    </div>
                                </form>
                            </div>
                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include 'require/footer.php';?>