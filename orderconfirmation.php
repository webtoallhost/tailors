<?php include 'require/header.php';
// error_reporting(1);
// ini_set('display_errors','1');
// error_reporting(E_ALL);
if ($_SESSION['FUID'] == '') {
    //echo '<meta http-equiv="refresh" content="0;url=' . $fsitename . 'pages/404.htm' . '">';
    echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/login.htm'.'">';
    exit;
}

if(isset($_REQUEST['checkout']))
{
@extract($_REQUEST);
 
$tempcart = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=?");
$tempcart->execute(array($_SESSION['FUID']));
if ($tempcart->rowCount() > 0) {
while($tempcartfetch = $tempcart->fetch()) {
    
$vendor=getproduct('vendor',$tempcartfetch['productid']);

$sg1 = $db->prepare("UPDATE `tempcart` SET `vendor`=? WHERE `id`=?");
$sg1->execute(array($vendor,$tempcartfetch['id']));

// $sg1 = $db->prepare("INSERT INTO `order` SET `vendor`=?,`CusID`=?,`ship_country`=?,`ship_state`=?,`ship_postcode`=?,`subtotal`=?,`shipping_charge`=?,`over_all_total`=?,`billing_address`=?,`order_comments`=?");
// $sg1->execute(array($vendor,$_SESSION['FUID'],$ship_country,$ship_state,$shp_postcode,$subtotal,$ship_price,$fintotal,$delivery_address,$order_notes));

}
}

//place order
$tempcart = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=? GROUP BY `vendor`");
$tempcart->execute(array($_SESSION['FUID']));
if ($tempcart->rowCount() > 0) {
while($tempcartfetch = $tempcart->fetch()) {
    $vendor=getproduct('vendor',$tempcartfetch['productid']);

$sg1 = $db->prepare("INSERT INTO `norder` SET `promotional_code`=?,`discounted_amount`=?,`payment_mode`=?,`cus_name`=?,`cus_mobile`=?,`cus_email`=?,`vendor`=?,`CusID`=?,`ship_country`=?,`ship_state`=?,`ship_postcode`=?,`subtotal`=?,`shipping_charge`=?,`over_all_total`=?,`billing_address`=?,`order_comments`=?");
$sg1->execute(array($coupon_code,$discounted_amount,$payment_mode,$fname,$mobileno,$emailid,$vendor,$_SESSION['FUID'],$ship_country,$ship_state,$shp_postcode,$subtotal,$ship_price,$fintotal,$delivery_address,$order_notes));


$insert_id = $db->lastInsertId(); 
$orderid='#00'.$insert_id;

$updsg11 = $db->prepare("UPDATE `norder` SET `order_id`=? WHERE `oid`=?");
$updsg11->execute(array($orderid,$insert_id));    
   
$message=getcustomer('fname',$_SESSION['FUID']).' placed one new order.';

$notif = $db->prepare("INSERT INTO `notification` SET `userid`=?,`orderid`=?,`from`=?,`to`=?,`type`=?,`message`=?");
$notif->execute(array($_SESSION['FUID'],$insert_id,'user','admin','order',$message));

$notif1 = $db->prepare("INSERT INTO `notification` SET `userid`=?,`orderid`=?,`from`=?,`to`=?,`type`=?,`message`=?");
$notif1->execute(array($_SESSION['FUID'],$insert_id,'user',$vendor,'vendor',$message));

    
$tempcart1 = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=? AND `vendor`=? ");
$tempcart1->execute(array($_SESSION['FUID'],$vendor));
if ($tempcart1->rowCount() > 0) {
while($tempcartfetch1 = $tempcart1->fetch()) {
$sg11 = $db->prepare("INSERT INTO `order` SET `vendor`=?,`Order_id`=?,`product_id`=?,`product_name`=?,`product_price`=?,`product_qty`=?,`product_total_price`=?,`size`=?");
$sg11->execute(array($tempcartfetch1['vendor'],$insert_id,$tempcartfetch1['productid'],getproduct('productname',$tempcartfetch1['productid']),$tempcartfetch1['price'],$tempcartfetch1['qty'],$tempcartfetch1['totprice'],$tempcartfetch1['size']));    

    
$dsg11 = $db->prepare("DELETE FROM `tempcart` WHERE `id`=?");
$dsg11->execute(array($tempcartfetch1['id']));    
    
}

}


}

}
$am = FETCH_all("SELECT * FROM `norder` WHERE `oid`=?", $insert_id);

$manageprofile = FETCH_all("SELECT * FROM `manageprofile` WHERE `pid` = ?", '1');
$to = $manageprofile['recoveryemail'];
$subject = "Your Lorikeet Order Confirmation " . $am['order_id'];
$subject1 = "You have new order " . $am['order_id'];
  if ($am['order_status'] == '0') {
        $ostatus = 'Awaiting for Payment';
    } elseif ($am['order_status'] == '1') {
        $ostatus = 'Awaiting Fulfillment';
    } elseif ($am['order_status'] == '2') {
        $ostatus = 'Completed';
    } elseif ($am['order_status'] == '3') {
        $ostatus = 'Cancelled';
    } elseif ($am['order_status'] == '4') {
        $ostatus = 'Declined';
    } elseif ($am['order_status'] == '5') {
        $ostatus = 'Refunded';
    } elseif ($am['order_status'] == '6') {
        $ostatus = 'Partially Refunded';
    }   
  $message = '<table style="width:100%; font-family:arial; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
<td><h2 style="color:#cc6600; border-bottom:1px solid #cc6600;">Thanks for Your Order</h2></td>
</tr>
<tr>
<td>
<table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
	<td width="40%;"><h1>Tailior</h1>' . $general['about'] . '
	</td>
	<td width="60%;" align="right">
		<h4>Order Number : ' . $am['order_id'] . '</h4>
		<p>Order Date : ' . date('d-M-Y H:i:s A', strtotime($am['datetime'])) . '</p>		
		<h4>Order Status : ' . $ostatus . '</h4>
	</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
	<td width="100%;">
		<h4>Billing Address</h4>
		<p>' . $am['cus_name'] . '</p>
		<p>' . $am['cus_mobile'] . '</p>
		<p>' . $am['billing_address'] . '</p>
	</td>
	
	
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
	<td style="width:100%;">
		<p><b>Order Comments : </b> ' . $am['order_comments'] . '</p>
	</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table style="width:100%; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
	<td width="100%;">
		<h4>Your Order Contains</h4>
		' . mailcart($am['oid']) . '
	</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
	<td width="50%;" valign="top">
		<h4>Payment Method : ' . $am['payment_mode'].'</h4>
	</td>
	<td width="50%;" align="right">
		<table style="width:100%; font-size:13px;">
			<tr>
				<td style="border-bottom:1px dotted #cc6600;"><b>SubTotal  :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right">$&nbsp;' . number_format($am['subtotal'], '2', '.', '') . '</td>
			</tr>
			<tr>
				<td style="border-bottom:1px dotted #cc6600;"><b>Discount  :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right">$&nbsp;' . number_format($am['discounted_amount'], '2', '.', '') . '</td>
			</tr>
			<tr>
				<td style="border-bottom:1px dotted #cc6600;"><b>Shipping Cost  :</b></td><td  style="border-bottom:1px dotted #cc6600;" align="right">&nbsp;' . $am['shipping_charge'] . '</td>
			</tr>
			<tr>
				<td style="border-bottom:1px dotted #cc6600;"><b>Total :</b></td><td style="border-bottom:1px dotted #cc6600;" align="right">$&nbsp;' . number_format($am['over_all_total'], '2', '.', '') . '</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table style="width:100%; border-bottom:1px solid #cc6600; padding:2%; font-size:13px;" cellpadding="0" cellspacing="0">
<tr>
	<td width="50%;">
		<a href="'.$fsitename.'" target="_blank">Lorikeet</a>
	</td>
	<td width="50%;" align="right"></td>
</table>
</td>
</tr>
</table>';

//Customer Email

    sendgridApiMail($emailid, $message, $subject, $to, '', '');


//sendoldmail($subject1, $message, $cemail, $to);
//Admin Email

    //sendgridApiMail($emailid, $message, $subject, $to, '', '');
   
  
$orconfres = '<div class="successalert"><span class="closebtn" onclick="this.parentElement.style.display='."'".none."'".';">&times;</span><strong>Your Order Placed Successfully..Our Admin will contact you soon.</strong></div>'; 
$_SESSION['orconfres']=$orconfres;
echo '<meta http-equiv="refresh" content="0;url=' . $fsitename. 'pages/orderconfirmation.htm'.'">';
exit;
}

if (isset($_POST['guest-checkout'])) {
    if ($_SESSION['GUEST_ID'] != '') {
        $ds = $db->prepare("DELETE FROM `guest` WHERE `id`=?");
        $ds->execute(array($_SESSION['GUEST_ID']));
        $ds = $db->prepare("DELETE FROM `bill_ship_address` WHERE `Guest_ID`=?");
        $ds->execute(array($_SESSION['GUEST_ID']));
    }
    $sg = $db->prepare("INSERT INTO `guest` SET `email`=?,`ip`=?");
    $sg->execute(array($_POST['guest'], $_SERVER['REMOTE_ADDR']));
    $lastid = $db->lastInsertId();

    $sg = $db->prepare("INSERT INTO `bill_ship_address` SET `Guest_ID`=?,`bemail`=?");
    $sg->execute(array($lastid, $_POST['guest']));
    $_SESSION['GUEST'] = '1';
    $_SESSION['GUEST_ID'] = $lastid;
    if ($_POST['hiddenagent'] != '') {
        $_SESSION['AGENT_CODE'] = $_POST['hiddenagent'];
    }
    header("location:" . $fsitename . "checkout.htm");
    exit;
}
?>

<style type="text/css">
.tt-shopcart-col .form-control
{
height : none !important;
}

.tt-shopcart-table-02 .tt-title {
    font-style: normal;
}
.tt-shopcart-table-02 table {
    width: 94%;
    margin-left: 15px;
}
#bgnone{
    background:none;
}
.tt-shopcart-col .tt-shopcart-box {

    background: #110c098f;
}
.tt-shopcart-table-02 table.tt-shopcart-table01 tr td {
    vertical-align: middle;
    padding: 5px 0;
}
.tt-shopcart-table01 tbody tr th {
    font-size: 16px;
    font-weight: 500;
    letter-spacing: .02em;
    padding: 0px 0;
}
.form-control{
    border:0;
}
body  select.form-control {
    background-image: linear-gradient(
45deg
, transparent 50%, gray 50%), linear-gradient(
135deg
, gray 50%, transparent 50%), radial-gradient(#fff 70%, transparent 72%) !important;
    background-position: calc(100% - 20px) calc(1em + 1px), calc(100% - 15px) calc(1em + 1px), calc(100% - .5em) .5em !important;
    background-size: 5px 5px, 5px 5px, 1.5em 1.5em !important;
    background-repeat: no-repeat !important;
    -webkit-appearance: none;
}
.tt-shopcart-table-02 table td:nth-child(4) {
    color: #fff;
}
.tt-title-subpages.noborder {
    font-size: 30px;
    padding: 0 0 30px;
    }
    .tt-shopcart-table-02 table tr{
        border: 0
    }
    .tt-shopcart-table01 tfoot tr td {
    font-size: 19px;
    color: #000;
}

    
    
    .checkcontainer {
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    padding-top: 3px;
}
.checkcontainer input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

.radiobtn {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: transparent;
    border-radius: 50%;
    border: 1px solid #ddd;
}
.checkcontainer input:checked ~ .radiobtn:after {
    display: block;
}
.checkcontainer .radiobtn:after {
   top: 8px;
    left: 8px;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    background: #000;
}

.radiobtn:after {
    content: "";
    position: absolute;
    display: none;
}
.tt-payment-list {
    list-style: none;
    margin: -6px 0 0;
    padding: 0;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: flex-end;
}
ul.tt-payment-list li {
    padding-right: 5px;
    }
</style>
        <div class="tt-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>Checkout</li>
                </ul>
            </div>
        </div>
       <div id="tt-pageContent">
        <div class="container-indent">
            <div class="container">
              
                <h1 class="tt-title-subpages noborder text-capitalize text-left">Checkout</h1>
                <div class="row">
                    <div class="col-sm-12 col-xl-8">
                    <form name="checkout_form" method="post" id="bgnone">
                            <div class="tt-shopcart-box colorbg p-4">
                                 <?php
                if ($_SESSION['FUID'] != '') {
                $customer = FETCH_all("SELECT * FROM `customer` WHERE `CusId` = ?", $_SESSION['FUID']);
                //print_R($customer);
                }
            ?>    
            
                                <h4 class="tt-title">BILLING ADDRESS</h4>
                                
                                <p>Name</p>
                                <input type="text" name="fname" value="<?php echo $customer['fname']; ?>" class="form-control">
                                <p>Email</p>
                                <input type="email" name="emailid"  class="form-control" value="<?php echo $customer['emailid']; ?>">
                                <p>Address</p>
                               <textarea name="delivery_address" required="required" class="form-control" style="height:100px !important;"></textarea>
                                <!-- <p>Enter Your Contact No</p>
                                 <input type="number" name="mobileno"  class="form-control" value="<?php echo $customer['mobileno']; ?>"> -->
                                  
                                   <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                   <p>State/Proviance</p>
                                            <select class="form-control" name="state" id="state" required="required" onchange="getstate1(this.value)">
                                                <option value=""></option>
                                                
                                            </select>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <p>Country</p>
                                            <select id="country" class="form-control" name="country" required="required"  onchange="getstate(this.value)">
                                                <option value=""></option>
                                             <?php
                                              $country = DB("SELECT * FROM `countries` WHERE `status`='1' ORDER BY `name` ASC ");
                                              $ccount = mysqli_num_rows($country);
                                               while ($countrylist = mysqli_fetch_array($country)) {
                                             ?>
                                             <option value="<?php echo $countrylist['id']; ?>"><?php echo $countrylist['name']; ?></option>
                                             <?php } ?>
                                            </select>
                                        </div>
                                        </div>
                                           
                                            <p>ZIP/POSTAL CODE </p>
                                            <input type="text" name="ship_postcode" required="required" onkeyup="getpostcode(this.value)" class="form-control" id="ship_postcode" placeholder="" />
                                
                               <!--  <p>Add special instructions for your order...</p>
                                <textarea name="order_notes"  class="form-control" style="height:100px !important;"></textarea> -->
                                <input type="hidden" name="ship_country" id="ship_country">
                                <input type="hidden" name="ship_state" id="ship_state">
                                <input type="hidden" name="shp_postcode" id="shp_postcode">
                                <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $cartftotoal; ?>">
                                 <input type="hidden" name="ship_price" id="ship_price" required="required">
                                 <input type="hidden" name="ship_price1" id="ship_price1" required="required">
                                 <input type="hidden" name="fintotal" id="fintotal">
                                 <input type="hidden" name="discounted_amount" id="discounted_amount">
                                <fieldset>
      <div class="row mt-3">
        <div class="col-md-6">
        <label class="checkcontainer">Same as shipping details
  <input type="radio" checked="checked" name="radio">
  <span class="radiobtn"></span>
</label>
</div>
<div class="col-md-6 text-right">
<label class="checkcontainer">Use different billing details
  <input type="radio" name="radio">
  <span class="radiobtn"></span>
</label>
</div>
      </div>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d125766.12980003307!2d78.05278255426747!3d9.91799869040521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b00c582b1189633%3A0xdc955b7264f63933!2sMadurai%2C%20Tamil%20Nadu!5e0!3m2!1sen!2sin!4v1604738507494!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:2px ​solid #ff;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </fieldset>
                            </div>
                            <div class="tt-shopcart-box colorbg p-4 mt-3">
                                <h4 class="tt-title mb-3">SHIPPING METHOD</h4>
                                 <label class="checkcontainer d-block">Standared delivery:$10.00
  <input type="radio" checked="checked" name="radio">
  <span class="radiobtn"></span>
</label>
<label class="checkcontainer d-block">International Flat rate:$20.0
  <input type="radio" name="radio">
  <span class="radiobtn"></span>
</label>
<label class="checkcontainer d-block">Local Delivery:$20.0
  <input type="radio" name="radio">
  <span class="radiobtn"></span>
</label>
                            </div>

                             <div class="tt-shopcart-box colorbg p-4 mt-3">
                                <h4 class="tt-title mb-3">PAYMENT METHOD</h4>
                                <div class="row border-bottom">
                                    <div class="col-sm-12 col-md-6">
                                <label class="checkcontainer d-block">Credit or Debit Card
  <input type="radio" name="radio">
  <span class="radiobtn"></span>
</label>
</div>
<div class="col-sm-12 col-md-6"><ul class="tt-payment-list">
                            <li><a href="page404.html"><span class="icon-visa">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-mastercard">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-mastercard">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span>
                            </span></a></li>
                            <li><a href="page404.html"><span class="icon-american-express">
                            <span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span>
                            </span></a></li>
                        </ul>
                        </div>
</div>
                           <p>Card Number</p>
                                <input type="number" name="number"  class="form-control" value="<?php echo $customer['mobileno']; ?>">
                                <div class="row mb-4">
                                    <div class="col-sm-12 col-md-6">
                                <p>Card on Name</p>
                                <input type="text" name="name"  class="form-control" value="<?php echo $customer['mobileno']; ?>">
                            </div>
                             <div class="col-sm-12 col-md-4">
                                <p>Expiration Date</p>
                                <input type="date" name="date"  class="form-control" value="<?php echo $customer['mobileno']; ?>">
                            </div>
                            <div class="col-sm-12 col-md-2">
                                <p>CVV</p>
                                <input type="text" name="date"  class="form-control" value="<?php echo $customer['mobileno']; ?>">
                            </div>
                        </div>
                        <div class="row border-bottom py-2 ml-1">
                                 <label class="checkcontainer d-block">Bank Transfer Payment
  <input type="radio" checked="checked" name="radio">
  <span class="radiobtn"></span>
</label>
                            </div>
                            <div class="row  pt-4 ml-1">
                                 <label class="checkcontainer d-block">Cash on Delivery
  <input type="radio" checked="checked" name="radio">
  <span class="radiobtn"></span>
</label>
                            </div>
                            
                            </div>

                        </form>
                    </div>
                    <div class="col-sm-12 col-xl-4">
                <?php if($_SESSION['orconfres']!='') { ?>
                  <p><?php echo $_SESSION['orconfres'];  ?></p>
                  <?php } else { ?>
                  <?php 
                    $user_id = $_SESSION['FUID']; 
                                         $tempcart = $db->prepare("SELECT * FROM `tempcart` WHERE `userid`=?");
         $tempcart->execute(array($user_id));
		 if ($tempcart->rowCount() > 0) {
                  ?>

                <div class="tt-shopcart-table-02 colorbg px-3">
                    <h3 class="codetext pt-4 pb-2">ORDERS OVERVIEW</h3>
                    <table>
                                    <tbody>
                                        
                                        <?php 
                                      
		 while($tempcartfetch = $tempcart->fetch()) {
		       $im = explode(",", getproduct('image', $tempcartfetch['productid']));
                                        ?>
                                         <tr>
                                         
                                            <td>
                                                <input type="hidden" name="sizeid" id="sizeid" value="<?php echo $tempcartfetch['size']; ?>">
                                                <div class="tt-product-img"><a href="<?php echo $fsitename . 'view/' . getproduct('link', $tempcartfetch['productid']) . '.htm'; ?>">
                                                                <?php if($im[0]!='') { ?>
                                                                <img src="<?php echo $fsitename . 'images/product/' . getproduct('imagefolder', $tempcartfetch['productid']) . '/' . $im[0]; ?>" alt="<?php echo getproduct('productname', $tempcartfetch['productid']); ?>" style="width:100px; float:left;" />
<?php } else { ?>
                                                                <img src="<?php echo $fsitename . 'images/noimage1.png'; ?>" alt="<?php echo getproduct('productname', $tempcartfetch['productid']); ?>" style="width:100px; float:left;" />
                                                                <?php } ?>
                                                            </a></div>
                                            </td>
                                            <td>
                                                <h2 class="tt-title"><a href="<?php echo $fsitename . 'view/' . getproduct('link', $tempcartfetch['productid']) . '.htm'; ?>"><?php echo getproduct('productname', $tempcartfetch['productid']); ?></a>
                                        <br>
                                        <i>Size : <?php echo $tempcartfetch['size']; ?></i>
                                                </h2>
                                                <ul class="tt-list-parameters">
                                                    <li><div class="tt-price"> <?php
                                                    
                                                    $as = $db->prepare("SELECT * FROM `sizeprice` WHERE `size`=? AND `product_id`=?");
         $as->execute(array($tempcartfetch['size'],$tempcartfetch['productid']));
		 if ($as->rowCount() > 0) {
		      $asd = $as->fetch();
		   $sprice = $asd['sprice'];
           
            $price = $asd['price'];  
		 }
		 else
		 {
		    $sprice = getproduct('sprice', $tempcartfetch['productid']);
           
            $price = getproduct('price', $tempcartfetch['productid']);   
		 }
                                                  
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></li>
                                                    <li><div class="detach-quantity-mobile"></div></li>
                                                    <li><div class="tt-price subtotal">$124</div></li>
                                                </ul>
                                            </td>
                                            <!-- <td><div class="tt-price"><?php
                           
                           
                                                    if (($sprice != '') && ($sprice > 0)) {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($sprice, '2', '.', '') . '</span>';
                                                    } else {
                                                        echo '<span class="product-price">$&nbsp;' . number_format($price, '2', '.', '') . '</span>';
                                                    }
                                                    ?></div></td> -->
                                            <!-- <td style="text-align:left;">
                                              
                                                    <?php echo $tempcartfetch['qty']; ?> 
                                                <input type="hidden" name="product_id[]" value="<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>" />
                                            </td> -->
                                            <td><div class="tt-price subtotal">$&nbsp;<span class="subtotal-col cart-id-<?php echo $tempcartfetch['productid'].'-'.$tempcartfetch['size']; ?>"><?php
                                            if ($sprice > 0) {
                                                echo number_format($sprice * $tempcartfetch['qty'], '2', '.', '');
                                                   $cartftotoal+=number_format($sprice * $tempcartfetch['qty'], '2', '.', '');
                                            } else {
                                                echo number_format($price * $tempcartfetch['qty'], '2', '.', '');
                                                   $cartftotoal+=number_format($price * $tempcartfetch['qty'], '2', '.', '');
                                            }
                                            
                                                    ?></span></div></td>
                                        </tr>
                                        <?php }  ?>
                                        </tbody>
                                </table>
                                    <table class="tt-shopcart-table01">
                                        <tbody>
                                            <tr>
                                                <th>Sub Total</th>
                                                <td>$<span id="subtotal-tds">900.00</span></td>
                                            </tr>
                                             <tr>
                                                <th>GST</th>
                                                <td>$<span id="subtotal-tds">900.00</span></td>
                                            </tr>
                                            <tr>
                                                <th style="padding-bottom: 20px;">Shipping</th>
                                                <td style="padding-bottom: 20px;">$<span id="subtotal-tds">900.00</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr style="border-top: 1px solid #e9e7e7;border-bottom:0">
                                                <th>TOTAL</th>
                                                <td>$<span id="total-tds">900.00</span></td>
                                            </tr>
                                        </tfoot>

                                    </table>


                                    
                    
                </div>
                <div class="tt-shopcart-btn mt-3">
                        <div class="col-left"><a class="btn w-100" href="<?php echo $fsitename; ?>listings.htm">PAY NOW</a></div>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
                <!-- <div class="tt-shopcart-col">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="tt-shopcart-box">
                                <h4 class="tt-title">ESTIMATE SHIPPING AND TAX</h4>
                                <p>Enter your destination to get a shipping estimate.</p>
								<p style="color:red;" id="shperrormsg"></p>
                                 <form class="form-default" method="post" id="shipingform">
                                        <div class="form-group">
                                            <label for="address_country">COUNTRY <sup>*</sup></label>
                                            <select id="country" class="form-control" name="country" required="required"  onchange="getstate(this.value)">
                                                <option value="">Select</option>
                                             <?php
                                              $country = DB("SELECT * FROM `countries` WHERE `status`='1' ORDER BY `name` ASC ");
                                              $ccount = mysqli_num_rows($country);
                                               while ($countrylist = mysqli_fetch_array($country)) {
                                             ?>
                                             <option value="<?php echo $countrylist['id']; ?>"><?php echo $countrylist['name']; ?></option>
                                             <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="address_province">STATE/PROVINCE <sup>*</sup></label>
                                            <select class="form-control" name="state" id="state" required="required" onchange="getstate1(this.value)">
                                                <option value="">State/Province</option>
                                                
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="address_zip">ZIP/POSTAL CODE <sup>*</sup></label>
                                            <input type="text" name="ship_postcode" required="required" onkeyup="getpostcode(this.value)" class="form-control" id="ship_postcode" placeholder="Zip/Postal Code" />
                                        </div>
                                        <ul class="tt-list-dot list-dot-large">
                                            <li><a href="#">Shipping Price : <span id="sipprice"> $0.00</span></a></li>
                                        </ul>
                                        <br>
                                        <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $cartftotoal; ?>">
                                        <input type="submit" class="btn btn-border" name="calculate_shipping" value="CALCULATE SHIPPING">
                                       
                                        
                                    </form>
                            </div>
                        </div>
                         <div class="col-md-6 col-lg-8">
                               <form name="checkout_form" method="post" id="bgnone">
                             <div class="row">
                                 <div class="col-md-6 col-lg-6">
                            <div class="tt-shopcart-box">
                                 <?php
                if ($_SESSION['FUID'] != '') {
                $customer = FETCH_all("SELECT * FROM `customer` WHERE `CusId` = ?", $_SESSION['FUID']);
                //print_R($customer);
                }
            ?>    -->
            
            <style>
                @media (min-width: 790px){
.tt-shopcart-col .form-control {
height: 46px !important;
}
                    
                }
            </style>
                                <!-- <h4 class="tt-title">ADDRESS DETAILS</h4>
                                
                                <p>Enter Your Name</p>
                                <input type="text" name="fname" value="<?php echo $customer['fname']; ?>" class="form-control">
                                <p>Enter Your Contact No</p>
                                 <input type="number" name="mobileno"  class="form-control" value="<?php echo $customer['mobileno']; ?>">
                                  <p>Enter Your Emailid</p>
                                <input type="email" name="emailid"  class="form-control" value="<?php echo $customer['emailid']; ?>">
                                <p>Enter Your Delivery Address</p>
                               <textarea name="delivery_address" required="required" class="form-control" style="height:100px !important;"></textarea>
                                <p>Add special instructions for your order...</p>
                                <textarea name="order_notes"  class="form-control" style="height:100px !important;"></textarea>
                                <input type="hidden" name="ship_country" id="ship_country">
                                <input type="hidden" name="ship_state" id="ship_state">
                                <input type="hidden" name="shp_postcode" id="shp_postcode">
                                <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $cartftotoal; ?>">
                                 <input type="hidden" name="ship_price" id="ship_price" required="required">
                                 <input type="hidden" name="ship_price1" id="ship_price1" required="required">
                                 <input type="hidden" name="fintotal" id="fintotal">
                                 <input type="hidden" name="discounted_amount" id="discounted_amount">
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                            <div class="col-md-12">
                             <div class="tt-shopcart-box tt-boredr-large">
                               <table class="tt-shopcart-table01">
                                        <tbody>
                                             <tr>
                                         <td style="text-align:left;">Enter Your Coupon Code</td>
                                         </tr>
                                        <tr>
                                          <td><input type="text" name="coupon_code" id="coupon_code" placeholder="Enter coupon code here" class="form-control" value="<?php //echo $_SESSION['PROMO_CODE'];       ?>"></td>
                                            </tr>
                                            <tr>
                                            <td colspan="2">    
                                            <?php if ($_SESSION['PROMO_CODE'] != '') { ?>
                                        <button type="button" name="coupon_code_btn_remove" id="coupon_code_btn_remove" class="btn btn-red min-width-sm">Remove</button>
                                    <?php } else { ?>
                                        <button type="button" name="coupon_code_btn" id="coupon_code_btn" class="btn btn-custom min-width-sm">Apply Now</button>
<?php } ?>   </td>
                                         </tr>
                                          
                                          
                                        </tbody>
                                        
                                    </table>
                                 </div>    
                            </div>    
                            </div>
                           <div class="row">
                            <div class="col-md-12">
                             <div class="tt-shopcart-box tt-boredr-large">
                               <table class="tt-shopcart-table01">
                                        <tbody>
                                             <tr>
                                                <th>PAYMENT MODE</th>
                                                <td>
                                                    <input type="radio" name="payment_mode" value="Cash on Delivery">
                                                    Cash on Delivery &nbsp;&nbsp; <input type="radio" name="payment_mode" value="Online Payment">
                                                    Online Payment</td>
                                            </tr>
                                            <tr>
                                                <th>SUBTOTAL</th>
                                                <td>$<span id="subtotal-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                              <tr>
                                                <th>DISCOUNT</th>
                                                <td>$<span id="cd">0.00</span></td>
                                            </tr>
                                            <tr>
                                                <th>SHIPPING PRICE</th>
                                                <td><span id="subtotal-shiptds">$0.00</span></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>GRAND TOTAL</th>
                                                <td>$<span id="total-tds"><?php echo number_format($cartftotoal, '2', '.', ''); ?></span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
									<input type="submit" name="checkout" id="confirmcheckout" class="btn btn-lg" onclick="return checksubmit();" value="PROCEED TO CHECKOUT">
                             </div>    
                            </div>    
                            </div>
                        </div> 
                             </div>
                              </form>
                             </div>
                     </div>
                </div>
                <?php } else { ?>
                <p align="center">Your Cart is Empty</p>
                <?php } } ?>
            </div>
        </div>
    </div> -->
    
    
    
    
        <?php include 'require/footer.php';?>
        <script type="text/javascript">
	function checksubmit(){
	if($('#shp_postcode').val()!='' && $('#ship_price').val()!='')
	{
	return true;	
	}
    else {	
		alert('Enter your Shipping Details and Calculate your shipping Charge');
		
		$('#shperrormsg').html('Enter your Shipping Details and Calculate your shipping Charge');
		return false;
    }
	}
	
		</script>
        <script>
$(document).ready(function(){
$('#shipingform').submit(function(){
// show that something is loading
$('#shpresponse').html("<b>Loading response...</b>");

// Call ajax for pass data to other place
$.ajax({
type: 'POST',
url: '<?php echo $fsitename; ?>js/formsubmission.php',
data: $(this).serialize(), // getting filed value in serialize form
success: function (data) { 
 //window.location.replace("https://www.jiovio.com/buy/thanks.htm");
 var result=data.split('#');
 
    $('#sipprice').html(result['0']);
  // alert(result['1']);
      $('#fintotal').val(result['1']);
     $('#subtotal-shiptds').html(result['0']);
      $('#ship_price').val(result['0']);
     $('#total-tds').html(result['1']);
      $('#ship_price1').val(result['2']);
    
    //  $("#contactForm").trigger("reset");
    }
});

// to prevent refreshing the whole page page
return false;

});

   });
   
   function getpostcode(a){
      // alert(a);
        $('#shp_postcode').val(a);
   }
   function getstate(a)
    {
        $('#ship_country').val(a);
        $.ajax({
            url: "<?php echo $sitename; ?>config/ajaxfunction.php",
            data: {country: a},
            success: function (data) {
              $("#state").html(data);
              
            }
        });
    }    
    function getstate1(a)
    {
        $('#ship_state').val(a);
        
    }
    
        </script>